﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Clickable : Photon.MonoBehaviour {

	public RadialButton[] menuOptions;

	protected float myYAngle = -1f;

	public virtual float MyYAngle() {
		// if( PhotonNetwork.offlineMode == true ) {
		// 	return 0f;
		// }

		if( myYAngle == -1f ) {

			List<int> sortedParticipants = new List<int>();
			for( int index = 0; index < PhotonNetwork.playerList.Length; index++ ) {
				if( PhotonNetwork.playerList[index].customProperties["spectating"] != null ) continue; // don't count spectators

				sortedParticipants.Add( Table.GetIDFor( PhotonNetwork.playerList[index] ) );
			}
			sortedParticipants.Sort();

			for( int index = 0; index < sortedParticipants.Count; index++ ) {
				if( sortedParticipants[index] == Table.GetIDFor( photonView.owner ) ) {
					myYAngle = index * (360f / Table.data.maxParticipants);

					return myYAngle;
				}
			}

			if( photonView.isSceneView ) {
				for( int index = 0; index < sortedParticipants.Count; index++ ) {
					if( sortedParticipants[index] == Table.GetIDFor( PhotonNetwork.player ) ) {
						myYAngle = index * (360f / Table.data.maxParticipants);

						return myYAngle;
					}
				}
			}
		}

		return myYAngle;
	}

	public void ResetMyYAngle() {
		myYAngle = -1f;
	}

	public virtual Vector3 MyRightDir() {
		return Quaternion.Euler(0f, MyYAngle() + 90f, 0f) * Vector3.forward;
	}

	public virtual Vector3 MyForwardDir() {
		return Quaternion.Euler( 0f, MyYAngle(), 0f ) * Vector3.forward;
	}

	// customizable actions

	public virtual void ClickActionDown( InputInfo input ) {
		if( !input.Matches( InputInfo.Action.ShowMenu ) ) {
			Table.data.radialMenu.Dismiss();
		}
		else {
			Table.data.radialMenu.gameObject.SetActive( false );
		}
	}

	public virtual void ClickActionUp( InputInfo input ) {
		if( input.Matches( InputInfo.Action.ShowMenu ) ) {
			Table.data.ShowRadialMenu( menuOptions, this );
		}
	}

	public virtual void DragActionStart( InputInfo input ) {

	}

	public virtual void DragActionEnd( InputInfo input ) {

	}

	public virtual void Hover( InputInfo input ) {

	}
}
