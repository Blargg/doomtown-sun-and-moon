﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Favor : CardDisplay {

	public float favorDistance = 5f;

	public Texture dragon;
	public Texture crane;
	public Texture crab;
	public Texture spider;
	public Texture mantis;
	public Texture phoenix;
	public Texture scorpion;
	public Texture lion;
	public Texture unicorn;
	public Texture unaligned;

	public Card.Clan displayingClan = Card.Clan.unaligned;

	public Dictionary<Card.Clan,Texture> favorImage = new Dictionary<Card.Clan,Texture>(10);

	Dictionary<Transform,Card.Clan> playerDecks = new Dictionary<Transform,Card.Clan>();

	Vector3 origFavorPos = Vector3.zero;

	void Start() {
		for( int index = 0; index < tokens.Length; index++ ) tokens[index].tokenIndex = index;

		favorImage.Add(Card.Clan.dragon,dragon);
		favorImage.Add(Card.Clan.crane,crane);
		favorImage.Add(Card.Clan.crab,crab);
		favorImage.Add(Card.Clan.spider,spider);
		favorImage.Add(Card.Clan.mantis,mantis);
		favorImage.Add(Card.Clan.phoenix,phoenix);
		favorImage.Add(Card.Clan.scorpion,scorpion);
		favorImage.Add(Card.Clan.lion,lion);
		favorImage.Add(Card.Clan.unicorn,unicorn);
		favorImage.Add(Card.Clan.unaligned,unaligned);
		favorImage.Add(Card.Clan.neutral,unaligned);

		origFavorPos = transform.position;

		Seize( (int)Card.Clan.neutral );

		Table.data.allCards.Add(this);

		// Table.data.TableResetEvent += delegate{ Reset(); Table.data.allCards.Add(this); Show(); Deselect(); };

		Show();
		Deselect();
	}

	[PunRPC]
	public void RegisterPlayer( int dynViewID, int clan ) {
		Transform dyn = PhotonView.Find( dynViewID ).GetComponent<Transform>();

		playerDecks.Add( dyn, (Card.Clan)clan );

		// print("Adding clan " + Persistent.clanToString[(Card.Clan)clan]);
	}

	[PunRPC]
	void Seize( int clan ) {
		print("Seizing " + clan + " (" + (Card.Clan)clan + ")" );

		myFront.material.mainTexture = (Texture2D)favorImage[(Card.Clan)clan];

		// origFront = myFront.material.mainTexture;

		displayingClan = (Card.Clan)clan;
	}

	public override void DragActionEnd( InputInfo input ) {
		base.DragActionEnd( input );

		if( !locked ) {

			float closestDistToDeck = 99999999f;
			Transform closestDeck = null;

			foreach( Transform dyn in playerDecks.Keys ) {
				if( dyn == null ) continue; // this can sometimes happen after a reconnect

				float distToThisDeck = Mathf.Abs(dyn.InverseTransformPoint(transform.position).z);

				print("Z distance of favor is " + distToThisDeck);

				if( distToThisDeck < favorDistance && distToThisDeck < closestDistToDeck ) {
					closestDeck = dyn;
					closestDistToDeck = distToThisDeck;
				}
			}

			if( closestDeck != null ) {
				photonView.RPC("Seize", PhotonTargets.All, (int)playerDecks[closestDeck]);
				photonView.RPC("RotateTo", PhotonTargets.All, closestDeck.rotation, 0.5f );
				print(Persistent.data.ClanEnumToString(playerDecks[closestDeck]) + " took favor " + Time.time);
			}
			else {
				photonView.RPC("Seize", PhotonTargets.All, (int)Card.Clan.neutral);
				displayingClan = Card.Clan.neutral;
				photonView.RPC("RevertRotation", PhotonTargets.All);

				print("Imperial favor reverting rotation " + Time.time);
			}
		}
	}

	[PunRPC]
	public void RevertRotation() {
		RotateTo( Table.data.myDynasty.transform.rotation, 0.5f );
	}

	public void Reset() {
		playerDecks = new Dictionary<Transform,Card.Clan>();

		MoveTo( origFavorPos );
		Seize( (int)Card.Clan.neutral );
	}

	public override float MyYAngle() {
		return Table.data.myDynasty.MyYAngle();
	}

	public override bool IsMine() {
		return false;
	}

	public override void ReturnToOrigPos() {
		// base.ReturnToOrigPos();

		photonView.RPC("Seize", PhotonTargets.All, (int)Card.Clan.neutral);
		photonView.RPC("MoveTo", PhotonTargets.All, origFavorPos, 0.5f );

		Table.data.cardBeingDragged = null;

		StartCoroutine( PreventSeizure(0.5f) );
	}

	// lock the card so that it can't be seized temporarily
	IEnumerator PreventSeizure( float duration ) {
		locked = true;
		yield return new WaitForSeconds( duration );
		locked = false;
	}
}
