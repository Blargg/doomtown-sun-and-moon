using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Vectrosity;
using Newtonsoft.Json;

using System.Text;

using System.Linq;

public class CardDisplay : Clickable {

	// public EchoGameObject fx;
	public Renderer outline;
	float outlineScaleSpeed = 0.5f;
	float outlineBloomSpeed = 0.5f;

	public Material defaultDynBack;
	public Material defaultFateBack;
	public Renderer myBack;
	public Renderer myFront;
	public Canvas standInCanvas;
	public GameObject tokenDisplay;
	public Token[] tokens;

	// public Texture origFront;
	[HideInInspector] public bool loadingImage = false;

	public Card card;
	public CardStandIn standIn;
	public bool standInNeeded = true;
	public bool initialized = false;

	new public Collider collider;

	public bool inDeck {
		get {
			return _inDeck;
		}
		set {
			_inDeck = value;
			if( _inDeck ) {
				// if( Time.timeSinceLevelLoad > 2f ) print("Card being put in deck " + Time.time);
				standInCanvas.gameObject.SetActive( false );
			}
			else {
			}
		}
	}
	bool _inDeck = false;

	public Hand inHand = null;
	public bool playedFromHand = false; // second var for the act of playing from hand... not just keeping track of dragging
	public CardDisplay inStack {
		get {
			return _inStack;
		}
		set {
			_inStack = value;

			// if( _inStack != null ) DisableTokenInput();
			// else EnableTokenInput();
		}
	}
	CardDisplay _inStack = null;

	public bool isFate {
		get {
			if( card == null ) {
				print("No card for " + name);
				return false;
			}
			return card.deckType == Card.DeckType.Fate;
		}
		set {
			if( card != null ) card.deckType = value ? Card.DeckType.Fate : Card.DeckType.Dynasty;
		}
	}
	public bool faceUp {
		get {
			return _faceUp;
		}
		set {
			bool beingRevealed = !_faceUp && value == true;

			if( value == true && inDeck ) {
				return;
			}

			_faceUp = value;
			standInCanvas.gameObject.SetActive( value );

			if( beingRevealed ) {
				// Put standin in place before loading so that black (null) textures are never shown
				PreloadImage();
			}
			else if( !_faceUp && gameObject.activeInHierarchy ) {
				UnloadImage();
			}
		}
	}
	bool _faceUp = true;

	public bool inProvince = false;
	public bool inRegion = false;
	public bool inFief = false;
	public bool inPlay {
		get {
			return _inPlay;
		}
		set {
			if( value == false ) {
				photonView.RPC("ClearTokens", PhotonTargets.All);
				standInCanvas.gameObject.SetActive( false );

				// if( Time.timeSinceLevelLoad > 2f ) print(name + " being set out of play " + Time.time);
			}
			_inPlay = value;
		}
	}
	bool _inPlay = false;

	public int attachedToProv = -1;

	public bool bowed = false;
	public bool dishonored = false;
	public bool dead = false;

	public int selected {
		get {
			return _selected;
		}
		set {
			_selected = value;
		}
	}
	int _selected = -1;
	public bool locked = false; // used in the tutorial to prevent opponent's cards from being draggable

	public Vector3 handPos = Vector3.zero;
	public Quaternion handRot = Quaternion.identity;

	// movement
	public float trackingGap = 0.25f;
	public float grabDistFromCamera = 20f;
	public float hoverDistFromFloor = 2f;
	public float flipDistFromFloor = 1.5f;
	public static float staticDistFromFloor = 0.05f;
	Vector3 prevPos = Vector3.zero; // used for tracking stack movement
	Vector3 origPos = Vector3.zero; // used for reverting unuseful movements
	Quaternion origRot = Quaternion.identity;

	DeckDisplay hoveringOverDeck = null;
	DeckDisplay hoveringUnderDeck = null;

	bool flipping = false;

	float bowAngle = 0f;
	Mesh mesh = null;

	public float stackHeight = 0.05f;
	public List<CardDisplay> stack = new List<CardDisplay>();

	int tweenIDMove = 0;
	int tweenIDRotate = 0;
	// for preserving onComplete actions after canceling tweens
	System.Action moveTweenOnComplete = null;
	System.Action rotTweenOnComplete = null;

	// guides
	public List<GuideLine> guides = new List<GuideLine>();
	public List<GuideLine> arrows = new List<GuideLine>();

	public List<GuideLine> attachedGuides = new List<GuideLine>();
	public List<GuideLine> attachedArrows = new List<GuideLine>();

	Vector3 dragStartPoint = Vector3.zero;

	// sound effects
	public AudioSource cardFoley;

	// serialization
	public int originalOwnerID = -1;

	// playtest
	// public bool cached {
	// 	get {
	// 		if( card.images.Count == 0 ) return false;

	// 		FileInfo imageLocation = new FileInfo( cachedPath );
	// 		return imageLocation.Exists;
	// 	}
	// 	set {
	// 		print("Loading image from " + Persistent.data.rootURL + cachedPath.SanitizePath());
	// 		myFront.material.mainTexture = ProcessImage( new WWW( Persistent.data.rootURL + cachedPath.SanitizePath() ) );
	// 		origFront = myFront.material.mainTexture;
	// 	}
	// }
	// string cachedPath {
	// 	get {
	// 		if( !card.cache ) return Application.persistentDataPath + Path.DirectorySeparatorChar + card.images[0]; // playtest card
	// 		else return Persistent.data.outsideApp.FullName + Path.DirectorySeparatorChar + card.images[0];
	// 	}
	// }

	void Awake() {
		myFront.enabled = false;
		myBack.enabled = false;
	}

	void Start() {
		for( int index = 0; index < tokens.Length; index++ ) tokens[index].tokenIndex = index;
	}

	public void MoveTo( Vector3 point, float moveTime, LeanTweenType opt, System.Action onComplete ) {
		if( tweenIDMove != tweenIDRotate ) LeanTween.cancel( gameObject, tweenIDMove );
		if( onComplete == null ) onComplete = moveTweenOnComplete;
		else moveTweenOnComplete = onComplete;

		tweenIDMove = LeanTween.move( gameObject, point, moveTime ).setEase(opt).setOnComplete( delegate(){ if(onComplete != null) onComplete(); moveTweenOnComplete = null; } ).id;

		Persistent.data.FrameRateUpFor( moveTime );
	}

	public void MoveToLocal( Vector3 localPoint, float moveTime, LeanTweenType opt, System.Action onComplete ) {
		if( tweenIDMove != tweenIDRotate ) LeanTween.cancel( gameObject, tweenIDMove );
		if( onComplete == null ) onComplete = moveTweenOnComplete;
		else moveTweenOnComplete = onComplete;

		tweenIDMove = LeanTween.moveLocal( gameObject, localPoint, moveTime ).setEase(opt).setOnComplete( delegate(){ if(onComplete != null) onComplete(); moveTweenOnComplete = null; } ).id;

		Persistent.data.FrameRateUpFor( moveTime );
	}

	public void RotateTo( Quaternion rotation, float rotationTime, LeanTweenType opt, System.Action onComplete ) {
		if( tweenIDMove != tweenIDRotate ) LeanTween.cancel( gameObject, tweenIDRotate );
		if( onComplete == null ) onComplete = rotTweenOnComplete;
		else rotTweenOnComplete = onComplete;

		tweenIDRotate = LeanTween.rotate( gameObject, rotation.eulerAngles, rotationTime ).setEase(opt).setOnComplete( delegate(){ if(onComplete != null) onComplete(); rotTweenOnComplete = null; } ).id;

		Persistent.data.FrameRateUpFor( rotationTime );
	}

	public IEnumerator FlipOver( float flipTime, LeanTweenType opt ) {
		if( photonView.isSceneView ) yield break;

		flipping = true;

		if( !inHand ) {
			EnableShadow();

			MoveTo( new Vector3( transform.position.x, transform.position.y + flipDistFromFloor, transform.position.z ), flipTime / 2f );
			// print("flipping up " + Time.time);
			if( faceUp ) {
				RotateTo( Quaternion.Euler(0f,transform.eulerAngles.y,180f), flipTime, opt, delegate{ faceUp = false; } );
			}
			else {
				faceUp = true;
				RotateTo( Quaternion.Euler(0f,transform.eulerAngles.y,0f), flipTime );
			}

			yield return new WaitForSeconds( flipTime / 2f );

			string flipped = faceUp ? "face up" : "face down";
			Table.data.chat.AddVerboseChatEntry( "I flipped " + name + " " + flipped );

			MoveTo( new Vector3( transform.position.x, staticDistFromFloor * (stack.Count + 1), transform.position.z ), flipTime / 2f, opt, delegate{ if( inStack ) inStack.AdjustStackInstantly(); } );
		}
		else {
			Quaternion flippedRot = handRot * Quaternion.Euler(0f,0f,180f);
			if( IsMine() ) {
				RotateTo( flippedRot, flipTime, opt, delegate{ faceUp = !faceUp; print("Face up on " + name + " being set to " + faceUp); } );
			}
			else {
				yield return new WaitForSeconds( flipTime );
				faceUp = !faceUp;
			}
			handRot = flippedRot;
			// print("Card face up is being set to " + faceUp + " " + Time.time);
		}

		yield return new WaitForSeconds( flipTime / 2f );
		flipping = false;

		if( Table.data.cardBeingDragged != this && !flipping ) DisableShadow();
	}

	public IEnumerator StackCard( CardDisplay bottomCard ) {

		flipping = true;

		float stackTime = 1f;
		float distAboveCard = LocalBounds().extents.z * 2f + 0.5f;

		// move all cards up to make room
		Vector3 yVariation = Vector3.up * staticDistFromFloor * (bottomCard.stack.Count + 1);
		MoveTo( transform.position + yVariation, stackTime / 2f );
		for( int index = 0; index < stack.Count; index++ ) {
			stack[index].MoveTo( stack[index].transform.position + yVariation, stackTime / 2f );
		}

		// move stacking card out so it can slide under
		Vector3 outPos = transform.position + MyForwardDir() * (distAboveCard + stackHeight * (stack.Count + 1)) - Vector3.up * staticDistFromFloor * (stack.Count - bottomCard.stack.Count);
		bottomCard.MoveTo( outPos, stackTime / 2f );

		yield return new WaitForSeconds( stackTime / 2f );

		stack.Add( bottomCard );

		// add that card's stack to this one
		for( int count = 0; count < bottomCard.stack.Count; count++ ) {
			stack.Add( bottomCard.stack[count] );
			bottomCard.stack[count].inStack = this;
		}
		bottomCard.stack.Clear();

		bottomCard.inStack = this;
		StartCoroutine( AdjustStack( stackTime / 2f ) );

		yield return new WaitForSeconds( stackTime / 2f );

		flipping = false;

		string attachedName = bottomCard.faceUp ? bottomCard.name : "a face down card";
		string attachedToName = faceUp ? name : "a face down card";
		Table.data.chat.AddVerboseChatEntry( "I attached " + attachedName + " to " + attachedToName );

		if( Table.data.cardBeingDragged != this && !flipping ) bottomCard.DisableShadow();
	}

	public void RemoveFromStack( CardDisplay stackedCard, bool pullOut ) {

		stack.Remove( stackedCard );
		stackedCard.inStack = null;
		StartCoroutine( AdjustStack( 0.5f ) );

		if( pullOut ) stackedCard.MoveTo( new Vector3( transform.position.x + LocalBounds().size.x, staticDistFromFloor, transform.position.z ) );
	}

	// Adjust whole stack
	public IEnumerator AdjustStack( float adjustTime ) {

		flipping = true;

		Vector3 basePosition = new Vector3( transform.position.x, 0f, transform.position.z );
		photonView.RPC("MoveTo", PhotonTargets.All, basePosition + Vector3.up * staticDistFromFloor * (stack.Count + 1), adjustTime );

		AdjustStackOnly( basePosition, adjustTime );

		yield return new WaitForSeconds( adjustTime );

		flipping = false;
	}

	// Adjust only part of stack (useful after moving a card that doesn't technically have stack references on opponent's end)
	public void AdjustStackOnly( Vector3 basePosition, float adjustTime ) {
		for( int index = stack.Count - 1; index > -1; index-- ) {
			Vector3 xVariation = MyRightDir() * stackHeight * (index + 1);
			Vector3 yVariation = Vector3.up * staticDistFromFloor * (stack.Count - index);
			Vector3 zVariation = MyForwardDir() * stackHeight * (index + 1);
			stack[index].photonView.RPC("MoveTo", PhotonTargets.All, basePosition + zVariation + yVariation + xVariation, adjustTime);

			float flippedAngle = faceUp ? 0f : 180f;
			// print("Rotating stack with y angle of " + stack[index].MyYAngle() + " at " + Time.time);
			stack[index].photonView.RPC("RotateTo", PhotonTargets.Others, Quaternion.Euler(0f, stack[index].MyYAngle() - stack[index].bowAngle,flippedAngle), adjustTime);
		}
	}

	public void AdjustStackInstantly() {

		for( int index = stack.Count - 1; index > -1; index-- ) {
			Vector3 xVariation = MyRightDir() * stackHeight * (index + 1);
			Vector3 yVariation = Vector3.up * staticDistFromFloor * (index + 1);
			Vector3 zVariation = MyForwardDir() * stackHeight * (index + 1);
			stack[index].transform.position = transform.position + zVariation - yVariation + xVariation;
		}
	}

	[PunRPC]
	public void Hide() {
		myBack.enabled = false;
		myFront.enabled = false;
		collider.enabled = false;
		standIn.gameObject.SetActive( false );
		standInCanvas.gameObject.SetActive( false );

		DeleteGuidesFromTo( true, true );
		Deselect();
	}

	[PunRPC]
	public void DeleteGuidesFromTo( bool from, bool to ) {
		if( from ) {
			for( int index = 0; index < guides.Count; index++ ) {
				if( !guides[index].active || !guides[index].target ) continue;
				guides[index].target.RmGuideFrom(photonView.viewID);

				guides[index].active = false;
				arrows[index].active = false;
			}
		}

		if( to ) {
			for( int index = 0; index < attachedGuides.Count; index++ ) {
				if( !attachedGuides[index].active || !attachedGuides[index].target ) continue;
				attachedGuides[index].source.RmGuideTo(photonView.viewID);
			}

			attachedGuides = new List<GuideLine>();
			attachedArrows = new List<GuideLine>();
		}
	}

	[PunRPC]
	public void Show() {
		myBack.enabled = true;
		myFront.enabled = true;
		collider.enabled = true;
		if( standIn && standInNeeded ) {
			standIn.gameObject.SetActive( true );
		}
	}

	public IEnumerator DelayedHide( float delay = 1f ) {
		yield return new WaitForSeconds( delay );

		Hide();
	}

	public void PeekUpFromHand() {

		if( !photonView.isSceneView && !IsMine() ) return;

		if( inHand ) {
			MoveToLocal( handPos + Vector3.up );
		}

		// if( faceUp ) {
		// 	if( !photonView.isMine && inHand ) {}
		// 	else Table.data.ShowPreview( this );
		// }
	}

	public virtual void ReturnToOrigPos() {
		print("subclass returning to orig pos " + Time.time);

		if( Camera.main.WorldToScreenPoint(origPos).y < Screen.height / 5f ) {
			Table.data.myHand.photonView.RPC("AddCard", PhotonTargets.All, photonView.viewID, true);
			TuckBackInHand();
		}
		else {
			MoveTo( origPos, 0.5f );
			RotateTo( origRot, 0.5f );
		}

		Table.data.cardBeingDragged = null;
	}

	public void TuckBackInHand() {
		MoveToLocal( handPos, 0.5f );
		RotateTo( handRot, 0.5f );
	}

	public void ClickUp( Vector3 worldPoint ) {

		if( Table.data.proxyPanel.IsVisible() ) Table.data.proxyPanel.proxyInput.text = gameObject.name;

		if( !(this is Favor) && !IsMine() ) return;
		
		if( hoveringUnderDeck && card != null && stack.Count == 0 ) {
			if( inHand ) {
				inDeck = true;
				Table.data.myHand.photonView.RPC("RemoveCard", PhotonTargets.All, photonView.viewID);
			}
			if( inProvince ) {
				Table.data.RemoveFromProvinces( this );
				string provinceName = faceUp ? name : "a face down card";
				Table.data.chat.AddVerboseChatEntry( "I cycled " + provinceName + " from my provinces" );
			}
			if( inFief ) Table.data.RemoveFromFief( this );
			if( attachedToProv != -1 ) Table.data.RemoveFromRegions( this );

			StartCoroutine( hoveringUnderDeck.AddCardUnderDeck( this ) );

			hoveringUnderDeck = null;

			Table.data.TakeAction();
		}
		else if( hoveringOverDeck && card != null ) {
			if( inHand ) {
				Table.data.myHand.photonView.RPC("RemoveCard", PhotonTargets.All, photonView.viewID);
			}
			if( inProvince ) {
				// bool refillProvince = (Table.data.currentPhase == Table.Phase.Dynasty && PlayerPrefs.GetInt("autorefilldynasty", 0) == 1) ? true : false;
				Table.data.RemoveFromProvinces( this, false );

				if( hoveringOverDeck.isDiscard /* && refillProvince == false */ ) Table.data.chat.AddVerboseChatEntry( "I removed " + name + " from my provinces" );
			}
			if( inFief ) Table.data.RemoveFromFief( this );
			if( attachedToProv != -1 ) Table.data.RemoveFromRegions( this );

			StartCoroutine( hoveringOverDeck.AddCardToTopOfDeck( this ) );

			hoveringOverDeck = null;

			// remove attachments and tokens
			CardDisplay[] stackedCardsToDiscard = stack.ToArray();
			foreach( CardDisplay attachment in stackedCardsToDiscard ) {
				RemoveFromStack( attachment, false );
				if( attachment.inFief ) Table.data.RemoveFromFief( attachment );
				if( attachment.isFate ) StartCoroutine( Table.data.myFateDiscard.AddCardUnderDeck(attachment) );
				else StartCoroutine( Table.data.myDynDiscard.AddCardUnderDeck(attachment) );
			}

			Table.data.TakeAction();
		}
		else if( inHand ) {
			TuckBackInHand();
		}
		else if( !isFate && IsMine() && Table.data.CardNearProvinces( this ) && card != null ) {
			if( stack.Count != 0 ) {
				ReturnToOrigPos();
				return;
			}
			if( inFief ) Table.data.RemoveFromFief( this );
			if( attachedToProv == -1 ) Table.data.ReplaceRegionWith(this);
		}
		else if( !isFate && IsMine() && card != null && faceUp && (card.type == "holding" || card.type == "retainer") && Table.data.CardNearFief( this ) && !inStack ) {
			if( inProvince ) Table.data.RemoveFromProvinces( this );
			if( attachedToProv != -1 ) Table.data.RemoveFromRegions( this );

			Table.data.AddToFief( this );
			string verboseName = faceUp ? name : "a face down card";
			Table.data.chat.AddVerboseChatEntry( "I added " + verboseName + " to my fief" );

			Table.data.TakeAction();
		}
		else if( !flipping && inStack == null ) {

			if( Table.data.cardBeingDragged == this ) {
				// test below card
				foreach( DeckDisplay deck in Table.data.allDecks ) {
					if( deck == null ) {
						print("Could not find deck in list of all decks when dropping card! " + Time.time);
						continue;
					}
					if( !deck.photonView.isMine ) continue;

					float xDistFromDeck = Mathf.Abs(deck.transform.position.x - transform.position.x);
					float zDistFromDeck = Mathf.Abs(deck.transform.position.z - transform.position.z);
					if( xDistFromDeck < deck.renderer.bounds.extents.x && zDistFromDeck < deck.renderer.bounds.extents.z ) {
						if( this is Favor || deck.isDynasty == isFate ) {
							// not matched
							ReturnToOrigPos();
							return;
						}
					}
				}

				CardDisplay[] cards = Table.data.allCards.ToArray();
				for( int index = 0; index < cards.Length; index++ ) {
					if( cards[index].IsMine() && cards[index] != this && !stack.Contains(cards[index]) && !cards[index].stack.Contains(this) && !cards[index].inHand ) {
						float xDistFromCard = Mathf.Abs(cards[index].transform.position.x - transform.position.x);
						float zDistFromCard = Mathf.Abs(cards[index].transform.position.z - transform.position.z);
						if( xDistFromCard < cards[index].myFront.bounds.extents.x && zDistFromCard < cards[index].myFront.bounds.extents.z ) {

							if( cards[index].inStack ) StartCoroutine( cards[index].inStack.StackCard( this ) );
							else StartCoroutine( cards[index].StackCard( this ) );
							// if( cards[index].inStack ) cards[index].inStack.photonView.RPC("StackCard", PhotonTargets.All, photonView.viewID);
							// else cards[index].photonView.RPC("StackCard", PhotonTargets.All, photonView.viewID);

							if( inProvince ) Table.data.RemoveFromProvinces( this );
							if( attachedToProv != -1 ) Table.data.RemoveFromRegions( this );
							if( inFief ) Table.data.RemoveFromFief( this );
							inPlay = true;

							Table.data.TakeAction();
							Table.data.cardBeingDragged = null;
							playedFromHand = false;

							return;
						}
					}
				}
			}

			Vector3 newPos = new Vector3( transform.position.x, staticDistFromFloor * (stack.Count + 1), transform.position.z );

			photonView.RPC("MoveTo", PhotonTargets.All, newPos, 0.3f); // doesn't carry shadow setting; how are you going to set this on remote player?
			StartCoroutine( AdjustStack( 0.3f ) );

			if( inProvince ) {
				Table.data.RemoveFromProvinces( this );

				string verboseName = faceUp ? name : "a face down card";
				Table.data.chat.AddVerboseChatEntry( "I played " + verboseName + " from my provinces" );

				Table.data.TakeAction();
			}
			if( attachedToProv != -1 ) {
				Table.data.RemoveFromRegions( this );
			}
			if( inFief ) {
				Table.data.RemoveFromFief( this );
				Table.data.TakeAction();
			}
			if( playedFromHand ) {
				Table.data.TakeAction();

				string verboseName = faceUp ? name : "a face down card";
				Table.data.chat.AddVerboseChatEntry( "I played " + verboseName + " from my hand");

				playedFromHand = false;
			}
			inPlay = true;

			// transfer control if behind other player's cards.
			foreach( DeckDisplay deck in Table.data.allDecks ) {
				if( deck.photonView.isMine ) continue;

				float distFromDeck = deck.transform.InverseTransformPoint( transform.position ).z;
				if( distFromDeck < 0f ) {
					int deckOwner = Table.GetIDFor( deck.photonView.owner );
					photonView.TransferOwnership( deckOwner );
					for( int index = 0; index < stack.Count; index++ ) {

						stack.Remove( stack[index] );
						stack[index].inStack = null;
						// stack[index].photonView.RPC( "ForceIntoStack", PhotonTargets.Others, photonView.viewID );
						stack[index].photonView.TransferOwnership( deckOwner );
						// RemoveFromStack( stack[index], false );
					}
				}
			}

			float flippedAngle = faceUp || inHand ? 0f : 180f;
			photonView.RPC("RotateTo", PhotonTargets.All, Quaternion.Euler(0f,MyYAngle() - bowAngle,flippedAngle), 0.3f);
		}

		Table.data.cardBeingDragged = null;
	}

	public override void ClickActionDown( InputInfo input ) {
		base.ClickActionDown( input );

		if( input.Matches( InputInfo.Action.Target ) ) {
			if( input.isSimulated ) dragStartPoint = transform.position;
			else dragStartPoint = input.hit.point; // so that drag actions appear to start from the point where they started
		}
		if( input.Matches( InputInfo.Action.Move ) ) PeekUpFromHand();
		if( input.Matches( InputInfo.Action.Bow ) && IsMine() ) {
			if( (faceUp || card.secondSide != "") && !inHand ) {
				if( !inProvince ) Bow();
				else photonView.RPC("FlipOver", PhotonTargets.All, 0.5f);

				Table.data.TakeAction();
			}
			else if( !inHand ) photonView.RPC("FlipOver", PhotonTargets.All, 0.5f);
		}
		if( input.Matches( InputInfo.Action.Flip ) && IsMine() ) photonView.RPC("FlipOver", PhotonTargets.All, 0.5f);
		if( input.Matches( InputInfo.Action.Dishonor ) && !inHand && !inProvince && IsMine() ) {
			if( !input.isSimulated ) Unbow();

			if( dishonored ) photonView.RPC("Rehonor", PhotonTargets.All);
			else photonView.RPC("Dishonor", PhotonTargets.All);
		}
		if( input.Matches( InputInfo.Action.Token ) && !(this is Favor) && card.isToken ) Table.data.EditProxy( this, input.hit.point );
	}

	public override void ClickActionUp( InputInfo input ) {
		base.ClickActionUp( input );

		if( input.Matches( InputInfo.Action.Highlight ) ) {
			if( !Table.data.drawingGuide ) {
				photonView.RPC("Select", PhotonTargets.All, Table.data.spectating ? "spectator" : Table.data.myClan );
				Table.data.TakeAction();
			}
			else Table.data.drawingGuide = false;
		}
		if( input.Matches( InputInfo.Action.Deselect ) ) {
			if( !Table.data.drawingGuide && (IsMine() || photonView.isSceneView) ) {
				print("Deleting guides from " + gameObject.name + " " + Time.time);
				photonView.RPC("DeleteGuidesFromTo", PhotonTargets.All, true, false );
			}

			if( selected > -1 ) photonView.RPC("Deselect", PhotonTargets.All);
		}
		if( input.Matches( InputInfo.Action.RemoveFromGame ) ) {
			print("Remove " + name + " from game?");
			if( !Table.data.drawingGuide && IsMine() ) Table.data.removalPanel.ConfirmRemoval( this );
		}
		if( inHand ) TuckBackInHand();
	}

	public override void DragActionStart( InputInfo input ) {
		if( input.Matches( InputInfo.Action.Target ) ) {
			if( !Table.data.drawingGuide && (IsMine() || photonView.isSceneView) ) {
				if( input.isSimulated ) dragStartPoint = transform.position;
				StartCoroutine( DrawGuide() );
			}
		}
		if( input.Matches( InputInfo.Action.Move ) && (this is Favor || IsMine()) ) {

			if( inStack != null ) inStack.RemoveFromStack( this, false );
			Table.data.cardBeingDragged = this;
			origPos = transform.position;
			origRot = transform.rotation;
			EnableShadow();

			StopCoroutine( "FollowCursor" );
			StartCoroutine( "FollowCursor" );
		}
		if( input.Matches( InputInfo.Action.CloneCard ) ) {
			if( IsMine() ) {
				print("Cloning card " + card.name);
				CardDisplay clonedCard = PhotonNetwork.Instantiate("Card", transform.position, transform.rotation, 0).GetComponent<CardDisplay>();
				print("Created card " + clonedCard.name);
				// Debug.Break();
				clonedCard.photonView.RPC("CloneCard", PhotonTargets.All, photonView.viewID );

				string verboseName = faceUp ? name : "a face down card";
				Table.data.chat.AddVerboseChatEntry( "I cloned " + verboseName );

				Table.data.cardBeingDragged = clonedCard;
				clonedCard.origPos = transform.position;
				clonedCard.origRot = transform.rotation;
				clonedCard.EnableShadow();

				Table.data.input.SetClickedObj( clonedCard );
				clonedCard.StartCoroutine( "FollowCursor" );
				// RPC the clone action with this card's photon ID
				// copy properties from one to the other in the RPC method
				// apply all the same movement actions that you would above for the move action
			}
		}
	}

	public override void DragActionEnd( InputInfo input ) {
		if( Table.data.cardBeingDragged == this ) ClickUp( input.hit.point );
		if( Table.data.drawingGuide ) Table.data.drawingGuide = false;
	}

	public override void Hover( InputInfo input ) {
		if( input.Matches( InputInfo.Action.Preview ) ) {
			if( faceUp || input.isSimulated || card.secondSide != "" ) {
				// don't preview card if it's face down

				if( (!IsMine() && !Table.data.spectating && !input.isSimulated) && ((transform.eulerAngles.z > 90f || transform.eulerAngles.z < -90f) && card.secondSide == "") ) {}
				else if( Table.data.cardBeingDragged == null && !flipping ) {

					if( !inHand ) Table.data.ShowPreview( this );
					else {
						if( inHand.faceUp || inHand == Table.data.myHand || Vector3.Dot(transform.up, Vector3.up) > 0f ) Table.data.ShowPreview( this );
						else Table.data.ShowPreview( null );
					}
				}
				else {
					Table.data.ShowPreview( null ); // don't preview cards that are being dragged
				}
			}
		}
		else {
			Table.data.ShowPreview( null );
		}
	}

	[PunRPC]
	public void Select( string clan ) {
		
		if( selected > -1 ) {
			print("Deselecting " + name + " because selected value is " + selected);
			Deselect();
		}
		else {
			selected = (int)Persistent.data.ClanStringToEnum(clan);
			print("Selecting " + name + " with clan color " + selected);

			outline.material.color = clan == "spectator" ? Color.white : Persistent.data.GetColorsForClan(clan)[1];
			// SetOutlineColor( outlineColor );
			outline.gameObject.SetActive( true );

			LeanTween.cancel( outline.gameObject );
			LeanTween.value( 
				outline.gameObject, 
				s => outline.transform.localScale = new Vector3(s, s, s), 
				0.5f, 
				1f, 
				outlineScaleSpeed )
			.setEase( LeanTweenType.easeOutQuad );

			StartCoroutine( OffsetOutline() );

			// LeanTween.value( 
			// 	outline.gameObject, 
			// 	a => SetOutlineColor( new Color( outlineColor.r, outlineColor.g, outlineColor.b, a )), 
			// 	0f, 
			// 	1f, 
			// 	outlineSpeed )
			// .setEase( LeanTweenType.easeInOutQuad )
			// .setLoopPingPong();

			// fx.SetOutlineColor( clan == "spectator" ? Color.white : Persistent.data.GetColorsForClan(clan)[1] );
			// fx.Outline(true);
			// myFront.material.mainTexture = origFront;
			Table.data.selectedCards.Add( this );
		}
	}

	IEnumerator OffsetOutline() {
		while( outline.gameObject.activeInHierarchy ) {
			outline.material.mainTextureOffset += new Vector2( 0f, Time.deltaTime * outlineBloomSpeed );

			yield return null;
		}
	}

	// public void SetOutlineColor( Color colorToSet ) {
	// 	Color32 convertedColor = (Color32)colorToSet;
 //        Vector3[] vertices = outline.mesh.vertices;
 //        Color32[] colors32 = new Color32[vertices.Length];
 //        int i = 0;
 //        while (i < vertices.Length) {
 //            colors32[i] = convertedColor;
 //            i++;
 //        }
 //        outline.mesh.colors32 = colors32;
	// }

	// public Color GetOutlineColor() {
	// 	if( outline.mesh.colors32.Length > 0 ) {
	// 		return (Color)outline.mesh.colors32[0];
	// 	}
		
	// 	return Color.white;
	// }

	[PunRPC]
	public void Deselect() {
		selected = -1;

		LeanTween.cancel( outline.gameObject );
			LeanTween.value( 
				outline.gameObject, 
				s => outline.transform.localScale = new Vector3(s, s, s), 
				1f, 
				0.5f, 
				outlineScaleSpeed )
			.setEase( LeanTweenType.easeOutQuad )
			.setOnComplete( () => outline.gameObject.SetActive( false ) );

		// LeanTween.cancel( outline.gameObject );
		// Color curOutlineColor = GetOutlineColor();
		// LeanTween.value( 
		// 	outline.gameObject, 
		// 	a => SetOutlineColor( new Color( curOutlineColor.r, curOutlineColor.g, curOutlineColor.b, a )), 
		// 	curOutlineColor.a, 
		// 	0f, 
		// 	0.1f )
		// .setEase( LeanTweenType.easeInOutQuad )
		// .setOnComplete( () => outline.gameObject.SetActive(false) );

		// fx.Outline(false);
		// myFront.material.mainTexture = origFront; // because removing the outline reverts to the sharedMaterial

		Table.data.selectedCards.Remove( this );
	}

	[PunRPC]
	public void CloneCard( int viewID ) {
		print("Cloning " + viewID);
		CardDisplay c = Table.data.FindCard( viewID );

		if( !c.card.isToken ) {
			print("Loading " + c.card.id);
			StartCoroutine( LoadCard( c.card.id, "", c.card.edition ) );
			StartCoroutine( DownloadImage() );
		}
		else {
			string imageURL = "";
			if( c.card.images.Count > 0 ) imageURL = c.card.images[0];
			LoadNewCard( c.card.name, c.card.id, imageURL, (int)c.card.deckType, c.card.type );
			Texture frontImage = c.myFront.material.mainTexture;
			myFront.material.mainTexture = frontImage;
		}

		if( card == null ) card = c.card; // this happens for proxies
		name = c.name;
		inDeck = c.inDeck;
		faceUp = c.faceUp;
		inPlay = c.inPlay;
		inFief = c.inFief;
		bowed = c.bowed;
		isFate = c.isFate;
		dishonored = c.dishonored;

		Show();

		// if( isFate ) myBack.material.mainTexture = defaultFateBack.mainTexture;
		// else myBack.material.mainTexture = defaultDynBack.mainTexture;

		standInCanvas.gameObject.SetActive( c.standInCanvas.gameObject.activeInHierarchy );

		for( int index = 0; index < c.tokens.Length; index++ ) {
			if( c.tokens[index].isActive ) {
				ActivateToken( index );
				ChangeTokenValue( index, c.tokens[index].tokenValue );
				tokens[index].tokenImage.color = c.tokens[index].tokenImage.color;
			}
		}
	}

	IEnumerator DrawGuide() {

		print(gameObject.name + " drawingGuide = true " + Time.time);
		Table.data.drawingGuide = true;

		GuideLine guide = null;
		GuideLine arrow = null;
		
		GenerateGuide( ref guide, ref arrow, Table.data.myClan );

		Vector3 prevHit = Vector3.zero;
		RaycastHit hit = new RaycastHit();

		while( Table.data.drawingGuide ) {

			if( InputHandler.CastAgainst( Table.data.collider, out hit ) ) {

				if( hit.point == prevHit ) {
					yield return null;
					continue;
				}

				guide.active = true;
				arrow.active = true;

				PaintGuide( guide, arrow, dragStartPoint, hit.point );

				prevHit = hit.point;

				Persistent.data.FrameRateUpFor( 1f );
			}

			yield return null;
		}

		if( InputHandler.CastAgainstAll( out hit ) ) {
			CardDisplay hitCard = hit.transform.GetComponent<CardDisplay>();

			if( hitCard ) {
				print("Hit card = " + hitCard.gameObject.name + " while this card is " + gameObject.name);
				if( hitCard == this ) {
					guide.active = false;
					arrow.active = false;
				}
				else {
					for( int index = 0; index < guides.Count; index++ ) {
						if( guides[index].target == hitCard ) {
							hitCard.photonView.RPC("RmGuideFrom", PhotonTargets.All, photonView.viewID);

							guide.active = false;
							arrow.active = false;

							yield break;
						}
					}

					guide.active = false;
					arrow.active = false;

					photonView.RPC("AttachGuideTo", PhotonTargets.All, hitCard.photonView.viewID, Table.data.myClan, transform.InverseTransformPoint( dragStartPoint ), hitCard.transform.InverseTransformPoint( hit.point ) );

					string targeterName = faceUp ? name : "My face down card";
					string targetedName = hitCard.faceUp ? hitCard.name : "a face down card";
					Table.data.chat.AddVerboseChatEntry( targeterName + " targeted " + targetedName );
					Table.data.TakeAction();
				}
			}
			else {
				guide.active = false;
				arrow.active = false;
			}
		}
	}

	public void PaintGuide( GuideLine guide, GuideLine arrow, Vector3 origin, Vector3 dest ) {
		float guideScale = 3f;
		guide.textureScale = guideScale;

		float[] arrowWidths = new float[2]{Table.data.lineWidth * Table.data.arrowWidth,0f};

		Vector3 dirToDest = dest - origin;
		Vector3 lineBend = dirToDest * Table.data.lineBend;
		guide.MakeCurve( new Vector3[4]{origin, origin + Table.data.lineHeight + lineBend, dest, dest + Table.data.lineHeight - lineBend} );

		int numberOfSegmentsToHide = 2;
		Vector3 newEndPoint = guide.points3[guide.points3.Count - (numberOfSegmentsToHide + 1)];

		for( int index = 1; index <= numberOfSegmentsToHide; index++ ) {
			guide.points3[guide.points3.Count - index] = newEndPoint;
		}

		Vector3 arrowEndPoint = dest;

		arrow.points3[0] = newEndPoint;
		arrow.points3[1] = arrowEndPoint;
		arrow.points3[2] = arrowEndPoint;
		arrow.SetWidths( arrowWidths.ToList() );

		guide.Draw3D();
		arrow.Draw3D();
	}

	public void GenerateGuide( ref GuideLine guide, ref GuideLine arrow, string clan ) {

		Color[] clanColors = Persistent.data.GetColorsForClan(clan);
		for( int index = 0; index < guides.Count; index++ ) {
			if( guides[index].active == false ) {
				guide = guides[index];
				arrow = arrows[index];
			}
		}
		
		if( guide == null ) {
			Vector3[] linePoints = new Vector3[Table.data.lineResolution];

			guide = new GuideLine("Guide", linePoints.ToList(), Table.data.guideMaterial.mainTexture, Table.data.lineWidth, LineType.Continuous, Joins.Fill);
			guide.material = Table.data.guideMaterial;
			guide.clan = clan;
			arrow = new GuideLine("Arrow", new Vector3[3].ToList(), Table.data.arrowMaterial.mainTexture, Table.data.lineWidth, LineType.Continuous, Joins.None);
			arrow.material = Table.data.arrowMaterial;
			arrow.smoothWidth = true;
			arrow.clan = clan;

			guide.SetColor( clanColors[2] );
			arrow.SetColor( clanColors[2] );
			guides.Add( guide );
			arrows.Add( arrow );
		}
	}

	public void RefreshGuides() {
		foreach( GuideLine guide in guides ) {
			if( guide != null && guide.active ) guide.Draw3D();
		}
		foreach( GuideLine arrow in arrows ) {
			if( arrow != null && arrow.active ) arrow.Draw3D();
		}
	}

	[PunRPC]
	public void AttachGuideTo( int viewID, string clan, Vector3 sourceOffset, Vector3 targetOffset ) {
		CardDisplay targetCard = Table.data.FindCard( viewID );

		GuideLine guide = null;
		GuideLine arrow = null;

		GenerateGuide( ref guide, ref arrow, clan );

		guide.SetEnds( this, targetCard, sourceOffset, targetOffset );

		PaintGuide( guide, arrow, guide.GetSourceOffsetPos(), guide.GetTargetOffsetPos() );

		guide.active = true;
		arrow.active = true;

		targetCard.attachedGuides.Add( guide );
		targetCard.attachedArrows.Add( arrow );

		UpdateGuides();
	}

	[PunRPC]
	public void RmGuideFrom( int viewID ) {
		CardDisplay sourceCard = null;
		if( Table.data != null ) sourceCard = Table.data.FindCard( viewID );

		if( sourceCard == null ) return;

		for( int index = 0; index < attachedGuides.Count; index++ ) {
			if( attachedGuides[index].source == sourceCard ) {
				attachedGuides[index].SetEnds( null, null, Vector3.zero, Vector3.zero );
				attachedGuides[index].active = false;
				attachedArrows[index].active = false;

				attachedGuides.RemoveAt(index);
				attachedArrows.RemoveAt(index);
				break;
			}
		}
	}

	[PunRPC]
	public void RmGuideTo( int viewID ) {
		CardDisplay targetCard = Table.data.FindCard( viewID );

		for( int index = 0; index < guides.Count; index++ ) {
			if( guides[index].target == targetCard ) {
				guides[index].SetEnds( null, null, Vector3.zero, Vector3.zero );
				guides[index].active = false;
				arrows[index].active = false;
				break;
			}
		}
	}

	void LateUpdate() {

		if( stack.Count > 0 ) {
			if( !flipping && transform.position != prevPos ) {
				AdjustStackInstantly();

				prevPos = transform.position;
			}
		}

		if( transform.position != prevPos ) {
			UpdateGuides();

			prevPos = transform.position;
		}

		OffsetGuides();
	}

	public void UpdateGuides() {
		for( int index = 0; index < guides.Count; index++ ) {
			if( guides[index].active == false || guides[index].target == null ) continue;
			PaintGuide( guides[index], arrows[index], guides[index].GetSourceOffsetPos(), guides[index].GetTargetOffsetPos() );
		}

		for( int index = 0; index < attachedGuides.Count; index++ ) {
			if( attachedGuides[index].active == false || attachedGuides[index].target == null ) continue;

			PaintGuide( attachedGuides[index], attachedArrows[index], attachedGuides[index].GetSourceOffsetPos(), attachedGuides[index].GetTargetOffsetPos() );
		}
	}

	void OffsetGuides() {
		const float guideOffsetSpeed = 2f;
		foreach( GuideLine guide in guides ) {
			guide.textureOffset = (guide.textureOffset - Time.deltaTime * guideOffsetSpeed) % 10f;
		}
	}

	IEnumerator FollowCursor() {

		while( Table.data.cardBeingDragged == this && !flipping ) {

			RaycastHit hit;
			if( InputHandler.CastAgainst( Table.data.collider, out hit ) ) {
				if( card == null ) print("No card! " + Time.time);

				if( card != null && card.isToken && NearDeck( Table.data.myDynDiscard, hit.point ) ) {
					ApproachDeck( Table.data.myDynDiscard, hit.point );
				}
				else if( !isFate && NearDeck( Table.data.myDynasty, hit.point ) ) {
					ApproachDeck( Table.data.myDynasty, hit.point );
				}
				else if( isFate && NearDeck( Table.data.myFate, hit.point ) ) {
					ApproachDeck( Table.data.myFate, hit.point );
				}
				else if( !isFate && NearDeck( Table.data.myDynDiscard, hit.point ) ) {
					ApproachDeck( Table.data.myDynDiscard, hit.point );
				}
				else if( isFate && NearDeck( Table.data.myFateDiscard, hit.point ) ) {
					ApproachDeck( Table.data.myFateDiscard, hit.point );
				}
				else if( isFate && stack.Count == 0 && Input.mousePosition.y < Screen.height / 5f ) {
					MoveTo( Camera.main.ScreenPointToRay( Input.mousePosition ).GetPoint( grabDistFromCamera ), 0.15f, LeanTweenType.linear, null );

					if( inHand ) Table.data.myHand.photonView.RPC("AddCard", PhotonTargets.All, photonView.viewID, false);
					else Table.data.myHand.photonView.RPC("AddCard", PhotonTargets.All, photonView.viewID, true);

					hoveringOverDeck = null;
					hoveringUnderDeck = null;
					// if not in hand, then add to hand and shift other cards around its expected position
				}
				else {
					MoveTo( hit.point + new Vector3( 0f, hoverDistFromFloor, 0f ), 0.15f, LeanTweenType.linear, null );
					
					float flippedAngle = faceUp ? 0f : 180f;
					if( inHand ) {
						Table.data.myHand.photonView.RPC("RemoveCard", PhotonTargets.All, photonView.viewID);
						if( PlayerPrefs.GetInt( "disableSounds", 0 ) == 0 ) cardFoley.Play();
					}
					RotateTo( Quaternion.Euler(0f,MyYAngle() - bowAngle,flippedAngle) );

					hoveringOverDeck = null;
					hoveringUnderDeck = null;
				}
			}
			if( Table.data.cardBeingDragged == null ) print("Card being dragged by table = null");
			if( flipping ) print("Flippin'");

			yield return new WaitForSeconds( trackingGap );
		}
	}

	bool NearDeck( DeckDisplay deck, Vector3 comparisonPoint ) {
		Vector3 distFromDeck = deck.transform.InverseTransformPoint( comparisonPoint );
		distFromDeck = new Vector3( Mathf.Abs(distFromDeck.x), Mathf.Abs(distFromDeck.y), Mathf.Abs(distFromDeck.z) );

		Vector3 deckExtents = deck.LocalBounds().extents;

		return distFromDeck.x < deckExtents.x + 0.5f && distFromDeck.z < deckExtents.z + 0.5f;
	}

	void ApproachDeck( DeckDisplay deck, Vector3 inputPoint ) {
		if( deck.transform.InverseTransformPoint( inputPoint ).z < -deck.LocalBounds().extents.z + 1f ) {
			MoveTo( deck.transform.position - deck.transform.forward * deck.PeekAmount, 0.5f );
			hoveringUnderDeck = deck;
			hoveringOverDeck = null;
		}
		else {
			MoveTo( deck.transform.position + Vector3.up * (deck.transform.localScale.y + 0.5f), 0.5f);
			hoveringOverDeck = deck;
			hoveringUnderDeck = null;
		}

		float rotAngle = deck.faceUp ? 0f : 180f;
		if( deck.faceUp ) {
			if( myFront.material.mainTexture == null ) {
				PreloadImage();
			}
		}

		RotateTo( Quaternion.Euler(0f, MyYAngle(), rotAngle), 0.5f );
	}

	[PunRPC]
	public void Unbow() {
		float flippedAngle = faceUp ? 0f : 180f;
		bowAngle = dishonored ? -180f : 0f;
		if( !inDeck && !inHand ) {
			photonView.RPC("RotateTo", PhotonTargets.All, Quaternion.Euler(0f,MyYAngle() - bowAngle,flippedAngle), 0.25f);
		}

		if( bowed == true ) {
			string bowedName = faceUp ? name : "a face down card";
			Table.data.chat.AddVerboseChatEntry( "I straightened " + bowedName );
		}

		bowed = false;
	}

	[PunRPC]
	public void Bow() {
		if( bowed ) Unbow();
		else {

			bowAngle = dishonored ? 90f : -90f;
			float flippedAngle = faceUp ? 0f : 180f;
			photonView.RPC("RotateTo", PhotonTargets.All, Quaternion.Euler(0f,MyYAngle() - bowAngle,flippedAngle), 0.25f);
			bowed = true;

			if( bowed == true ) {
				string bowedName = faceUp ? name : "a face down card";
				Table.data.chat.AddVerboseChatEntry( "I bowed " + bowedName );
			}
		}
	}

	[PunRPC]
	public void Dishonor() {
		dishonored = true;
		
		bowAngle = bowed ? 90f : -180f;
		float flippedAngle = faceUp ? 0f : 180f;
		photonView.RPC("RotateTo", PhotonTargets.All, Quaternion.Euler(0f,MyYAngle() - bowAngle,flippedAngle), 0.25f);
	}

	[PunRPC]
	public void Rehonor() {
		dishonored = false;
		
		bowAngle = bowed ? -90f : 0f;
		float flippedAngle = faceUp ? 0f : 180f;
		photonView.RPC("RotateTo", PhotonTargets.All, Quaternion.Euler(0f,MyYAngle() - bowAngle,flippedAngle), 0.25f);
	}

	public void DisableShadow() {
		myBack.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
		myFront.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
	}

	public void EnableShadow() {
		myBack.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
		myFront.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
	}

	public Bounds LocalBounds() {
		if( mesh == null ) {
			mesh = myFront.GetComponent<MeshFilter>().mesh;
		}

		return mesh.bounds;
	}

	[PunRPC]
	public virtual void MoveTo( Vector3 point, float moveTime = 1f ) {
		MoveTo( point, moveTime, LeanTweenType.easeOutQuad, null );
	}

	[PunRPC]
	public void MoveToLocal( Vector3 point, float moveTime = 1f ) {
		MoveToLocal( point, moveTime, LeanTweenType.easeOutQuad, null );
	}

	[PunRPC]
	public void RotateTo( Quaternion rotation, float rotTime = 1f ) {
		RotateTo( rotation, rotTime, LeanTweenType.easeOutQuad, null );
	}

	[PunRPC]
	public void FlipOver( float flipTime = 1f ) {
		StartCoroutine( FlipOver( flipTime, LeanTweenType.easeOutQuad ) );
	}

	[PunRPC]
	public IEnumerator LoadCard( string cardID, string forceType, string printing ) {
		bool firstTimeLoading = originalOwnerID == -1 && photonView.isMine;
		if( firstTimeLoading ) originalOwnerID = Table.data.overrideID; // needs to be assigned before LoadCard is called if want to make card portable across disconnections

		Table.data.allCards.Add( this );

		card = Persistent.data.VerifyIDAgainstDatabase( cardID );
		if( card == null ) {
			yield return StartCoroutine( Persistent.data.VerifyIDAgainstOracle( cardID, delegate(Card returnedCard, int quantity){ card = returnedCard; } ) );
		}

		// if( card.linkedCard != null ) {
		// 	if( card.linkedCard.images.Count > 0 ) {
		// 		card.secondSide = card.linkedCard.images[0];
		// 	}
		// }

		if( card != null ) {

			if( card.linkedCard != null ) {
				card.secondSide = card.linkedCard.images[0];
			}

			if( printing != "" ) card.edition = printing;

			// print("Loaded card onto table with name " + card.name + " and printing " + printing);
			// forced proxy deck type
			if( forceType != "" ) {
				card.isProxy = true;
				card.type = forceType;
				if( forceType == "strategy" || forceType == "follower" || forceType == "item" || forceType == "spell" || forceType == "ring" || forceType == "other" ) {
					card.deckType = Card.DeckType.Fate;
				}
				else card.deckType = Card.DeckType.Dynasty;
			}

			if( (Card.DeckType)card.deckType == Card.DeckType.Dynasty ) isFate = false;
			else isFate = true;

			gameObject.name = card.name;

			// print("Loaded new card " + card.name + " of type " + card.type);

			if( card.type == "stronghold" ) {
				PreloadImage();
			}

			SetCardBack();

			if( this == null ) yield break; // in case card was destroyed while loading its image, e.g. bamboo harvesters
		}
		else {
			Destroy( gameObject );
		}

		initialized = true;
	}

	public void SetCardBack() {
		if( card == null ) return;
		if( card.secondSide != "" ) return;

		int ownerID = Table.GetIDFor(photonView.owner);
		if( (Card.DeckType)card.deckType == Card.DeckType.Dynasty ) {
			myBack.material = defaultDynBack;

			if( Table.data.customDynBacks.ContainsKey( ownerID ) ) {
				myBack.material.mainTexture = Table.data.customDynBacks[ownerID];
			}
		}
		else {
			myBack.material = defaultFateBack;

			if( Table.data.customFateBacks.ContainsKey( ownerID ) ) {
				myBack.material.mainTexture = Table.data.customFateBacks[ownerID];
			}
		}
	}

	public IEnumerator DownloadImage( bool secondSide = false ) {
		while( card == null ) yield return null;

		if( !secondSide && card.secondSide != "" ) StartCoroutine( DownloadImage(true) );

		loadingImage = true;

		WWW request = null;

		int imageToLoadIndex = card.GetImageIndex();

		if( card.images.Count > 0 && imageToLoadIndex > -1 ) {
			// check for local image, in case it's been loaded with another card
			if( card.isFromOracle ) {

				// if forced to download from a second side - kind of an ugly hack
				string localImageURL = Persistent.data.pathToOracleCache + card.id + ".jpg";
				if( secondSide ) localImageURL = Persistent.data.pathToOracleCache + card.id + "_2.jpg";

				request = new WWW( localImageURL );
				yield return request;

				if( string.IsNullOrEmpty( request.error ) ) {
					standInNeeded = false;
				}
				else {
					standInNeeded = true;
				}
			}
			else {
				// print("Card has " + card.images.Count + " images and is loading the one at image index " + imageToLoadIndex);
				string whichSide = secondSide && card.secondSide != "" ? card.secondSide : card.images[imageToLoadIndex];
				string localImageURL = Persistent.data.outsideApp.FullName + Path.DirectorySeparatorChar + whichSide;

				FileInfo localFile = new FileInfo( localImageURL );
				if( localFile.Exists ) {
					request = new WWW( Persistent.data.pathToImages + Path.DirectorySeparatorChar + whichSide );
					yield return request;

					if( string.IsNullOrEmpty( request.error ) ) {
						standInNeeded = false;
					}
				}
				else {
					standInNeeded = true;
				}
			}
		}

		if( standInNeeded ) {
			if( !inDeck ) standIn.gameObject.SetActive( true );
			standIn.CreateStandInCardFrom( this.card );
		}

		if( card.images.Count > 0 && standInNeeded ) {
			// otherwise, download image and cache
			if( card.isFromOracle ) {
				string streamingImageURL = Persistent.data.pathToOracle + card.images[imageToLoadIndex];
				string localImageURL = Persistent.data.cleanPathToOracleCache + card.id + ".jpg";

				if( secondSide ) {
					streamingImageURL = Persistent.data.pathToOracle + card.secondSide;
					localImageURL = Persistent.data.cleanPathToOracleCache + card.id + "_2.jpg";
				}

				request = new WWW( streamingImageURL );
				yield return request;

				if( string.IsNullOrEmpty( request.error ) ) {
					print( "Writing file to " + localImageURL );
					File.WriteAllBytes(localImageURL, request.bytes);

					standInNeeded = false;
				}
				else {
					standInNeeded = true;
					Debug.LogWarning("Failed to fetch card image at path " + streamingImageURL + " with error " + request.error);
				}
			}
			else {
				string whichSide = secondSide && card.secondSide != "" ? card.secondSide : card.images[imageToLoadIndex];

				string streamingImageURL = Persistent.data.pathToStreamingImages + "/" + whichSide;
				string localImageURL = Persistent.data.outsideApp.FullName + Path.DirectorySeparatorChar + whichSide;

				request = new WWW( streamingImageURL );
				yield return request;

				if( string.IsNullOrEmpty( request.error ) ) {

					DirectoryInfo cardDir = new DirectoryInfo(localImageURL.Substring(0, localImageURL.LastIndexOf("/") ) );
					if( !cardDir.Exists ) {
						cardDir.Create();
					}
					File.WriteAllBytes(localImageURL, request.bytes);

					standInNeeded = false;
					if( standIn ) standIn.gameObject.SetActive( false );
				}
				else {
					standInNeeded = true;
					Debug.LogWarning("Failed to fetch card image for " + card.name + " at path " + streamingImageURL + " with error " + request.error);
				}
			}
		}

		if( this == null ) yield break;

		if( !standInNeeded && request != null ) {

			if( secondSide ) {
				myBack.material.mainTexture = ProcessImage( request );
			}
			else {
				myFront.material.mainTexture = ProcessImage( request );
			}

			standIn.gameObject.SetActive( false );

			if( Table.data.currentPreview == this ) {
				Table.data.previewImage.texture = myFront.material.mainTexture;
				Table.data.standInPreview.gameObject.SetActive(false);
				Table.data.previewImage.gameObject.SetActive(true);
			}
		}
		else {
			StartCoroutine( Persistent.data.GetCardFromOracle( card, DownloadOracleImageToFolderCallback ) );
		}

		loadingImage = false;
		if( request != null ) request.Dispose();
	}

	public void DownloadOracleImageToFolderCallback( Card oracleCard ) {
		if( oracleCard != null && oracleCard.images.Count > 0 ) StartCoroutine( DownloadOracleImageToFolder( oracleCard ) );
	}

	IEnumerator DownloadOracleImageToFolder( Card oracleCard ) {

		print("Getting first oracle image attached to card " + oracleCard.name);
		string streamingImageURL = Persistent.data.pathToOracle + oracleCard.images[0]; // oracle image

		string localImageURL = "";
		if( card.isFromOracle ) {
			localImageURL = Persistent.data.pathToOracleCache + card.id + ".jpg";
		}
		else {
			print("Saving oracle image to local folder with path of the first image of " + card.name);
			localImageURL = Persistent.data.outsideApp.FullName + Path.DirectorySeparatorChar + card.images[0]; // local path
		}

		print("Downloading oracle image to folder in CardDisplay with local path " + localImageURL.Substring(0, localImageURL.LastIndexOf("/") ) );
		DirectoryInfo cardDir = new DirectoryInfo(localImageURL.Substring(0, localImageURL.LastIndexOf("/") ) );
		if( !cardDir.Exists ) {
			cardDir.Create();
		}

		WWW request = new WWW( streamingImageURL );
		yield return request;

		if( string.IsNullOrEmpty( request.error ) ) {
			print( "Writing file to " + localImageURL );
			File.WriteAllBytes(localImageURL, request.bytes);

			standInNeeded = false;
			standIn.gameObject.SetActive( false );
			myFront.material.mainTexture = ProcessImage( request );

			if( Table.data.currentPreview == this ) {
				Table.data.previewImage.texture = myFront.material.mainTexture;
				Table.data.standInPreview.gameObject.SetActive(false);
				Table.data.previewImage.gameObject.SetActive(true);
			}
		}

		if( request != null ) request.Dispose();
	}

	public void UnloadImage() {
		myFront.material.mainTexture = null;
	}

	public void PreloadImage() {
		standIn.CreateStandInCardFrom( this.card );
		standIn.gameObject.SetActive(true);
		
		StartCoroutine( DownloadImage() );
	}

	Texture2D ProcessImage( WWW fromRequest ) {

		if( card.encrypted ) {
			byte[] rawData = fromRequest.bytes;
			Cipher.EncryptDecrypt( rawData );
			Texture2D decryptedTexture = new Texture2D( 2, 2 );
			decryptedTexture.LoadImage( rawData );
			
			if( PlayerPrefs.GetInt("mipmap", 0) == 1 || PlayerPrefs.GetInt("downscale", 0) == 1 ) {
				return ProcessImage( decryptedTexture );
			}
			else {
				return decryptedTexture;
			}
		}

		if( PlayerPrefs.GetInt("mipmap", 0) == 1 || PlayerPrefs.GetInt("downscale", 0) == 1 ) {
			return ProcessImage( fromRequest.texture );
		}
		else {
			return fromRequest.textureNonReadable;
		}
	}

	Texture2D ProcessImage( Texture2D fromTexture ) {
		Texture2D frontImage = null;

		if( PlayerPrefs.GetInt("mipmap", 0) == 1 ) {
			frontImage = new Texture2D(fromTexture.width, fromTexture.height);
			frontImage.SetPixels(fromTexture.GetPixels());
		}
		else {
			frontImage = Instantiate( fromTexture );
		}

		if( PlayerPrefs.GetInt("downscale", 0) == 1 ) {
			TextureScale.Bilinear( frontImage, 512, 512 );
			frontImage.mipMapBias = 1f;
			frontImage.anisoLevel = 9;
			frontImage.filterMode = FilterMode.Trilinear;
		}

		frontImage.Apply( PlayerPrefs.GetInt("mipmap", 0) == 1, false );	

		return frontImage;
	}

	// IEnumerator DownloadRemoteImage( string imageURL ) {
	// 	WWW request = new WWW( Persistent.data.pathToOracle + imageURL );

	// 	yield return request;

	// 	if( string.IsNullOrEmpty( request.error ) ) {
	// 		standInNeeded = false;
	// 		standIn.gameObject.SetActive( false );
	// 	}
	// 	else {
	// 		Debug.LogWarning("Failed to fetch card image at path " + imageURL + " with error " + request.error);
	// 	}

	// 	if( !standInNeeded ) {
	// 		myFront.material.mainTexture = ProcessImage( request ); // could be destroyed, i.e. bamboo harvesters

	// 		origFront = myFront.material.mainTexture;

	// 		if( Table.data.currentPreview == this ) {
	// 			Table.data.previewImage.texture = origFront;
	// 			Table.data.standInPreview.gameObject.SetActive(false);
	// 			Table.data.previewImage.gameObject.SetActive(true);
	// 		}
	// 	}
	// }

	[PunRPC]
	public void LoadNewCard( string cardName, string cardID, string imageURL, int deckType, string cardType ) {
		Table.data.allCards.Remove( this );

		gameObject.name = cardName;

		if( (Card.DeckType)deckType == Card.DeckType.Dynasty ) isFate = false;
		else isFate = true;

		card = new Card();
		card.name = cardName;
		if( cardType == "" ) cardType = "personality";
		card.type = cardType;
		card.cost = 0;
		card.edition = "none";
		card.editions.Add( "none" );
		card.isToken = true;
		inPlay = true;

		if( imageURL != "" ) {
			card.images.Add( imageURL );

			if( cardID != "" ) {
				card.isFromOracle = true;
				card.id = cardID;
			}

			StartCoroutine( DownloadImage() );
		}
		else {
			standInNeeded = true;
		}

		Table.data.allCards.Add( this );
		standIn.gameObject.SetActive( true );
		standIn.CreateStandInCardFrom( this.card );
		myFront.material.mainTexture = standIn.cardImage.texture;

		SetCardBack();

		initialized = true;

		// origFront = standIn.cardImage.texture;
		// myFront.material.mainTexture = origFront;

		Show();
	}

	[PunRPC]
	public void ParentTo( int photonView ) {
		if( photonView == -1 ) transform.parent = null;
		else {
			transform.parent = PhotonView.Find( photonView ).transform;
		}
	}

	[PunRPC] // used for forcing cards to be face up at the beginning of the game
	public IEnumerator ForceFaceUp( bool state ) {
		while( card == null ) yield return null;

		if( state == true ) {
			PreloadImage();
			Show();

			faceUp = true;
			inPlay = true;
		}
		else {
			UnloadImage();
			Show();

			faceUp = false;
			inPlay = true;
		}
	}

	// [PunRPC]
	// public void ForceIntoStack( int viewID ) {
	// 	CardDisplay display = PhotonView.Find( viewID ).GetComponent<CardDisplay>();

	// 	stack.Add( display );
	// 	display.inStack = this;
	// }

	[PunRPC]
	public void MakeDead( bool state ) {
		dead = state;
	}

	[PunRPC]
	public void DisableCollider() {
		collider.enabled = false;
	}

	// Tokens

	// activate token
	[PunRPC]
	public void ActivateToken( int tokenIndex ) {
		tokens[tokenIndex].Activate();
		RebuildTokens();
	}

	// change token shape
	[PunRPC]
	public void ChangeTokenShape( int tokenIndex, int shapeIndex ) {
		tokens[tokenIndex].ChangeShape( shapeIndex );
	}

	// change token color
	[PunRPC]
	public void ChangeTokenColor( int tokenIndex, Vector3 tokenColor ) {
		tokens[tokenIndex].ChangeColor( new Color(tokenColor.x, tokenColor.y, tokenColor.z, 1f) );
	}

	[PunRPC]
	public void ChangeTokenValue( int tokenIndex, int toValue ) {
		tokens[tokenIndex].tokenValue = toValue;
	}

	// clear token
	[PunRPC]
	public void ClearToken( int tokenIndex ) {
		tokens[tokenIndex].ClearToken();
		RebuildTokens();
	}

	[PunRPC]
	public void ClearTokens() {
		foreach( Token token in tokens ) token.ClearToken();
		RebuildTokens();
	}

	public void RebuildTokens() {
		int activeIndex = 0;
		for( int index = 0; index < tokens.Length; index++ ) {
			if( tokens[index].isActive ) activeIndex++;
			else tokens[index].gameObject.SetActive( false );
		}

		// find first inactive token and put it at the forefront of the active ones, disabled
		if( activeIndex < tokens.Length ) {
			for( int index = 0; index < tokens.Length; index++ ) {
				if( !tokens[index].isActive ) {
					tokens[index].gameObject.SetActive( true );
					tokens[index].transform.SetAsLastSibling();
					tokens[index].ClearToken();
					break;
				}
			}
		}
	}

	public void DisableTokenInput() {
		for( int index = 0; index < tokens.Length; index++ ) {
			tokens[index].DisableInput();
		}
	}

	public void EnableTokenInput() {
		for( int index = 0; index < tokens.Length; index++ ) {
			tokens[index].EnableInput();
		}
	}

	public bool AnyTokensActive() {
		foreach( Token token in tokens ) {
			if( token.isActive ) return true;
		}

		return false;
	}

	// Utility

	public override float MyYAngle() {
		if( PhotonNetwork.offlineMode == true ) {
			if( photonView.isSceneView && !(this is Favor) ) {
				return 180f;
			}
			else {
				return 0f;
			}
		}

		return base.MyYAngle();
	}

	public virtual bool IsMine() {
		if( locked ) return false;

		if( this == null || photonView == null ) {
			print("Detected no photon view " + Time.time);

			Table.data.CleanUpCards();
			return false;
		}

		return photonView.isMine || photonView.isSceneView;
	}

	void OnDestroy() {
		DeleteGuidesFromTo( true, true );
		if( Table.data ) Table.data.allCards.Remove( this );
	}

	// Serialization

	public SerializedCardDisplay Serialized() {
		SerializedToken[] serializedTokens = new SerializedToken[tokens.Length];
		for( int index = 0; index < tokens.Length; index++ ) {
			serializedTokens[index] = new SerializedToken {
				tokenActive = tokens[index].isActive,
				tokenValue = tokens[index].tokenValue,
				shapeIndex = tokens[index].shapeIndex,
				tokenColor = new Vector3( tokens[index].tokenImage.color.r, tokens[index].tokenImage.color.g, tokens[index].tokenImage.color.b ),
			};
		}

		List<int> stackedCardIds = new List<int>();
		for( int index = 0; index < stack.Count; index++ ) {
			stackedCardIds.Add( stack[index].photonView.viewID );
		}

		return new SerializedCardDisplay {
			cardID = card.id,
			pos = transform.position,
			rot = transform.eulerAngles,
			createdName = card.isToken ? name : "",
			remoteURL = card.images.Count > 0 ? card.images[0] : "",
			cardInDeck = this.inDeck,
			cardFaceUp = this.faceUp,
			cardInPlayer = this.inPlay,
			// cardInHand = this.inHand,
			cardInProvince = inProvince,
			cardInRegion = inRegion,
			cardInFief = inFief,
			cardVisible = myFront.enabled,
			cardSelected = this.selected,
			stackedCards = stackedCardIds.ToArray(),
			tokenValues = serializedTokens,
			deckType = (int)card.deckType,
			fromOracle = card.isFromOracle,
			originalOwner = originalOwnerID,
		};
	}

	// Serialization

	public void DeserializeCard( SerializedCardDisplay s ) {
		originalOwnerID = s.originalOwner;

		if( s.cardID != "" ) StartCoroutine( LoadCard( s.cardID, "", "" ) );
		else LoadNewCard( s.createdName, s.cardID, s.remoteURL, (int)s.deckType, "" );

		// if( !s.cardInHand ) {
			transform.position = s.pos;
			transform.eulerAngles = s.rot;
			myFront.enabled = s.cardVisible;
			myBack.enabled = myFront.enabled;
		// }

		inDeck = s.cardInDeck;
		faceUp = s.cardFaceUp;
		inPlay = s.cardInPlayer;
		// inHand = s.cardInHand;
		inProvince = s.cardInProvince;
		inRegion = s.cardInRegion;
		inFief = s.cardInFief;
		standInCanvas.gameObject.SetActive( s.canvasVisible );

		// Reselect
		if( s.cardSelected > -1 ) {
			print( s.cardID + " is selected with clan index " + s.cardSelected );
			Select( Persistent.data.ClanEnumToString( (Card.Clan)s.cardSelected ) );
		}

		// Reassign stack
		if( s.stackedCards.Length > 0 ) print("Assigning stacked cards to " + name + " " + Time.time);
		for( int index = 0; index < s.stackedCards.Length; index++ ) {
			CardDisplay cardToStack = PhotonView.Find( s.stackedCards[index] ).GetComponent<CardDisplay>();
			stack.Add( cardToStack );
			cardToStack.inStack = this;

			print("Added " + cardToStack.name + " to " + name + "'s stack");
		}

		// Reassign tokens
		if( faceUp ) {
			standInCanvas.gameObject.SetActive( true );
			tokenDisplay.SetActive( true );
		}
		for( int index = 0; index < s.tokenValues.Length; index++ ) {
			if( s.tokenValues[index].tokenActive ) {
				ActivateToken( index );
				ChangeTokenValue( index, s.tokenValues[index].tokenValue );
				ChangeTokenColor( index, s.tokenValues[index].tokenColor );
				// ChangeTokenShape( index, s.tokenValues[index].shapeIndex );
			}
		}

		if( faceUp ) PreloadImage();

		AdjustStackInstantly();
	}
}

// [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
public class SerializedCardDisplay {
	public string cardID;

	public Vector3 pos;
	public Vector3 rot;

	public string createdName = "";
	public string remoteURL = "";
	public bool fromOracle = false;

	public bool cardInDeck = false;
	public bool cardFaceUp = false;
	public bool cardInPlayer = false;
	// public bool cardInHand = false;
	public bool cardInProvince = false;
	public bool cardInRegion = false;
	public bool cardInFief = false;

	public bool cardVisible = false;
	public bool canvasVisible = false;
	public int cardSelected = -1;

	public int deckType = (int)Card.DeckType.Fate;

	public int[] stackedCards = new int[0];

	public SerializedToken[] tokenValues;
	public SerializedGuideLine[] attachedGuides;

	public int originalOwner = -1;
}