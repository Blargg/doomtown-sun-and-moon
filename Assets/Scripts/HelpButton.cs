﻿using UnityEngine;
using System.Collections;

public class HelpButton : MonoBehaviour {

	public string helpURL = "";

	public bool forceQuit = false;

	public void Clicked() {
		Application.OpenURL( helpURL );

		if( forceQuit ) Application.Quit();
	}
}
