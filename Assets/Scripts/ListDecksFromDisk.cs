﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class ListDecksFromDisk : MonoBehaviour {

	public RectTransform thisTransform;

	// IO
	List<string> decksOnDisk;

	public LobbyListItem deckInListPrefab;

	public LoadLevel levelLoadScript;
	public Deck deckEmissaryPrefab;

	DirectoryInfo deckDir;

	// listed deck label / button prefab

	IEnumerator Start () {
		// destroy any existing deck emissary
		GameObject existingEmissary = GameObject.Find("DeckEmissary(Clone)");
		if( existingEmissary != null ) Destroy(existingEmissary);

		thisTransform.anchoredPosition = new Vector2( thisTransform.anchoredPosition.x, 0f ); // remove this once the beta bug is fixed

		deckDir = new DirectoryInfo( Persistent.data.pathToDecks );
		FileInfo[] decksOnDisk = deckDir.GetFiles();

		while( Persistent.data.loading ) yield return null;

		for( int index = decksOnDisk.Length - 1; index > -1; index-- ) {
			FileInfo deckOnDisk = decksOnDisk[index];

			string extension = deckOnDisk.Extension;
			if( !deckOnDisk.Name.StartsWith(".") && (extension == ".txt" || extension == ".snm") ) {

				LobbyListItem listItem = Instantiate( deckInListPrefab ) as LobbyListItem;
				listItem.rectTransform.SetParent( thisTransform, false );

				listItem.StartSpinner();
				StartCoroutine( listItem.associatedDeck.GenerateCards( deckOnDisk, listItem.StopSpinner ) );
				string legality = ((Card.Legality)listItem.associatedDeck.legality).ToString();
				listItem.gameTitle.text = "[" + legality.Replace("_", " ") + "] " + listItem.associatedDeck.deckName;
				listItem.SetClan( Persistent.data.ClanStringToEnum(listItem.associatedDeck.clan) );

				listItem.callbackOnClicked = DeckInListClicked;
			}
		}
	}
	
	public void CreateNewDeck() {

		int numberOfExistingDecks = 0;
		string deckTitle = "Untitled";
		FileInfo deckOnDisk = new FileInfo(deckDir.FullName + deckTitle + ".txt");
		while( deckOnDisk.Exists ) {
			numberOfExistingDecks++;
			deckOnDisk = new FileInfo( deckDir.FullName + deckTitle + " " + numberOfExistingDecks + ".txt" );
		}
		StreamWriter sw = deckOnDisk.CreateText();
		sw.Close();

		LobbyListItem listItem = Instantiate( deckInListPrefab, Vector3.zero, Quaternion.identity ) as LobbyListItem;
		listItem.rectTransform.SetParent( thisTransform, false );
		listItem.rectTransform.SetAsFirstSibling();

		StartCoroutine( listItem.associatedDeck.GenerateCards( deckOnDisk, null ) );
		listItem.gameTitle.text = listItem.associatedDeck.deckName;
		listItem.SetClan( Persistent.data.ClanStringToEnum(listItem.associatedDeck.clan) );

		listItem.callbackOnClicked = DeckInListClicked;
	}

	public void DeckInListClicked( LobbyListItem item ) {
		Deck deckEmissary = Instantiate( deckEmissaryPrefab ) as Deck;
		
		Deck.CopyDeck( item.associatedDeck, deckEmissary );

		DontDestroyOnLoad( deckEmissary.gameObject );

		if( PhotonNetwork.offlineMode == true ) {
			// skip directly to the card table if loading separately
			levelLoadScript.levelToLoad = "Card Table";
			levelLoadScript.LoadNetworkLevelByName();
		}
		else {
			levelLoadScript.LoadLevelByName();
		}
	}
}
