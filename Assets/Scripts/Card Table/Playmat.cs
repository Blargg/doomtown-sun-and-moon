﻿using UnityEngine;
using System.Collections;

public class Playmat : Photon.MonoBehaviour {

	float fadeSpeed = 0.5f;

	public Material opaqueMaterial;
	new public Renderer renderer;

	void Start() {
		renderer.material.color = new Color( 1f, 1f, 1f, 0f );
	}

	[PunRPC]
	public IEnumerator Initialize( string url ) {
		print("Initializing playmat with url " + url);
		WWW request = new WWW( url );

		yield return request;

		if( string.IsNullOrEmpty( request.error ) ) {
			renderer.material.mainTexture = request.textureNonReadable;

			print("Playmat texture set");

			float aspectR = (float)request.texture.width / (float)request.texture.height;

			transform.localScale = new Vector3( aspectR, 1f, transform.localScale.z );

			print("Playmat scale set to " + transform.localScale);

			if( renderer.material.color.a != 1f ) LeanTween.value( gameObject, Fade, renderer.material.color.a, 1f, fadeSpeed ).setEase(LeanTweenType.easeInOutQuad).setOnComplete( MakeOpaque );
		}
	}

	public void Fade( float a ) {
		renderer.material.color = new Color( 1f, 1f, 1f, a );
	}

	public void MakeOpaque() {
		Texture origTexture = renderer.material.mainTexture;
		renderer.material = opaqueMaterial;
		renderer.material.mainTexture = origTexture;
	}
}
