﻿using UnityEngine;
using System.Collections;

public class CameraZoom : MonoBehaviour {

	public Transform pivot;

	public float zoomSpeed = 1f;
	public float minDistFromTable = 10f;
	public float maxDistFromTable = 100f;

	public void Zoom( float byAmount ) {

		transform.Translate( new Vector3(0f, 0f, byAmount * zoomSpeed * Time.deltaTime) );
		if( transform.localPosition.z < minDistFromTable ) transform.localPosition = new Vector3( 0f, 0f, minDistFromTable );
		if( transform.localPosition.z > maxDistFromTable ) transform.localPosition = new Vector3( 0f, 0f, maxDistFromTable );

		Persistent.data.FrameRateUpFor( 0.5f );
	}
}
