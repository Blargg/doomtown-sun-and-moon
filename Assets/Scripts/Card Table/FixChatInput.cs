﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class FixChatInput : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler {

	public ChatPanel chatPanel;
	public InputField chatInput;

	// Needed until Unity fixes CanvasGroup parent inheritance
	public void OnPointerDown( PointerEventData evt ) {
		if( !chatPanel.chatScroll.gameObject.activeInHierarchy ) return;
		
		chatInput.Select();
		chatInput.ActivateInputField();
	}

	public void OnPointerEnter( PointerEventData evt ) {
		if( !chatPanel.chatScroll.gameObject.activeInHierarchy ) return;

		if( EventSystem.current.currentSelectedGameObject == chatInput.gameObject ) {
			print("Input field already selected " + Time.time);
		}
		else {
			chatInput.Select();
			chatInput.ActivateInputField();
		}
	}
}
