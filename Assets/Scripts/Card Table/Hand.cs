﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;

public class Hand : Clickable {

	public float angleRange = 35f;
	public float maxAngleInterval = 5f;
	public float distanceFromPivot = 15f;

	public List<CardDisplay> cards;
	public Transform cardLocation;

	public bool faceUp {
		get {
			return _faceUp;
		}
		set {
			if( value == false ) transform.rotation = Quaternion.Euler( 270f, -180f, 0f );
			if( value == true ) transform.rotation = Quaternion.Euler( 90f, 0f, 0f );
		}
	}
	bool _faceUp = false;

	[PunRPC]
	public void Initialize( bool hidden ) {

		if( photonView.isMine ) {
			transform.parent = Camera.main.transform;
			transform.localPosition = Table.data.myHandLocalPos;
			transform.localRotation = Quaternion.identity;
		}
		else if( Table.data.spectating ) {
			if( hidden ) faceUp = false;
			else faceUp = true;
			
			transform.RotateAround( Vector3.zero, Vector3.up, MyYAngle() );
		}
		else {
			faceUp = false;
			transform.RotateAround( Vector3.zero, Vector3.up, MyYAngle() );
		}
	}

	[PunRPC]
	public void FlipForSpectators() {
		if( Table.data.spectating ) {
			faceUp = !faceUp;
		}
	}

	[PunRPC]
	public void AddCard( int addViewID, bool forceFaceUp ) {
		if( cards == null ) {
			cards = new List<CardDisplay>();
		}

		CardDisplay card = PhotonView.Find(addViewID).GetComponent<CardDisplay>();

		cards.Remove( card ); // in case it's just being repositioned in the hand

		card.photonView.RPC("ParentTo", PhotonTargets.All, photonView.viewID);

		int indexToAdd = 0;
		for( int index = 0; index < cards.Count; index++ ) {
			if( cards[index].transform.localPosition.x < card.transform.localPosition.x ) indexToAdd++;
		}

		if( card.photonView.isMine ) {
			photonView.RPC("InsertCard", PhotonTargets.All, addViewID, indexToAdd);
		}

		card.inHand = this;
		card.Unbow();
		card.inDeck = false;
		card.faceUp = forceFaceUp ? true : card.faceUp;
		card.playedFromHand = true;
		card.tokenDisplay.SetActive( false );

		if( card.photonView.isMine ) card.DisableShadow();
		else card.EnableShadow();

		card.photonView.RPC("Show", PhotonTargets.All);
		photonView.RPC("ArrangeCards", PhotonTargets.All);
	}

	[PunRPC]
	public void InsertCard( int addViewID, int atIndex ) {
		CardDisplay card = PhotonView.Find(addViewID).GetComponent<CardDisplay>();

		cards.Insert( atIndex, card );
	}

	[PunRPC]
	public void RemoveCard( int removeViewID ) {

		CardDisplay card = null;
		for( int index = 0; index < cards.Count; index++ ) {
			if( cards[index].photonView.viewID == removeViewID ) {
				card = cards[index];
				cards.RemoveAt( index );
			}
		}

		if( card == null ) {
			CleanHand();
			photonView.RPC("ArrangeCards", PhotonTargets.All);
			return;
		}

		card.inHand = null;
		card.EnableShadow();
		card.photonView.RPC("ParentTo", PhotonTargets.All, -1);
		card.tokenDisplay.SetActive( true );

		photonView.RPC("ArrangeCards", PhotonTargets.All);

		if( !photonView.isMine ) card.MoveTo( card.transform.position + card.transform.forward * 2f );
	}

	[PunRPC]
	public void ArrangeCards() {
		float angleInterval = Mathf.Min( angleRange / cards.Count, maxAngleInterval );
		float angleStart = (angleInterval / 2f * (cards.Count - 1f));

		Vector3 zIndexFudge = new Vector3(0f,0f,0.02f);

		cardLocation.position = transform.position + transform.up * distanceFromPivot;
		cardLocation.RotateAround( transform.position, transform.forward, angleStart );
		for( int index = 0; index < cards.Count; index++ ) {

			Vector3 flippedDirection = cards[index].faceUp ? -transform.forward : transform.forward;
			Quaternion cardRotation = Quaternion.LookRotation( (cardLocation.position - transform.position).normalized, flippedDirection );
			Vector3 handPos = cardLocation.localPosition + zIndexFudge * index;
			cards[index].handPos = handPos;
			cards[index].handRot = cardRotation;

			if( Table.data.cardBeingDragged != cards[index] ) {
				if( cards[index].photonView.isMine ) cards[index].MoveToLocal( handPos, 1f );
				else cards[index].transform.localPosition = handPos; // for opponents, cards should not animate
			}

			if( cards[index].photonView.isMine ) cards[index].RotateTo( cardRotation, 1f );
			else cards[index].transform.rotation = cardRotation;

			cardLocation.RotateAround( transform.position, transform.forward, -angleInterval );
		}

		// reset card location for reference later
		cardLocation.position = transform.position + transform.up * distanceFromPivot;
	}

	void CleanHand() {
		foreach( CardDisplay card in cards ) {
			if( card == null ) {
				cards.Remove( card );
				CleanHand();
				return;
			}
		}
	}

	// Serialization

	public SerializedHand Serialized() {

		int[] viewsInHand = new int[cards.Count];
		for( int index = 0; index < cards.Count; index++ ) {
			viewsInHand[index] = cards[index].photonView.viewID;
		}

		return new SerializedHand {
			pos = transform.position,
			rot = transform.eulerAngles,
			myYAngle = MyYAngle(),
			views = viewsInHand,
			hiddenFromSpectators = PlayerPrefs.GetInt("hidehand", 0) == 1,
		};
	}

	[PunRPC]
	public void DeserializeHandString( string serialized ) {
		DeserializeHand( JsonConvert.DeserializeObject<SerializedHand>(serialized) );
	}

	public void DeserializeHand( SerializedHand s ) {
		CardDisplay[] incomingCards = new CardDisplay[s.views.Length];
		for( int index = 0; index < s.views.Length; index++ ) {
			incomingCards[index] = PhotonView.Find( s.views[index] ).GetComponent<CardDisplay>();
			incomingCards[index].transform.SetParent( transform );
			incomingCards[index].inHand = this;
			incomingCards[index].Show();
			incomingCards[index].EnableShadow();
			incomingCards[index].tokenDisplay.SetActive( false );
		}

		cards = new List<CardDisplay>( incomingCards );

		if( Table.data.spectating ) {
			if( s.hiddenFromSpectators ) faceUp = false;
			else faceUp = true;
			
			transform.RotateAround( Vector3.zero, Vector3.up, s.myYAngle );
		}
		else {
			transform.position = s.pos;
			transform.rotation = Quaternion.Euler( s.rot );
		}

		ArrangeCards();
	}
}

public class SerializedHand {
	public Vector3 pos = Vector3.zero;
	public Vector3 rot = Vector3.zero;

	public float myYAngle;
	public int[] views;
	public bool hiddenFromSpectators = false;
}
