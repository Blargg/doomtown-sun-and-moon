﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ChatItem : MonoBehaviour {

	public Text nameLabel;
	public Text message;
	public Image backgroundImage;
	public RectTransform backgroundRect;
}
