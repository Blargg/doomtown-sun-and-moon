﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;

using System.Text;
using SevenZip.Compression.LZMA;

public class DeckDisplay : Clickable {

	public bool faceUp = false;
	public bool isDynasty = true;
	public bool isDiscard = false;

	Deck deck;

	public List<CardDisplay> cards;

	new public Renderer renderer;
	public Material defaultFateBack;
	public Material defaultDynBack;

	// drawing
	Hand hand;
	public float PeekAmount {
		get { return peekAmount; }
		set { peekAmount = value; }
	}
	float peekAmount = 0.5f;
	CardDisplay drawCandidate = null;
	Mesh mesh = null;

	// sound effects
	public AudioSource drewCard;

	public int originalOwnerID = -1;

	void Awake() {
		renderer.enabled = false;
	}

	public void RegenerateDeck() {
		float yScale = Mathf.Min(cards.Count / 30f, 1.5f);
		yScale = Mathf.Max( 0.05f, yScale );

		if( cards.Count == 0 ) {
			renderer.enabled = false;
		}
		else {
			renderer.enabled = true;
			for( int index = 0; index < cards.Count; index++ ) {
				cards[index].transform.position = new Vector3( transform.position.x, index * yScale / cards.Count + 0.1f, transform.position.z );
			}

			if( faceUp ) {
				if( cards[cards.Count - 1].myFront.material.mainTexture != null ) renderer.material.mainTexture = cards[cards.Count - 1].myFront.material.mainTexture;
				else StartCoroutine( LoadTopImage() );
			}
		}

		transform.localScale = new Vector3( 1f, yScale, 1f );
	}

	public override void ClickActionDown( InputInfo input ) {
		if( input.Matches( InputInfo.Action.SearchDeck ) && !Table.data.deckListUI.panel.displaying ) Table.data.deckListUI.ListDeck( this, 0 );
		if( input.Matches( InputInfo.Action.Move ) && photonView.isMine ) {
			PrepareCard( input.hit.point );
		}
		if( input.Matches( InputInfo.Action.ShuffleDeck ) ) {
			photonView.RPC( "Shuffle", PhotonTargets.All, System.Environment.TickCount );

			string deckOwnership = photonView.isMine ? "my" : "your";
			Table.data.chat.photonView.RPC("AddChatEntry", 
				PhotonTargets.All,
				PhotonNetwork.player.ID,
				Table.data.myName,
				"I shuffled " + deckOwnership + " " + name,
				(int)Persistent.data.ClanStringToEnum(Table.data.myClan) );
		}
	}

	public override void ClickActionUp( InputInfo input ) {
		base.ClickActionUp( input );

		if( drawCandidate ) ReturnCard( input.hit.point );
	}

	public override void DragActionStart( InputInfo input ) {
		print("Drag action start on " + name);
		if( input.Matches( InputInfo.Action.Move ) && drawCandidate ) {
			photonView.RPC("RemoveCardFromDeck", PhotonTargets.All, drawCandidate.photonView.viewID );

			drawCandidate.DragActionStart( input );
			Table.data.input.SetClickedObj( drawCandidate );

			drewCard.Play();

			if( faceUp ) Table.data.chat.AddVerboseChatEntry( "I drew " + drawCandidate.name + " from my " + name );
			else Table.data.chat.AddVerboseChatEntry( "I drew a card from my " + name );
		}
	}

	public IEnumerator DrawCards( int quantity ) {
		for( int index = 0; index < quantity; index++ ) {

			while( cards[cards.Count - 1].initialized == false ) yield return null;

			DrawCard();

			yield return new WaitForSeconds( 0.5f );
		}
	}

	// from the top of the deck
	public void DrawCard() {
		CardDisplay card = cards[cards.Count - 1];

		DrawCard( card );
	}

	public void DrawCard( CardDisplay card ) {

		if( card == null ) return;
		if( !card.photonView.isMine ) return;
		if( card.standInNeeded ) card.standIn.gameObject.SetActive(true);
		photonView.RPC("RemoveCardFromDeck", PhotonTargets.All, card.photonView.viewID);

		card.faceUp = true;
		Table.data.myHand.photonView.RPC("AddCard", PhotonTargets.All, card.photonView.viewID, true);
	}

	public void PlayCard( CardDisplay card ) {
		// plays card directly to table

		if( card == null ) return;
		if( !card.photonView.isMine ) return;
		if( card.standInNeeded ) card.standIn.gameObject.SetActive(true);
		photonView.RPC("RemoveCardFromDeck", PhotonTargets.All, card.photonView.viewID);

		// Move to center of table
		card.photonView.RPC("ForceFaceUp", PhotonTargets.All, true);
		card.bowed = false;
		card.inPlay = true;
		card.photonView.RPC("MoveTo", PhotonTargets.All, new Vector3(0f, CardDisplay.staticDistFromFloor, 0f), 0.5f);
		card.photonView.RPC("RotateTo", PhotonTargets.All, Quaternion.Euler(0f,MyYAngle(),0f), 0.5f);
	}

	public IEnumerator FillProvinces( float delay ) {

		yield return new WaitForSeconds(delay);

		for( int index = 0; index < 4; index++ ) {
			CardDisplay card = cards[cards.Count - 1];

			photonView.RPC("RemoveCardFromDeck", PhotonTargets.All, card.photonView.viewID);
			Table.data.AddToProvinces( card );

			yield return new WaitForSeconds( 0.5f );
		}
	}

	public void PrepareCard( Vector3 originPoint ) {
		if( cards.Count == 0 ) return;

		if( originPoint.y > transform.localScale.y / 2f ) {
			// draw from top
			CardDisplay cardToDraw = cards[cards.Count - 1];
			drawCandidate = cardToDraw;
		}
		else {
			// draw from bottom
			CardDisplay cardToDraw = cards[0];
			drawCandidate = cardToDraw;
			cardToDraw.photonView.RPC("Show", PhotonTargets.All);
			cardToDraw.MoveTo( cardToDraw.transform.position - cardToDraw.transform.forward * peekAmount, 0.25f );
		}
	}

	public void ReturnCard( Vector3 originPoint ) {
		if( cards.Count == 0 ) return;
		
		if( originPoint.y > transform.localScale.y / 2f ) {
			CardDisplay cardToDraw = cards[cards.Count - 1];
			cardToDraw.MoveTo( new Vector3(transform.position.x, transform.localScale.y + 0.05f, transform.position.z), 0.25f );
			StartCoroutine( cardToDraw.DelayedHide( 0.25f ) );
		}
		else {
			CardDisplay cardToDraw = cards[0];
			cardToDraw.MoveTo( transform.position, 0.25f );
			StartCoroutine( cardToDraw.DelayedHide( 0.25f ) );
		}
	}

	public override void Hover( InputInfo input ) {
		if( input.Matches( InputInfo.Action.Preview ) ) {
			if( (faceUp || input.isSimulated) && cards.Count > 0 ) {
				Table.data.ShowPreview( cards[cards.Count - 1] );
			}
		}
	}

	public IEnumerator AddCardUnderDeck( CardDisplay card ) {
		if( card.inProvince == true ) yield break;

		card.photonView.RPC("MoveTo", PhotonTargets.All, transform.position, 0.5f);
		card.photonView.RPC("RotateTo", PhotonTargets.Others, card.transform.rotation, 0.5f);

		yield return new WaitForSeconds( 0.5f );

		if( card.faceUp ) {
			if( !card.playedFromHand ) Table.data.chat.AddVerboseChatEntry( "I put " + card.name + " on the bottom of my " + name );
			else Table.data.chat.AddVerboseChatEntry( "I put a card on the bottom of my " + name );
		}
		else Table.data.chat.AddVerboseChatEntry( "I put a card on the bottom of my " + name );

		photonView.RPC( "AddCardToDeckInstantly", PhotonTargets.All, card.photonView.viewID, 0 );
	}

	public IEnumerator AddCardToTopOfDeck( CardDisplay card ) {
		if( card.inProvince == true ) yield break;

		card.photonView.RPC("MoveTo", PhotonTargets.All, transform.position + Vector3.up * (transform.localScale.y + 0.05f), 0.5f);
		card.photonView.RPC("RotateTo", PhotonTargets.Others, card.transform.rotation, 0.5f);
		print("Moving card " + card.gameObject.name + " into place in deck");

		yield return new WaitForSeconds( 0.5f );

		if( card.faceUp ) {
			if( faceUp || !card.playedFromHand ) Table.data.chat.AddVerboseChatEntry( "I put " + card.name + " on the top of my " + name );
			else Table.data.chat.AddVerboseChatEntry( "I put a card on the top of my " + name );
		}
		else Table.data.chat.AddVerboseChatEntry( "I put a card on the top of my " + name );

		print("Sending RPC to add " + card.gameObject.name + " to deck");
		photonView.RPC( "AddCardToDeckInstantly", PhotonTargets.All, card.photonView.viewID, cards.Count );
	}

	[PunRPC]
	public void SwitchCards( int origIndex, int newIndex ) {
		CardDisplay tempIndex = cards[newIndex];
		cards[newIndex] = cards[origIndex];
		cards[origIndex] = tempIndex;
	}

	public Bounds LocalBounds() {
		if( !mesh ) mesh = GetComponent<MeshFilter>().mesh;

		return mesh.bounds;
	}

	[PunRPC]
	public void Initialize( int deckType ) {
		if( originalOwnerID == -1 && photonView.isMine ) originalOwnerID = Table.data.overrideID;

		SetupDeck( deckType );
		transform.RotateAround( Vector3.zero, Vector3.up, MyYAngle() );
	}

	void SetupDeck( int deckType ) {
		int ownerID = Table.GetIDFor(photonView.owner);
		switch( deckType ) {
			case 0: // fate
				isDynasty = false;
				renderer.material = defaultFateBack;

				if( Table.data.customFateBacks.ContainsKey( ownerID ) ) {
					renderer.material.mainTexture = Table.data.customFateBacks[ownerID];
				}
				gameObject.name = "Fate Deck";
			break;

			case 1: // dynasty
				renderer.material = defaultDynBack;

				if( Table.data.customDynBacks.ContainsKey( ownerID ) ) {
					renderer.material.mainTexture = Table.data.customDynBacks[ownerID];
				}
				gameObject.name = "Dynasty Deck";
			break;

			case 2: // fate discard
				isDynasty = false;
				faceUp = true;
				isDiscard = true;
				gameObject.name = "Fate Discard";
			break;

			case 3: // dynasty discard
				faceUp = true;
				isDiscard = true;
				gameObject.name = "Dynasty Discard";
			break;
		}

		Table.data.allDecks.Add( this );

		if( !faceUp ) renderer.enabled = true;

		RegenerateDeck();
	}

	[PunRPC]
	public void FinishedLoadingCustomBack( PhotonMessageInfo info ) { // called by Table after SetCustomXBack coroutine completes
		int ownerID = Table.GetIDFor(photonView.owner);
		if( isDynasty ) renderer.material.mainTexture = Table.data.customDynBacks[ownerID];
		else renderer.material.mainTexture = Table.data.customFateBacks[ownerID];
	}

	[PunRPC]
	public void Shuffle( int seed )
	{
		
		Random.InitState( seed );
		
    for (int i = 0; i < cards.Count; i++)
    {
        CardDisplay temp = cards[i];
        int r = Random.Range(i, cards.Count);
        cards[i] = cards[r];
        cards[r] = temp;
    }

    RegenerateDeck();
	}

	[PunRPC]
	public void AddCardToDeckInstantly( int viewID, int index ) {
		CardDisplay card = PhotonView.Find( viewID ).GetComponent<CardDisplay>();

		cards.Insert( index, card );

		card.photonView.RPC("Hide", PhotonTargets.All);
		if( !faceUp ) {
			card.transform.eulerAngles = new Vector3( 0f, MyYAngle(), 180f );
			card.faceUp = false;
			if( card.dishonored ) card.photonView.RPC("Rehonor", PhotonTargets.All);
		}
		else {
			card.transform.eulerAngles = new Vector3( 0f, MyYAngle(), 0f );
			card.faceUp = true;
		}

		// only called once because inPlay is not synced; only owner will call
		if( card.card.type == "personality" && (card.inPlay || card.dead) ) card.photonView.RPC("MakeDead", PhotonTargets.All, true);

		card.inDeck = true;
		if( isDynasty ) card.isFate = false;
		else card.isFate = true;
		card.inPlay = false;
		card.playedFromHand = false;
		Table.data.RemoveFromRegions(card);

		RegenerateDeck();
		if( Table.data.deckListUI.panel.displaying && Table.data.deckListUI.currentDeck == this ) Table.data.deckListUI.ListDeck( this );
	}

	[PunRPC]
	public void RemoveCardFromDeck( int viewID ) {

		foreach( CardDisplay card in cards ) {

			if( card.photonView.viewID == viewID ) {
				cards.Remove( card );

				card.photonView.RPC("Show", PhotonTargets.All);

				card.inDeck = false;
				if( faceUp ) {
					card.faceUp = true;
				}
				card.dead = false;

				break;
			}
		}

		RegenerateDeck();
	}

	IEnumerator LoadTopImage() {
		CardDisplay topCard = cards[cards.Count - 1];
		yield return StartCoroutine( topCard.DownloadImage() );
		renderer.material.mainTexture = topCard.myFront.material.mainTexture;
	}

	// Serialization

	public SerializedDeckDisplay Serialized() {
		int[] photonViewList = new int[cards.Count];
		for( int index = 0; index < cards.Count; index++ ) {
			photonViewList[index] = cards[index].photonView.viewID;
		}

		return new SerializedDeckDisplay {
			pos = transform.position,
			rot = transform.eulerAngles,
			scale = transform.localScale.y,
			isDynasty = this.isDynasty,
			isDiscard = this.isDiscard,
			cardList = photonViewList,
			originalOwner = originalOwnerID,
		};
	}

	[PunRPC]
	public void DeserializeDeck( SerializedDeckDisplay serializedDeck ) {
		originalOwnerID = serializedDeck.originalOwner;

		transform.position = serializedDeck.pos;
		transform.eulerAngles = serializedDeck.rot;
		transform.localScale = new Vector3( 1f, serializedDeck.scale, 1f );

		CardDisplay[] cardDisplays = new CardDisplay[serializedDeck.cardList.Length];
		if( serializedDeck.cardList != null ) {
			for( int index = 0; index < serializedDeck.cardList.Length; index++ ) {
				CardDisplay cardToAdd = PhotonView.Find( serializedDeck.cardList[index] ).GetComponent<CardDisplay>();
				cardDisplays[index] = cardToAdd;
				cardToAdd.Hide();
			}
		}
		cards = new List<CardDisplay>( cardDisplays );

		int deckType = 0;
		if( serializedDeck.isDynasty ) {
			deckType = 1;
			if( serializedDeck.isDiscard ) deckType = 3;
		}
		else if( serializedDeck.isDiscard ) deckType = 2;

		SetupDeck( deckType );

		if( faceUp && cards.Count > 0 ) StartCoroutine( LoadTopImage() );
	}

	void OnDestroy() {
		if( Table.data ) Table.data.allDecks.Remove( this );
	}
}

public class SerializedDeckDisplay {
	public Vector3 pos;
	public Vector3 rot;
	public float scale;

	public bool isFaceUp = false;

	public bool isDynasty = false;
	public bool isDiscard = false;

	public int[] cardList;

	public int originalOwner = -1;
}
