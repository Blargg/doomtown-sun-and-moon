using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ListDeckDisplay : MonoBehaviour {

	public Text deckName;
	public TranslatePanel panel;
	public DeckDisplayItem deckListItemPrefab;
	public VerticalLayoutGroup layout;
	public RectTransform layoutExtents;
	public InputField cardQuantity;
	public Text partOfDeck;
	int cardsListed = 0;

	public RectTransform wholeScroll;

	public ChatPanel chatPanel;

	public Color rowColor;

	List<DeckDisplayItem> deckListEntries = new List<DeckDisplayItem>();
	[HideInInspector] public DeckDisplay currentDeck;

	[HideInInspector] public CardDisplay selectedCard = null;

	void Start() {
		cardQuantity.onEndEdit.AddListener( delegate(string v) { int i; ListDeck( currentDeck, int.TryParse(v, out i) ? i : 0 ); } );
	}

	public void ListDeck( DeckDisplay deckDisplay ) {
		ListDeck( deckDisplay, cardsListed );
	}

	public void ListDeck( DeckDisplay deckDisplay, int listLength ) {
		print("Listing " + deckDisplay.name + " with " + listLength);

		if( listLength > 0 ) {
			string deckOwnership = deckDisplay.photonView.isMine ? "my" : "your";
			chatPanel.photonView.RPC("AddChatEntry", 
				PhotonTargets.All,
				PhotonNetwork.player.ID,
				Table.data.myName,
				"I looked at the " + partOfDeck.text + " " + listLength + " cards of " + deckOwnership + " " + deckDisplay.name,
				(int)Persistent.data.ClanStringToEnum(Table.data.myClan) );
		}

		if( deckListEntries.Count > 0 ) {
			foreach( DeckDisplayItem oldListEntry in deckListEntries ) Destroy( oldListEntry.gameObject );
		}
		else {
			// if not currently displaying
			if( !panel.displaying ) {
				cardQuantity.text = deckDisplay.cards.Count.ToString();
				panel.Toggle();
			}
		}

		deckListEntries = new List<DeckDisplayItem>();

		currentDeck = deckDisplay;

		bool colorRow = false;
		if( partOfDeck.text == "top" ) {
			print("Listing deck with size " + currentDeck.cards.Count + ".  Counting down from that to " + Mathf.Max(currentDeck.cards.Count - listLength, 0) );
			for( int index = currentDeck.cards.Count - 1; index >= Mathf.Max(currentDeck.cards.Count - listLength, 0); index-- ) {
				BuildListItemAt( index, ref colorRow );
			}
		}
		else {
			print("Listing bottom " + listLength);
			for( int index = 0; index < Mathf.Min(currentDeck.cards.Count, listLength); index++ ) {
				BuildListItemAt( index, ref colorRow );
			}
		}

		cardsListed = Mathf.Min(currentDeck.cards.Count, listLength);

		deckName.text = currentDeck.gameObject.name + " ...";
	}

	public void BuildListItemAt( int index, ref bool colorRow ) {
		DeckDisplayItem listEntry = Instantiate( deckListItemPrefab ) as DeckDisplayItem;
		listEntry.transform.SetParent( transform, false );

		listEntry.cardName.text = currentDeck.cards[index].card.name;
		listEntry.cardIndex.text = (currentDeck.cards.Count - index).ToString() + ".";

		if( colorRow ) listEntry.bground.color = rowColor;
		else listEntry.bground.color = Color.clear;
		colorRow = !colorRow;

		if( currentDeck.cards[index].card.type == "personality" && currentDeck.isDiscard ) {
			listEntry.discardStatus.gameObject.SetActive( true );
			if( currentDeck.cards[index].dead ) {
				listEntry.discardStatus.text = currentDeck.cards[index].dishonored ? "(DHon. Dead)" : "(Hon. Dead)";
			}
		}

		deckListEntries.Add( listEntry );
		listEntry.name = listEntry.cardName.text;
		listEntry.listedCard = currentDeck.cards[index];
		listEntry.deckList = this;
	}

	public void Dismiss() {
		panel.Toggle();

		foreach( DeckDisplayItem oldListEntry in deckListEntries ) {
			Destroy( oldListEntry.gameObject );
		}

		deckListEntries = new List<DeckDisplayItem>();
	}

	// deselects all items in list
	public void DeselectAll() {
		foreach( DeckDisplayItem entry in deckListEntries ) {
			entry.Deselect();
		}
	}

	public void DrawSelectedCard() {
		if( selectedCard ) {
			currentDeck.DrawCard( selectedCard );

			cardsListed--;
			cardQuantity.text = cardsListed.ToString();

			ListDeck( currentDeck, cardsListed );
		}

		chatPanel.photonView.RPC("AddChatEntry", 
			PhotonTargets.All,
			PhotonNetwork.player.ID,
			Table.data.myName,
			"I drew a card from my " + currentDeck.name,
			(int)Persistent.data.ClanStringToEnum(Table.data.myClan) );
	}

	public void PlaySelectedCard() {
		if( selectedCard ) {
			currentDeck.PlayCard( selectedCard );

			cardsListed--;
			cardQuantity.text = cardsListed.ToString();

			ListDeck( currentDeck, cardsListed );
		}
	}

	public void ShuffleDeck() {
		currentDeck.photonView.RPC("Shuffle", PhotonTargets.All, System.Environment.TickCount);

		string deckOwnership = currentDeck.photonView.isMine ? "my" : "your";
		chatPanel.photonView.RPC("AddChatEntry", 
			PhotonTargets.All,
			PhotonNetwork.player.ID,
			Table.data.myName,
			"I shuffled " + deckOwnership + " " + currentDeck.name,
			(int)Persistent.data.ClanStringToEnum(Table.data.myClan) );

		Dismiss();

		selectedCard = null;
	}

	public void SwitchDeckPart() {
		partOfDeck.text = partOfDeck.text == "bottom" ? "top" : "bottom";

		if( panel.displaying ) ListDeck( currentDeck );
	}

	public void SwitchEntries( DeckDisplayItem entryToSwitch, int newIndex ) {
		// Don't switch entries out of deck list range
		int indexMax = entryToSwitch.transform.parent.childCount;
		if( newIndex >= indexMax || newIndex < 0 ) return;

		int oldIndex = entryToSwitch.transform.GetSiblingIndex();

		DeckDisplayItem tempEntry = deckListEntries[newIndex];
		deckListEntries[newIndex] = deckListEntries[oldIndex];
		deckListEntries[oldIndex] = tempEntry;

		entryToSwitch.transform.SetSiblingIndex( newIndex );

		if( partOfDeck.text == "top" ) {
			currentDeck.photonView.RPC("SwitchCards", PhotonTargets.All, currentDeck.cards.Count - 1 - oldIndex, currentDeck.cards.Count - 1 - newIndex);
		}
		else {
			currentDeck.photonView.RPC("SwitchCards", PhotonTargets.All, oldIndex, newIndex);
		}

		bool colorRow = false;
		for( int i = 0; i < transform.childCount; i++ ) {
			DeckDisplayItem entry = transform.GetChild(i).GetComponent<DeckDisplayItem>();
			if( partOfDeck.text == "top" ) {
				entry.cardIndex.text = (i+1).ToString() + ".";
			}
			else {
				entry.cardIndex.text = (currentDeck.cards.Count - i).ToString() + ".";
			}

			if( colorRow ) entry.bground.color = rowColor;
			else entry.bground.color = Color.clear;
			colorRow = !colorRow;
		}

		// Unity bug
		layout.enabled = false;
		layout.enabled = true;
	}

}
