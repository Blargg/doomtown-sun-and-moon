﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ModifyToken : MonoBehaviour {

	public CanvasScaler uiScale;

	public Pop popAnimation;
	public RectTransform popRect;

	public Text tokenNumber;
	public Text tokenIncrement;
	public Text tokenDecrement;
	public Image tokenImage;
	public Image deleteToken;

	Token currentToken;

	public Image[] shapes;

	public static ModifyToken data;

	void Start() {
		data = this;
	}

	public void Reveal( Token forToken ) {
		currentToken = forToken;

		Vector3 guiPos = Camera.main.WorldToScreenPoint( forToken.transform.position ) * (uiScale.referenceResolution.y / Screen.height);
		popRect.anchoredPosition = new Vector2( guiPos.x, guiPos.y );
		tokenImage.sprite = forToken.activeSprite;
		tokenImage.color = forToken.activeColor;
		tokenNumber.text = forToken.tokenValue.ToString();

		popAnimation.PopIn();
	}

	public void Increment() {
		tokenNumber.text = (currentToken.tokenValue + 1).ToString();
		currentToken.cardDisplay.photonView.RPC( "ChangeTokenValue", PhotonTargets.All, currentToken.tokenIndex, currentToken.tokenValue + 1 );
	}

	public void Decrement() {
		tokenNumber.text = (currentToken.tokenValue - 1).ToString();
		currentToken.cardDisplay.photonView.RPC( "ChangeTokenValue", PhotonTargets.All, currentToken.tokenIndex, currentToken.tokenValue - 1 );
	}

	public void Dismiss() {
		popAnimation.PopOut();
	}

	public void Delete() {
		currentToken.cardDisplay.photonView.RPC( "ClearToken", PhotonTargets.All, currentToken.tokenIndex );
		Dismiss();
	}

	public void ChangeTokenColor( Color toColor ) {
		currentToken.cardDisplay.photonView.RPC( "ChangeTokenColor", PhotonTargets.All, currentToken.tokenIndex, new Vector3(toColor.r, toColor.g, toColor.b) );
		tokenImage.color = toColor;
	}

	public void ChangeTokenShape( int shapeIndex ) {
		currentToken.cardDisplay.photonView.RPC( "ChangeTokenShape", PhotonTargets.All, currentToken.tokenIndex, shapeIndex );
		tokenImage.sprite = shapes[shapeIndex].sprite;
	}
}
