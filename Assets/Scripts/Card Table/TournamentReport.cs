﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;

using System.Reflection;

public class TournamentReport : MonoBehaviour {

	public int registeredDecks {
		get {
			return playerData.Count;
		}
	}
	List<TournamentData> playerData = new List<TournamentData>();

	public void RegisterDeck( TournamentData deckInfo ) {

		// Generate entry for player's deck
		deckInfo.deckHash = deckInfo.deckList.GetHashCode();

		foreach( TournamentData addedReport in playerData ) {
			if( deckInfo.playerName == addedReport.playerName && deckInfo.deckHash == addedReport.deckHash ) return;
		}

		playerData.Add( deckInfo );
	}

	public void Reset() {
		playerData = new List<TournamentData>();
	}

	public void Send() {
		// Prepare log

		print("Preparing to send tournament report " + Time.time);

		string tournamentName = Persistent.data.tournamentName;
		string subjectLine = tournamentName == "" ? "" : tournamentName + ": ";

		for( int index = 0; index < playerData.Count; index++ ) {
			subjectLine += playerData[index].playerName + "(" + playerData[index].playerClan.Capitalize() + ") [" + playerData[index].deckHash.ToString().ShortenString(5) + "]";

			if( index < playerData.Count - 1 ) subjectLine += " vs ";
		}

		string messageBody = "Started " + LegalityFilter.ParseLegality( (Card.Legality)Table.data.myDeck.legality ) + " game named: '" + (string)PhotonNetwork.room.customProperties["password"] + "'";

		messageBody += "\n\n";

		for( int index = 0; index < playerData.Count; index++ ) {
			messageBody += playerData[index].playerName + "'s deck list was...\n\n";
			messageBody += playerData[index].deckList + "\n\n***\n\n";
		}

		// Email log

		try {
			MailMessage mail = new MailMessage();
			SmtpClient SmtpServer = new SmtpClient("gator4138.hostgator.com");
			mail.From = new MailAddress("sendtournamentreport@hwstn.com");
			// mail.To.Add("drewc@hwstn.com");
			mail.To.Add("tournamentreport@gmail.com");
			mail.Subject = subjectLine;
			mail.Body = messageBody;
			SmtpServer.Port = 587;
			SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
			SmtpServer.Credentials = new System.Net.NetworkCredential("sendtournamentreport@hwstn.com", "AmaterasuAndOnnotangu2015") as ICredentialsByHost;
			SmtpServer.EnableSsl = true;
			ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
			
			SmtpServer.SendAsync(mail, null);
		}
		catch( Exception e ) {
			Console.WriteLine("Tournament report failed to send: "+e.ToString());
		}

		print("Tournament report sent.");
	}
}

public class TournamentData {
	public string playerName = "";
	public string playerClan = "";
	public string deckList = "";
	public int deckHash = 0;
}
