﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DeckDisplayItem : MonoBehaviour {

	public Text cardIndex;
	public Text cardName;
	public RectTransform cardNamePanel;
	public Image bground;
	public Color selectedColor;

	public Text discardStatus;

	public float previewOffsetX = 10f;

	[HideInInspector] public ListDeckDisplay deckList;

	[HideInInspector] public CardDisplay listedCard; // set when list item is created by ListDeckDisplay

	float uiScale = 1f;
	float origY = -1f;
	int origSiblingIndex = -1;

	bool rearranged = false;

	public void Start() {
		uiScale = Table.data.uiScale.referenceResolution.y / Screen.height;
	}

	public void Select() {
		deckList.DeselectAll();

		cardName.color = selectedColor;

		deckList.selectedCard = listedCard;

		Preview();
	}

	public void Preview() {
		
		Vector3 screenSpaceTextPos = Table.data.guiCamera.WorldToScreenPoint( cardNamePanel.position );
		Vector3 screenSpaceLayoutPos = Table.data.guiCamera.WorldToScreenPoint( deckList.layoutExtents.position );
		float layoutExtents = screenSpaceLayoutPos.x
			 + deckList.layoutExtents.rect.width * deckList.wholeScroll.localScale.x / 2f / uiScale
			 + Table.data.previewPanel.sizeDelta.x / 2f
			 - previewOffsetX;

		Table.data.ShowPreview( listedCard, new Vector2(layoutExtents, screenSpaceTextPos.y) );
	}

	public void ClearPreview() {
		if( Table.data.currentPreview == listedCard ) Table.data.ShowPreview( null );
	}

	public void Deselect() {
		cardName.color = Color.black;
	}

	public void DragStart() {
		if( Table.data.spectating ) return;
		
		origY = Table.data.input.inputInfo.inputPos.y;
		origSiblingIndex = transform.GetSiblingIndex();
	}

	void Update() {
		if( origY >= 0f ) {
			
			int newSiblingIndex = origSiblingIndex + (int)Mathf.Round( (origY - Table.data.input.inputInfo.inputPos.y) / (cardNamePanel.rect.height * deckList.wholeScroll.localScale.y / uiScale) );
			if( newSiblingIndex != transform.GetSiblingIndex() ) {
				print("New sibling index is " + newSiblingIndex + " vs old of " + transform.GetSiblingIndex());
				Preview();
				deckList.SwitchEntries( this, newSiblingIndex );
				rearranged = true;
			}
		}
	}

	public void DragEnd() {
		origY = -1f;
		origSiblingIndex = -1;

		Table.data.ShowPreview( null );

		if( rearranged ) {
			print("Ending drag " + Time.time );
			string deckOwnership = deckList.currentDeck.photonView.isMine ? "my" : "your";
			deckList.chatPanel.photonView.RPC("AddChatEntry", 
				PhotonTargets.All,
				PhotonNetwork.player.ID,
				Table.data.myName,
				"I rearranged cards in " + deckOwnership + " " + deckList.currentDeck.name,
				(int)Persistent.data.ClanStringToEnum(Table.data.myClan) );

			rearranged = false;
		}
	}

	public void SwitchDiscardStatus() {
		// Toggle between statuses
		print("Toggling status " + Time.time);
		switch( discardStatus.text ) {
			case "(Discarded)":
				discardStatus.text = "(Hon. Dead)";
				listedCard.photonView.RPC("MakeDead", PhotonTargets.All, true);
				listedCard.photonView.RPC("Rehonor", PhotonTargets.All);
			break;

			case "(Hon. Dead)":
				discardStatus.text = "(DHon. Dead)";
				listedCard.photonView.RPC("MakeDead", PhotonTargets.All, true);
				listedCard.photonView.RPC("Dishonor", PhotonTargets.All);
			break;

			case "(DHon. Dead)":
				discardStatus.text = "(Discarded)";
				listedCard.photonView.RPC("MakeDead", PhotonTargets.All, false);
				listedCard.photonView.RPC("Rehonor", PhotonTargets.All);
			break;
		}
	}
}
