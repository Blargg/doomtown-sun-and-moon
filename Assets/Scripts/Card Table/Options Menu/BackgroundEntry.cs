﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BackgroundEntry : MonoBehaviour {

	public RectTransform rect;
	public RawImage thumbnail;
	public Image selectionBorder;
	public Button button;

	string backgroundName = "";
	WWW loading;

	public IEnumerator Initialize( string imageURL, string shortName ) {

		loading = new WWW(imageURL);
		backgroundName = shortName;

		yield return loading;

		thumbnail.texture = loading.texture;
		LeanTween.value( gameObject, FadeThumbnail, thumbnail.color.a, 1f, 0.5f ).setEase(LeanTweenType.easeInOutQuad);

		CheckIfHighlighted();
	}

	IEnumerator InitializeMaterial( int toScale ) {

		while( !loading.isDone ) {
			yield return null;
		}

		thumbnail.uvRect = new Rect( thumbnail.uvRect.x, thumbnail.uvRect.y, toScale, toScale );
	}

	public bool CheckIfHighlighted() {
		bool highlighted = PlayerPrefs.GetString("background", "default.jpg") == loading.url;
		if( highlighted ) LeanTween.value( gameObject, FadeSelection, selectionBorder.color.a, 1f, 0.5f ).setEase(LeanTweenType.easeInOutQuad);
		else LeanTween.value( gameObject, FadeSelection, selectionBorder.color.a, 0f, 0.5f ).setEase(LeanTweenType.easeInOutQuad);

		return highlighted;
	}

	public void Clicked() {
		print("Set background to " + backgroundName);
		PlayerPrefs.SetString("background", loading.url);

		if( Table.data ) {
			SetBackground cardTable = Table.data.GetComponent<SetBackground>();

			if( cardTable ) StartCoroutine( cardTable.Start() );
		}

	}

	public void FadeSelection( float a ) {
		selectionBorder.color = new Color( selectionBorder.color.r, selectionBorder.color.g, selectionBorder.color.b, a );
	}

	public void FadeThumbnail( float a ) {
		thumbnail.color = new Color( thumbnail.color.r, thumbnail.color.g, thumbnail.color.b, a );
	}

	// Registered as an event in the right place by BackgroundsPanel
	public void TilingChanged( int changedTo ) {
		StartCoroutine(InitializeMaterial(changedTo));
	}
}