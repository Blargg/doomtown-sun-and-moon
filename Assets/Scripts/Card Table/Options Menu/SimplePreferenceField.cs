﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SimplePreferenceField : MonoBehaviour {

	public InputField fieldToInitialize;
	public string keyToInitializeFrom;
	public string defaultValue;

	void Start() {
		fieldToInitialize.text = PlayerPrefs.GetString( keyToInitializeFrom, defaultValue );
	}

	public void SetPrefs( string valueToSet ) {
		PlayerPrefs.SetString( keyToInitializeFrom, valueToSet );
	}
}
