﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ModifierPreferenceBox : InputPreferenceBox {

	protected override void BuildPresetsFromEnum() {
		#if UNITY_EDITOR || UNITY_STANDALONE
			presetValues = (int[])System.Enum.GetValues(typeof(InputHandler.CustomModifierKeyboard));
		#endif
	}	

	protected override void SetLabel() {
		#if UNITY_EDITOR || UNITY_STANDALONE
			boxLabel.text = ((InputHandler.CustomModifierKeyboard)presetValues[valueIndex]).ToString().Replace("_"," ");
		#endif

		boxSpacer.text = "*" + boxLabel.text + "*";
	}
}
