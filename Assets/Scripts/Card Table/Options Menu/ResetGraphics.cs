﻿using UnityEngine;
using System.Collections;
using System.IO;

public class ResetGraphics : MonoBehaviour {

	public bool setAtStart = false;
	public bool forceSet = false;

	public BackgroundsPanel backgroundsPanel;
	public MultiPreferenceBox tilingPrefs;
	public PreferenceCheckbox mipmappingPrefs;
	public SetFullscreen fullscreenPrefs;

	void Start () {
		if( setAtStart ) Reset();
	}

	public void Reset() {

		print("Resetting graphics defaults " + Time.time);

		if( PlayerPrefs.GetInt("fullscreen", -1) == -1 || forceSet ) {
			Screen.fullScreen = false;
			PlayerPrefs.SetInt("fullscreen", 0);
		}
		if( PlayerPrefs.GetInt("mipmap", -1) == -1 || forceSet ) PlayerPrefs.SetInt("mipmap", 0);

		DirectoryInfo outsideApp = new DirectoryInfo(Application.dataPath).Parent.Parent;
		DirectoryInfo backgroundsDir = new DirectoryInfo(outsideApp.FullName + "/Backgrounds/");

		if( PlayerPrefs.GetString("background", "") == "" || forceSet ) PlayerPrefs.SetString("background", "file://" + backgroundsDir.FullName.Replace(" ", "%20") + "default.jpg" );
		if( PlayerPrefs.GetInt("tile", -1 ) == -1 || forceSet ) PlayerPrefs.SetInt("tile", 10);

		if( PlayerPrefs.GetInt("Screenmanager Resolution Width", -1) == 0 || forceSet ) {
			PlayerPrefs.SetInt("Screenmanager Resolution Width", 1024);
		}
		if( PlayerPrefs.GetInt("Screenmanager Resolution Height", -1) == 0 || forceSet ) {
			PlayerPrefs.SetInt("Screenmanager Resolution Height", 768);
		}

		if( backgroundsPanel ) backgroundsPanel.VerifyBackgrounds();
		if( tilingPrefs ) tilingPrefs.Start();
		if( mipmappingPrefs ) mipmappingPrefs.Start();
		if( fullscreenPrefs ) fullscreenPrefs.Start();
	}
}
