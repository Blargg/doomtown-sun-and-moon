﻿using UnityEngine;
using System.Collections;

public class ResetModifiers : MonoBehaviour {

	public bool setAtStart = false;
	public bool forceSet = false;

	public GameObject modifiersParent;

	void Start () {
		if( setAtStart ) Reset();
	}
	
	public void Reset() {
		if( PlayerPrefs.GetInt("bowMod", -1) == -1 || forceSet ) PlayerPrefs.SetInt("bowMod", 0);
		if( PlayerPrefs.GetInt("deselMod", -1) == -1 || forceSet ) PlayerPrefs.SetInt("deselMod", 2);
		if( PlayerPrefs.GetInt("dishMod", -1) == -1 || forceSet ) PlayerPrefs.SetInt("dishMod", 0);
		if( PlayerPrefs.GetInt("flipMod", -1) == -1 || forceSet ) PlayerPrefs.SetInt("flipMod", 0);
		if( PlayerPrefs.GetInt("highMod", -1) == -1 || forceSet ) PlayerPrefs.SetInt("highMod", 1);
		if( PlayerPrefs.GetInt("moveMod", -1) == -1 || forceSet ) PlayerPrefs.SetInt("moveMod", 0);
		if( PlayerPrefs.GetInt("panMod", -1) == -1 || forceSet ) PlayerPrefs.SetInt("panMod", 0);
		if( PlayerPrefs.GetInt("prevMod", -1) == -1 || forceSet ) PlayerPrefs.SetInt("prevMod", 0);
		if( PlayerPrefs.GetInt("searchMod", -1) == -1 || forceSet ) PlayerPrefs.SetInt("searchMod", 0);
		if( PlayerPrefs.GetInt("targMod", -1) == -1 || forceSet ) PlayerPrefs.SetInt("targMod", 1);
		if( PlayerPrefs.GetInt("tokenMod", -1) == -1 || forceSet ) PlayerPrefs.SetInt("tokenMod", 1);
		if( PlayerPrefs.GetInt("unbowMod", -1) == -1 || forceSet ) PlayerPrefs.SetInt("unbowMod", 0);
		if( PlayerPrefs.GetInt("removeMod", -1) == -1 || forceSet ) PlayerPrefs.SetInt("removeMod", 2);
		if( PlayerPrefs.GetInt("cloneMod", -1) == -1 || forceSet ) PlayerPrefs.SetInt("cloneMod", 2);
		if( PlayerPrefs.GetInt("menuMod", -1) == -1 || forceSet ) PlayerPrefs.SetInt("menuMod", 0);

		if( modifiersParent ) {
			ModifierPreferenceBox[] modifiers = modifiersParent.GetComponentsInChildren<ModifierPreferenceBox>();
			foreach( ModifierPreferenceBox modChild in modifiers ) {
				modChild.Start();
			}
		}
	}
}
