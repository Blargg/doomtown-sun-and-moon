﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PreferenceCheckbox : MonoBehaviour {

	public Toggle checkbox;
	public string preferenceKey = "";
	public int defaultValue = 0;

	public GameObject[] enableOtherObjects;
	public GameObject[] disableOtherObjects;

	public void Start () {
		int currentPrefValue = PlayerPrefs.GetInt(preferenceKey, -1);
		if( currentPrefValue == -1 ) PlayerPrefs.SetInt( preferenceKey, defaultValue );
		checkbox.isOn = PlayerPrefs.GetInt(preferenceKey, 0) == 1;

		ToggleOtherObjects( checkbox.isOn );
	}
	
	public void Clicked( bool toggleState ) {
		PlayerPrefs.SetInt( preferenceKey, toggleState ? 1 : 0 );

		ToggleOtherObjects( toggleState );
	}

	public void ToggleOtherObjects( bool toggleState ) {
		foreach( GameObject otherObject in enableOtherObjects ) otherObject.SetActive( toggleState );
		foreach( GameObject otherObject in disableOtherObjects ) otherObject.SetActive( !toggleState );
	}
}
