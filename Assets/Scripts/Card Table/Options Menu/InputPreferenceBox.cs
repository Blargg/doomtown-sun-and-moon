﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class InputPreferenceBox : MonoBehaviour {

	public Text boxLabel;
	public Text boxSpacer; // expands the layout group with effective padding on either side
	public string preferenceKey = "";
	public int defaultValueIndex = -1;
	public virtual int DefaultValueIndex {
		get{ return defaultValueIndex; }
		set{ defaultValueIndex = value; }
	}
	protected int[] presetValues;
	protected int valueIndex = 0;

	public InputHandler resetRealtimeInput;

	public delegate void PrefsChanged();
	public static event PrefsChanged inputPreferencesChanged;

	public void Start () {
		BuildPresetsFromEnum();
		int currentPrefValue = PlayerPrefs.GetInt(preferenceKey, -1);
		if( currentPrefValue == -1 ) {
			print("Setting " + preferenceKey + " to " + DefaultValueIndex + " on preset values with length " + presetValues.Length);
			PlayerPrefs.SetInt( preferenceKey, (int)presetValues[DefaultValueIndex] );
			valueIndex = DefaultValueIndex;
		}
		else {
			for( int index = 0; index < presetValues.Length; index++ ) {
				if( (int)presetValues[index] == currentPrefValue ) valueIndex = index;
			}
		}

		SetLabel();
	}

	protected virtual void BuildPresetsFromEnum() {
		#if UNITY_EDITOR || UNITY_STANDALONE
			presetValues = (int[])System.Enum.GetValues(typeof(InputHandler.CustomInputMouse));
		#endif
	}
	
	public void Clicked() {
		print("Clicked " + name + " to increment value index currently set to " + valueIndex + " to " + ((valueIndex + 1) % presetValues.Length));
		valueIndex = (valueIndex + 1) % presetValues.Length;
		SetLabel();

		PlayerPrefs.SetInt( preferenceKey, (int)presetValues[valueIndex] );

		if( inputPreferencesChanged != null ) inputPreferencesChanged();
	}

	protected virtual void SetLabel() {
		#if UNITY_EDITOR || UNITY_STANDALONE
			boxLabel.text = ((InputHandler.CustomInputMouse)presetValues[valueIndex]).ToString().Replace("_"," ");
		#endif

		boxSpacer.text = "*" + boxLabel.text + "*";
	}
}
