﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DragPreferenceBox : InputPreferenceBox {

	int regularInputLength = -1;
	public override int DefaultValueIndex {
		get{
			if( regularInputLength == -1 ) {
				#if UNITY_EDITOR || UNITY_STANDALONE
					regularInputLength = System.Enum.GetValues(typeof(InputHandler.CustomInputMouse)).Length;
				#endif
			}

			return defaultValueIndex % regularInputLength;
		}
		set{
			defaultValueIndex = value;
		}
	}

	protected override void BuildPresetsFromEnum() {
		#if UNITY_EDITOR || UNITY_STANDALONE
			presetValues = (int[])System.Enum.GetValues(typeof(InputHandler.CustomDragMouse));
		#endif
	}	

	protected override void SetLabel() {
		#if UNITY_EDITOR || UNITY_STANDALONE
			boxLabel.text = ((InputHandler.CustomDragMouse)presetValues[valueIndex]).ToString().Replace("_"," ");
		#endif

		boxSpacer.text = "*" + boxLabel.text + "*";
	}
}
