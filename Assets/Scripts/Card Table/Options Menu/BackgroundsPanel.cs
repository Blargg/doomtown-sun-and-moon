﻿using UnityEngine;
using System.Collections;
using System.IO;

public class BackgroundsPanel : MonoBehaviour {

	public RectTransform thisTransform;

	public BackgroundEntry backgroundEntryPrefab;

	BackgroundEntry[] backgroundEntries;

	public MultiPreferenceBox tilingPreferences;

	public SetBackground cardTableBackground; // not required, but allows hot-swapping of backgrounds on card table

	void Awake () {
		
		DirectoryInfo backgroundsDir = new DirectoryInfo( Persistent.data.pathToBackgrounds );
		FileInfo[] backgroundsOnDisk = backgroundsDir.GetFiles();

		backgroundEntries = new BackgroundEntry[backgroundsOnDisk.Length];

		for( int index = 0; index < backgroundsOnDisk.Length; index++ ) {
			FileInfo backgroundOnDisk = backgroundsOnDisk[index];

			if( (backgroundOnDisk.Extension == ".jpg" || backgroundOnDisk.Extension == ".jpeg") && !backgroundOnDisk.Name.StartsWith(".") ) {

				BackgroundEntry backgroundEntry = Instantiate( backgroundEntryPrefab, Vector3.zero, Quaternion.identity ) as BackgroundEntry;
				backgroundEntry.rect.SetParent( thisTransform, false );

				StartCoroutine( backgroundEntry.Initialize( "file://" + backgroundOnDisk.FullName.SanitizePath(), backgroundOnDisk.Name ) );

				backgroundEntry.button.onClick.AddListener( VerifyBackgrounds );

				backgroundEntries[index] = backgroundEntry;

				tilingPreferences.ValueChangedEvent += backgroundEntry.TilingChanged;
			}
		}

		if( cardTableBackground ) tilingPreferences.ValueChangedEvent += cardTableBackground.SetTiling;
	}

	public void VerifyBackgrounds() {
		foreach( BackgroundEntry backgroundEntry in backgroundEntries ) {
			if( backgroundEntry && backgroundEntry.CheckIfHighlighted() ) {
				if( cardTableBackground ) StartCoroutine( cardTableBackground.Start() );
			}
		}
	}

	public void OnDisable() {
		if( cardTableBackground ) tilingPreferences.ValueChangedEvent -= cardTableBackground.SetTiling;
	}
}
