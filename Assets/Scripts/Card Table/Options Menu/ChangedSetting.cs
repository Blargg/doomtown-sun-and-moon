﻿using UnityEngine;
using System.Collections;

public class ChangedSetting : MonoBehaviour {

	public virtual void NewIntValue( int newValue ) {

	}

	public virtual void NewStringValue( string newValue ) {

	}

	public virtual void NewBoolValue( bool newValue ) {

	}
}
