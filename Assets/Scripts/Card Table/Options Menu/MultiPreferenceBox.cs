﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class MultiPreferenceBox : MonoBehaviour {

	public Text interiorValue;
	public string preferenceKey = "";
	public string displaySuffix = "";
	public int defaultValueIndex = -1;
	public List<int> presetValues;
	int valueIndex = 0;

	public delegate void ValueChangedDelegate( int toValue );
	public event ValueChangedDelegate ValueChangedEvent;

	// Use this for initialization
	public void Start () {
		int currentPrefValue = PlayerPrefs.GetInt(preferenceKey, -1);
		if( currentPrefValue == -1 ) {
			PlayerPrefs.SetInt( preferenceKey, presetValues[defaultValueIndex] );
			valueIndex = defaultValueIndex;
		}
		else {
			valueIndex = presetValues.IndexOf(currentPrefValue);
		}

		SetLabel();

		if( ValueChangedEvent != null ) ValueChangedEvent( presetValues[valueIndex] );
	}
	
	public void Clicked() {
		valueIndex = (valueIndex + 1) % presetValues.Count;
		SetLabel();

		PlayerPrefs.SetInt( preferenceKey, presetValues[valueIndex] );
		if( ValueChangedEvent != null ) ValueChangedEvent( presetValues[valueIndex] );
	}

	void OnDestroy() {
		ValueChangedEvent = null;
	}

	protected virtual void SetLabel() {
		interiorValue.text = presetValues[valueIndex].ToString() + displaySuffix;
	}
}
