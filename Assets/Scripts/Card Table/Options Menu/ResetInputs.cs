﻿using UnityEngine;
using System.Collections;

public class ResetInputs : MonoBehaviour {

	public bool setAtStart = false;
	public bool forceSet = false;

	public GameObject inputParent;

	void Start () {
		if( setAtStart ) Reset();
	}
	
	public void Reset() {
		if( PlayerPrefs.GetInt("bowInput", -1) == -1 || forceSet) PlayerPrefs.SetInt("bowInput", 1);
		if( PlayerPrefs.GetInt("deselInput", -1) == -1 || forceSet ) PlayerPrefs.SetInt("deselInput", 0);
		if( PlayerPrefs.GetInt("dishInput", -1) == -1 || forceSet ) PlayerPrefs.SetInt("dishInput", 2);
		if( PlayerPrefs.GetInt("flipInput", -1) == -1 || forceSet ) PlayerPrefs.SetInt("flipInput", 4);
		if( PlayerPrefs.GetInt("highInput", -1) == -1 || forceSet ) PlayerPrefs.SetInt("highInput", 0);
		if( PlayerPrefs.GetInt("moveInput", -1) == -1 || forceSet ) PlayerPrefs.SetInt("moveInput", 6);
		if( PlayerPrefs.GetInt("panInput", -1) == -1 || forceSet ) PlayerPrefs.SetInt("panInput", 6);
		if( PlayerPrefs.GetInt("prevInput", -1) == -1 || forceSet ) PlayerPrefs.SetInt("prevInput", 9);
		if( PlayerPrefs.GetInt("searchInput", -1) == -1 || forceSet ) PlayerPrefs.SetInt("searchInput", 1);
		if( PlayerPrefs.GetInt("targInput", -1) == -1 || forceSet ) PlayerPrefs.SetInt("targInput", 6);
		if( PlayerPrefs.GetInt("tokenInput", -1) == -1 || forceSet ) PlayerPrefs.SetInt("tokenInput", 4);
		if( PlayerPrefs.GetInt("unbowInput", -1) == -1 || forceSet ) PlayerPrefs.SetInt("unbowInput", 2);
		if( PlayerPrefs.GetInt("removeInput", -1) == -1 || forceSet ) PlayerPrefs.SetInt("removeInput", 3);
		if( PlayerPrefs.GetInt("cloneInput", -1) == -1 || forceSet ) PlayerPrefs.SetInt("cloneInput", 7);
		if( PlayerPrefs.GetInt("menuInput", -1) == -1 || forceSet ) PlayerPrefs.SetInt("menuInput", 11);

		if( inputParent ) {
			InputPreferenceBox[] inputs = inputParent.GetComponentsInChildren<InputPreferenceBox>();
			foreach( InputPreferenceBox modChild in inputs ) {
				modChild.Start();
			}
		}
	}
}
