﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SetFullscreen : ChangedSetting {

	public Toggle fullscreenToggle;

	public void Start() {
		fullscreenToggle.isOn = PlayerPrefs.GetInt("fullscreen", 0) == 1 ? true : false;
	}

	public void Toggled( bool toggle ) {
		if( !toggle ) Screen.SetResolution( Screen.currentResolution.width - 20, Screen.currentResolution.height - 20, false );
		else Screen.SetResolution( Screen.currentResolution.width, Screen.currentResolution.height, true );

		PlayerPrefs.SetInt("fullscreen", toggle ? 1 : 0 );
	}
}
