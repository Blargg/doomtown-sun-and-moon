﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Vectrosity;

public class GuideLine : VectorLine {

	public CardDisplay source = null;
	public CardDisplay target = null;
	public Vector3 targetOffset = Vector3.zero;
	public Vector3 sourceOffset = Vector3.zero;
	public string clan = "";

	public GuideLine( string lineName, List<Vector3> linePoints, Texture lineTex, float width, LineType lineType, Joins joins ) : base( lineName, linePoints, lineTex, width, lineType, joins ) {
		
	}

	public void SetEnds( CardDisplay source, CardDisplay target, Vector3 sourceOffset, Vector3 targetOffset ) {
		this.source = source;
		this.target = target;
		this.targetOffset = targetOffset;
		this.sourceOffset = sourceOffset;
	}

	public Vector3 GetTargetOffsetPos() {
		return target.transform.TransformPoint( targetOffset );
	}

	public Vector3 GetSourceOffsetPos() {
		return source.transform.TransformPoint( sourceOffset );
	}
}

public class SerializedGuideLine {
	public int sourceView = 0;
	public int targetView = 0;
	public Vector3 sourceOffset = Vector3.zero;
	public Vector3 targetOffset = Vector3.zero;
	public string clan = "";
}