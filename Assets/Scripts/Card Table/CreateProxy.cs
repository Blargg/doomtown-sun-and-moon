﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CreateProxy : MonoBehaviour {

	public UnfurlScroll proxyScroll;
	public CanvasScaler uiScale;
	public InputHandler inputHandler;

	public RectTransform proxyPanel;
	public Text proxyInput;
	public InputField proxyInputField;
	CardDisplay proxyToEdit = null;

	string lastProxyName = "New Token";

	Vector3 cardWorldPos = Vector3.zero;

	void Start() {
		proxyInputField.onEndEdit.AddListener(delegate{ CreateNewProxy(); }); // hacky because deselecting the input field has the result of calling CreateToken
	}

	public void Reveal( Vector3 worldPos, CardDisplay existingProxy = null ) {

		cardWorldPos = new Vector3( worldPos.x, 0.05f, worldPos.z );
		if( existingProxy ) {
			print("Editing existing proxy of " + existingProxy.name + " " + Time.time);
			proxyToEdit = existingProxy;
			proxyInputField.text = existingProxy.name;
		}
		else {
			proxyInputField.text = lastProxyName;
		}

		proxyPanel.pivot = new Vector2( inputHandler.inputInfo.inputPos.x > Screen.width / 2f ? 1f : 0f, 0.5f );
		proxyPanel.anchoredPosition = inputHandler.inputInfo.inputPos * (uiScale.referenceResolution.y / Screen.height); // for standalone

		proxyScroll.UnfurlNow();
	}

	public void Cancel() {
		proxyScroll.FurlNow();
	}

	public void CreateNewProxy() {
		if( proxyScroll.furling ) return;

		if( proxyToEdit == null ) {
			proxyToEdit = PhotonNetwork.Instantiate("Card", cardWorldPos, Table.data.myDynasty.transform.rotation, 0).GetComponent<CardDisplay>();

			Table.data.chat.AddVerboseChatEntry( "I created a new card with name " + proxyInputField.text );

			lastProxyName = proxyInput.text;
		}
		else {
			Table.data.chat.AddVerboseChatEntry( "I changed the name of created card " + lastProxyName + " to " + proxyInputField.text );
		}

		proxyToEdit.photonView.RPC("LoadNewCard", PhotonTargets.All, proxyInputField.text, "", "", (int)Card.DeckType.Fate, "");

		proxyToEdit = null;

		proxyScroll.FurlNow();
	}

	public void DeleteProxy() {
		Table.data.photonView.RPC( "RemoveFromGame", PhotonTargets.All, proxyToEdit.photonView.viewID );
		PhotonNetwork.Destroy( proxyToEdit.photonView );

		proxyToEdit = null;
	}

	public bool IsVisible() {
		return proxyPanel.gameObject.activeInHierarchy;
	}
}
