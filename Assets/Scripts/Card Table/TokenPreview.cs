﻿using UnityEngine;
using System.Collections;

public class TokenPreview : MonoBehaviour {

	public Token[] previewTokens;

	public void PreviewTokensFor( Token[] cardTokens ) {
		for( int index = 0; index < previewTokens.Length; index++ ) {
			if( index < cardTokens.Length ) {
				if( cardTokens[index].isActive ) {

					cardTokens[index].CopyTo( previewTokens[index] );
					print("Building token " + index + " with source sprite " + cardTokens[index].tokenImage.name + " and destination " + previewTokens[index].tokenImage.name );
					previewTokens[index].gameObject.SetActive( true );

					continue;
				}
			}

			previewTokens[index].gameObject.SetActive( false );
		}
	}

	public void ClearTokens() {
		for( int index = 0; index < previewTokens.Length; index++ ) {
			previewTokens[index].gameObject.SetActive( false );
		}
	}

	public void SetClanColors( Color colorToSet ) {
		foreach( Token previewToken in previewTokens ) {
			previewToken.tableText.color = colorToSet;
		}
	}
}
