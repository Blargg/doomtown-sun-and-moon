﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class ChangeDeck : MonoBehaviour {

	public RectTransform deckListLayout;

	// IO
	List<string> decksOnDisk;

	public LobbyListItem deckInListPrefab;
	public Deck deckEmissaryPrefab;
	DirectoryInfo deckDir;

	IEnumerator Start () {
		deckListLayout.anchoredPosition = new Vector2( deckListLayout.anchoredPosition.x, 0f ); // remove this once the beta bug is fixed

		deckDir = new DirectoryInfo( Persistent.data.pathToDecks );
		FileInfo[] decksOnDisk = deckDir.GetFiles();

		while( Persistent.data.loading ) yield return null;

		foreach( FileInfo deckOnDisk in decksOnDisk ) {
			string extension = deckOnDisk.Extension;
			if( !deckOnDisk.Name.StartsWith(".") && (extension == ".txt" || extension == ".snm") ) {

				LobbyListItem listItem = Instantiate( deckInListPrefab ) as LobbyListItem;
				listItem.rectTransform.SetParent( deckListLayout, false );

				listItem.StartSpinner();
				StartCoroutine( listItem.associatedDeck.GenerateCards( deckOnDisk, listItem.StopSpinner ) );
				string legality = ((Card.Legality)listItem.associatedDeck.legality).ToString();
				listItem.gameTitle.text = "[" + legality.Replace("_", " ") + "] " + listItem.associatedDeck.deckName;
				listItem.SetClan( Persistent.data.ClanStringToEnum(listItem.associatedDeck.clan) );

				listItem.callbackOnClicked = RestartGame;
			}
		}
	}

	public void RestartGame( LobbyListItem item ) {
		if( Table.data.curParticipants == 1 ) {
			// destroy any existing deck emissary
			GameObject emissary = GameObject.Find("DeckEmissary(Clone)");
			DestroyImmediate( emissary );

			// create new one
			Deck deckEmissary = Instantiate( deckEmissaryPrefab ) as Deck;
			Deck.CopyDeck( item.associatedDeck, deckEmissary );
			DontDestroyOnLoad( deckEmissary.gameObject );

			// Reload level
			Table.data.photonView.RPC("HardReset", PhotonTargets.MasterClient);
		}
		else {
			Table.data.myDeck = item.associatedDeck;
			Table.data.photonView.RPC("Reset", PhotonTargets.All);
		}
	}

	public void RestartGameWithCurrentDeck() {
		Table.data.photonView.RPC("Reset", PhotonTargets.All);
	}
}
