﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class InputHandler : MonoBehaviour {

	public CameraZoom cameraControl;

	float dragGap = 25f; // pixels

	public float doubleClickSpeed = 0.5f;
	float clickTimer = 0f;

	int clickCount = 0;
	int altClickCount = 0;
	int tertClickCount = 0;
	int draggingButton = -1;

	Clickable[] obj = new Clickable[3];
	Clickable hoveringOver = null;

	float inputDelay = 0.5f;
	float inputTimer = 0f;

	// custom input layer
	public enum CustomModifierKeyboard { 
		#if UNITY_STANDALONE || UNITY_EDITOR
			None = 0, Shift = 1, Control = 2, Alt = 3, 
		#endif
		#if UNITY_STANDALONE_OSX || UNITY_EDITOR
			Command = 4 }
		#elif UNITY_ANDROID || UNITY_IOS
			One_Finger = 5, Two_Finger = 6, Three_Finger = 7, Four_Finger = 8 }
		#elif UNITY_STANDALONE_WIN || UNITY_STANDALONE_LINUX
			}
		#endif
	
	public enum CustomInputMouse{
		Left_Click = 0,
		Double_Left_Click = 1,
		Triple_Left_Click = 2,
		Right_Click = 3,
		Double_Right_Click = 4,
		Triple_Right_Click = 5,
		Middle_Click = 11,
		Double_Middle_Click = 12,
		Triple_Middle_Click = 13
	}
	public enum CustomDragMouse{ Left_Drag = 6, Right_Drag = 7, Middle_Drag = 14 }
	public enum CustomHoverMouse{ Left_Drag = 8, Right_Drag = 9, Middle_Drag = 15, None = 10 }

	public InputInfo inputInfo = new InputInfo();

	int bowMod = -1;
	int dishMod = -1;
	int flipMod = -1;
	int highMod = -1;
	int tokenMod = -1;
	int prevMod = -1;
	int deselMod = -1;
	int moveMod = -1;
	int panMod = -1;
	int targMod = -1;
	int unbowMod = -1;
	int searchMod = -1;
	int removeMod = -1;
	int cloneMod = -1;
	int menuMod = -1;

	int bowInput = -1;
	int dishInput = -1;
	int flipInput = -1;
	int highInput = -1;
	int tokenInput = -1;
	int prevInput = -1;
	int deselInput = -1;
	int unbowInput = -1;
	int searchInput = -1;
	int removeInput = -1;
	int cloneInput = -1;
	int menuInput = -1;

	int moveDrag = -1;
	int panDrag = -1;
	int targDrag = -1;

	void Start() {
		LoadSettings();

		InputPreferenceBox.inputPreferencesChanged += LoadSettings;
	}

	public void LoadSettings() {
		print("Loading input settings " + Time.time);

		bowMod = PlayerPrefs.GetInt("bowMod", -1);
		dishMod = PlayerPrefs.GetInt("dishMod", -1);
		flipMod = PlayerPrefs.GetInt("flipMod", -1);
		highMod = PlayerPrefs.GetInt("highMod", -1);
		tokenMod = PlayerPrefs.GetInt("tokenMod", -1);
		prevMod = PlayerPrefs.GetInt("prevMod", -1);
		deselMod = PlayerPrefs.GetInt("deselMod", -1);
		moveMod = PlayerPrefs.GetInt("moveMod", -1);
		panMod = PlayerPrefs.GetInt("panMod", -1);
		targMod = PlayerPrefs.GetInt("targMod", -1);
		unbowMod = PlayerPrefs.GetInt("unbowMod", -1);
		searchMod = PlayerPrefs.GetInt("searchMod", -1);
		removeMod = PlayerPrefs.GetInt("removeMod", -1);
		cloneMod = PlayerPrefs.GetInt("cloneMod", -1);
		menuMod = PlayerPrefs.GetInt("menuMod", -1);

		bowInput = PlayerPrefs.GetInt("bowInput", -1);
		dishInput = PlayerPrefs.GetInt("dishInput", -1);
		flipInput = PlayerPrefs.GetInt("flipInput", -1);
		highInput = PlayerPrefs.GetInt("highInput", -1);
		tokenInput = PlayerPrefs.GetInt("tokenInput", -1);
		deselInput = PlayerPrefs.GetInt("deselInput", -1);
		unbowInput = PlayerPrefs.GetInt("unbowInput", -1);
		searchInput = PlayerPrefs.GetInt("searchInput", -1);
		removeInput = PlayerPrefs.GetInt("removeInput", -1);
		cloneInput = PlayerPrefs.GetInt("cloneInput", -1);
		menuInput = PlayerPrefs.GetInt("menuInput", -1);

		moveDrag = PlayerPrefs.GetInt("moveInput", -1);
		panDrag = PlayerPrefs.GetInt("panInput", -1);
		targDrag = PlayerPrefs.GetInt("targInput", -1);

		prevInput = PlayerPrefs.GetInt("prevInput", -1);
	}

	void Update() {
		// set up keys for custom input layer
		if( inputInfo == null ) inputInfo = new InputInfo();
		inputInfo.Clear();
		if( inputInfo.hit.transform == null ) return;

		inputInfo.pointerOverUI = EventSystem.current.IsPointerOverGameObject(-1);

		#if UNITY_STANDALONE || UNITY_EDITOR
			// Modifiers
			if( Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift) ) CompareToCustomModifier( (int)CustomModifierKeyboard.Shift );
			#if UNITY_STANDALONE_OSX
			else if( Input.GetKey(KeyCode.LeftCommand) || Input.GetKey(KeyCode.RightCommand) ) CompareToCustomModifier( (int)CustomModifierKeyboard.Command );
			#endif
			else if( Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl) ) CompareToCustomModifier( (int)CustomModifierKeyboard.Control );
			else if( Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt) ) CompareToCustomModifier( (int)CustomModifierKeyboard.Alt );
			else CompareToCustomModifier( (int)CustomModifierKeyboard.None );

			// Mouse input
			if( Input.GetMouseButtonDown(0) ) {

				obj[0] = inputInfo.hit.collider.GetComponent<Clickable>();

				StartCoroutine( MultiClick(0) );
				if( !inputInfo.pointerOverUI ) StartCoroutine( MonitorForDrag(0, obj[0]) );
				if( clickCount == 1 ) CompareToCustomInput( (int)CustomInputMouse.Left_Click );
				else if( clickCount == 2 ) CompareToCustomInput( (int)CustomInputMouse.Double_Left_Click );
				else if( clickCount == 3 ) CompareToCustomInput( (int)CustomInputMouse.Triple_Left_Click );
				CompareToCustomInput( (int)CustomDragMouse.Left_Drag );

				if( !inputInfo.pointerOverUI ) {
					obj[0].ClickActionDown( inputInfo );
				}
			}
			else if( Input.GetMouseButtonDown(1) ) {

				obj[1] = inputInfo.hit.collider.GetComponent<Clickable>();

				StartCoroutine( MultiClick(1) );
				if( !inputInfo.pointerOverUI ) StartCoroutine( MonitorForDrag(1, obj[1]) );
				if( altClickCount == 1 ) CompareToCustomInput( (int)CustomInputMouse.Right_Click );
				else if( altClickCount == 2 ) CompareToCustomInput( (int)CustomInputMouse.Double_Right_Click );
				else if( altClickCount == 3 ) CompareToCustomInput( (int)CustomInputMouse.Triple_Right_Click );
				CompareToCustomInput( (int)CustomDragMouse.Right_Drag );

				if( !inputInfo.pointerOverUI ) obj[1].ClickActionDown( inputInfo );
			}
			else if( Input.GetMouseButtonDown(2) ) {

				obj[2] = inputInfo.hit.collider.GetComponent<Clickable>();

				StartCoroutine( MultiClick(2) );
				if( !inputInfo.pointerOverUI ) StartCoroutine( MonitorForDrag(2, obj[2]) );
				if( tertClickCount == 1 ) CompareToCustomInput( (int)CustomInputMouse.Middle_Click );
				else if( tertClickCount == 2 ) CompareToCustomInput( (int)CustomInputMouse.Double_Middle_Click );
				else if( tertClickCount == 3 ) CompareToCustomInput( (int)CustomInputMouse.Triple_Middle_Click );
				CompareToCustomInput( (int)CustomDragMouse.Middle_Drag );

				if( !inputInfo.pointerOverUI ) obj[2].ClickActionDown( inputInfo );
			}
			else if( Input.GetMouseButtonUp(0) ) {
				if( draggingButton == 0 ) {
					if( obj[0] ) obj[0].DragActionEnd( inputInfo );

					draggingButton = -1;
				}
				else {
					if( obj[0] == null ) obj[0] = inputInfo.hit.collider.GetComponent<Clickable>();

					if( clickCount == 1 ) CompareToCustomInput( (int)CustomInputMouse.Left_Click );
					else if( clickCount == 2 ) CompareToCustomInput( (int)CustomInputMouse.Double_Left_Click );
					else if( clickCount == 3 ) CompareToCustomInput( (int)CustomInputMouse.Triple_Left_Click );
					if( obj[0] && !inputInfo.pointerOverUI ) obj[0].ClickActionUp( inputInfo );
				}

				obj[0] = null;
			}
			else if( Input.GetMouseButtonUp(1) ) {
				if( draggingButton == 1 ) {
					if( obj[1] ) obj[1].DragActionEnd( inputInfo );

					draggingButton = -1;
				}
				else {
					if( obj[1] == null ) obj[1] = inputInfo.hit.collider.GetComponent<Clickable>();

					if( altClickCount == 1 ) CompareToCustomInput( (int)CustomInputMouse.Right_Click );
					else if( altClickCount == 2 ) CompareToCustomInput( (int)CustomInputMouse.Double_Right_Click );
					else if( altClickCount == 3 ) CompareToCustomInput( (int)CustomInputMouse.Triple_Right_Click );
					if( obj[1] && !inputInfo.pointerOverUI ) obj[1].ClickActionUp( inputInfo );
				}

				obj[1] = null;
			}
			else if( Input.GetMouseButtonUp(2) ) {
				if( draggingButton == 2 ) {
					if( obj[2] ) obj[2].DragActionEnd( inputInfo );

					draggingButton = -1;
				}
				else {
					if( obj[2] == null ) obj[2] = inputInfo.hit.collider.GetComponent<Clickable>();

					if( tertClickCount == 1 ) CompareToCustomInput( (int)CustomInputMouse.Middle_Click );
					else if( tertClickCount == 2 ) CompareToCustomInput( (int)CustomInputMouse.Double_Middle_Click );
					else if( tertClickCount == 3 ) CompareToCustomInput( (int)CustomInputMouse.Triple_Middle_Click );
					if( obj[2] && !inputInfo.pointerOverUI ) obj[2].ClickActionUp( inputInfo );
				}

				obj[2] = null;
			}

			MonitorForHover();
		#endif

		float zoom = 0f;
		if( Input.GetKey( KeyCode.DownArrow ) ) zoom = -0.1f;
		else if( Input.GetKey( KeyCode.UpArrow ) ) zoom = 0.1f;

		float scrollWheelMovement = Input.GetAxis( "Mouse ScrollWheel" );
		if( scrollWheelMovement != 0f ) zoom = scrollWheelMovement;

		if( zoom != 0f && !inputInfo.pointerOverUI ) cameraControl.Zoom( zoom );

		if( Input.GetButtonUp( "Pass Priority" ) && InputGateOpen() && !Table.data.SettingsOpen() && !Table.data.chatPanel.activeInHierarchy ) {

			Table.data.ToggleTurn();
			StartCoroutine( InputDelay() );
		}
	}

	void CompareToCustomModifier( int customMod ) {
		if( bowMod == customMod ) inputInfo.modifier = inputInfo.modifier | InputInfo.Action.Bow;
		if( dishMod == customMod ) inputInfo.modifier = inputInfo.modifier | InputInfo.Action.Dishonor;
		if( flipMod == customMod ) inputInfo.modifier = inputInfo.modifier | InputInfo.Action.Flip;
		if( highMod == customMod ) inputInfo.modifier = inputInfo.modifier | InputInfo.Action.Highlight;
		if( tokenMod == customMod ) inputInfo.modifier = inputInfo.modifier | InputInfo.Action.Token;
		if( prevMod == customMod ) inputInfo.modifier = inputInfo.modifier | InputInfo.Action.Preview;
		if( deselMod == customMod ) inputInfo.modifier = inputInfo.modifier | InputInfo.Action.Deselect;
		if( moveMod == customMod ) inputInfo.modifier = inputInfo.modifier | InputInfo.Action.Move;
		if( panMod == customMod ) inputInfo.modifier = inputInfo.modifier | InputInfo.Action.Pan;
		if( targMod == customMod ) inputInfo.modifier = inputInfo.modifier | InputInfo.Action.Target;
		if( unbowMod == customMod ) inputInfo.modifier = inputInfo.modifier | InputInfo.Action.UnbowAndFlipAll;
		if( searchMod == customMod ) inputInfo.modifier = inputInfo.modifier | InputInfo.Action.SearchDeck;
		if( removeMod == customMod ) inputInfo.modifier = inputInfo.modifier | InputInfo.Action.RemoveFromGame;
		if( cloneMod == customMod ) inputInfo.modifier = inputInfo.modifier | InputInfo.Action.CloneCard;
		if( menuMod == customMod ) inputInfo.modifier = inputInfo.modifier | InputInfo.Action.ShowMenu;
	}

	void CompareToCustomInput( int customInput ) {
		if( bowInput == customInput ) inputInfo.input = inputInfo.input | InputInfo.Action.Bow;
		if( dishInput == customInput ) inputInfo.input = inputInfo.input | InputInfo.Action.Dishonor;
		if( flipInput == customInput ) inputInfo.input = inputInfo.input | InputInfo.Action.Flip;
		if( highInput == customInput ) inputInfo.input = inputInfo.input | InputInfo.Action.Highlight;
		if( tokenInput == customInput ) inputInfo.input = inputInfo.input | InputInfo.Action.Token;
		if( prevInput == customInput ) inputInfo.input = inputInfo.input | InputInfo.Action.Preview;
		if( deselInput == customInput ) inputInfo.input = inputInfo.input | InputInfo.Action.Deselect;
		if( unbowInput == customInput ) inputInfo.input = inputInfo.input | InputInfo.Action.UnbowAndFlipAll;
		if( searchInput == customInput ) inputInfo.input = inputInfo.input | InputInfo.Action.SearchDeck;
		if( removeInput == customInput ) inputInfo.input = inputInfo.input | InputInfo.Action.RemoveFromGame;
		if( cloneInput == customInput ) inputInfo.input = inputInfo.input | InputInfo.Action.CloneCard;
		if( menuInput == customInput ) inputInfo.input = inputInfo.input | InputInfo.Action.ShowMenu;

		if( moveDrag == customInput ) inputInfo.input = inputInfo.input | InputInfo.Action.Move;
		if( panDrag == customInput ) inputInfo.input = inputInfo.input | InputInfo.Action.Pan;
		if( targDrag == customInput ) inputInfo.input = inputInfo.input | InputInfo.Action.Target;
	}

	IEnumerator MultiClick( int buttonNumber ) {
		if( buttonNumber == 0 ) clickCount++;
		if( buttonNumber == 1 ) altClickCount++;
		if( buttonNumber == 2 ) tertClickCount++;

		if( clickTimer == 0f ) {
			clickTimer = doubleClickSpeed;

			while( clickTimer > 0f ) {
				clickTimer -= Time.deltaTime;

				yield return null;
			}
			clickTimer = 0f;
			clickCount = 0;
			altClickCount = 0;
			tertClickCount = 0;
		}
	}

	IEnumerator MonitorForDrag( int buttonNumber, Clickable clickedObj ) {
		if( draggingButton > -1 ) yield break; // prevents calling this again when chording

		#if UNITY_STANDALONE || UNITY_EDITOR
		Vector3 origInputPos = inputInfo.inputPos;

		while( Input.GetMouseButton(buttonNumber) ) {
			Vector3 currentInputPos = inputInfo.inputPos;
			if( Mathf.Abs(currentInputPos.x - origInputPos.x) > dragGap || Mathf.Abs(currentInputPos.y - origInputPos.y) > dragGap ) {
				draggingButton = buttonNumber;
				if( Input.GetMouseButton(0) ) CompareToCustomInput( (int)CustomDragMouse.Left_Drag );
				if( Input.GetMouseButton(1) ) CompareToCustomInput( (int)CustomDragMouse.Right_Drag );
				if( Input.GetMouseButton(2) ) CompareToCustomInput( (int)CustomDragMouse.Middle_Drag );

				if( !inputInfo.pointerOverUI ) clickedObj.DragActionStart( inputInfo );

				break;
			}

			yield return null;
		}
		#endif
	}

	void MonitorForHover() {
		if( Input.GetMouseButton(0) ) CompareToCustomInput( (int)CustomHoverMouse.Left_Drag );
		else if( Input.GetMouseButton(1) ) CompareToCustomInput( (int)CustomHoverMouse.Right_Drag );
		else if( Input.GetMouseButton(2) ) CompareToCustomInput( (int)CustomHoverMouse.Middle_Drag );
		else CompareToCustomInput( (int)CustomHoverMouse.None );

		if( hoveringOver == null || inputInfo.hit.collider != hoveringOver.transform ) {
			if( inputInfo.hit.collider ) hoveringOver = inputInfo.hit.collider.GetComponent<Clickable>();
		}

		if( hoveringOver && !inputInfo.pointerOverUI ) hoveringOver.Hover( inputInfo );
	}

	// for objects that are "clicked" programmatically
	public void SetClickedObj( Clickable forcedObj ) {
		int objIndex = 0;
		if( Input.GetMouseButton(1) ) objIndex = 1;
		if( Input.GetMouseButton(2) ) objIndex = 2;
		obj[objIndex] = forcedObj;
	}

	public static bool CastAgainst( Collider againstCollider, out RaycastHit hit ) {
		return againstCollider.Raycast( Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100f );
	}

	public static bool CastAgainstAll( out RaycastHit hit ) {
		return Physics.Raycast( Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100f );
	}

	// Prevents pass priority from being repeatedly triggered
	IEnumerator InputDelay() {
		inputTimer = inputDelay;

		while( inputTimer > 0f ) {

			inputTimer -= Time.deltaTime;
			yield return null;
		}

		inputTimer = 0f;
	}

	bool InputGateOpen() {
		return inputTimer == 0f;
	}

	void OnDestroy() {
		InputPreferenceBox.inputPreferencesChanged -= LoadSettings;
	}
}

public class InputInfo {
	public enum Action{ 
		None = 1 << 0,
		Move = 1 << 1,
		Bow = 1 << 2, 
		Dishonor = 1 << 3, 
		Flip = 1 << 4, 
		Highlight = 1 << 5, 
		Token = 1 << 6, 
		Preview = 1 << 7, 
		Deselect = 1 << 8, 
		Pan = 1 << 9, 
		Target = 1 << 10,
		UnbowAndFlipAll = 1 << 11,
		SearchDeck = 1 << 12,
		RemoveFromGame = 1 << 13,
		CloneCard = 1 << 14,
		ShowMenu = 1 << 15,
		ShuffleDeck = 1 << 16,
	}
	public Action input = Action.None;
	public Action modifier = Action.None;

	public Vector3 inputPos = Vector3.zero;
	Vector3 lastPos = Vector3.zero;
	public bool pointerOverUI = false;

	public RaycastHit hit;

	public bool isSimulated = false; // used to distinguish between real input and fabricated input, i.e. for peeking at opponent's cards

	public InputInfo() {}

	public void Clear() {
		input = Action.None;
		modifier = Action.None;

		#if UNITY_STANDALONE || UNITY_EDITOR
		inputPos = Input.mousePosition;
		#endif

		pointerOverUI = false;

		if( lastPos != inputPos ) Physics.Raycast( Camera.main.ScreenPointToRay(inputPos), out hit, 100f );

		lastPos = inputPos;
	}

	public bool Matches( Action action ) {
		return (input & action) == action && (modifier & action) == action; // if input and modifier enum contains submitted action
	}
}
