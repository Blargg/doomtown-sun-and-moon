﻿using UnityEngine;
using System.Collections;

public class AnchorToScreen : MonoBehaviour {

	public enum ScreenAnchor { LowerLeft, LowerRight }
	public ScreenAnchor screenAnchor;

	public float xAnchor = 0f;
	public float yAnchor = 0f;

	public Camera cameraToAnchor;

	void Start () {

		float relativeX = 0f;
		float relativeY = 0f;

		switch( screenAnchor ) {
			case ScreenAnchor.LowerLeft:
				relativeX = xAnchor;
				relativeY = yAnchor;
			break;

			case ScreenAnchor.LowerRight:
				relativeX = Screen.width - xAnchor;
				relativeY = yAnchor;
			break;
		}
		Vector3 newPosition = cameraToAnchor.ScreenToWorldPoint( new Vector3(relativeX, relativeY, 1f) );

		transform.position = newPosition;
	}
}
