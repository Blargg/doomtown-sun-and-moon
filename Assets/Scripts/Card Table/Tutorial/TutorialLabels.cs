﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TutorialLabels : MonoBehaviour {

	public RectTransform cardDisplay;
	public RectTransform thisPanel;
	public Text typeLabel;
	public Text typeLabel2;
	public GameObject typePanel;
	public GameObject typePanel2;
	public GameObject namePanel;
	public GameObject forcePanel;
	public GameObject chiPanel;
	public GameObject PSPanel;
	public GameObject GPPanel;
	public GameObject SHPanel;
	public GameObject PHPanel;
	public GameObject HRPanel;
	public GameObject costPanel1;
	public GameObject costPanel2;
	public GameObject focusPanel;
	public GameObject keywordsPanel;
	public GameObject abilitiesPanel;

	Vector3 prevPos = Vector3.zero;

	bool toggled = false;

	public void Update() {
		if( cardDisplay.position != prevPos ) {
			thisPanel.position = cardDisplay.position;
			prevPos = cardDisplay.position;
		}

		if( Table.data.currentPreview != null ) {
			if( !toggled ) ResetLabels();
		}
		else if( toggled ) HideLabels();
	}

	public void HideLabels() {
		forcePanel.SetActive( false );
		chiPanel.SetActive( false );
		PSPanel.SetActive( false );
		GPPanel.SetActive( false );
		SHPanel.SetActive( false );
		PHPanel.SetActive( false );
		HRPanel.SetActive( false );
		costPanel1.SetActive( false );
		costPanel2.SetActive( false );
		focusPanel.SetActive( false );
		keywordsPanel.SetActive( false );
		abilitiesPanel.SetActive( false );
		typePanel.SetActive( false );
		typePanel2.SetActive( false );

		toggled = false;
	}
	
	public void ResetLabels() {

		if( Table.data.currentPreview == null || Table.data.currentPreview.card == null ) {
			return;
		}

		HideLabels();

		abilitiesPanel.SetActive( true );

		print("Card preview type is " + Table.data.currentPreview.card.type);

		string firstLetter = Table.data.currentPreview.card.type.Substring(0,1);
		string cardTypeText = firstLetter.ToUpper() + Table.data.currentPreview.card.type.Substring(1);
		typeLabel.text = cardTypeText;
		typeLabel2.text = cardTypeText;

		switch( Table.data.currentPreview.card.type ) {
			case "ring":
				focusPanel.SetActive( true );
				typePanel.SetActive( true );
			break;
			
			case "stronghold":
				GPPanel.SetActive( true );
				SHPanel.SetActive( true );
				PSPanel.SetActive( true );
				typePanel.SetActive( true );
			break;

			case "personality":
				forcePanel.SetActive( true );
				chiPanel.SetActive( true );
				PHPanel.SetActive( true );
				HRPanel.SetActive( true );
				costPanel2.SetActive( true );
				keywordsPanel.SetActive( true );
				typePanel2.SetActive( true );
			break;

			case "event":
				typePanel.SetActive( true );
			break;

			case "holding":
				costPanel1.SetActive( true );
				typePanel.SetActive( true );
			break;

			case "fortification":
				costPanel1.SetActive( true );
				typePanel.SetActive( true );
			break;

			case "strategy":
				focusPanel.SetActive( true );
				typePanel.SetActive( true );
			break;

			case "item":
				costPanel1.SetActive( true );
				focusPanel.SetActive( true );
				forcePanel.SetActive( true );
				chiPanel.SetActive( true );
				typePanel2.SetActive( true );
			break;

			case "follower":
				costPanel1.SetActive( true );
				focusPanel.SetActive( true );
				forcePanel.SetActive( true );
				chiPanel.SetActive( true );
				typePanel2.SetActive( true );
			break;

			case "spell":
				costPanel1.SetActive( true );
				focusPanel.SetActive( true );
				typePanel.SetActive( true );
			break;
		}

		toggled = true;
	}
}
