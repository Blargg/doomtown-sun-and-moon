﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using Vectrosity;

using System.Linq;

public class TutorialTip : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler {

	public RectTransform thisPanel;
	public Tutorial tutorial;

	public GameObject[] gameObjectsToTarget;
	public string[] gameObjectsToFind;
	public string[] targetTypesInProv;
	public string[] targetTypesInPlay;
	public string[] targetTypesInHand;
	public string[] targetVisible;
	public string targetCardsWithText;
	public string targetCardsWithText2;
	public bool targetRandomEnemyProv = false;
	public bool targetPersistentTarg = false;

	public Color lineColor;

	float lineWidth = 5f;
	int lineResolution = 100;

	List<GameObject> targets = new List<GameObject>();
	List<VectorLine> circles = new List<VectorLine>();
	List<VectorLine> lines = new List<VectorLine>();

	public UnfurlScroll scroll;

	bool toggled = false;
	Vector2 dragDelta = Vector2.zero;
	int dragMargin = 20;

	// transitions

	public List<TutorialTip> nextTips = new List<TutorialTip>();
	public bool alreadyDisplayed = false;
	public bool requireTargetsToPlay = false;

	// events
	public delegate void TipDelegate( TutorialTip tip );
	public event TipDelegate TipFinishedEvent;

	public bool preventDismiss = false;
	public bool repeatable = false;
	public bool setPersistentTarget = false;

	// conditions
	public GameObject proceedOnVisible;
	public GameObject proceedOnInvisible;
	public bool proceedOnProvincesFlipped = false;
	public string proceedOnPhase = "";
	public bool proceedOnBowed = false;
	public bool proceedOnTargetsEnteringDeck = false;
	public string proceedOnTypeInPlay = "";
	public bool proceedOnTargetsVisible = false;
	public bool proceedOnLineDrawn = false;
	public int proceedOnHonorGreaterThan = 0;
	public string proceedOnTookFavor = "";
	public int proceedOnCardCount = -1;

	public bool proceedOnDishonored = false;
	public bool proceedOnTokenCreated = false;
	public bool proceedOnExchangedControl = false;
	public Transform proceedOnHasChildren;

	// actions
	public string moveCard;
	public Vector3 moveCardTo;
	public Vector3 rotCardTo;
	public string drawLineFromPersToPersistent;

	// clicking
	float timeClicked = 0f;
	float clickInterval = 0.2f;

	public GameObject[] objectsToEnable;

	void Start() {
		scroll.Faded += FinishedFading;
		VectorLine.canvas.renderMode = RenderMode.ScreenSpaceOverlay;
		VectorLine.canvas.sortingOrder = 0;
	}

	public void NewTarget( GameObject specificTarget = null ) {

		if( specificTarget == null ) return;

		VectorLine circle = GenerateCircleAround( specificTarget );
		circles.Add( circle );
		circle.Draw();

		VectorLine line = new VectorLine("Line", new Vector2[] { thisPanel.anchoredPosition, ClosestPointFromTo(thisPanel.anchoredPosition, circle) }.ToList(), null, lineWidth, LineType.Continuous, Joins.Fill);
		lines.Add( line );
		line.Draw();

		targets.Add( specificTarget );

		Table.data.CameraMovedEvent += Redraw;
	}

	public VectorLine GenerateCircleAround( GameObject target ) {

		print("Generating circle for " + target.name);

		Renderer tutTarget = target.GetComponent<Renderer>();
		if( tutTarget == null ) {
			CardDisplay tutCard = target.GetComponent<CardDisplay>();
			if( tutCard ) tutTarget = tutCard.myFront;
		}

		Camera cam = Camera.main;
		print("Main camera is " + cam.name);

		Vector3 lineCenter = cam.WorldToScreenPoint(tutTarget.bounds.center);
		print("Line center is located at " + lineCenter);

		bool leftSideScreen = lineCenter.x < Screen.width / 2f;
		Vector3 maxOnScreen = leftSideScreen ? 
			cam.WorldToScreenPoint(tutTarget.bounds.center + new Vector3(tutTarget.bounds.extents.x, tutTarget.bounds.extents.y, tutTarget.bounds.extents.z)) :
			cam.WorldToScreenPoint(tutTarget.bounds.center + new Vector3(-tutTarget.bounds.extents.x, tutTarget.bounds.extents.y, tutTarget.bounds.extents.z));

		Vector3 minOnScreen = leftSideScreen ? 
			cam.WorldToScreenPoint(tutTarget.bounds.center - new Vector3(tutTarget.bounds.extents.x, tutTarget.bounds.extents.y, tutTarget.bounds.extents.z)) :
			cam.WorldToScreenPoint(tutTarget.bounds.center - new Vector3(-tutTarget.bounds.extents.x, tutTarget.bounds.extents.y, tutTarget.bounds.extents.z));

		Vector2 sizeOnScreen = new Vector2( maxOnScreen.x - minOnScreen.x, maxOnScreen.y - minOnScreen.y );

		print("Size on screen is " + sizeOnScreen);

		float diameterX = sizeOnScreen.x / 2f;
		float diameterY = sizeOnScreen.y / 2f;		

		VectorLine circle = new VectorLine("Circle", new Vector2[lineResolution].ToList(), null, lineWidth, LineType.Continuous, Joins.Fill);
		circle.MakeEllipse(new Vector2(lineCenter.x, lineCenter.y), diameterX, diameterY);

		return circle;
	}

	public Vector2 ClosestPointFromTo( Vector2 otherPoint, VectorLine line ) {
		int closestSegEfficiency = 5; // increase to increase efficiency and decrease accuracy

		// find closest segment on circle
		float distToClosestSeg = 9999999999f;
		Vector2 closestSeg = Vector2.zero;
		for( int index = 0; index < line.points2.Count; index += closestSegEfficiency ) {
			float distToThisSeg = Vector2.Distance(line.points2[index], otherPoint);
			if( distToThisSeg < distToClosestSeg ) {
				distToClosestSeg = distToThisSeg;
				closestSeg = line.points2[index];
			}
		}

		return closestSeg;
	}

	public void Redraw() {
		for( int index = 0; index < circles.Count; index++ ) {
			ClearGuide( index );

			circles[index] = GenerateCircleAround( targets[index] );
			circles[index].Draw();

			lines[index] = new VectorLine(lines[index].name, new Vector2[] { thisPanel.anchoredPosition, ClosestPointFromTo(thisPanel.anchoredPosition, circles[index]) }.ToList(), null, lineWidth, LineType.Continuous, Joins.Fill);
			lines[index].Draw();
		}
	}

	public void BuildIn() {
		thisPanel.gameObject.SetActive( true );
		thisPanel.anchoredPosition = tutorial.tipPosition;

		for( int index = 0; index < objectsToEnable.Length; index++ ) {
			objectsToEnable[index].SetActive( true );
		}

		for( int index = 0; index < gameObjectsToFind.Length; index++ ) {
			GameObject foundObject = GameObject.Find( gameObjectsToFind[index] );

			if( foundObject != null ) {
				NewTarget( foundObject );
				if( setPersistentTarget ) tutorial.persistentTarget = foundObject;
			}
		}

		for( int index = 0; index < gameObjectsToTarget.Length; index++ ) {
			NewTarget( gameObjectsToTarget[index] );
			if( setPersistentTarget ) tutorial.persistentTarget = gameObjectsToTarget[index];
		}

		for( int index = 0; index < targetTypesInProv.Length; index++ ) {
			foreach( CardDisplay card in Table.data.allCards ) {
				if( card.photonView.isMine && card.faceUp && card.inProvince && card.card.type == targetTypesInProv[index] ) {
					NewTarget( card.gameObject );
				}
			}
		}

		for( int index = 0; index < targetTypesInPlay.Length; index++ ) {
			foreach( CardDisplay card in Table.data.allCards ) {
				if( card.IsMine() && card.faceUp && card.inPlay && card.card != null && card.card.type == targetTypesInPlay[index] ) {
					NewTarget( card.gameObject );
				}
			}
		}

		for( int index = 0; index < targetTypesInHand.Length; index++ ) {
			foreach( CardDisplay card in Table.data.allCards ) {
				if( card.photonView.isMine && card.inHand && card.card.type == targetTypesInHand[index] ) {
					NewTarget( card.gameObject );
				}
			}
		}

		for( int index = 0; index < targetVisible.Length; index++ ) {
			foreach( CardDisplay card in Table.data.allCards ) {
				if( card.myFront.isVisible && card.gameObject.name == targetVisible[index] ) {
					NewTarget( card.gameObject );
					if( setPersistentTarget ) tutorial.persistentTarget = card.gameObject;
				}
			}
		}

		if( targetCardsWithText != "" ) {
			foreach( CardDisplay card in Table.data.allCards ) {
				if( card.photonView.ownerId == PhotonNetwork.player.ID && card.faceUp && !card.inProvince && card.card != null && (card.card.text.Contains(targetCardsWithText)) ) {
					NewTarget( card.gameObject );
					if( setPersistentTarget ) tutorial.persistentTarget = card.gameObject;
				}
			}
		}

		if( targetCardsWithText2 != "" ) {
			foreach( CardDisplay card in Table.data.allCards ) {
				if( card.photonView.ownerId == PhotonNetwork.player.ID && card.faceUp && !card.inProvince && card.card != null && (card.card.text.Contains(targetCardsWithText2)) ) {
					NewTarget( card.gameObject );
					if( setPersistentTarget ) tutorial.persistentTarget = card.gameObject;
				}
			}
		}

		if( targetRandomEnemyProv ) {
			foreach( CardDisplay card in Table.data.allCards ) {
				if( card.photonView.isSceneView && card.inProvince ) {
					NewTarget( card.gameObject );
					if( setPersistentTarget ) tutorial.persistentTarget = card.gameObject;
					break;
				}
			}
		}

		if( targetPersistentTarg ) {
			foreach( CardDisplay card in Table.data.allCards ) {
				if( card.gameObject == tutorial.persistentTarget ) {
					NewTarget( card.gameObject );
					break;
				}
			}
		}

		if( requireTargetsToPlay ) {
			if( targets.Count == 0 ) {
				thisPanel.gameObject.SetActive( false );
				Dismiss();
				alreadyDisplayed = true;
				TipFinishedEvent( this );

				return;
			}
		}

		if( moveCard != "" ) {
			GameObject objToMove = GameObject.Find( moveCard );

			CardDisplay cardToMove = objToMove.GetComponent<CardDisplay>();

			cardToMove.photonView.RPC("Show", PhotonTargets.All);
			cardToMove.photonView.RPC("MoveTo", PhotonTargets.All, moveCardTo, 0.5f);
			cardToMove.photonView.RPC("RotateTo", PhotonTargets.All, Quaternion.Euler(rotCardTo), 0.5f);
			cardToMove.inPlay = true;
			cardToMove.inDeck = false;
			cardToMove.faceUp = true;

			if( setPersistentTarget ) tutorial.persistentTarget = objToMove;
		}

		if( drawLineFromPersToPersistent != "" ) {
			foreach( CardDisplay card in Table.data.allCards ) {
				if( card.gameObject.name == drawLineFromPersToPersistent && card.inPlay == true ) {
					CardDisplay persistentCard = tutorial.persistentTarget.GetComponent<CardDisplay>();
					card.photonView.RPC("AttachGuideTo", PhotonTargets.All, persistentCard.photonView.viewID, tutorial.tutDeck.clan, Vector3.zero );
					print("Attaching guide from " + card.gameObject.name + " to " + persistentCard.gameObject.name);
				}
			}
		}

		scroll.UnfurlNow();
	}

	public void OnBeginDrag(PointerEventData evt) {
		evt.Use();
	}

	public void OnPointerDown(PointerEventData evt) {
		Vector2 screenPos = thisPanel.anchoredPosition;
		dragDelta = screenPos - evt.position;

		if( Time.time - timeClicked < clickInterval ) {
			if( toggled && !preventDismiss ) {
				Dismiss();
			}
		}
		timeClicked = Time.time;

		evt.Use();
	}

	public void OnDrag(PointerEventData evt) {
		if( evt.position.x < Screen.width - dragMargin && evt.position.x > dragMargin && evt.position.y < Screen.height - dragMargin && evt.position.y > dragMargin ) {
			print("Drag delta is " + dragDelta);
			thisPanel.anchoredPosition = evt.position + dragDelta;
			Redraw();

			Persistent.data.FrameRateUpFor( 1f );
		}

		evt.Use();
	}

	public void OnPointerUp(PointerEventData evt) {
		print("Dragging finished : changing " + tutorial.tipPosition + " to " + thisPanel.anchoredPosition + " " + Time.time);

		tutorial.tipPosition = thisPanel.anchoredPosition;

		evt.Use();
	}

	public void OnEndDrag(PointerEventData evt) {
		evt.Use();
	}

	public void Dismiss() {
		if( thisPanel.gameObject.activeInHierarchy ) {
			scroll.FurlNow();
		}

		for( int atIndex = 0; atIndex < circles.Count; atIndex++ ) {
			ClearGuide(atIndex);
		}

		circles.Clear();
		lines.Clear();
		targets.Clear();

		Table.data.CameraMovedEvent -= Redraw;
	}

	public void FinishedFading( float toAlpha )
	{
		toggled = toAlpha == 0f ? false : true;

		if( toggled == false ) {
			alreadyDisplayed = true;

			TipFinishedEvent( this );
		}
		else {
			WaitForConditions();
		}
	}

	public void ClearGuide( int atIndex ) {
		VectorLine circleToDestroy = circles[atIndex];
		VectorLine lineToDestroy = lines[atIndex];

		VectorLine.Destroy(ref circleToDestroy);
		VectorLine.Destroy(ref lineToDestroy);
	}

	public void ClearGuide( GameObject toTarget ) {
		for( int index = 0; index < targets.Count; index++ ) {
			if( targets[index] == toTarget ) {
				ClearGuide(index);

				targets.RemoveAt( index );
				circles.RemoveAt( index );
				lines.RemoveAt( index );
				break;
			}
		}
	}

	// conditions

	public void WaitForConditions() {
		if( proceedOnVisible ) StartCoroutine( ProceedOnVisible(proceedOnVisible) );
		if( proceedOnProvincesFlipped ) StartCoroutine( ProceedOnProvincesFlipped() );
		if( proceedOnPhase != "" ) StartCoroutine( ProceedOnPhase() );
		if( proceedOnBowed ) StartCoroutine( ProceedOnBowed() );
		if( proceedOnTargetsEnteringDeck ) StartCoroutine( WaitForTargetsToEnterDeck() );
		if( proceedOnTypeInPlay != "" ) StartCoroutine( ProceedOnTypeInPlay() );
		if( proceedOnTargetsVisible ) StartCoroutine( ProceedOnTargetsVisible() );
		if( proceedOnLineDrawn ) StartCoroutine( ProceedOnLineDrawn() );
		if( proceedOnHonorGreaterThan > 0 ) StartCoroutine( ProceedOnHonorGreaterThan() );
		if( proceedOnTookFavor != "" ) StartCoroutine( ProceedOnTookFavor() );
		if( proceedOnCardCount > -1 ) StartCoroutine( ProceedOnCardCount() );
		if( proceedOnInvisible ) StartCoroutine( ProceedOnInvisible(proceedOnInvisible) );
		if( proceedOnTokenCreated ) StartCoroutine( ProceedOnTokenCreated() );
		if( proceedOnDishonored ) StartCoroutine( ProceedOnDishonored() );
		if( proceedOnExchangedControl ) StartCoroutine( ProceedOnExchangedControl() );
		if( proceedOnHasChildren ) StartCoroutine( ProceedOnHasChildren() );
	}

	IEnumerator ProceedOnHasChildren() {
		while( proceedOnHasChildren.childCount == 0 ) yield return new WaitForSeconds(0.5f);

		Dismiss();
	}

	IEnumerator ProceedOnExchangedControl() {
		CardDisplay exchanged = null;
		while( exchanged == null ) {
			yield return new WaitForSeconds(0.5f);

			foreach( CardDisplay card in Table.data.allCards ) {
				if( card.photonView.ownerId == PhotonNetwork.player.ID && card.transform.position.z > Table.data.distanceFromDivider ) {
					exchanged = card;
					break;
				}
			}
		}

		Dismiss();
	}

	IEnumerator ProceedOnDishonored() {
		CardDisplay dishonored = null;
		while( dishonored == null ) {
			yield return new WaitForSeconds(0.5f);

			foreach( CardDisplay card in Table.data.allCards ) {
				if( card.dishonored ) {
					dishonored = card;
					break;
				}
			}
		}

		Dismiss();
	}

	IEnumerator ProceedOnTokenCreated() {
		CardDisplay token = null;
		while( token == null ) {
			yield return new WaitForSeconds(0.5f);

			foreach( CardDisplay card in Table.data.allCards ) {
				if( card.card != null && card.card.isToken ) {
					token = card;
					break;
				}
			}
		}

		Dismiss();
	}

	IEnumerator ProceedOnCardCount() {
		while( Table.data.myHand.cards.Count > proceedOnCardCount ) {
			yield return new WaitForSeconds( 0.5f );
		}

		Dismiss();
	}

	IEnumerator ProceedOnTookFavor() {
		Favor favor = targets[0].GetComponent<Favor>();

		if( proceedOnTookFavor == "mine" ) proceedOnTookFavor = Table.data.myClan;

		while( favor.displayingClan != Persistent.data.ClanStringToEnum(proceedOnTookFavor) ) {
			yield return new WaitForSeconds( 0.5f );
		}

		Dismiss();
	}

	IEnumerator ProceedOnHonorGreaterThan() {

		print( "Current honor is " + int.Parse(Table.data.myHonor.text));

		while( proceedOnHonorGreaterThan > int.Parse(Table.data.myHonor.text) ) {
			print( int.Parse(Table.data.myHonor.text));
			yield return new WaitForSeconds( 1f );
		}

		yield return new WaitForSeconds( 1f );

		Dismiss();
	}

	IEnumerator ProceedOnLineDrawn() {
		bool foundGuideLine = false;
		while( !foundGuideLine ) {
			foreach( CardDisplay card in Table.data.allCards ) {
				if( card.attachedGuides.Count > 0 ) {
					foundGuideLine = true;
					if( setPersistentTarget ) tutorial.persistentTarget = card.gameObject;
				}
			}

			yield return new WaitForSeconds( 2f );
		}

		Dismiss();
	}

	IEnumerator ProceedOnTargetsVisible() {
		while( true ) {
			bool targetsVisible = true;

			foreach( GameObject target in targets ) {
				if( target.GetComponent<Renderer>().enabled == false ) {
					targetsVisible = false;
					print("Renderer target is not enabled " + Time.time + " " + gameObject.name);
				}
			}

			if( targetsVisible ) break;

			yield return new WaitForSeconds( 1f );
		}

		Dismiss();
	}

	IEnumerator ProceedOnVisible( GameObject controlToBeRevealed ) {
		while( controlToBeRevealed.activeInHierarchy == true ) yield return new WaitForSeconds(0.5f);
		while( controlToBeRevealed.activeInHierarchy == false ) yield return new WaitForSeconds(0.5f);

		Dismiss();
	}

	IEnumerator ProceedOnInvisible( GameObject controlToBeHidden ) {

		while( controlToBeHidden.activeInHierarchy == false ) yield return new WaitForSeconds(0.5f);
		while( controlToBeHidden.activeInHierarchy == true ) yield return new WaitForSeconds(0.5f);

		Dismiss();
	}

	IEnumerator ProceedOnProvincesFlipped() {
		int provinceCount = 0;
		foreach( CardDisplay card in Table.data.allCards ) {
			if( card.photonView.isMine && card.faceUp && card.inProvince ) provinceCount++;
		}

		if( provinceCount == 4 ) {
			Dismiss();
		}
		else {
			yield return new WaitForSeconds(1f);
			StartCoroutine( ProceedOnProvincesFlipped() );
		}
	}

	IEnumerator WaitForTargetsToEnterDeck() {
		List<CardDisplay> targetedCards = new List<CardDisplay>();

		// convert targets to card displays
		foreach( GameObject target in targets ) {
			CardDisplay targetCard = target.GetComponent<CardDisplay>();

			if( targetCard != null ) targetedCards.Add( targetCard );
		}

		while( targetedCards.Count > 0 ) {
			CardDisplay[] cards = targetedCards.ToArray();

			foreach( CardDisplay targetedCard in cards ) {
				if( targetedCard.inDeck ) {
					targetedCards.Remove( targetedCard );
					ClearGuide( targetedCard.gameObject );
					break;
				}
			}

			yield return null;
		}

		StartCoroutine( ProceedOnProvincesFlipped() );
	}

	IEnumerator ProceedOnTypeInPlay() {
		CardDisplay foundCard = null;

		while( foundCard == null ) {
			yield return new WaitForSeconds(1f);

			foreach( CardDisplay card in Table.data.allCards ) {
				if( card.IsMine() && card.inPlay && card.card.type == proceedOnTypeInPlay ) {
					foundCard = card;
					break;
				}
			}
		}

		Dismiss();
	}

	IEnumerator ProceedOnPhase() {

		while( Table.data.currentPhase.ToString() == proceedOnPhase ) yield return null; // proceed on *next* phase, not if we are already there

		while( Table.data.currentPhase.ToString() != proceedOnPhase ) yield return new WaitForSeconds(1f);

		Dismiss();
	}

	IEnumerator ProceedOnBowed() {
		print("Waiting for card to bow");
		CardDisplay persistentCard = null;
		if( tutorial.persistentTarget ) persistentCard = tutorial.persistentTarget.GetComponent<CardDisplay>();

		if( persistentCard != null ) {
			print("Found pers card");
			while( persistentCard.bowed == false ) yield return null;
			print("Pers card bowed");
		}
		else print("No persistent card found");

		Dismiss();
	}

}