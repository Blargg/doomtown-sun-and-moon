﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Tutorial : MonoBehaviour {

	public TutorialDeck tutDeck;
	public Image tutHonorFan;
	public Text tutHonor;
	public Camera canvasCamera;

	public SunAndMoon tutTurnIndicator;
	public Favor imperialFavor;

	string myClan = "";
	int myStartingHonor = 0;
	Hand myHand;

	CardDisplay stronghold;
	CardDisplay secondStronghold;
	DeckDisplay myDynasty;
	DeckDisplay myFate;
	DeckDisplay myDynDiscard;
	DeckDisplay myFateDiscard;

	public List<TutorialTip> tutPanels = new List<TutorialTip>();

	// preset tutorial objects
	public GameObject provinces;

	public int startAtTutorialStep = 0;

	// for tutorial tips referencing cards across sessions
	public GameObject persistentTarget;

	public Vector2 tipPosition = Vector2.zero;

	// Use this for initialization
	IEnumerator Start () {

		if( canvasCamera ) Vectrosity.VectorLine.SetCanvasCamera( canvasCamera );
		Vectrosity.VectorLine.canvas.planeDistance = 2f;

		while( Persistent.data.loading ) yield return null;

		if( tutDeck.cardList.Count == 0 ) yield return StartCoroutine( tutDeck.GenerateCards() );

		SetTable();
	}
	
	void SetTable() {
		// set up player scene elements
		myHand = PhotonNetwork.InstantiateSceneObject( "Hand", new Vector3(0,1f,-Table.data.distanceFromDivider - Table.data.enemyHandDistFromFief), Quaternion.identity, 0, null ).GetComponent<Hand>();
		myHand.photonView.RPC("Initialize", PhotonTargets.All, false);
		myHand.transform.RotateAround(Vector3.zero, Vector3.up, 180f);

		Vector3 dynastyLocation = new Vector3( -Table.data.distanceBetweenDecks / 2f, 0.05f, -Table.data.distanceFromDivider );
		DeckDisplay dynasty = PhotonNetwork.InstantiateSceneObject( "Deck", dynastyLocation, Quaternion.identity, 0, null ).GetComponent<DeckDisplay>();
		dynasty.photonView.RPC( "Initialize", PhotonTargets.All, 1 );
		myDynasty = dynasty;
		myDynasty.gameObject.name = "Opponent Dynasty";
		myDynasty.transform.RotateAround(Vector3.zero, Vector3.up, 180f);

		Vector3 dynDiscardLocation = dynastyLocation - new Vector3(Table.data.distanceToDiscard, 0f, 0f);
		DeckDisplay dynDiscard = PhotonNetwork.InstantiateSceneObject( "Deck", dynDiscardLocation, Quaternion.identity, 0, null ).GetComponent<DeckDisplay>();
		dynDiscard.photonView.RPC( "Initialize", PhotonTargets.All, 3 );
		myDynDiscard = dynDiscard;
		myDynDiscard.gameObject.name = "Opponent Dynasty Discard";
		myDynDiscard.transform.RotateAround(Vector3.zero, Vector3.up, 180f);

		print("Initialized dynasty and discard");

		Vector3 fateLocation = new Vector3( Table.data.distanceBetweenDecks / 2f, 0.05f, -Table.data.distanceFromDivider );
		DeckDisplay fate = PhotonNetwork.InstantiateSceneObject( "Deck", fateLocation, Quaternion.identity, 0, null ).GetComponent<DeckDisplay>();
		fate.photonView.RPC( "Initialize", PhotonTargets.All, 0 );
		myFate = fate;
		myFate.gameObject.name = "Opponent Fate";
		myFate.transform.RotateAround(Vector3.zero, Vector3.up, 180f);

		Vector3 fateDiscardLocation = fateLocation + new Vector3(Table.data.distanceToDiscard, 0.05f, 0f);
		DeckDisplay fateDiscard = PhotonNetwork.InstantiateSceneObject( "Deck", fateDiscardLocation, Quaternion.identity, 0, null ).GetComponent<DeckDisplay>();
		fateDiscard.photonView.RPC( "Initialize", PhotonTargets.All, 2 );
		myFateDiscard = fateDiscard;
		myFateDiscard.gameObject.name = "Opponent Fate Discard";
		myFateDiscard.transform.RotateAround(Vector3.zero, Vector3.up, 180f);

		print("Initialized fate and discard");

		Vector3 strongholdLocation = dynastyLocation + new Vector3( 0f, 0f, Table.data.strongholdDistFromDyn );
		stronghold = PhotonNetwork.InstantiateSceneObject( "Card", strongholdLocation, Quaternion.identity, 0, null ).GetComponent<CardDisplay>();
		secondStronghold = PhotonNetwork.InstantiateSceneObject( "Card", strongholdLocation, Quaternion.identity, 0, null ).GetComponent<CardDisplay>();

		print("Initialized stronghold");

		myClan = tutDeck.clan;

		List<Card> myCards = tutDeck.cardList;
		print("Tutorial prepping " + myCards.Count + " cards");

		for( int index = 0; index < myCards.Count; index++ ) {

			string cardID = myCards[index].id;

			if( myCards[index].type == "stronghold" ) {

				print("Identified stronghold");

				stronghold.photonView.RPC("LoadCard", PhotonTargets.All, cardID, "", myCards[index].edition );
				myStartingHonor += myCards[index].starting_honor;

				if( stronghold.card.linkedCard != null ) {
					secondStronghold.photonView.RPC("LoadCard", PhotonTargets.All, stronghold.card.linkedCard.id, "", myCards[index].edition );
					stronghold.card.secondSide = secondStronghold.card.images[0];
					secondStronghold.card.secondSide = stronghold.card.images[0];
				}

				stronghold.photonView.RPC("ForceFaceUp", PhotonTargets.All, true);
				secondStronghold.photonView.RPC("ForceFaceUp", PhotonTargets.All, true);
				StartCoroutine( stronghold.DownloadImage( true ) );
				StartCoroutine( secondStronghold.DownloadImage( true ) );

				stronghold.transform.RotateAround( Vector3.zero, Vector3.up, 180f );
				secondStronghold.transform.RotateAround( Vector3.zero, Vector3.up, 180f );

				continue;
			}

			GameObject displayObject = PhotonNetwork.Instantiate( "Card", Vector3.zero, Quaternion.identity, 0 );
			CardDisplay display = displayObject.GetComponent<CardDisplay>();
			display.photonView.RPC( "LoadCard", PhotonTargets.All, cardID, "", myCards[index].edition );

			if( myCards[index].deckType == Card.DeckType.Fate ) {
				print("Adding tutorial card to fate deck instantly");
				fate.photonView.RPC( "AddCardToDeckInstantly", PhotonTargets.All, display.photonView.viewID, 0 );
			}
			else {
				print("Adding tutorial card to dynasty deck instantly");
				dynasty.photonView.RPC( "AddCardToDeckInstantly", PhotonTargets.All, display.photonView.viewID, 0 );
			}

			display.locked = true;
		}

		stronghold.photonView.RPC("ForceFaceUp", PhotonTargets.All, true);

		dynasty.photonView.RPC( "Shuffle", PhotonTargets.All, System.Environment.TickCount );
		fate.photonView.RPC( "Shuffle", PhotonTargets.All, System.Environment.TickCount );

		// lay out first four provinces
		StartCoroutine( FillTutorialProvinces() );

		Color[] clanColors = Persistent.data.GetColorsForClan(tutDeck.clan);
		tutHonor.color = clanColors[0];
		tutHonorFan.color = clanColors[2];

		tutHonor.text = myStartingHonor.ToString();
		tutHonorFan.gameObject.SetActive( true );

		// configure sun and moon
		tutTurnIndicator.gameObject.SetActive(true);

		Table.data.photonView.RPC("SetOppClanHonor", PhotonTargets.Others, myClan, 0, myStartingHonor);

		imperialFavor.photonView.RPC("RegisterPlayer", PhotonTargets.All, myDynasty.photonView.viewID, (int)Persistent.data.ClanStringToEnum(myClan));
		imperialFavor.Show();

		Table.data.photonView.RPC( "RemoveFromGame", PhotonTargets.All, stronghold.photonView.viewID );
		PhotonNetwork.Destroy( stronghold.photonView );

		stronghold = secondStronghold;

		tipPosition = new Vector2( Screen.width / 2f, Screen.height / 1.5f ); // starting tip position
		tutPanels[startAtTutorialStep].gameObject.SetActive( true );
		tutPanels[startAtTutorialStep].BuildIn();
		tutPanels[startAtTutorialStep].TipFinishedEvent += TutorialTipFinished;
		tipPosition = tutPanels[startAtTutorialStep].thisPanel.anchoredPosition;
	}

	IEnumerator FillTutorialProvinces() {
		float provDist = Table.data.distanceBetweenDecks / 5f;

		for( int index = 0; index < 4; index++ ) {
			CardDisplay cardToFill = myDynasty.cards[myDynasty.cards.Count - 1];
			myDynasty.photonView.RPC("RemoveCardFromDeck", PhotonTargets.All, cardToFill.photonView.viewID);
			cardToFill.photonView.RPC("MoveTo", PhotonTargets.All, myDynasty.transform.position - new Vector3(provDist * (index+1), 0f, 0f), 0.5f);
			cardToFill.transform.rotation = Quaternion.Euler( cardToFill.transform.eulerAngles.x, cardToFill.MyYAngle(), cardToFill.transform.eulerAngles.z );
			cardToFill.inProvince = true;

			yield return new WaitForSeconds(0.5f);
		}
	}

	public void TutorialTipFinished( TutorialTip finishedTip ) {

		print("Tip finished fading out! " + Time.time);

		finishedTip.TipFinishedEvent -= TutorialTipFinished;

		for( int index = 0; index < finishedTip.nextTips.Count; index++ ) {

			if( finishedTip.nextTips[index].alreadyDisplayed == true && finishedTip.nextTips[index].repeatable == false ) continue;

			finishedTip.nextTips[index].TipFinishedEvent += TutorialTipFinished;
			finishedTip.nextTips[index].BuildIn();

			return;
		}

		// if no next tip indicated
		for( int index = startAtTutorialStep; index < tutPanels.Count; index++ ) {
			if( tutPanels[index].alreadyDisplayed ) continue;

			tutPanels[index].gameObject.SetActive( true );
			tutPanels[index].BuildIn();
			tutPanels[index].TipFinishedEvent += TutorialTipFinished;
			return;
		}

		print("Tutorial complete");

		PhotonNetwork.LoadLevel("Main Menu");
	}
}
