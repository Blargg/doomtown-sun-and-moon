﻿using UnityEngine;
using System.Collections;

public class TutTextAnchor : MonoBehaviour {

	public string link = "";

	public void LoadLink() {
		Application.OpenURL(link);
	}
}