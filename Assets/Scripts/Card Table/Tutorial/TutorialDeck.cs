﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using System.IO;

public class TutorialDeck : Deck {

	public TextAsset tutorialDeck;

	public override IEnumerator GenerateCards() {
		if( cardList != null && cardList.Count > 0 ) yield break;

		cardList = new List<Card>();
		string deckSlush = tutorialDeck.text.Replace( "\r\n", "\n" );

		string[] splitSlush = deckSlush.Split('\n');

		for( int index = 0; index < splitSlush.Length; index++ ) {
			Card cardToAdd = null;
			string cardLine = splitSlush[index].ReplaceCommonTerms();

			cardToAdd = Persistent.data.VerifyTextAgainstDatabase( cardLine );

			if( cardToAdd != null ) {
				int quantity = Persistent.data.GetCardQuantityInt( cardLine );

				for( int count = 0; count < quantity; count++ ) {
					cardList.Add( cardToAdd );
				}

				if( cardToAdd.type == "stronghold" ) {
					clan = cardToAdd.clans[0];
				}
			}
		}
	}
}
