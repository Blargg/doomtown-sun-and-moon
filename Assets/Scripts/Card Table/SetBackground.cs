﻿using UnityEngine;
using System.Collections;
using System.IO;

public class SetBackground : MonoBehaviour {

	public IEnumerator Start () {

		string customBackground = PlayerPrefs.GetString("background", Persistent.data.rootURL + Persistent.data.pathToBackgrounds.SanitizePath() + "/default.jpg");

		WWW selectedBackground = new WWW( customBackground );

		yield return selectedBackground;

		// mipmap background
		if( string.IsNullOrEmpty( selectedBackground.error ) ) {
			Renderer rend = GetComponent<Renderer>();

			Texture2D backImage;
			if( PlayerPrefs.GetInt("mipmap", 0) == 1 ) {
				Texture2D fullTexture = selectedBackground.texture;
				backImage = new Texture2D(fullTexture.width, fullTexture.height);
				backImage.SetPixels(fullTexture.GetPixels());
				backImage.Apply(true);
			}
			else {
				backImage = selectedBackground.textureNonReadable;
			}
			rend.material.mainTexture = backImage;
			rend.material.color = Color.white;

			SetTiling( PlayerPrefs.GetInt("tile", 10) );
		}
		else print("Error: " + selectedBackground.error);
	}

	public void SetTiling( int tiling ) {
		GetComponent<Renderer>().material.mainTextureScale = new Vector2( tiling, tiling );
	}

}
