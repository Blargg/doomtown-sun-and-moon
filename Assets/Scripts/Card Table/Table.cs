using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Vectrosity;
using Newtonsoft.Json;
using UnityEngine.SceneManagement;

using System.Text;
using SevenZip.Compression.LZMA;
using System.Linq;

using System.IO;

using ExitGames.Client.Photon;
using PhotonHash = ExitGames.Client.Photon.Hashtable;

public class Table : Clickable {

	public float distanceFromDivider = 13f;
	[HideInInspector] public float distanceBetweenDecks = 15f;
	[HideInInspector] public float distanceToDiscard = 3f;
	float xDistanceBetweenProxies = 3f;
	float zDistanceBetweenProxies = 4f;
	float cameraDistFromDiv = 6.5f;
	[HideInInspector] public float cameraHeight = 13f;
	[HideInInspector] public float strongholdDistFromDyn = 3.5f;
	[HideInInspector] public float holdingsRightOfStrong = 3f;
	[HideInInspector] public float senseiLeftOfStrong = 3f;
	[HideInInspector] public float enemyHandDistFromFief = 20f;
	[HideInInspector] public float fiefSize = 4f;
	[HideInInspector] public float regionHedge = 0.3f;

	public static Table data;
	public List<CardDisplay> allCards;
	public List<DeckDisplay> allDecks;

	public Hand myHand;
	public DeckDisplay myFate;
	public DeckDisplay myFateDiscard;
	public DeckDisplay myDynasty;
	public DeckDisplay myDynDiscard;
	public string myClan = "unaligned";
	public Vector3 myForwardDir = Vector3.forward;
	public Vector3 myHandLocalPos;

	// Preview
	public CardStandIn standInPreview;
	public TokenPreview tokenPreview;
	public RawImage previewImage;
	public RectTransform previewPanel;
	public Camera guiCamera;
	public CanvasScaler uiScale;
	public CardDisplay currentPreview;
	public float previewHedge = 10f;
	public Text watermark;

	// Navigation
	public Transform cameraPivot;
	public GameObject cameraRotation;
	public float navigationSpeed = .002f;
	public bool navigating = false;
	Vector3 origClickPos = Vector3.zero;
	Vector3 origCamPos = Vector3.zero;

	// Unity
	public CardDisplay cardBeingDragged = null;
	public bool drawingGuide = false;
	public InputHandler input;
	new public Collider collider;

	// Deck listing
	public ListDeckDisplay deckListUI;
	public TranslatePanel changeDeckList;
	public GameObject changeDeckSpinner;

	// Options listing
	public TranslatePanel optionsUI;

	// Honor
	enum StartState { initialized, notInitialized };
	StartState startState = StartState.notInitialized;
	public Text myHonor;
	public Image myHonorFan;
	public Text myHonorPlus;
	public Text myHonorMinus;
	public Text theirHonor;
	public Image theirHonorFan;
	int myStartingHonor = 0;
	CardDisplay borderKeep = null;
	CardDisplay bamboo = null;
	CardDisplay stronghold = null;
	CardDisplay secondStronghold = null;
	float honorTimer = 0f;
	int honorCount = 0;
	[HideInInspector] public int theirHonorID = -1;
	[HideInInspector] public int myHonorID = -1; // always my player ID, except for spectator

	public Favor imperialFavor;

	// Connection
	public UnfurlScroll waitingButton;
	public Text waitingText;
	public Text privateGameName;
	[HideInInspector] public Deck myDeck = null;
	bool connectionFailed = false;
	bool reconnecting = false;
	bool disconnected {
		get {
			return PhotonNetwork.room.customProperties["disconnected"] != null;
		}
		set {
			string specificValue = value ? "1" : null;
			PhotonHash roomProperties = new PhotonHash();
			roomProperties.Add("disconnected", specificValue);
			PhotonNetwork.room.SetCustomProperties( roomProperties );
		}
	}

	// Turn order
	bool firstTurn = true;
	float turnBackwardDelay = 0f;

	public UnfurlScroll concedePanel;

	public SunAndMoon myTurnIndicator;
	public SunAndMoon theirTurnIndicator;
	public Text currentPhaseLabel;
	public Text currentSegLabel;
	public CanvasGroup attackDeclaration;
	public enum Phase {Action, AttackOpportunity, ChooseDefender, InviteAllies, AssignAttackers, AlliedAttackers, AssignDefenders, AlliedDefenders, Resolve, Engage, Battle, Resolution, Dynasty, Discard}
	public Phase currentPhase = Phase.Action;
	public Phase prevPhase = Phase.Action;
	public GameObject prevPhaseButton;
	bool pass = true;
	int otherPlayersPassed = 0;
	int playersFinishedSettingUp = 0;
	[HideInInspector] public int theirID = -1;
	[HideInInspector] public int myID = -1; // always my player ID, except for spectator
	[HideInInspector] public int overrideID {
		get {
			return Table.GetIDFor( PhotonNetwork.player );
		}
		set {
			PhotonHash myOrder = new PhotonHash();
			myOrder.Add( "overrideID", value.ToString() );
			PhotonNetwork.player.SetCustomProperties( myOrder );
		}
	}
	int whoseTurn = -1;
	int whoseAction = -1;
	int startingPlayer = -1;

	// Multiplayer specific
	int defenderID;
	int[] alliedAttackers = new int[0];
	int[] alliedDefenders = new int[0];

	// Tournament reporting
	public TournamentReport tournamentReport;

	// Sound effects
	public AudioSource participantJoined;
	public AudioSource spectatorJoined;

	// Customization
	public Dictionary<int, Texture2D> customFateBacks = new Dictionary<int, Texture2D>();
	public Dictionary<int, Texture2D> customDynBacks = new Dictionary<int, Texture2D>();
	Playmat playmat;
	float playmatHedge = 3.5f;

	// Room properties
	int finishedResetting = 0;

	public int maxParticipants {
		get {	
			if( PhotonNetwork.offlineMode ) return 1;
			return int.Parse( (string)PhotonNetwork.room.customProperties["maxParticipants"] );
		}
	}

	public int curParticipants {
		get {
			int _curParticipants = 0;

			if( PhotonNetwork.playerList.Length == 0 ) print("Player list is empty! " + Time.time);
			foreach( PhotonPlayer player in PhotonNetwork.playerList ) {
				if( player.customProperties["spectating"] == null ) _curParticipants++;
			}
			return _curParticipants;
		}
		set {
			PhotonHash participantProps = new PhotonHash();
			participantProps.Add("curParticipants", curParticipants.ToString() );
			PhotonNetwork.room.SetCustomProperties( participantProps );
		}
	}

	public bool spectating {
		get {
			return PhotonNetwork.player.customProperties["spectating"] != null;
		}
	}

	public bool isMasterPlayer {
		get {
			int lowestPlayerId = 99999;

			foreach( PhotonPlayer player in PhotonNetwork.playerList ) {
				if( player.customProperties["spectating"] != null ) continue;
				if( player.ID < lowestPlayerId ) lowestPlayerId = player.ID;
			}

			return lowestPlayerId == PhotonNetwork.player.ID;
		}
	}

	// Provinces and Fief
	public List<CardDisplay> provinces = new List<CardDisplay>();
	List<CardDisplay> fief = new List<CardDisplay>();
	List<List<CardDisplay>> regions = new List<List<CardDisplay>>(4);

	// Card Removal
	public RemoveFromGame removalPanel;

	// Proxies
	public CreateProxy proxyPanel;
	public ModifyToken tokenPanel;

	// Menu
	public RadialMenu radialMenu;

	// Chat
	public GameObject chatPanel;
	public ChatPanel chat;
	public string myName {
		get {
			return PlayerPrefs.GetString("onlineName", "");
		}
	}

	// guide lines
	public Material guideMaterial;
	public Material arrowMaterial;
	// float guideOffsetSpeed = 1f;
	public Vector3 lineHeight = new Vector3(0f,3f,0f);
	public float lineBend = 0f;
	public int lineResolution = 50;
	public float arrowLength = 1f;
	public float arrowWidth = 2.5f;
	public float lineWidth = 10f;
	public float refreshGuideRate = 2f;
	float refreshGuideTimer = 0f;

	// Selection
	public List<CardDisplay> selectedCards = new List<CardDisplay>();

	// Events
	public delegate void CameraMovedDelegate();
	public event CameraMovedDelegate CameraMovedEvent;

	public delegate void TableResetDelegate( string withNewClan );
	public event TableResetDelegate TableResetEvent;

	void Awake() {
		Table.data = this;
		allCards = new List<CardDisplay>();
		allDecks = new List<DeckDisplay>();

		GameObject emissary = GameObject.Find("DeckEmissary(Clone)");

		myDeck = emissary.GetComponent<Deck>();
		myClan = myDeck.clan;

		PhotonNetwork.offlineMode = true; // debug
		if( PhotonNetwork.offlineMode == true ) {
			PhotonNetwork.JoinRandomRoom();
		}
	}

	void Start() {
		if( PlayerPrefs.GetInt("resetting", 0) == 1 ) {
			overrideID = PhotonNetwork.player.ID;

			// Reclan the room
			PhotonHash roomProperties = new PhotonHash();
			roomProperties.Add( "clan", Persistent.data.ClanStringToEnum(myClan) );
			roomProperties.Add( "legality", myDeck.legality.ToString() );
			PhotonNetwork.room.SetCustomProperties( roomProperties );

			StartCoroutine( WaitForOtherPlayers() );
		}
	}

	void OnJoinedRoom() {
		if( !PhotonNetwork.offlineMode ) {
			PlayerPrefs.SetString("resumeGameName", PhotonNetwork.room.name);
		}

		curParticipants = curParticipants;

		if( curParticipants > maxParticipants ) {
			Concede();
		}

		cameraPivot.position = new Vector3( 0f, 0f, -cameraDistFromDiv );

		if( spectating ) {
			cameraRotation.SetActive( true );
			chat.photonView.RPC("AddChatEntry", PhotonTargets.All, overrideID, Table.data.myName, "I joined your game as a spectator", (int)Persistent.data.ClanStringToEnum(Table.data.myClan) );
		}
		else if( !reconnecting ) {
			string roomPassword = (string)PhotonNetwork.room.customProperties["password"];
			if( PlayerPrefs.GetInt( "forbidspectators", 0 ) == 1 && Persistent.data.playtestGroup == "" && string.IsNullOrEmpty(roomPassword) ) {
				PhotonHash roomProperties = new PhotonHash();
				roomProperties.Add( "forbidSpectators", "1" );
				PhotonNetwork.room.SetCustomProperties( roomProperties );
			}

			overrideID = PhotonNetwork.player.ID;
			StartCoroutine( WaitForOtherPlayers() );
		}
	}

	IEnumerator WaitForOtherPlayers() {

		print("Current participants is " + curParticipants + " and maximum is " + maxParticipants);

		cameraPivot.position = new Vector3( 0f, 0f, -cameraDistFromDiv );

		if( spectating ) yield break;
		if( disconnected ) yield break;

		while( Persistent.data.loading ) yield return null;

		myID = overrideID;
		myHonorID = myID;

		if( !PhotonNetwork.offlineMode ) {
			
			if( PlayerPrefs.GetInt("resetting", 0) == 0 ) {
				print("Unfurling waiting button at " + Time.time);
				waitingButton.UnfurlNow();
			}

			if( PhotonNetwork.connected && Persistent.data.roomPassword != "" ) {
				privateGameName.text = "Private game name: \"" + Persistent.data.roomPassword + "\"";
			}
			else if( Persistent.data.roomPassword == "" ) privateGameName.gameObject.SetActive( false ); // not a private game

			while( PhotonNetwork.connected && curParticipants != maxParticipants) {

				yield return null;
			}

			print("Finished waiting for other players");
			if( !PhotonNetwork.connected ) yield break; // disconnected while waiting for other players

			// prevent other users from joining during the RPC-heavy setup
			if( PhotonNetwork.player.isMasterClient ) {
				PhotonNetwork.room.open = false;
				PhotonNetwork.room.visible = false;
			}

			if( PlayerPrefs.GetInt("resetting", 0) == 0 ) waitingButton.FurlNow();
		}

		PlayerPrefs.SetInt("resetting", 0);

		while( Persistent.data == null ) yield return null;

		print("Setting table with my ID " + overrideID + " and Y angle " + MyYAngle() );
		if( myDeck.gameObject.activeInHierarchy ) yield return StartCoroutine( myDeck.GenerateCards() );

		SetTable();

		StartCoroutine( myFate.DrawCards(5) );
	}

	void SetTable() {
		playersFinishedSettingUp = 0;

		Quaternion rotToCenter = Quaternion.AngleAxis(MyYAngle(),Vector3.up);
		myForwardDir = rotToCenter * myForwardDir;

		// 3+ multiplayer
		if( maxParticipants > 2 ) {
			distanceFromDivider = 23f;
			cameraDistFromDiv = 34.5f;
		}

		// set up player scene elements
		myHand = PhotonNetwork.Instantiate( "Hand", new Vector3(0,1f,-distanceFromDivider - enemyHandDistFromFief), Quaternion.identity, 0 ).GetComponent<Hand>();
		bool handHidden = PlayerPrefs.GetInt("hidehand", 0) == 1;
		myHand.photonView.RPC("Initialize", PhotonTargets.All, handHidden);

		Vector3 dynastyLocation = new Vector3( -distanceBetweenDecks / 2f, 0.05f, -distanceFromDivider );
		DeckDisplay dynasty = PhotonNetwork.Instantiate( "Deck", dynastyLocation, Quaternion.identity, 0 ).GetComponent<DeckDisplay>();
		dynasty.photonView.RPC( "Initialize", PhotonTargets.All, 1 );
		myDynasty = dynasty;

		Vector3 dynDiscardLocation = dynastyLocation - new Vector3(distanceToDiscard, 0f, 0f);
		DeckDisplay dynDiscard = PhotonNetwork.Instantiate( "Deck", dynDiscardLocation, Quaternion.identity, 0 ).GetComponent<DeckDisplay>();
		dynDiscard.photonView.RPC( "Initialize", PhotonTargets.All, 3 );
		myDynDiscard = dynDiscard;

		print("Initialized dynasty and discard");

		Vector3 fateLocation = new Vector3( distanceBetweenDecks / 2f, 0.05f, -distanceFromDivider );
		DeckDisplay fate = PhotonNetwork.Instantiate( "Deck", fateLocation, Quaternion.identity, 0 ).GetComponent<DeckDisplay>();
		fate.photonView.RPC( "Initialize", PhotonTargets.All, 0 );
		myFate = fate;

		Vector3 fateDiscardLocation = fateLocation + new Vector3(distanceToDiscard, 0.05f, 0f);
		DeckDisplay fateDiscard = PhotonNetwork.Instantiate( "Deck", fateDiscardLocation, Quaternion.identity, 0 ).GetComponent<DeckDisplay>();
		fateDiscard.photonView.RPC( "Initialize", PhotonTargets.All, 2 );
		myFateDiscard = fateDiscard;

		print("Initialized fate and discard");

		Transform placementPivot = new GameObject("placementPivot").GetComponent<Transform>();
		Quaternion placementRotation = Quaternion.Euler( new Vector3(0f, MyYAngle(), 0f) );

		Vector3 strongholdLocation = dynastyLocation + new Vector3( 0f, 0f, strongholdDistFromDyn );
		placementPivot.position = strongholdLocation;
		placementPivot.RotateAround( Vector3.zero, Vector3.up, MyYAngle() );

		stronghold = PhotonNetwork.Instantiate( "Card", placementPivot.position, placementRotation, 0 ).GetComponent<CardDisplay>();
		stronghold.name = "Stronghold 1";
		secondStronghold = PhotonNetwork.Instantiate( "Card", placementPivot.position, placementRotation, 0 ).GetComponent<CardDisplay>();
		secondStronghold.name = "Stronghold 2";
		// will configure these later when we scan through cards

		print("Initialized stronghold");

		cameraPivot.RotateAround( Vector3.zero, Vector3.up, MyYAngle() );

		int proxyCount = 0;
		int proxyWidth = 5;

		print( "Loading cards in deck with " + myDeck.proxies.Count + " proxies" );
		for( int index = 0; index < myDeck.cardList.Count; index++ ) {

			string cardID = myDeck.cardList[index].id;

			// print("Instantiating card with name " + myDeck.cardList[index].name + " and printing " + myDeck.cardList[index].imageIndex);

			if( myDeck.cardList[index].type == "sensei" ) {
				Vector3 senseiLocation = strongholdLocation - new Vector3( senseiLeftOfStrong, 0f, 0f );
				placementPivot.position = senseiLocation;
				placementPivot.RotateAround( Vector3.zero, Vector3.up, MyYAngle() );

				CardDisplay sensei = PhotonNetwork.Instantiate( "Card", placementPivot.position, placementRotation, 0 ).GetComponent<CardDisplay>();

				sensei.photonView.RPC("LoadCard", PhotonTargets.All, cardID, "", myDeck.cardList[index].edition );
				sensei.photonView.RPC("ForceFaceUp", PhotonTargets.All, true);
				myStartingHonor += myDeck.cardList[index].starting_honor;

				// udly special cases until the Kamisasori database has a special method for setting honor
				if( myDeck.cardList[index].name == "Akeha Sensei" ) myStartingHonor = 5;

				continue;
			}

			if( myDeck.cardList[index].type == "stronghold" ) {

				stronghold.photonView.RPC("LoadCard", PhotonTargets.All, cardID, "", myDeck.cardList[index].edition);
				myStartingHonor += myDeck.cardList[index].starting_honor;

				if( stronghold.card.linkedCard != null ) {
					secondStronghold.photonView.RPC("LoadCard", PhotonTargets.All, stronghold.card.linkedCard.id, "", myDeck.cardList[index].edition );
					// stronghold.card.secondSide = secondStronghold.card.images[0];
					// secondStronghold.card.secondSide = stronghold.card.images[0];
				}
				else if( stronghold.card.isFromOracle && stronghold.card.secondSide != "" ) {
					// Oracle stronghold, will be flipped later
					secondStronghold.photonView.RPC("LoadCard", PhotonTargets.All, stronghold.card.id, "", myDeck.cardList[index].edition );
				}

				StartCoroutine( stronghold.DownloadImage( true ) );
				StartCoroutine( secondStronghold.DownloadImage( true ) );

				continue;
			}

			myDeck.cardList[index].isProxy = false;
			if( myDeck.proxies.ContainsKey( myDeck.cardList[index] ) ) {
				print("Loading card " + myDeck.cardList[index].name + " in deck " + myDeck.deckName + " flagged as proxy");
				myDeck.cardList[index].isProxy = true; // set this true here so that the card is only technically a proxy in this game, not for all games

				int proxyDepth = proxyCount / proxyWidth;
				// print("Loading proxy at proxy depth " + proxyDepth);
				placementPivot.position = fateDiscardLocation + new Vector3(xDistanceBetweenProxies + xDistanceBetweenProxies * (proxyCount % proxyWidth), 0f, -zDistanceBetweenProxies * proxyDepth);
				placementPivot.RotateAround( Vector3.zero, Vector3.up, MyYAngle() );
				CardDisplay proxy = PhotonNetwork.Instantiate("Card", placementPivot.position, Quaternion.Euler( new Vector3(0f, MyYAngle(), 180f) ), 0).GetComponent<CardDisplay>();
				proxy.photonView.RPC("LoadCard", PhotonTargets.All, cardID, myDeck.proxies[ myDeck.cardList[index] ], myDeck.cardList[index].edition );
				proxy.photonView.RPC("ForceFaceUp", PhotonTargets.All, false);
				proxyCount++;

				continue;
			}

			GameObject displayObject = PhotonNetwork.Instantiate( "Card", Vector3.zero, Quaternion.identity, 0 );
			CardDisplay display = displayObject.GetComponent<CardDisplay>();
			display.photonView.RPC( "LoadCard", PhotonTargets.All, cardID, "", myDeck.cardList[index].edition );

			if( myDeck.cardList[index].deckType == Card.DeckType.Fate ) {
				fate.photonView.RPC( "AddCardToDeckInstantly", PhotonTargets.All, display.photonView.viewID, 0 );
			}
			else {
				dynasty.photonView.RPC( "AddCardToDeckInstantly", PhotonTargets.All, display.photonView.viewID, 0 );
			}
		}

		dynasty.photonView.RPC( "Shuffle", PhotonTargets.All, System.Environment.TickCount );
		fate.photonView.RPC( "Shuffle", PhotonTargets.All, System.Environment.TickCount );

		if( bamboo != null ) AddToFief( bamboo );
		if( borderKeep != null ) AddToFief( borderKeep );
		if( borderKeep != null ) borderKeep.Unbow();

		// lay out first four provinces
		StartCoroutine( myDynasty.FillProvinces( 2f ) );

		theirHonorFan.gameObject.SetActive(!PhotonNetwork.offlineMode);

		myHonor.text = myStartingHonor.ToString();
		myHonorFan.gameObject.SetActive(true);

		// register player details
		PhotonHash myPlayerDetails = new PhotonHash();
		myPlayerDetails.Add( "honor", myStartingHonor.ToString() );
		myPlayerDetails.Add( "clan", myClan );
		PhotonNetwork.player.SetCustomProperties( myPlayerDetails );

		// imperial favor
		imperialFavor.photonView.RPC("RegisterPlayer", PhotonTargets.All, myDynasty.photonView.viewID, (int)Persistent.data.ClanStringToEnum(myClan));
		imperialFavor.Show();

		// configure sun and moon
		myTurnIndicator.gameObject.SetActive(true);
		if( PhotonNetwork.offlineMode == false ) {
			theirTurnIndicator.gameObject.SetActive(true);

			photonView.RPC("WhoStartsFirst", PhotonTargets.All);
		}
		else {
			photonView.RPC("StartAction", PhotonTargets.All, myID, (int)Phase.Action, true, -1);
			stronghold.photonView.RPC("ForceFaceUp", PhotonTargets.All, true);
			RefreshIndicators();
			photonView.RPC( "RemoveFromGame", PhotonTargets.All, secondStronghold.photonView.viewID );
			PhotonNetwork.Destroy( secondStronghold.gameObject );
		}

		// custom playmat
		string playmatURL = PlayerPrefs.GetString( "playmatURL", "" );
		if( playmatURL != "" ) {
			placementPivot.position = Vector3.zero - new Vector3( 0f, -0.001f, distanceFromDivider - playmatHedge );
			placementPivot.RotateAround( Vector3.zero, Vector3.up, MyYAngle() );

			playmat = PhotonNetwork.Instantiate( "Playmat", placementPivot.position, Quaternion.Euler( new Vector3(0f, MyYAngle(), 0f) ), 0 ).GetComponent<Playmat>();
			playmat.photonView.RPC("Initialize", PhotonTargets.All, playmatURL );
		}

		// relay custom card backs
		string customFateCardBack = PlayerPrefs.GetString( "fateCardbackURL", "" );
		if( customFateCardBack != "" ) photonView.RPC("SetCustomFateCardBack", PhotonTargets.All, customFateCardBack );

		string customDynastyCardBack = PlayerPrefs.GetString( "dynastyCardbackURL", "" );
		if( customDynastyCardBack != "" ) photonView.RPC("SetCustomDynCardBack", PhotonTargets.All, customDynastyCardBack );
	}

	[PunRPC]
	public void WhoStartsFirst() {

		// count number of players registered, and proceed if all have done so
		playersFinishedSettingUp++;
		if( playersFinishedSettingUp < curParticipants ) return;

		startState = StartState.initialized;

		// Set up tournament reporting
		if( Persistent.data.tournamentMode ) {
			print("Host broadcasting deck request to all other players, including self " + Time.time);
			photonView.RPC("DeckRequested", PhotonTargets.All, Persistent.data.tournamentName);
		}

		if( PhotonNetwork.isMasterClient ) {

			RenameRoom();

			PhotonNetwork.room.open = true;
			PhotonNetwork.room.visible = true;

			print("Master client checking who starts first " + Time.time);
			
			List<PhotonPlayer> playersWithHighestHonor = new List<PhotonPlayer>();

			int highestHonor = -99;
			foreach( PhotonPlayer candidate in PhotonNetwork.playerList ) {
				if( candidate.customProperties["spectating"] != null ) continue;

				int thisPlayersHonor = int.Parse( (string)candidate.customProperties["honor"] );
				print("This player's starting honor is " + thisPlayersHonor);
				if( thisPlayersHonor > highestHonor ) {
					playersWithHighestHonor = new List<PhotonPlayer>();
					playersWithHighestHonor.Add( candidate );
					highestHonor = thisPlayersHonor;
				}
				else if( thisPlayersHonor == highestHonor ) {
					playersWithHighestHonor.Add( candidate );
				}
			}

			if( playersWithHighestHonor.Count == 1 ) {
				print("Determined that player with highest honor is " + Table.GetIDFor( playersWithHighestHonor[0] ) + " " + Time.time);
				photonView.RPC( "StartFirst", PhotonTargets.All, Table.GetIDFor( playersWithHighestHonor[0] ) );
			}
			else {
				float randomRange = Random.Range( 0f, playersWithHighestHonor.Count - 1f );
				float playerToStart = Mathf.Round( randomRange );

				print("Chose player id " + Table.GetIDFor( playersWithHighestHonor[(int)playerToStart] ) + " randomly");

				photonView.RPC( "StartFirst", PhotonTargets.All, Table.GetIDFor( playersWithHighestHonor[(int)playerToStart] ) );
			}
		}
	}

	[PunRPC]
	public void StartFirst( int startingID ) {
		whoseTurn = startingID;
		whoseAction = startingID;

		startingPlayer = startingID;

		print(startingID + " starting first " + Time.time);
		if( !spectating ) {
			if( theirID == -1 ) {
				if( whoseTurn != startingID ) theirID = whoseTurn;
				else theirID = NextPlayer();

				theirHonorID = theirID;
			}
			RefreshIndicators();
		}
		else {
			SpectatorChangedPosition();
			return;
		}

		if( firstTurn && startingID == this.overrideID ) {

			print("Going first, so destroying " + secondStronghold.name + " " + Time.time);
			photonView.RPC( "RemoveFromGame", PhotonTargets.All, secondStronghold.photonView.viewID );
			PhotonNetwork.Destroy( secondStronghold.gameObject );

			prevPhaseButton.SetActive( true );
		}
		else if( firstTurn && startingID != this.overrideID ) {

			print("It's my first turn and I'm not starting first " + Time.time);

			if( secondStronghold != null && stronghold.card.linkedCard != null ) {
				print("I have a second stronghold object and it is linked to another card " + Time.time);

				photonView.RPC( "RemoveFromGame", PhotonTargets.All, stronghold.photonView.viewID );
				PhotonNetwork.Destroy( stronghold.gameObject );

				stronghold = secondStronghold;
			}
			else if( stronghold.card.linkedCard == null ) {
				print("I have a second stronghold object but it is not linked to another card " + Time.time);

				photonView.RPC( "RemoveFromGame", PhotonTargets.All, secondStronghold.photonView.viewID );
				PhotonNetwork.Destroy( secondStronghold.gameObject );

				// make sure the correct side of an Oracle stronghold is facing up
				if( stronghold.card.isFromOracle && stronghold.card.secondSide != "" ) {
					stronghold.photonView.RPC("FlipOver", PhotonTargets.All, 0.1f);
				}
			}
			else print("I have no second stronghold object " + Time.time);
		}

		stronghold.photonView.RPC("ForceFaceUp", PhotonTargets.All, true);

		firstTurn = false;
		myTurnIndicator.firstChime = false;
	}

	public void ToggleTurn() {
		if( spectating ) return;
		if( !IsMyAction() ) return;
		if( proxyPanel.gameObject.activeInHierarchy ) return;

		Phase nextPhase = Phase.Action;
 
		print( myID + " toggling turn.  Their ID is " + NextPlayer() );
		int theirs = NextPlayer();

		int mine = overrideID;

		prevPhase = currentPhase;

		print("Toggling turn with number of other players passed: " + otherPlayersPassed + " " + Time.time);

		// ENDING ACTION

		// if you click the sun to end your action, then what to do next?
		switch( currentPhase ) {
			case Phase.Action:
				if( PhotonNetwork.offlineMode == true || (pass && otherPlayersPassed >= curParticipants - 1) ) {
					print("Because I passed and the opponents all passed, advancing from action to attack");
					whoseAction = whoseTurn;
					nextPhase = Phase.AttackOpportunity;
				}
				else {
					print("Because there are still passes left in the chain, am pushing priority on to " + theirs + " still in the action phase");
					whoseAction = theirs;
					nextPhase = Phase.Action;
				}
			break;

			case Phase.AttackOpportunity:
				whoseAction = whoseTurn;
				nextPhase = Phase.Dynasty;

				attackDeclaration.gameObject.SetActive(false);
			break;

			case Phase.ChooseDefender:
				if( selectedCards.Count > 0 ) {
					photonView.RPC( "SetDefenderID", PhotonTargets.All, Table.GetIDFor(selectedCards[0].photonView.owner) );
					whoseAction = whoseTurn;
					nextPhase = Phase.InviteAllies;
				}
				else {
					whoseAction = whoseTurn;
					nextPhase = Phase.Dynasty;
				}
			break;

			case Phase.InviteAllies:

				// Identify all highlighted cards; these are allies if they aren't us
				List<int> allies = new List<int>();
				if( selectedCards.Count > 0 ) {
					foreach( CardDisplay selectedCard in selectedCards ) {
						int cardOwner = Table.GetIDFor( selectedCard.photonView.owner );
						if( cardOwner != myID && !allies.Contains( cardOwner ) ) {
							allies.Add( cardOwner );
						}
					}
					allies.Sort();
				}

				if( whoseAction == whoseTurn ) {
					// If you are the attacker
					photonView.RPC( "SetAttackingAllies", PhotonTargets.All, allies.ToArray() );
					whoseAction = defenderID;
					nextPhase = Phase.InviteAllies;
				}
				else {
					// If you are the defender
					photonView.RPC( "SetDefendingAllies", PhotonTargets.All, allies.ToArray() );
					whoseAction = whoseTurn;
					nextPhase = Phase.AssignAttackers;
				}
			break;

			case Phase.AssignAttackers:
				if( curParticipants > 2 && alliedAttackers.Length > 0 ) {
					whoseAction = alliedAttackers[0];
					nextPhase = Phase.AlliedAttackers;
				}
				else {
					if( PhotonNetwork.offlineMode == true ) {
						whoseAction = mine;
						nextPhase = Phase.Resolve;
					}
					else {
						whoseAction = defenderID;
						nextPhase = Phase.AssignDefenders;
					}
				}
			break;

			case Phase.AlliedAttackers:

				// select next ally in list
				whoseAction = defenderID;
				nextPhase = Phase.AssignDefenders;
				for( int index = 0; index < alliedAttackers.Length; index++ ) {
					if( alliedAttackers[index] == myID ) {
						if( index + 1 < alliedAttackers.Length ) {
							whoseAction = alliedAttackers[index + 1];
							nextPhase = Phase.AlliedAttackers;
						}
						break;
					}
				}
			break;

			case Phase.AssignDefenders:
				if( alliedDefenders.Length > 0 ) {
					whoseAction = alliedDefenders[0];
					nextPhase = Phase.AlliedDefenders;
				}
				else {
					whoseAction = whoseTurn;
					nextPhase = Phase.Resolve;
				}
			break;

			case Phase.AlliedDefenders:

				// select next ally in list
				whoseAction = whoseTurn;
				nextPhase = Phase.Resolve;
				for( int index = 0; index < alliedDefenders.Length; index++ ) {
					if( alliedDefenders[index] == myID ) {
						if( index + 1 < alliedDefenders.Length ) {
							whoseAction = alliedDefenders[index + 1];
							nextPhase = Phase.AlliedDefenders;
						}
						break;
					}
				}
			break;

			case Phase.Resolve:
				if( selectedCards.Count > 0 ) {
					whoseAction = theirs;
					nextPhase = Phase.Engage;

					pass = false;
				}
				else {
					whoseAction = whoseTurn;
					nextPhase = Phase.Dynasty;
				}
			break;

			case Phase.Engage:
				if( PhotonNetwork.offlineMode == true ) {
					nextPhase = Phase.Battle;
				}
				else if( pass && otherPlayersPassed >= curParticipants - 1 ) {
					if( whoseTurn == mine ) whoseAction = theirs;
					else whoseAction = mine;
					nextPhase = Phase.Battle;

					pass = false;
				}
				else {
					whoseAction = theirs;
					nextPhase = Phase.Engage;
				}
			break;

			case Phase.Battle:
				if( PhotonNetwork.offlineMode == true ) {
					nextPhase = Phase.Resolution;
				}
				else if( pass && otherPlayersPassed == curParticipants - 1 ) {
					whoseAction = whoseTurn;
					nextPhase = Phase.Resolution;
				}
				else {
					whoseAction = theirs;
					nextPhase = Phase.Battle;
				}
			break;

			case Phase.Resolution:
				whoseAction = whoseTurn;
				nextPhase = Phase.Resolve;
			break;

			case Phase.Dynasty:
				whoseAction = mine;

				nextPhase = Phase.Discard;

				if( PlayerPrefs.GetInt("dontdrawatend", 0) == 0 ) StartCoroutine( myFate.DrawCards(1) );
			break;

			case Phase.Discard:
				whoseAction = theirs;

				nextPhase = Phase.Action;
			break;
		}

		if( PhotonNetwork.offlineMode == true ) {
			whoseTurn = mine;
			whoseAction = mine;
		}

		Table.data.photonView.RPC( "StartAction", PhotonTargets.All, whoseAction, (int)nextPhase, pass, -1 );
	}

	public void ToggleTurnBackward() {

		if( turnBackwardDelay > 0f ) return;

		int mine = overrideID;

		// if you want to back to a previous phase, then how?
		switch( currentPhase ) {
			case Phase.Action:
				mine = NextPlayer();
				prevPhase = Phase.Action;
				myTurnIndicator.turnCounter--;

				print("Player turn ID to revert to is " + mine + " and targeted phase is " + prevPhase);
			break;

			case Phase.AttackOpportunity:
				prevPhase = Phase.Action;
				attackDeclaration.gameObject.SetActive(false);
			break;

			case Phase.ChooseDefender:
				prevPhase = Phase.AttackOpportunity;
			break;

			case Phase.InviteAllies:
				prevPhase = Phase.ChooseDefender;
			break;

			case Phase.AssignAttackers:
				prevPhase = Phase.AttackOpportunity;
			break;

			case Phase.AlliedAttackers:
				prevPhase = Phase.AssignAttackers;
			break;

			case Phase.AssignDefenders:
			return;

			case Phase.AlliedDefenders:
				prevPhase = Phase.AssignDefenders;
			break;

			case Phase.Resolve:
				prevPhase = Phase.AssignAttackers;
			break;

			case Phase.Engage:
				prevPhase = Phase.Resolve;
			break;

			case Phase.Battle:
				prevPhase = Phase.Engage;
			break;

			case Phase.Resolution:
				prevPhase = Phase.Battle;
				pass = true;
			break;

			case Phase.Dynasty:
				// previous phase let to remain the prior phase as set in ToggleTurn
			break;

			case Phase.Discard:
				prevPhase = Phase.Dynasty;
			break;
		}

		prevPhaseButton.SetActive( false );

		print("Sending start action message " + Time.time);

		Table.data.photonView.RPC( "StartAction", PhotonTargets.All, mine, (int)prevPhase, pass, mine );

		StartCoroutine( TurnBackwardDelay() );
	}

	IEnumerator TurnBackwardDelay() {
		turnBackwardDelay = 0.5f;

		while( turnBackwardDelay > 0f ) {
			yield return null;
			turnBackwardDelay -= Time.deltaTime;
		}

		turnBackwardDelay = 0f;
	}

	[PunRPC]
	public void StartAction( int forID, int whichPhase, bool playerPassed, int forceTurn ) {

		Resources.UnloadUnusedAssets();

		print("Starting action " + Time.time);

		print("Toggling " + ((Phase)whichPhase).ToString() + " for " + forID + " at " + Time.time + " forcing turn = " + forceTurn);

		// Toggle the sun and moon indicators correctly

		whoseAction = forID;
		if( forceTurn > -1 ) whoseTurn = forceTurn;

		if( playerPassed ) otherPlayersPassed++;
		else otherPlayersPassed = 0;

		pass = true;

		// Set turn labels appropriately

		if( currentPhase == Phase.Discard && (Phase)whichPhase == Phase.Action ) {
			// starting new turn
			whoseTurn = whoseAction;

			if( whoseTurn == overrideID || PhotonNetwork.offlineMode ) {
				if( PlayerPrefs.GetInt("dontunbowall", 0) == 0 ) UnbowAndFlipAll();
				otherPlayersPassed = 0;
			}

			if( whoseTurn == startingPlayer || PhotonNetwork.offlineMode ) myTurnIndicator.turnCounter++;

			ClearAllSelections();
			DisableAllGuides();
		}

		if( whichPhase == (int)Phase.ChooseDefender ||
			whichPhase == (int)Phase.InviteAllies ||
			whichPhase == (int)Phase.Resolve ) {
			ClearAllSelections(); // so that important selections can be made
		}

		if( whichPhase == (int)Phase.Dynasty ) {
			ClearAllSelections();
			DisableAllGuides();
		}

		currentPhase = (Phase)whichPhase;

		RefreshIndicators();

		prevPhaseButton.SetActive( whoseTurn == myID && IsMyAction() );
	}

	[PunRPC]
	public void RefreshIndicators() {
		// call this after setting theirID or myID to refresh the indicators for honor and priority

		myHonor.gameObject.SetActive( true );
		myHonorFan.gameObject.SetActive( true );

		if( PhotonNetwork.offlineMode == false ) {
			theirHonor.gameObject.SetActive( true );
			theirHonorFan.gameObject.SetActive( true );
		}

		foreach( PhotonPlayer p in PhotonNetwork.playerList ) {
			if( p.customProperties["spectating"] != null ) continue;

			if( Table.GetIDFor(p) == theirID ) {
				if( whoseAction == Table.GetIDFor(p) ) {
					theirTurnIndicator.StartAction();
					myTurnIndicator.EndAction();
				}
				else {
					theirTurnIndicator.EndAction();
				}
			}

			if( Table.GetIDFor(p) == theirHonorID ) {
				Color[] clanColors = Persistent.data.GetColorsForClan((string)p.customProperties["clan"]);

				theirHonor.color = clanColors[0];
				theirHonorFan.color = clanColors[1];

				theirHonor.text = (string)p.customProperties["honor"];
			}

			if( Table.GetIDFor(p) == myID ) {
				if( whoseAction == Table.GetIDFor(p) ) {
					myTurnIndicator.StartAction();
					theirTurnIndicator.EndAction();
				}
				else {
					myTurnIndicator.EndAction();
				}
			}

			if( Table.GetIDFor(p) == myHonorID ) {
				if( TableResetEvent != null ) TableResetEvent( (string)p.customProperties["clan"] );

				myHonor.text = (string)p.customProperties["honor"];
			}
		}

		bool isMyAction = whoseAction == myID;
		bool isMyTurn = whoseTurn == myID;

		PhotonPlayer currentPlayer = null;
		foreach( PhotonPlayer player in PhotonNetwork.playerList ) {
			if( player.customProperties["spectating"] != null ) continue;
			if( Table.GetIDFor(player) == whoseAction ) currentPlayer = player;
		}

		if( currentPlayer == null ) return;

		string currentPlayerName = (string)currentPlayer.customProperties["name"];
		string theirName = currentPlayerName == "" ? "Their" : currentPlayerName + "'s";
		if( theirName == "Their" && spectating ) theirName = "Unnamed Ronin's";

		string myName = "";
		if( myID == overrideID ) myName = "Your";
		
		if( spectating ) {
			if( myName == "Your" ) myName = "Unnamed Ronin's";
			else myName = theirName;
		}

		// STARTING ACTION
		
		switch( currentPhase ) {
			case Phase.Action:
				if( isMyTurn || PhotonNetwork.offlineMode ) {
					if( isMyAction || PhotonNetwork.offlineMode ) {
						currentPhaseLabel.text = myName;
						currentSegLabel.text = "Limited Action";
					}
					else {
						currentPhaseLabel.text = theirName;
						currentSegLabel.text = "Open Action";
					}
				}
				else {
					if( isMyAction ) {
						currentPhaseLabel.text = myName;
						currentSegLabel.text = "Open Action";
					}
					else {
						currentPhaseLabel.text = theirName;
						currentSegLabel.text = "Limited Action";
					}
				}
			break;

			case Phase.AttackOpportunity:
				if( isMyTurn ) {
					if( !spectating ) {
						attackDeclaration.gameObject.SetActive(true);
						LeanTween.value( attackDeclaration.gameObject, delegate(float a){ attackDeclaration.alpha = a; }, 0f, 1f, 1f ).setEase(LeanTweenType.linear).setRepeat(-1).setLoopPingPong();
					}

					currentPhaseLabel.text = "Attack?";
					currentSegLabel.text = "";
				}
				else {
					currentPhaseLabel.text = theirName;
					currentSegLabel.text = "Attack Phase";
				}
			break;

			case Phase.ChooseDefender:
				if( isMyTurn ) {
					currentPhaseLabel.text = "Choose";
					currentSegLabel.text = "Defender";
					LeanTween.cancel(attackDeclaration.gameObject);
					attackDeclaration.gameObject.SetActive(false);
				}
				else {
					currentPhaseLabel.text = theirName;
					currentSegLabel.text = "Choosing Defender";
				}
			break;

			case Phase.InviteAllies:

				if( isMyAction ) {
					currentPhaseLabel.text = "Invite";
					currentSegLabel.text = "Allies";
				}
				else {
					currentPhaseLabel.text = theirName;
					currentSegLabel.text = "Inviting Allies";
				}
			break;

			case Phase.AssignAttackers:
				if( isMyTurn ) {
					currentPhaseLabel.text = "Assign";
					currentSegLabel.text = "Attackers";
					LeanTween.cancel(attackDeclaration.gameObject);
					attackDeclaration.gameObject.SetActive(false);
				}
				else {
					currentPhaseLabel.text = theirName;
					currentSegLabel.text = "Assigning Attackers";
				}
			break;

			case Phase.AlliedAttackers:
				if( isMyAction ) {
					currentPhaseLabel.text = "Assign";
					currentSegLabel.text = "Attacking Allies";
				}
				else {
					currentPhaseLabel.text = theirName;
					currentSegLabel.text = "Assigning Allies";
				}
			break;

			case Phase.AssignDefenders:
				if( isMyTurn ) {
					currentPhaseLabel.text = theirName;
					currentSegLabel.text = "Assigning Defenders";
				}
				else {
					currentPhaseLabel.text = "Assign";
					currentSegLabel.text = "Defenders";
				}
			break;

			case Phase.AlliedDefenders:
				if( isMyAction ) {
					currentPhaseLabel.text = "Assign";
					currentSegLabel.text = "Defending Allies";
				}
				else {
					currentPhaseLabel.text = theirName;
					currentSegLabel.text = "Assigning Allies";
				}
			break;

			case Phase.Resolve:
				if( isMyTurn ) {
					if( currentPhase == Phase.Resolution ) {
						currentPhaseLabel.text = "Resolve";
						currentSegLabel.text = "Another Battle?";
					}
					else {
						currentPhaseLabel.text = "Resolve";
						currentSegLabel.text = "Which Battle?";
					}
				}
				else {
					currentPhaseLabel.text = "Choosing";
					currentSegLabel.text = "Battle";
				}
			break;

			case Phase.Engage:
				if( isMyAction ) {
					currentPhaseLabel.text = myName;
					currentSegLabel.text = "Engage Action";
				}
				else {
					currentPhaseLabel.text = theirName;
					currentSegLabel.text = "Engage Action";
				}
			break;

			case Phase.Battle:
				if( isMyAction ) {
					currentPhaseLabel.text = myName;
					currentSegLabel.text = "Battle Action";
				}
				else {
					currentPhaseLabel.text = theirName;
					currentSegLabel.text = "Battle Action";
				}
			break;

			case Phase.Resolution:
				currentPhaseLabel.text = "Battle";
				currentSegLabel.text = "Resolution";
			break;

			case Phase.Dynasty:
				if( isMyTurn ) {
					currentPhaseLabel.text = myName;
					currentSegLabel.text = "Dynasty";
				}
				else {
					currentPhaseLabel.text = theirName;
					currentSegLabel.text = "Dynasty";
				}

				attackDeclaration.gameObject.SetActive(false);
			break;

			case Phase.Discard:
				if( isMyTurn ) {
					currentPhaseLabel.text = "Discard?";
					currentSegLabel.text = "";
				}
				else {
					currentPhaseLabel.text = theirName;
					currentSegLabel.text = "Discard";
				}

				pass = false; // otherwise, next action phase only supports one action
			break;
		}
	}

	// Sun and moon orientation

	[PunRPC]
	public void SpectatorChangedPosition() {
		DeckDisplay closestDeck = null;
		DeckDisplay furthestDeck = null;
		float closestY = 999999999f;
		float furthestY = 0f;
		
		foreach( DeckDisplay deck in allDecks ) {
			Vector3 deckOnScreen = Camera.main.WorldToScreenPoint( deck.transform.position );
			if( deckOnScreen.y < closestY ) {
				closestDeck = deck;
				closestY = deckOnScreen.y;
			}
			if( deckOnScreen.y > furthestY ) {
				furthestDeck = deck;
				furthestY = deckOnScreen.y;
			}
		}

		int closestDeckOwner = Table.GetIDFor( closestDeck.photonView.owner );
		int furthestDeckOwner = Table.GetIDFor( furthestDeck.photonView.owner );

		myID = closestDeckOwner;
		myHonorID = closestDeckOwner;

		theirID = furthestDeckOwner;
		theirHonorID = furthestDeckOwner;

		RefreshIndicators();
	}

	void PlayerChangedPosition() {
		DeckDisplay nearestDeck = null;
		float nearestDistance = 99999999999f;
		foreach( DeckDisplay deck in allDecks ) {
			if( deck.photonView.isMine ) continue;

			float distanceToDeck = Vector3.Distance( Camera.main.transform.position, deck.transform.position );
			if( distanceToDeck < nearestDistance ) {
				nearestDeck = deck;
				nearestDistance = distanceToDeck;
			}		
		}

		if( nearestDeck ) {
			int deckOwner = Table.GetIDFor( nearestDeck.photonView.owner );
			if( nearestDeck.photonView.owner == null ) print("No owner for deck " + nearestDeck.name);

			theirID = deckOwner;
			theirHonorID = deckOwner;
		}

		RefreshIndicators();
	}

	// Multiplayer specific
	[PunRPC]
	public void SetDefenderID( int id ) {
		defenderID = id;
		print("Setting defender ID to " + id);
	}

	[PunRPC]
	public void SetAttackingAllies( int[] ids ) {
		alliedAttackers = ids;
	}

	[PunRPC]
	public void SetDefendingAllies( int[] ids ) {
		alliedDefenders = ids;
	}

	public void DeclareAttack() {
		Phase nextPhase = Phase.AssignAttackers;
		if( PlayerPrefs.GetInt( "disableSounds", 0 ) == 0 ) myTurnIndicator.attackChime.Play();

		if( curParticipants > 2 ) {
			nextPhase = Phase.ChooseDefender;
		}
		else {
			photonView.RPC("SetDefenderID", PhotonTargets.All, NextPlayer());
		}

		photonView.RPC("StartAction", PhotonTargets.All, whoseTurn, (int)nextPhase, false, -1);
	}

	public void TakeAction() {
		pass = false;
	}

	public void AddOneHonor() {
		if( spectating ) return;

		IncrementHonor( 1 );

		StartCoroutine( CountHonorGain(1) );
	}

	public void RemoveOneHonor() {
		if( spectating ) return;
		IncrementHonor( -1 );
		
		StartCoroutine( CountHonorGain(-1) );
	}

	public void IncrementHonor( int byQuantity ) {

		foreach( PhotonPlayer player in PhotonNetwork.playerList ) {
			if( player.customProperties["spectating"] != null ) continue;

			if( Table.GetIDFor(player) == overrideID ) {
				int newHonor = int.Parse( (string)player.customProperties["honor"] ) + byQuantity;
				PhotonHash properties = new PhotonHash();
				properties.Add( "honor", newHonor.ToString() );
				player.SetCustomProperties( properties );
			}
		}

		photonView.RPC("RefreshIndicators", PhotonTargets.All);
	}

	IEnumerator CountHonorGain( int increment ) {
		honorCount += increment;

		if( honorTimer == 0f ) {
			honorTimer = 1f;

			while( honorTimer > 0f ) {
				yield return null;

				honorTimer -= Time.deltaTime;
			}
			honorTimer = 0f;

			if( honorCount != 0 ) {
				string gainedOrLost = honorCount < 0 ? "lost" : "gained";
				chat.photonView.RPC("AddChatEntry", PhotonTargets.All, overrideID, Table.data.myName, "I " + gainedOrLost + " " + Mathf.Abs(honorCount) + " honor", (int)Persistent.data.ClanStringToEnum(Table.data.myClan) );
				honorCount = 0;
			}
		}
		else {
			honorTimer = 1f;
		}
	}

	public void FlipHand() {
		// if toggling from one hidden state to another, flip the hand
		myHand.photonView.RPC("FlipForSpectators", PhotonTargets.All);
	}

	[PunRPC]
	public void RemoveFromGame( int viewID ) {
		for( int index = 0; index < allCards.Count; index++ ) {
			CardDisplay cardToRemove = allCards[index];

			if( cardToRemove.photonView.viewID == viewID ) {
				if( cardToRemove.inHand ) myHand.photonView.RPC("RemoveCard", PhotonTargets.All, allCards[index].photonView.viewID);
				if( cardToRemove.inProvince ) RemoveFromProvinces( cardToRemove, false );
				if( cardToRemove.inFief ) RemoveFromFief( cardToRemove );
				if( cardToRemove.inStack ) cardToRemove.inStack.photonView.RPC("RemoveFromStack", PhotonTargets.All, cardToRemove.photonView.viewID, false);
				cardToRemove.StopAllCoroutines();
				break;
			}
		}
	}

	public CardDisplay FindCard( int viewID ) {
		for( int index = 0; index < allCards.Count; index++ ) {

			if( allCards[index].photonView.viewID == viewID ) {
				return allCards[index];
			}
		}

		return null;
	}

	public void ShowPreview( CardDisplay cardDisplay, Vector2 forceScreenPos = default(Vector2) ) {
		if( SettingsOpen() ) return;

		if( cardDisplay == null ) {
			previewImage.gameObject.SetActive(false);
			standInPreview.gameObject.SetActive(false);
			tokenPreview.ClearTokens();
			if( currentPreview && !currentPreview.gameObject.activeInHierarchy ) currentPreview.UnloadImage();
			currentPreview = null;
			return;
		}

		StartCoroutine( RenderPreview(cardDisplay, forceScreenPos) );
	}

	// show preview for existing card or force the preview to display based on a provided screen position (for displaying previews in lists, for example)
	IEnumerator RenderPreview( CardDisplay cardDisplay, Vector2 forceScreenPos ) {

		if( cardDisplay == null ) {
			watermark.gameObject.SetActive( false );
			
			yield break;
		}

		// Render second side if that's what's facing up
		Texture cardImage = null;
		if( !cardDisplay.faceUp && cardDisplay.card.secondSide != "" ) {
			cardImage = cardDisplay.myBack.material.mainTexture;
		}
		else {
			cardImage = cardDisplay.myFront.material.mainTexture;
		}

		if( cardDisplay is Favor ) {
			Favor favor = (Favor)cardDisplay;
			previewImage.texture = cardImage;
			previewImage.gameObject.SetActive(true);

			if( favor.displayingClan == Card.Clan.neutral || favor.displayingClan == Card.Clan.unaligned ) {
				standInPreview.CreateStandInForFavor( favor.displayingClan );
				standInPreview.gameObject.SetActive(true);
			}
			else {
				standInPreview.gameObject.SetActive(false);
				previewImage.gameObject.SetActive(true);
			}
		}
		else if( currentPreview != cardDisplay ) { // normal card preview

			currentPreview = cardDisplay;
			// print("Previewing card display " + cardDisplay.name);
			// print("Current preview is " + currentPreview.name);

			if( cardImage != null && !cardDisplay.standInNeeded && cardDisplay.card.images.Count > 0 && !cardDisplay.loadingImage ) { // if token or image is not loaded
				previewImage.texture = cardImage;
				standInPreview.gameObject.SetActive(false);

				previewImage.gameObject.SetActive(true);
			}
			else {
				yield return StartCoroutine( currentPreview.DownloadImage() );
				if( cardDisplay.standInNeeded ) {
					standInPreview.CreateStandInCardFrom( cardDisplay.card );
					standInPreview.gameObject.SetActive(true);
				}

				previewImage.gameObject.SetActive(true);
			}

			tokenPreview.PreviewTokensFor( cardDisplay.tokens );

			Persistent.data.FrameRateUpFor( 1f );

			if( currentPreview ) {
				if( currentPreview.card != null ) {
					if( !currentPreview.faceUp && currentPreview.card.secondSide == "" ) {
						string myOrYours = "my";
						if( !currentPreview.IsMine() ) myOrYours = "your";
						Table.data.chat.photonView.RPC("AddChatEntry", PhotonTargets.Others, overrideID, Table.data.myName, "I peeked at " + myOrYours + " face down card", (int)Persistent.data.ClanStringToEnum(Table.data.myClan) );
					}
				}

				if( currentPreview.card != null && !currentPreview.standInNeeded ) {
					// if the card is external
					if( cardDisplay.card.encrypted && watermark ) {
						watermark.text = Persistent.data.playtestGroup;
						watermark.gameObject.SetActive( true );
					}
					else {
						if( watermark ) watermark.gameObject.SetActive( false );
					}
				}
			}
		}

		if( forceScreenPos == default(Vector2) ) PositionPreview3D( cardDisplay );
		else PositionPreview2D( forceScreenPos );

		if( currentPreview != cardDisplay ) yield break; // don't capture card image if the loaded image has changed
	}

	void PositionPreview3D( CardDisplay cardDisplay ) {
		Vector3 cardPosOnScreen = Camera.main.WorldToScreenPoint(cardDisplay.transform.position);
		Vector3 cardExtents = Vector3.zero;
		Vector2 hedge = Vector2.zero;

		float yAdjust = 0f;
		float xAdjust = 0f;
		float maxExtents = cardDisplay.LocalBounds().extents.z;

		float oneThirdScreenHeight = Screen.height / 3f;
		float halfScreenWidth = Screen.width / 2f;

		if( cardPosOnScreen.y > oneThirdScreenHeight * 2f ) {
			yAdjust = -previewPanel.sizeDelta.y / 2f;
			cardExtents += new Vector3( 0f, 0f, -maxExtents );
			hedge.y = -previewHedge;
		}
		else if( cardPosOnScreen.y < oneThirdScreenHeight ) {
			yAdjust = previewPanel.sizeDelta.y / 2f;
			cardExtents += new Vector3( 0f, 0f, maxExtents );
			hedge.y = previewHedge;

			if( cardDisplay.inHand ) {}
		}
		else { // if card is in center third of screen
			if( cardPosOnScreen.x > halfScreenWidth ) {
				xAdjust = -previewPanel.sizeDelta.x / 2f;
				cardExtents += new Vector3( -maxExtents, 0f, 0f );
				hedge.x = -previewHedge;
			}
			else {
				xAdjust = previewPanel.sizeDelta.x / 2f;
				cardExtents += new Vector3( maxExtents, 0f, 0f );
				hedge.x = previewHedge;
			}
		}

		if( cardDisplay.bowed ) cardExtents = new Vector3( cardExtents.z, cardExtents.y, cardExtents.x );
		Vector3 worldPointExtents = cardDisplay.transform.TransformPoint(cardExtents);
		Vector3 extentsPos = Camera.main.WorldToScreenPoint( worldPointExtents );
		Vector2 relPos = new Vector2( extentsPos.x, extentsPos.y ) + new Vector2( xAdjust,yAdjust ) + hedge;

		// Keep preview on screen
		if( relPos.y - previewPanel.sizeDelta.y / 2f < 0f ) {
			relPos.y = previewPanel.sizeDelta.y / 2f;
		}
		else if( relPos.y + previewPanel.sizeDelta.y / 2f > Screen.height ) {
			relPos.y = Screen.height - previewPanel.sizeDelta.y / 2f;
		}

		if( relPos.x - previewPanel.sizeDelta.x / 2f < 0f ) {
			relPos.x = previewPanel.sizeDelta.x / 2f;
		}
		else if( relPos.x + previewPanel.sizeDelta.x / 2f > Screen.width ) {
			relPos.x = Screen.width - previewPanel.sizeDelta.x / 2f;
		}

		previewPanel.anchoredPosition = relPos;
	}

	void PositionPreview2D( Vector2 relPos ) {

		// Keep preview on screen
		if( relPos.y - previewPanel.sizeDelta.y / 2f < 0f ) {
			relPos.y = previewPanel.sizeDelta.y / 2f;
		}
		else if( relPos.y + previewPanel.sizeDelta.y / 2f > Screen.height ) {
			relPos.y = Screen.height - previewPanel.sizeDelta.y / 2f;
		}

		if( relPos.x - previewPanel.sizeDelta.x / 2f < 0f ) {
			relPos.x = previewPanel.sizeDelta.x / 2f;
		}
		else if( relPos.x + previewPanel.sizeDelta.x / 2f > Screen.width ) {
			relPos.x = Screen.width - previewPanel.sizeDelta.x / 2f;
		}

		previewPanel.anchoredPosition = relPos;
	}

	public override void DragActionStart( InputInfo input ) {
		if( input.Matches( InputInfo.Action.Pan ) ) StartCoroutine( Pan( input ) );

		if( tokenPanel.gameObject.activeInHierarchy ) tokenPanel.Dismiss();
	}

	public override void DragActionEnd( InputInfo input ) {
		navigating = false;
	}

	public IEnumerator Pan( InputInfo input ) {
		if( !deckListUI.panel.displaying && !optionsUI.displaying ) navigating = true;

		origClickPos = input.inputPos;
		origCamPos = cameraPivot.position;

		while( navigating ) {
			float deltaX = (input.inputPos.x - origClickPos.x) * navigationSpeed * Camera.main.transform.position.y;
			float deltaZ = (input.inputPos.y - origClickPos.y) * navigationSpeed * Camera.main.transform.position.y;

			float yRot = spectating || PhotonNetwork.offlineMode ? cameraPivot.eulerAngles.y - 180f : MyYAngle();
			cameraPivot.position = origCamPos - Quaternion.Euler( new Vector3(0f, yRot, 0f) ) * new Vector3(deltaX,0f,deltaZ);

			CameraMoved();

			Persistent.data.FrameRateUpFor( 0.5f );

			yield return null;
		}

		if( spectating ) SpectatorChangedPosition();
		else PlayerChangedPosition();
	}

	public override void ClickActionUp( InputInfo inputInfo ) {

		if( deckListUI.panel.displaying ) {
			deckListUI.Dismiss();
		}

		if( optionsUI.displaying ) {
			print("Clicking up " + Time.time);
			optionsUI.Toggle(); // dismiss
		}

		if( proxyPanel.IsVisible() ) {
			proxyPanel.Cancel();
		}

		if( concedePanel.gameObject.activeInHierarchy ) concedePanel.FurlNow();

		if( removalPanel.IsVisible() ) removalPanel.Cancel();

		if( tokenPanel.gameObject.activeInHierarchy ) tokenPanel.Dismiss();

		if( changeDeckList.displaying ) {
			changeDeckSpinner.SetActive( false );
			changeDeckList.Toggle();
		}

		if( cardBeingDragged ) {
			cardBeingDragged.DragActionEnd( inputInfo );
		}

		if( inputInfo.Matches( InputInfo.Action.Token ) ) EditProxy( null, inputInfo.hit.point );
		if( inputInfo.Matches( InputInfo.Action.Deselect ) ) DeselectAll();
		if( inputInfo.Matches( InputInfo.Action.UnbowAndFlipAll ) ) UnbowAndFlipAll();

		base.ClickActionUp( inputInfo );
	}

	public bool SettingsOpen() {
		return 
			optionsUI.displaying || 
			proxyPanel.IsVisible() || 
			concedePanel.gameObject.activeInHierarchy || 
			removalPanel.IsVisible() || 
			tokenPanel.gameObject.activeInHierarchy || 
			changeDeckList.displaying;
	}

	public override void Hover( InputInfo inputInfo ) {
		ShowPreview( null );
	}

	public void DeselectAll() {
		for( int index = 0; index < allCards.Count; index++ ) {

			if( allCards[index].IsMine() ) {
				allCards[index].photonView.RPC("DeleteGuidesFromTo", PhotonTargets.All, true, false );

				if( allCards[index].selected > -1 ) allCards[index].photonView.RPC("Deselect", PhotonTargets.All);
			}
		}
	}

	void Update() {
		if( navigating ) {
			if( refreshGuideTimer > refreshGuideRate ) {
				for(int index = 0; index < allCards.Count; index++) {
					allCards[index].RefreshGuides();
				}

				refreshGuideTimer = 0f;
			}
			else {
				refreshGuideTimer += Time.deltaTime;
			}
		}

		if( Input.GetKeyDown(KeyCode.Escape) ) {
			if( deckListUI.panel.displaying ) deckListUI.Dismiss();
			else if( proxyPanel.IsVisible() ) proxyPanel.Cancel();
			else if( spectating ) {
				if( concedePanel.gameObject.activeInHierarchy ) concedePanel.FurlNow();
				else concedePanel.UnfurlNow();
			}
			else changeDeckList.Toggle();
		}

		// if( Input.GetKeyDown( KeyCode.J ) ) {
		// 	StartCoroutine( ReconnectToGame() );
		// }

		// if( Input.GetKeyDown( KeyCode.D ) ) {
		// 	PhotonNetwork.Disconnect();
		// 	connectionFailed = true;
		// }

		// float texOffset = (guideMaterial.mainTextureOffset.x - Time.deltaTime * guideOffsetSpeed) % 10f;
		// guideMaterial.mainTextureOffset = new Vector2( texOffset, 0f );

	}

	void OnConnectionFail( DisconnectCause cause ) {
		if( !spectating ) {
			connectionFailed = true;
			StartCoroutine( ReconnectToGame() );
		}
		else {
			SceneManager.LoadScene("Main Menu");
		}
		print("Connection in established game failed because of the following exception: " + cause.ToString() );
	}

	IEnumerator ReconnectToGame() {
		reconnecting = true;

		// Serialize table state
		Dictionary<int,SerializedCardDisplay> serializedCards = new Dictionary<int,SerializedCardDisplay>();
		List<int> myCardIDs = new List<int>();
		foreach( CardDisplay card in allCards ) {
			if( card is Favor ) {}
			else {
				serializedCards.Add( card.photonView.viewID, card.Serialized() );
			}
		}
		print("Serialized " + allCards.Count + " cards, " + myCardIDs.Count + " of which were mine");

		Dictionary<int,SerializedDeckDisplay> serializedDecks = new Dictionary<int,SerializedDeckDisplay>();
		foreach( DeckDisplay deck in allDecks ) {
			serializedDecks.Add( deck.photonView.viewID, deck.Serialized() );
		}

		Hand[] hands = GameObject.FindObjectsOfType( typeof(Hand) ) as Hand[];
		int myHandId = -1;
		Dictionary<int,SerializedHand> serializedHands = new Dictionary<int,SerializedHand>();
		foreach( Hand hand in hands ) {
			if( hand == myHand ) {
				myHandId = hand.photonView.viewID;
				print("Found my hand with ID " + myHandId);
			}
			serializedHands.Add( hand.photonView.viewID, hand.Serialized() );
		}

		List<int> serializedProvinces = new List<int>();
		for( int index = 0; index < provinces.Count; index++ ) serializedProvinces.Add( provinces[index].photonView.viewID );

		List<int> serializedFief = new List<int>();
		for( int index = 0; index < fief.Count; index++ ) serializedFief.Add( fief[index].photonView.viewID );

		List<List<int>> serializedRegions = new List<List<int>>();
		for( int index = 0; index < regions.Count; index++ ) {
			List<int> regionsInProvince = new List<int>();
			serializedRegions.Add( regionsInProvince );

			for( int subIndex = 0; subIndex < regions[index].Count; subIndex++ ) {
				regionsInProvince.Add( regions[index][subIndex].photonView.viewID );
			}
		}

		// Misc table info
		Vector3 oldPivotPos = cameraPivot.position;
		Quaternion oldPivotRot = cameraPivot.rotation;
		Vector3 oldCameraPos = Camera.main.transform.position;

		// Destroy all old objects
		foreach( CardDisplay card in allCards ) {
			if( card is Favor ) {}
			else {
				Destroy( card.gameObject );
			}
		}
		allCards = new List<CardDisplay>();

		foreach( DeckDisplay deck in allDecks ) {
			Destroy( deck.gameObject );
		}
		allDecks = new List<DeckDisplay>();

		foreach( Hand hand in hands ) {
			Destroy( hand.gameObject );
		}

		// Reconnect

		PhotonNetwork.Disconnect();

		string resumeGameName = PlayerPrefs.GetString("resumeGameName", "");

		if( resumeGameName != "" ) {
			if( PhotonNetwork.connectionStateDetailed == ClientState.PeerCreated ) {
				PhotonNetwork.ConnectUsingSettings(ConnectToCloud.version);
			}
			else print( "Peer not created " + Time.time );

			waitingButton.UnfurlNow();

			while( !PhotonNetwork.connectedAndReady ) {
				print("Waiting to connect to photon " + Time.time);
				if (PhotonNetwork.connectionState == ConnectionState.Connecting) {
					waitingText.text = "Reconnecting ...";
				}

				Persistent.data.FrameRateUpFor(0.5f);
				yield return new WaitForSeconds(0.5f);

				if( connectionFailed ) {
					// display error and allow repeat connection:
					print("Failed.  Reconnecting.");
					connectionFailed = false;
					PhotonNetwork.ConnectUsingSettings(ConnectToCloud.version);
					continue;
				}
			}

			while (PhotonNetwork.connectionStateDetailed != ClientState.JoinedLobby) yield return null;

			PhotonNetwork.JoinRoom( resumeGameName );

			while( PhotonNetwork.connectionStateDetailed != ClientState.Joined ) yield return null;

			print("Successfully rejoined room " + Time.time);

			// Deserialize

			DeserializeCards( serializedCards );
			DeserializeDecks( serializedDecks );

			foreach( int handID in serializedHands.Keys ) {
				Hand newHand = PhotonView.Find( handID ).GetComponent<Hand>();
				newHand.DeserializeHand( serializedHands[handID] );

				if( newHand.photonView.viewID == myHandId ) {
					print(newHand.name + " being made mine " + Time.time);
					myHand = newHand;

					myHand.transform.parent = Camera.main.transform;
					myHand.transform.localPosition = Table.data.myHandLocalPos;
					myHand.transform.localRotation = Quaternion.identity;

					myHand.ArrangeCards();

					myHand.photonView.TransferOwnership( PhotonNetwork.player );
				}
				else {
					newHand.faceUp = false;
				}
			}

			provinces = new List<CardDisplay>();
			for( int index = 0; index < serializedProvinces.Count; index++ ) {
				CardDisplay deserializedProvince = PhotonView.Find( serializedProvinces[index] ).GetComponent<CardDisplay>();

				provinces.Add( deserializedProvince );
			}

			fief = new List<CardDisplay>();
			for( int index = 0; index < serializedFief.Count; index++ ) {
				CardDisplay deserializedFief = PhotonView.Find( serializedFief[index] ).GetComponent<CardDisplay>();

				fief.Add( deserializedFief );
			}

			regions = new List<List<CardDisplay>>();
			for( int index = 0; index < serializedRegions.Count; index++ ) {
				List<CardDisplay> regionsInProvince = new List<CardDisplay>();
				regions.Add( regionsInProvince );
				for( int subIndex = 0; subIndex < serializedRegions[index].Count; subIndex++ ) {
					regionsInProvince.Add( PhotonView.Find( serializedRegions[index][subIndex] ).GetComponent<CardDisplay>() );
				}
			}

			cameraPivot.position = oldPivotPos;
			cameraPivot.rotation = oldPivotRot;
			Camera.main.transform.position = oldCameraPos;

			print("New honor is " + (string)PhotonNetwork.player.customProperties["honor"]);

			waitingButton.FurlNow();

			// You loaded nicely, so tell the room that it's no longer broken
			disconnected = false;
		}
		else {
			print("Trying to resume game, but no prior room name saved");
		}

		reconnecting = false;
	}

	public void OnFailedToConnectToPhoton(object parameters) {
        connectionFailed = true;
        Debug.Log("OnFailedToConnectToPhoton. StatusCode: " + parameters);
    }

	[PunRPC]
	public void Reset() {

		finishedResetting = 0;

		print("Board reset! " + Time.time);

		StopAllCoroutines(); // cease waiting for other players

		// reset room parameters

		if( isMasterPlayer ) {
			PhotonHash roomProperties = new PhotonHash();
			roomProperties.Add( "clan", Persistent.data.ClanStringToEnum(myClan) );
			roomProperties.Add( "password", "" );
			roomProperties.Add( "legality", myDeck.legality.ToString() );

			PhotonNetwork.room.SetCustomProperties( roomProperties );
		}

		ClearAllSelections();
		DisableAllGuides();
		myTurnIndicator.turnCounter = 1;
		attackDeclaration.gameObject.SetActive(false);

		print("Player with id " + overrideID + "(" + PhotonNetwork.player.ID + ") destroying all objects that they own.");
		if( PhotonNetwork.player.isMasterClient ) {
			PhotonNetwork.DestroyAll();
			disconnected = false;
		}

		ResetMyYAngle();

		// initialize all to default values
		allCards = new List<CardDisplay>();
		allDecks = new List<DeckDisplay>();
		provinces = new List<CardDisplay>();
		fief = new List<CardDisplay>();
		regions = new List<List<CardDisplay>>(4);
		selectedCards = new List<CardDisplay>();

		myStartingHonor = 0;

		pass = true;
		otherPlayersPassed = 0;
		playersFinishedSettingUp = 0;
		firstTurn = true;
		currentPhase = Phase.Action;
		prevPhase = Phase.Action;
		startState = StartState.notInitialized;
		cameraPivot.rotation = Quaternion.Euler(320f, 180f, 180f);
		cameraPivot.position = Vector3.zero;

		theirID = -1;
		whoseTurn = -1;
		currentPhase = Phase.Action;

		// finished initialization

		// reset favor
		imperialFavor.Reset();
		waitingText.text = "Waiting for Players ...";

		if( playmat ) PhotonNetwork.Destroy( playmat.gameObject );

		Resources.UnloadUnusedAssets();

		if( spectating ) return;
		tournamentReport.Reset();

		// the new deck must be set in the calling method
		myClan = myDeck.clan;

		// finished changing room parameters

		TableResetEvent( myClan );

		PlayerPrefs.SetInt("resetting", 1);
		photonView.RPC("FinishedResetting", PhotonTargets.All);
	}

	[PunRPC]
	public void HardReset() {
		// Called when you're the only player in the room
		PhotonNetwork.DestroyAll();
		disconnected = false;

		PlayerPrefs.SetInt("resetting", 1);
		PhotonNetwork.LoadLevel("Card Table");
	}

	[PunRPC]
    public void FinishedResetting() {
    	finishedResetting++;

    	if( finishedResetting != curParticipants ) return;

    	StartCoroutine( WaitForOtherPlayers() );

		if( changeDeckList.displaying ) {
			changeDeckSpinner.SetActive( false );
			changeDeckList.Toggle();
		}
    }

	public void Concede() {

		PhotonHash myOrder = new PhotonHash();
		myOrder.Add( "overrideID", null );
		PhotonNetwork.player.SetCustomProperties( myOrder );

		PhotonNetwork.LeaveRoom();
		PhotonNetwork.Disconnect();

		SceneManager.LoadScene("Main Menu");
	}

	void OnPhotonPlayerDisconnected( PhotonPlayer otherPlayer ) {

		string connectedPlayerName = (string)otherPlayer.customProperties["name"];
		if( connectedPlayerName == null ) connectedPlayerName = "N/A";
		string connectedPlayerType = otherPlayer.customProperties["spectating"] == null ? "Participant" : "Spectator";
		print(connectedPlayerType + " with name " + connectedPlayerName + " disconnected from game");

		if( isMasterPlayer ) {
			string myName = (string)PhotonNetwork.player.customProperties["name"];
			print("Master player " + myName + " (ID#" + overrideID + ") handling it");
			curParticipants = curParticipants; // for the room
			RenameRoom();
		}

		// if you're a spectator in an emptied game
		if( spectating ) {
			if( curParticipants == 0 ) {
				print("Because number of participants is 0, MC conceding");
				Concede();
			}

			return;
		}

		// if this is not multiplayer, then make room for another challenger
		if( maxParticipants > 2 && startState == StartState.initialized ) { }
		else if( curParticipants == 1 ) {
			// Put up waiting for players dialogue
			waitingButton.UnfurlNow();
			waitingText.text = "Opponent Disconnected...";
			if( isMasterPlayer ) {
				// Potentially lift block on spectators if the room was cleared
				if( PlayerPrefs.GetInt( "forbidspectators", 0 ) != 1 ) {
					PhotonHash roomProperties = new PhotonHash();
					roomProperties.Add( "forbidSpectators", null );
					PhotonNetwork.room.SetCustomProperties( roomProperties );
				}

				if( startState == StartState.initialized ) {
					disconnected = true;
				}
			}
		}
	}

	void RenameRoom() {
		// rename room
		string roomTag = "[";
		for( int index = 0; index < PhotonNetwork.playerList.Length; index++ ) {
			if( PhotonNetwork.playerList[index].customProperties["spectating"] != null ) continue;

			roomTag += PhotonNetwork.playerList[index].customProperties["name"];
			if( index < PhotonNetwork.playerList.Length - 1 ) roomTag += " vs ";
		}
		roomTag += "] ";

		string existingName = (string)PhotonNetwork.room.customProperties["name"];
		if( existingName != null ) {
			PhotonHash nameHash = new PhotonHash();
			nameHash.Add( "name", roomTag + existingName.Substring( existingName.LastIndexOf("]") + 2 ) );

			PhotonNetwork.room.SetCustomProperties( nameHash );
		}
	}

	public bool CardNearProvinces( CardDisplay card ) {
		Vector3 relativeToDynasty = myDynasty.transform.InverseTransformPoint( card.transform.position );
		Vector3 relativeToFate = myFate.transform.InverseTransformPoint( card.transform.position );
		if( Mathf.Abs(relativeToDynasty.z) < card.LocalBounds().extents.z && relativeToDynasty.x > 0f && relativeToFate.x < 0f ) {
			return true;
		}

		return false;
	}

	public bool CardNearFief( CardDisplay card ) {
		Vector3 relativeToFief = myDynasty.transform.InverseTransformPoint( card.transform.position );
		if( Mathf.Abs(relativeToFief.z - strongholdDistFromDyn) < card.LocalBounds().size.z && relativeToFief.x > 0f ) {
			return true;
		}

		return false;
	}

	public void AddToProvinces( CardDisplay card, int provIndex = -1 ) {
		int existingIndex = provinces.IndexOf( card );
		List<CardDisplay> regionsToRescue = new List<CardDisplay>();
		if( existingIndex > -1 ) {
			provinces.RemoveAt( existingIndex );
			regionsToRescue = regions[existingIndex];
			regions.RemoveAt( existingIndex );
		}

		if( provinces.Count == 0 ) {
			provinces.Add( card );
			regions.Add( new List<CardDisplay>() );
		}
		else if( provIndex == -1 ) { // if prov added fresh
			for( int index = 0; index < provinces.Count; index++ ) {

				if( myDynasty.transform.InverseTransformPoint(card.transform.position).x < 
					myDynasty.transform.InverseTransformPoint(provinces[index].transform.position).x ) {
					provinces.Insert( index, card );

					regions.Insert( index, regionsToRescue );
					break;
				}
			}
			// if not behind any other cards, insert at end of list
			if( !provinces.Contains(card) ) {
				provinces.Add( card );
				regions.Add( regionsToRescue );
			}
		}
		else {
			provinces.Insert( provIndex, card );
		}

		if( !card.inProvince && !(card.bowed && card.card.type == "holding" && card.card.text.ToLower().Contains("fortification") && !card.faceUp) ) card.Unbow();

		card.inProvince = true;

		ArrangeProvinces();
	}

	public void RemoveFromProvinces( CardDisplay card, bool refillProvince = true ) {
		int origCardIndex = provinces.IndexOf(card);

		provinces.RemoveAt( origCardIndex );

		card.inProvince = false;

		if( refillProvince && myDynasty.cards.Count > 0 ) {
			CardDisplay refill = myDynasty.cards[myDynasty.cards.Count - 1];
			myDynasty.photonView.RPC("RemoveCardFromDeck", PhotonTargets.All, refill.photonView.viewID);
			AddToProvinces( refill, origCardIndex );
		}
		else {
			if( regions[origCardIndex].Count > 0 ) {
				CardDisplay regionToPromote = regions[origCardIndex][0];
				regions[origCardIndex][0].attachedToProv = -1;
				regions[origCardIndex].RemoveAt(0);
				AddToProvinces( regionToPromote, origCardIndex );
			}
			else {
				regions.RemoveAt( origCardIndex );
			}
		}

		ArrangeProvinces();
	}

	public void ArrangeProvinces() {

		for( int index = 0; index < provinces.Count; index++ ) {
			CardDisplay card = provinces[index];

			Vector3 provincePos = myDynasty.transform.TransformPoint( new Vector3( Table.data.distanceBetweenDecks / (provinces.Count + 1) * (index + 1), 0f , 0f ) );

			card.photonView.RPC("MoveTo", PhotonTargets.All, provincePos, 0.5f);

			int iterator = 1;
			foreach( CardDisplay region in regions[index] ) {
				region.photonView.RPC("MoveTo", PhotonTargets.All, provincePos + (myDynasty.transform.forward * regionHedge - new Vector3(0f,0.005f,0f)) * iterator, 0.5f);
				iterator++;
			}
		}
	}

	public void ReplaceRegionWith( CardDisplay card ) {

		if( !card.inProvince ) {

			CardDisplay region = null;
			for( int index = 0; index < provinces.Count; index++ ) {

				if( (provinces[index].card.type == "region" ||
					(provinces[index].card.type == "holding" && provinces[index].card.text.ToLower().Contains("fortification"))) 
					&& provinces[index].faceUp ) {

					Vector3 relativeLoc = provinces[index].transform.InverseTransformPoint(card.transform.position);
					if( Mathf.Abs(relativeLoc.z) < card.LocalBounds().extents.z && Mathf.Abs(relativeLoc.x) < card.LocalBounds().extents.x ) {
						print(card.gameObject.name + " near region " + provinces[index].gameObject.name + " by " + Mathf.Abs(relativeLoc.z) + " Z and " + Mathf.Abs(relativeLoc.x) + " X " + Time.time);
						
						region = provinces[index];
						regions[index].Add(region);
						region.inProvince = false;
						region.inRegion = true;
						region.attachedToProv = index;
						provinces.RemoveAt(index);

						provinces.Insert(index, card);
						card.inPlay = false;
						card.inProvince = true;

						ArrangeProvinces();
						print("Adding " + region.gameObject.name + " to regions in province " + index);
						return;
					}
				}
			}
		}

		AddToProvinces( card );
	}

	public void RemoveFromRegions( CardDisplay regionToRemove ) {
		foreach( List<CardDisplay> regionsList in regions ) {
			for( int index = 0; index < regionsList.Count; index++ ) {
				if( regionsList[index] == regionToRemove ) {
					regionToRemove.attachedToProv = -1;
					regionsList.RemoveAt( index );
					return;
				}
			}
		}
	}

	public void AddToFief( CardDisplay card ) {
		// stack card if there is an identical holding already in the fief
		foreach( CardDisplay holding in fief ) {
			if( holding != card && holding.card.name == card.name ) {
				StartCoroutine( holding.StackCard(card) );
				if( card.card.type == "holding" && !card.inPlay ) card.Bow();
				card.inPlay = true;
				return;
			}
		}

		// otherwise, arrange it with the others
		fief.Remove( card );

		if( fief.Count == 0 ) fief.Add( card );
		for( int index = 0; index < fief.Count; index++ ) {

			if( myDynasty.transform.InverseTransformPoint(card.transform.position).x < 
				myDynasty.transform.InverseTransformPoint(fief[index].transform.position).x ) {
				fief.Insert( index, card );
				print(card.gameObject.name + " is behind " + fief[index].gameObject.name);
				break;
			}
		}
		if( !fief.Contains(card) ) fief.Add( card );

		if( !card.inFief && card.card.type == "holding" && !card.inPlay  ) card.Bow();
		card.inFief = true;
		card.inPlay = true;

		ArrangeFief();
	}

	public void RemoveFromFief( CardDisplay card ) {
		int origCardIndex = fief.IndexOf(card);
		if( origCardIndex == -1 ) return;
		print("Removing " + card.gameObject.name + " from fief with a count of " + fief.Count + " at index " + origCardIndex);

		fief.RemoveAt( origCardIndex );

		card.inFief = false;

		ArrangeFief();
	}

	public void ArrangeFief() {

		for( int index = 0; index < fief.Count; index++ ) {
			CardDisplay card = fief[index];

			Vector3 fiefPos = myDynasty.transform.TransformPoint( new Vector3(holdingsRightOfStrong * (index+1), CardDisplay.staticDistFromFloor * (card.stack.Count + 1), strongholdDistFromDyn) );

			card.photonView.RPC("MoveTo", PhotonTargets.All, fiefPos, 0.5f);

			// make sure the opponent's view of your fief is rearranged properly (stacks are updated) when yours is
			card.AdjustStackOnly( fiefPos, 0.5f );
		}
	}

	public void EditProxy( CardDisplay card, Vector3 atPoint ) {
		proxyPanel.Reveal( atPoint, card );
	}

	public void EditToken( Token token ) {
		tokenPanel.Reveal( token );
	}

	public void ShowRadialMenu( RadialButton[] withButtons, Clickable forTarget ) {
		radialMenu.Initialize( withButtons, forTarget, input.inputInfo.inputPos );
	}

	public void UnbowAndFlipAll() {
		print("Trying to flip over all cards " + Time.time);
		bool cleanUp = false;
		for( int index = 0; index < allCards.Count; index++ ) {
			if( allCards[index] == null ) {
				print("Trying to flip over null card display " + Time.time);
				cleanUp = true;
				continue;
			}
			else if( allCards[index].photonView == null ) {
				print("Trying to flip over card display " + allCards[index].name + " with no attached photonView! " + Time.time);
				continue;
			}

			CardDisplay card = allCards[index];

			if( card.IsMine() && !card.inHand && !card.inDeck && card.myFront.enabled && !(card is Favor) && (!card.card.isProxy || card.faceUp) ) {
				card.Unbow();

				if( !card.isFate && !card.faceUp && myDynasty.transform.InverseTransformPoint( card.transform.position ).z < 1.5f ) { // if is a dynasty card in the fief
					card.photonView.RPC("FlipOver", PhotonTargets.All, 0.5f);
				}
			}
		}

		if( cleanUp ) CleanUpCards();
	}

	[PunRPC]
	public void DisableAllGuides() {
		for( int index = 0; index < allCards.Count; index++ ) {

			if( allCards[index].guides.Count > 0 ) {
				foreach( GuideLine guide in allCards[index].guides ) {
					guide.active = false;
					guide.SetEnds(null, null, Vector3.zero, Vector3.zero);
				}
				foreach( GuideLine arrow in allCards[index].arrows ) {
					arrow.active = false;
				}
				allCards[index].attachedGuides = new List<GuideLine>();
				allCards[index].attachedArrows = new List<GuideLine>();
			}
		}
	}

	[PunRPC]
	public void ClearAllSelections() {
		for( int index = 0; index < allCards.Count; index++ ) {

			if( allCards[index].selected > -1 ) allCards[index].Deselect();
		}

		selectedCards = new List<CardDisplay>();
	}

	public void CameraMoved() {
		if( CameraMovedEvent != null ) CameraMovedEvent();
	}

	public bool IsMyAction() {
		return PhotonNetwork.offlineMode ? true : myTurnIndicator.isMyAction;
	}

	void OnDestroy() {
		Table.data = null;
	}

	// Spectators

	public int NextPlayer() {

		if( PhotonNetwork.offlineMode == true ) return myID;

		print("Looking for next player after " + myID);

		// Sort players by their override ID
		List<int> sortedIDs = new List<int>();
		foreach( PhotonPlayer player in PhotonNetwork.playerList ) {
			if( player.customProperties["spectating"] != null ) continue; // don't count spectators

			sortedIDs.Add( Table.GetIDFor( player ) );
		}
		sortedIDs.Sort();

		// Find my index
		int myIndex = -1;
		for( int index = 0; index < sortedIDs.Count; index++ ) {
			if( sortedIDs[index] == Table.GetIDFor( PhotonNetwork.player ) ) {
				myIndex = index;
				break;
			}
		}

		int nextIndex = myIndex + 1;
		if( nextIndex == sortedIDs.Count ) nextIndex = 0;

		print("Determined my id is " + sortedIDs[myIndex] + " out of " + sortedIDs.Count + " players, so next index is " + sortedIDs[nextIndex] + " " + Time.time);

		return sortedIDs[nextIndex];
	}

	// Utility

	public static int GetIDFor( PhotonPlayer player ) {
		if( player == null ) {
			print("Getting id for null player");
			if( PhotonNetwork.player == null ) {
				print("I am a null player! Returning myID as a guess." + Time.time);
				return Table.data.myID;
			}

			return -1;
		}

		int order = -1;
		int.TryParse( (string)player.customProperties["overrideID"], out order );
		if( order < 1 ) order = player.ID;

		return order;
	}

	public void CleanUpCards() {
		for( int index = 0; index < allCards.Count; index++ ) {
			CardDisplay card = allCards[index];

			if( card == null ) {
				allCards.Remove( card );
				CleanUpCards();
				return;
			}
			else if( card.photonView == null ) {
				print("Destroying " + card.name + " because it has no attached photonView for some reason " + Time.time);
				allCards.Remove( card );
				Destroy( card.gameObject );
				CleanUpCards();
				return;
			}
		}
	}

	// Scene serialization

	[PunRPC]
	public void DeckRequested( string tourneyName, PhotonMessageInfo info ) {
		if( spectating ) return;

		byte[] deckAsBytes = Encoding.ASCII.GetBytes( myDeck.GetSlush() );
		byte[] compressedDeck = LZMAtools.CompressByteArrayToLZMAByteArray( deckAsBytes );

		// lock deck
		FileInfo oldDeck = new FileInfo( myDeck.pathToDeckOnDisk );
		int extensionIndex = myDeck.pathToDeckOnDisk.IndexOf( ".txt" );
		if( extensionIndex != -1 ) {

			print("Old deck path is " + myDeck.pathToDeckOnDisk);
			myDeck.pathToDeckOnDisk = myDeck.pathToDeckOnDisk.Substring( 0, extensionIndex ) + " - in " + tourneyName + " Tourney.snm";
			print("New deck path is set to " + myDeck.pathToDeckOnDisk);

			File.WriteAllBytes( myDeck.pathToDeckOnDisk, compressedDeck );
			myDeck.deckOnDisk = new FileInfo( myDeck.pathToDeckOnDisk );
			print("Deck on disk file is set to be located at " + myDeck.deckOnDisk.FullName);

			oldDeck.Delete();
		}

		// send deck to host
		print("My deck was requested by " + (string)info.sender.customProperties["name"] );

		photonView.RPC("DeckDelivered", info.sender, compressedDeck, myClan );
	}

	[PunRPC]
	public void DeckDelivered( byte[] incomingDeck, string deckClan, PhotonMessageInfo info ) {

		print("Tournament host receiving deck from other player " + Time.time);

		incomingDeck = LZMAtools.DecompressLZMAByteArrayToByteArray(incomingDeck);
		string deckSlush = Encoding.ASCII.GetString( incomingDeck );
		string senderName = (string)info.sender.customProperties["name"];

		TournamentData tournamentData = new TournamentData {
			playerName = senderName,
			playerClan = deckClan,
			deckList = deckSlush,
		};

		tournamentReport.RegisterDeck( tournamentData );

		print("Received tournament deck registered with report.  " + tournamentReport.registeredDecks + " received out of " + maxParticipants + " expected");
		if( tournamentReport.registeredDecks == maxParticipants ) {
			tournamentReport.Send();
		}
	}

	[PunRPC]
	public IEnumerator SetCustomFateCardBack( string cardbackURL, PhotonMessageInfo info ) {
		int senderID = GetIDFor( info.sender );

		if( !customFateBacks.ContainsKey(senderID) ) {

			WWW request = new WWW( cardbackURL );
			yield return request;

			if( string.IsNullOrEmpty( request.error ) ) {
				print("Loaded new custom fate card back - loading into " + allCards.Count);
				customFateBacks.Add( senderID, request.textureNonReadable );

				// set all existing cards to have this fate back
				foreach( CardDisplay cardDisplay in allCards ) {
					if( GetIDFor(cardDisplay.photonView.owner) == senderID ) cardDisplay.SetCardBack();
				}

				foreach( DeckDisplay deck in allDecks ) {
					if( GetIDFor( deck.photonView.owner ) == senderID ) {
						if( !deck.isDynasty && !deck.isDiscard ) deck.renderer.material.mainTexture = request.textureNonReadable;
					}
				}
			}
		}
	}

	[PunRPC]
	public IEnumerator SetCustomDynCardBack( string cardbackURL, PhotonMessageInfo info ) {
		int senderID = GetIDFor( info.sender );

		if( !customDynBacks.ContainsKey(senderID) ) {

			WWW request = new WWW( cardbackURL );
			yield return request;

			if( string.IsNullOrEmpty( request.error ) ) {
				customDynBacks.Add( senderID, request.textureNonReadable );

				// set all existing cards to have this fate back
				foreach( CardDisplay cardDisplay in allCards ) {
					if( GetIDFor(cardDisplay.photonView.owner) == senderID ) cardDisplay.SetCardBack();
				}

				foreach( DeckDisplay deck in allDecks ) {
					if( GetIDFor( deck.photonView.owner ) == senderID ) {
						if( deck.isDynasty && !deck.isDiscard ) deck.renderer.material.mainTexture = request.textureNonReadable;
					}
				}
			}
		}
	}

	public void OnPhotonPlayerConnected( PhotonPlayer otherPlayer ) {
		if( otherPlayer == null ) return;

		string connectedPlayerName = (string)otherPlayer.customProperties["name"];
		if( connectedPlayerName == null ) connectedPlayerName = "N/A";

		bool isSpectator = otherPlayer.customProperties["spectating"] != null;

		if( !isSpectator ) {
			print("Player " + connectedPlayerName + " joined game " + Time.time);
			if( PlayerPrefs.GetInt( "disableSounds", 0 ) == 0 ) participantJoined.Play();
		}
		else {
			print("Spectator " + connectedPlayerName + " joined game " + Time.time);
			if( PlayerPrefs.GetInt( "disableSounds", 0 ) == 0 ) spectatorJoined.Play();
		}

		if( !spectating && !isSpectator ) {
			if( curParticipants == maxParticipants ) {
				waitingButton.FurlNow();
			}
		}

		// If you're a player in a game that was joined by a spectator or a disconnected player
		if( !spectating && isSpectator ) {

			// Serialize cards
			Dictionary<int,string> cardsToSerialize = new Dictionary<int,string>();
			foreach( CardDisplay card in allCards ) {

				if( card.card != null && card.photonView.isMine ) {
					cardsToSerialize.Add( card.photonView.viewID, JsonConvert.SerializeObject( card.Serialized() ) );
				}
			}

			bool compressed = PlayerPrefs.GetInt( "compresstraffic", 1 ) == 1;

			byte[] cardsAsBytes = Encoding.ASCII.GetBytes(JsonConvert.SerializeObject( cardsToSerialize ));
			if( compressed ) cardsAsBytes = LZMAtools.CompressByteArrayToLZMAByteArray( cardsAsBytes );
			photonView.RPC("DeserializeCardsBytes", otherPlayer, cardsAsBytes, compressed );

			print("I successfully serialized all cards to joined player " + (string)otherPlayer.customProperties["name"] + " " + Time.time);

			// Serialize hand
			if( myHand != null ) myHand.photonView.RPC("DeserializeHandString", otherPlayer, JsonConvert.SerializeObject( myHand.Serialized() ) );

			print("I successfully serialized hand to joined player " + (string)otherPlayer.customProperties["name"] + " " + Time.time);

			// Serialize decks
			Dictionary<int,string> myDecks = new Dictionary<int,string>();
			foreach( DeckDisplay deck in allDecks ) {
				if( deck.photonView.isMine ) {
					myDecks.Add( deck.photonView.viewID, JsonConvert.SerializeObject( deck.Serialized() ) );
				}
			}
			byte[] decksAsBytes = Encoding.ASCII.GetBytes(JsonConvert.SerializeObject( myDecks ));
			if( compressed ) decksAsBytes = LZMAtools.CompressByteArrayToLZMAByteArray( decksAsBytes );
			photonView.RPC("DeserializeDecksBytes", otherPlayer, decksAsBytes, compressed );

			print("I successfully serialized decks to joined player " + (string)otherPlayer.customProperties["name"] + " " + Time.time);

			// Serialize guides
			foreach( CardDisplay card in allCards ) {

				if( card.photonView.isMine ) {

					if( card.guides.Count > 0 ) {
						SerializedGuideLine[] serializedGuides = new SerializedGuideLine[card.guides.Count];
						for( int index = 0; index < card.guides.Count; index++ ) {
							GuideLine guide = card.guides[index];
							serializedGuides[index] = new SerializedGuideLine {
								sourceView = guide.source.photonView.viewID,
								targetView = guide.target.photonView.viewID,
								targetOffset = guide.targetOffset,
								sourceOffset = guide.sourceOffset,
								clan = guide.clan
							};
						}

						photonView.RPC("DeserializeGuides", otherPlayer, JsonConvert.SerializeObject( serializedGuides ));
					}
				}
			}

			print("I successfully serialized guides to joined player " + (string)otherPlayer.customProperties["name"] + " " + Time.time);

			if( isSpectator ) photonView.RPC("SpectatorChangedPosition", otherPlayer);

			string customFateCardBack = PlayerPrefs.GetString("fateCardbackURL", "");
			string customDynCardBack = PlayerPrefs.GetString("dynastyCardbackURL", "");
			int playmatID = -1;
			if( playmat ) playmatID = playmat.photonView.viewID;
			photonView.RPC("DeserializeCustomizations", otherPlayer, playmatID, PlayerPrefs.GetString("playmatURL", "") );

			if( customFateCardBack != "" ) {
				photonView.RPC("SetCustomFateCardBack", otherPlayer, customFateCardBack);
			}

			if( customDynCardBack != "" ) {
				photonView.RPC("SetCustomDynCardBack", otherPlayer, customDynCardBack);
			}

			print("I successfully serialized customizations to joined player " + (string)otherPlayer.customProperties["name"] + " " + Time.time);

			print("Finished serializing to joined player " + (string)otherPlayer.customProperties["name"] + " " + Time.time);
		}

		photonView.RPC("DeserializeTableState", otherPlayer, whoseTurn, whoseAction, (int)currentPhase, myTurnIndicator.turnCounter, startingPlayer );
	}

	[PunRPC]
	public void DeserializeCardsBytes( byte[] incomingCards, bool compressed ) {
		print("Deserializing cards from bytes");
		if( compressed ) incomingCards = LZMAtools.DecompressLZMAByteArrayToByteArray(incomingCards);
		string serializedCardsSlush = Encoding.ASCII.GetString( incomingCards );

		DeserializeCards( JsonConvert.DeserializeObject<Dictionary<int,string>>(serializedCardsSlush) );
	}

	void DeserializeCards( Dictionary<int,SerializedCardDisplay> serializedCards ) {
		print("Deserializing " + serializedCards.Keys.Count + " cards");

		foreach( int key in serializedCards.Keys ) {
			PhotonView bufferedCard = PhotonView.Find( key );
			CardDisplay cardDisplay = bufferedCard.GetComponent<CardDisplay>();
			SerializedCardDisplay serializedCard = serializedCards[key];
			cardDisplay.DeserializeCard( serializedCard );

			if( serializedCard.originalOwner == myID ) {
				bufferedCard.TransferOwnership( PhotonNetwork.player );
			}
		}

		print("Deserialized " + allCards.Count);
	}

	void DeserializeCards( Dictionary<int,string> serializedCards ) {
		foreach( int key in serializedCards.Keys ) {
			PhotonView bufferedCard = PhotonView.Find( key );
			CardDisplay cardDisplay = bufferedCard.GetComponent<CardDisplay>();
			cardDisplay.DeserializeCard( JsonConvert.DeserializeObject<SerializedCardDisplay>( serializedCards[key] ) );
		}
	}

	[PunRPC]
	public void DeserializeDecksBytes( byte[] incomingDecks, bool compressed ) {
		if( compressed ) incomingDecks = LZMAtools.DecompressLZMAByteArrayToByteArray(incomingDecks);
		string serializedDecksSlush = Encoding.ASCII.GetString( incomingDecks );

		DeserializeDecks( JsonConvert.DeserializeObject<Dictionary<int,string>>(serializedDecksSlush) );
	}

	void DeserializeDecks( Dictionary<int,SerializedDeckDisplay> serializedDecks ) {
		foreach( int key in serializedDecks.Keys ) {
			PhotonView bufferedDeck = PhotonView.Find( key );
			DeckDisplay deckDisplay = bufferedDeck.GetComponent<DeckDisplay>();
			SerializedDeckDisplay serializedDeck = serializedDecks[key];
			deckDisplay.DeserializeDeck( serializedDeck );

			if( serializedDeck.originalOwner == myID ) {
				bufferedDeck.TransferOwnership( PhotonNetwork.player );

				if( deckDisplay.isDynasty ) {
					if( deckDisplay.isDiscard ) Table.data.myDynDiscard = deckDisplay;
					else Table.data.myDynasty = deckDisplay;
				}
				else {
					if( deckDisplay.isDiscard ) Table.data.myFateDiscard = deckDisplay;
					else Table.data.myFate = deckDisplay;
				}
			}
		}
	}

	void DeserializeDecks( Dictionary<int,string> serializedDecks ) {
		foreach( int key in serializedDecks.Keys ) {
			PhotonView bufferedDeck = PhotonView.Find( key );
			DeckDisplay deckDisplay = bufferedDeck.GetComponent<DeckDisplay>();
			deckDisplay.DeserializeDeck( JsonConvert.DeserializeObject<SerializedDeckDisplay>( serializedDecks[key] ) );
		}
	}

	[PunRPC]
	public void DeserializeTableState( int turn, int action, int phase, int turnCount, int playerThatWentFirst ) {
		whoseTurn = turn;
		whoseAction = action;
		currentPhase = (Phase)phase;
		myTurnIndicator.turnCounter = turnCount;
		startingPlayer = playerThatWentFirst;

		RefreshIndicators();
	}

	[PunRPC]
	public void DeserializeGuides( string serializedGuides ) {
		SerializedGuideLine[] guides = JsonConvert.DeserializeObject<SerializedGuideLine[]>( serializedGuides );

		foreach( SerializedGuideLine guide in guides ) {
			print("Deserializing a guide between " + guide.sourceView + " at local pos " + guide.sourceOffset + " and " + guide.targetView + " at local pos " + guide.targetOffset + " " + Time.time);
			CardDisplay source = PhotonView.Find( guide.sourceView ).GetComponent<CardDisplay>();

			source.AttachGuideTo( guide.targetView, guide.clan, guide.sourceOffset, guide.targetOffset );
		}
	}

	[PunRPC]
	public void DeserializeCustomizations( int playmatViewID, string playmatURL ) {
		print("Deserializing playmat with id " + playmatViewID + " with URL " + playmatURL);

		if( playmatViewID != -1 ) {
			Playmat playerPlayMat = PhotonView.Find( playmatViewID ).GetComponent<Playmat>();
			StartCoroutine( playerPlayMat.Initialize( playmatURL ) );
		}
	}
}