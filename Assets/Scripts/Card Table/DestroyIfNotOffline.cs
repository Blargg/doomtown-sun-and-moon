﻿using UnityEngine;
using System.Collections;

public class DestroyIfNotOffline : MonoBehaviour {

	void Start() {
		if( PhotonNetwork.offlineMode == false ) Destroy(gameObject);
	}
}
