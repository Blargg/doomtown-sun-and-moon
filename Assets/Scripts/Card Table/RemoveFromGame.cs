﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RemoveFromGame : MonoBehaviour {

	public UnfurlScroll scroll;
	public CanvasScaler uiScale;
	public InputHandler inputHandler;

	public RectTransform removePanel;
	CardDisplay cardToRemove = null;

	public void ConfirmRemoval( CardDisplay forCard ) {
		cardToRemove = forCard;

		removePanel.pivot = new Vector2( inputHandler.inputInfo.inputPos.x > Screen.width / 2f ? 1f : 0f, 0.5f );
		removePanel.anchoredPosition = inputHandler.inputInfo.inputPos * (uiScale.referenceResolution.y / Screen.height); // for standalone

		scroll.UnfurlNow();
		
	}

	public void Cancel() {
		scroll.FurlNow();
	}

	public void Confirmed() {
		if( scroll.furling ) return;

		if( cardToRemove.inStack ) cardToRemove.inStack.RemoveFromStack( cardToRemove, false );
		if( cardToRemove.stack.Count > 0 ) {
			foreach( CardDisplay stackMember in cardToRemove.stack ) {
				stackMember.inStack = null;
			}
		}
		Table.data.photonView.RPC( "RemoveFromGame", PhotonTargets.All, cardToRemove.photonView.viewID );
		PhotonNetwork.Destroy( cardToRemove.photonView );

		Table.data.chat.AddVerboseChatEntry( "I removed " + cardToRemove.name + " from the game" );

		cardToRemove = null;

		scroll.FurlNow();
	}

	public bool IsVisible() {
		return removePanel.gameObject.activeInHierarchy;
	}
}
