﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class CameraRotation : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler {

	public Camera mainCamera;
	public Transform cameraPivot;
	float speed = -0.5f;
	float lastRot = 0f;

	public void OnPointerDown( PointerEventData evt ) {
		lastRot = evt.position.x;
	}

	public void OnDrag( PointerEventData evt ) {

		cameraPivot.RotateAround( Vector3.zero, Vector3.up, (lastRot - evt.position.x) * speed );
		lastRot = evt.position.x;

		Table.data.navigating = true;
		Table.data.CameraMoved();
		Persistent.data.FrameRateUpFor( 0.5f );
	}

	public void OnPointerUp( PointerEventData evt ) {

		Table.data.navigating = false;

		Table.data.SpectatorChangedPosition();
	}
}
