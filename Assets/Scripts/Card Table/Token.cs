﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Token : MonoBehaviour {

	public CanvasGroup canvasInput;

	public Text tableText;

	[HideInInspector] public int tokenIndex = -1;
	[HideInInspector] public int shapeIndex = -1;

	public int tokenValue {
		get {
			return _tokenValue;
		}
		set {
			_tokenValue = value;
			tableText.text = _tokenValue.ToString();
		}
	}
	int _tokenValue = 1;

	public CardDisplay cardDisplay;
	public Image tokenImage;

	public Sprite inactiveSprite;
	public Sprite activeSprite;

	public Color activeColor = Color.white;
	public Color inactiveColor;

	public LayoutElement layoutElement;
	public RectTransform tokenRect;

	public bool isActive = false;

	public void Clicked() {
		if( !Table.data.spectating && (cardDisplay.photonView.isMine || cardDisplay is Favor) ) {
			Table.data.EditToken( this );
			cardDisplay.photonView.RPC( "ActivateToken", PhotonTargets.All, tokenIndex );
		}
	}

	public void Activate() {
		tableText.gameObject.SetActive( true );
		if( !isActive ) {
			tokenImage.sprite = activeSprite;
			tokenImage.color = activeColor;
			isActive = true;
		}
	}

	public void ClearToken() {
		tokenValue = 1;
		tableText.gameObject.SetActive( false );
		activeColor = Color.white;
		tokenImage.sprite = inactiveSprite;
		tokenImage.color = inactiveColor;

		isActive = false;
	}

	public void ChangeColor( Color toColor ) {
		tokenImage.color = toColor;
		activeColor = toColor;
	}

	public void ChangeShape( int shapeIndex ) {
		this.shapeIndex = shapeIndex;
		Sprite selectedSprite = ModifyToken.data.shapes[shapeIndex].sprite;
		float spriteRatio = selectedSprite.rect.width / selectedSprite.rect.height;

		tokenImage.sprite = selectedSprite;
		activeSprite = selectedSprite;
		ResizeToRatio( spriteRatio );
	}

	public void ResizeToRatio( float targetRatio ) {
		layoutElement.preferredWidth = layoutElement.preferredHeight * targetRatio;
	}

	public void CopyTo( Token otherToken ) {
		otherToken.tokenValue = this.tokenValue;
		otherToken.inactiveSprite = this.inactiveSprite;
		otherToken.activeSprite = this.activeSprite;
		otherToken.activeColor = this.activeColor;
		otherToken.inactiveColor = this.inactiveColor;
		otherToken.ResizeToRatio( this.layoutElement.preferredWidth / this.layoutElement.preferredHeight );
		otherToken.tokenImage.sprite = this.tokenImage.sprite;
		otherToken.tokenImage.color = this.tokenImage.color;
		otherToken.shapeIndex = this.shapeIndex;
	}

	void OnDisable() {
		isActive = false;
	}

	public void EnableInput() {
		canvasInput.blocksRaycasts = true;
	}

	public void DisableInput() {
		canvasInput.blocksRaycasts = false;
	}
}

public class SerializedToken {
	public bool tokenActive = false;
	public int tokenValue;
	public Vector3 tokenColor;
	public int shapeIndex;
}
