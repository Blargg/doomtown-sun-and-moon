﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class PickTokenColor : MonoBehaviour {

	public Image thisImage;
	public ModifyToken tokenModifier;

	public void Clicked() {
		tokenModifier.ChangeTokenColor( thisImage.color );
	}
}
