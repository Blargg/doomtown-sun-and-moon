﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

using UnityEngine.SceneManagement;

public class ChatPanel : Photon.MonoBehaviour {
	public RectTransform chatPanel;
	public Text chatStatus;
	public InputField chatInput;
	public Text chatInputName;
	public ScrollRect chatScroll;

	public ChatItem chatItemPrefab;

	int maxLines = 100;

	void Start() {
		chatInputName.text = Table.data.myName + ":";
		gameObject.SetActive(!PhotonNetwork.offlineMode || SceneManager.GetActiveScene().name == "Tutorial" );
	}

	public void OnSubmit( string submittedText ) {
		if( Input.GetKey( KeyCode.Escape ) ) {
			chatInput.text = "";
			return;
		}

		photonView.RPC("AddChatEntry", PhotonTargets.All, Table.GetIDFor(PhotonNetwork.player), Table.data.myName, submittedText, (int)Persistent.data.ClanStringToEnum(Table.data.myClan));
		chatInput.text = "";
	}

	public void Toggle( bool state ) {
		chatScroll.gameObject.SetActive( state );

		if( state == true ) {
			chatStatus.text = "...";

			chatInput.Select();
			chatInput.ActivateInputField();

			chatScroll.verticalNormalizedPosition = 0f;
		}
		else {
			chatStatus.text = "";
		}
	}

	// called by the ChatInput object
	[PunRPC]
	public void AddChatEntry( int playerID, string playerName, string chatMessage, int clan ) {
		if( chatMessage == "" ) return;

		ChatItem chatItem = Instantiate( chatItemPrefab ) as ChatItem;

		chatItem.message.text = chatMessage;

		if( playerID == PhotonNetwork.player.ID ) {
			if( playerName == "" ) playerName = "You";
		}
		else {
			if( playerName == "" ) playerName = "Them";

			if( !chatPanel.gameObject.activeInHierarchy ) {
				chatStatus.text = "!";
			}
		}
		playerName += ":";
		chatItem.nameLabel.text = playerName;

		Color col = Persistent.data.GetColorsForClan(Persistent.data.ClanEnumToString((Card.Clan)clan))[3];
		col.a = 0.2f;
		chatItem.backgroundImage.color = col;
		chatItem.backgroundRect.sizeDelta = new Vector2( chatItem.message.preferredWidth, chatItem.backgroundRect.sizeDelta.y );

		chatItem.transform.SetParent( chatPanel, false );

		if( chatPanel.childCount > maxLines ) Destroy(chatPanel.GetChild(0).gameObject);

		chatInput.transform.parent.SetAsLastSibling();

		chatInput.Select();
		chatInput.ActivateInputField();

		chatScroll.verticalNormalizedPosition = 0f;
	}

	public void AddVerboseChatEntry( string chatMessage ) {

		if( PlayerPrefs.GetInt( "verbosechat", 0 ) == 1 ) {
			photonView.RPC(
				"AddChatEntry",
				PhotonTargets.Others, 
				PhotonNetwork.player.ID, 
				Table.data.myName, 
				chatMessage, 
				(int)Persistent.data.ClanStringToEnum(Table.data.myClan)
			);
		}
	}
}
