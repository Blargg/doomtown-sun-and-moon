﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class CameraTilt : MonoBehaviour, IDragHandler, IPointerDownHandler {

	public float maxTiltDegrees = 60f;
	public float minTiltDegrees = -20f;

	public Transform pivot;
	float initTapPos = 0f;

	public float tiltSpeed = 1f;
	float tilt = 40f;
	float yRot = -1f;

	public void OnPointerDown(PointerEventData evt) {
		initTapPos = evt.position.y;

		if( !Mathf.Approximately( pivot.localEulerAngles.z, 180f ) ) {
			yRot = pivot.localEulerAngles.y + 180f;
		}
		else {
			yRot = pivot.localEulerAngles.y;
		}
	}

	public void OnDrag(PointerEventData evt) {
		float controlDelta = evt.position.y - initTapPos;

		tilt = Mathf.Clamp( tilt + Time.deltaTime * tiltSpeed * controlDelta, minTiltDegrees, maxTiltDegrees );

		pivot.localEulerAngles = new Vector3(360f - tilt, yRot, 180f);

		initTapPos = evt.position.y;
	}
}
