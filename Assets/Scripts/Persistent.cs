﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using System;
using Newtonsoft.Json;
using System.Text;
using UnityEngine.Networking;

using SevenZip.Compression.LZMA;

public class Persistent : MonoBehaviour {

	public Dictionary<string, Card> cardsByName = null;
	public Dictionary<string, Card> cardsByID = null;

	public Dictionary<string, Card> oracleNames = new Dictionary<string, Card>();
	public Dictionary<string, Card> oracleIDs = new Dictionary<string, Card>();

	public static Persistent data;

	Dictionary<string,Color[]> clanColors;
	Dictionary<string,Card.Clan> stringToClan;
	Dictionary<Card.Clan,string> clanToString;
	Dictionary<Card.Legality,string[]> legalityToStrings;
	public List<string> localDbSets = new List<string>();
	public List<string> baseSets = new List<string>() { "Imperial", "Obsidian", "Jade", "Pearl", "Gold", "Diamond", "Lotus", "Samurai", "Celestial", "Emperor", "Ivory" };

	[HideInInspector]
	public string roomPassword = "";

	// color array: 0 = top color, 1 = faded color, 2 = targeting arrow color, 3 = phase indicator color
	public Color[] dragonColors = new Color[4];
	public Color[] crabColors = new Color[4];
	public Color[] craneColors = new Color[4];
	public Color[] unicornColors = new Color[4];
	public Color[] spiderColors = new Color[4];
	public Color[] phoenixColors = new Color[4];
	public Color[] lionColors = new Color[4];
	public Color[] unalignedColors = new Color[4];
	public Color[] mantisColors = new Color[4];
	public Color[] nagaColors = new Color[4];
	public Color[] scorpionColors = new Color[4];

	public string rootURL = "";
	public DirectoryInfo outsideApp;
	public string pathToImages = "";
	public string pathToStreamingImages = "http://www.hwstn.com/sunandmoon";
	public string pathToOracle = "http://oracleofthevoid.com/oracle/";
	public string pathToPlaytestManifest = "http://www.hwstn.com/sunandmoon/Playtest/AuthorizedKeys.snm";
	public string pathToPrimaryDatabase = "";
	public string pathToOracleNames = "";
	public string pathToOracleIDs = "";
	public string pathToOracleCache = "";
	public string cleanPathToOracleCache = "";
	public string pathToBackgrounds = "";
	public string pathToDecks = "";
	public bool loading = true;

	// Network
	public static readonly string versionName = "1.2";
	public string playFabTitleID = "16A5";
	public string sessionTicket = "";
	public string playFabID = "";
	const string cipherKey = "AmaterasuAndOnnotangu2015";
	public byte[] cipher;

	// Playtesting
	public DirectoryInfo playtestFolder;
	public string playtestGroup = "";
	public string ptDbHash = "";
	public string mainAppID = "97ee5e0e-3078-48c1-855e-2f074c202a74";

	// Performance
	float performanceTimer = 0f;
	const int lowPowerFrameRate = 15;
	const int highPowerFrameRate = 60;

	// Tournament tracking
	[HideInInspector] public bool tournamentMode = false;
	[HideInInspector] public string tournamentName = "";

	// Deck editing
	[HideInInspector] public List<string> allLocalEditions = new List<string>();
	[HideInInspector] public string oracleCookie = "PHPSESSID=phhvgon2bo0a0l1uji5nblqv45";

	void Awake () {

		if( Time.time < 1.0f ) DontDestroyOnLoad( this );
		else {
			Destroy( gameObject );
			return;
		}

		Persistent.data = this;
		FrameRateDown();

		PhotonNetwork.PhotonServerSettings.AppID = mainAppID;

		StartCoroutine( BuildDatabase() );

		clanColors = new Dictionary<string,Color[]>();
		clanColors.Add("dragon", dragonColors );
		clanColors.Add("crab", crabColors );
		clanColors.Add("crane", craneColors );
		clanColors.Add("unicorn", unicornColors );
		clanColors.Add("spider", spiderColors );
		clanColors.Add("phoenix", phoenixColors );
		clanColors.Add("lion", lionColors );
		clanColors.Add("unaligned", unalignedColors );
		clanColors.Add("mantis", mantisColors );
		clanColors.Add("naga", nagaColors );
		clanColors.Add("scorpion", scorpionColors );

		stringToClan = new Dictionary<string,Card.Clan>();
		stringToClan.Add("dragon", Card.Clan.dragon);
		stringToClan.Add("crab", Card.Clan.crab);
		stringToClan.Add("crane", Card.Clan.crane);
		stringToClan.Add("unicorn", Card.Clan.unicorn);
		stringToClan.Add("unaligned", Card.Clan.unaligned);
		stringToClan.Add("lion", Card.Clan.lion);
		stringToClan.Add("mantis", Card.Clan.mantis);
		stringToClan.Add("naga", Card.Clan.naga);
		stringToClan.Add("scorpion", Card.Clan.scorpion);
		stringToClan.Add("spider", Card.Clan.spider);
		stringToClan.Add("phoenix", Card.Clan.phoenix);
		stringToClan.Add("neutral", Card.Clan.neutral);

		clanToString = new Dictionary<Card.Clan,string>();
		clanToString.Add(Card.Clan.dragon, "dragon");
		clanToString.Add(Card.Clan.crab, "crab");
		clanToString.Add(Card.Clan.crane, "crane");
		clanToString.Add(Card.Clan.unicorn, "unicorn");
		clanToString.Add(Card.Clan.unaligned, "unaligned");
		clanToString.Add(Card.Clan.lion, "lion");
		clanToString.Add(Card.Clan.mantis, "mantis");
		clanToString.Add(Card.Clan.naga, "naga");
		clanToString.Add(Card.Clan.scorpion, "scorpion");
		clanToString.Add(Card.Clan.spider, "spider");
		clanToString.Add(Card.Clan.phoenix, "phoenix");
		clanToString.Add(Card.Clan.neutral, "neutral");

		legalityToStrings = new Dictionary<Card.Legality,string[]>();
		legalityToStrings.Add(Card.Legality.Legacy, new string[] { "open" } );
		legalityToStrings.Add(Card.Legality.Modern, new string[] { "20F", "ivory", "emperor", "celestial", "samurai" } );
		// legalityToStrings.Add(Card.Legality.Twenty_Festivals_Strict, new string[] { "-ivory", "20F" } );
		// legalityToStrings.Add(Card.Legality.Twenty_Festivals_Extended, new string[] { "20F", "ivory" } );
		legalityToStrings.Add(Card.Legality.Twenty_Festivals, new string[] { "20F" } );
		legalityToStrings.Add(Card.Legality.Ivory_Edition, new string[] { "ivory" } );
		legalityToStrings.Add(Card.Legality.Emperor, new string[] { "emperor" } );
		legalityToStrings.Add(Card.Legality.Celestial, new string[] { "celestial" } );
		legalityToStrings.Add(Card.Legality.Samurai, new string[] { "samurai" } );
		legalityToStrings.Add(Card.Legality.Lotus, new string[] { "lotus" } );
		legalityToStrings.Add(Card.Legality.Diamond, new string[] { "diamond" } );
		legalityToStrings.Add(Card.Legality.Gold, new string[] { "gold" } );
		legalityToStrings.Add(Card.Legality.Jade, new string[] { "jade" } );
		legalityToStrings.Add(Card.Legality.Onyx, new string[] { "onyx" } );

		cipher = Encoding.ASCII.GetBytes( cipherKey );
	}

	public Color[] GetColorsForClan( string clanString ) {
		Color[] colorsForClan = unalignedColors;
		if( !clanColors.TryGetValue( clanString, out colorsForClan ) ) colorsForClan = unalignedColors;
		return colorsForClan;
	}

	public Card.Clan ClanStringToEnum( string clanString ) {
		Card.Clan clanEnum = Card.Clan.unaligned;
		if( !stringToClan.TryGetValue( clanString, out clanEnum ) ) clanEnum = Card.Clan.unaligned;
		return clanEnum;
	}

	public string ClanEnumToString( Card.Clan clan ) {
		string clanString = "unaligned";
		if( !clanToString.TryGetValue( clan, out clanString ) ) clanString = "unaligned";
		return clanString;
	}

	public string[] LegalityEnumToStrings( Card.Legality legality ) {
		string[] legalityStrings = new string[] { "open" };
		if( !legalityToStrings.TryGetValue(legality, out legalityStrings ) ) legalityStrings = new string[] { "open" };

		return legalityStrings;
	}

	public void CopyFilesFromTo( DirectoryInfo from, DirectoryInfo to, string extension ) {
		foreach( FileInfo file in from.GetFiles("*" + extension) ) {
			file.CopyTo( Path.Combine(to.FullName, file.Name) );
		}
	}

	IEnumerator BuildDatabase() {

		#if UNITY_STANDALONE_OSX || UNITY_EDITOR
			outsideApp = new DirectoryInfo(Application.dataPath).Parent.Parent;
			rootURL = "file:///";
		#elif UNITY_STANDALONE_WIN || UNITY_STANDALONE_LINUX
			outsideApp = new DirectoryInfo(Application.dataPath).Parent;
			rootURL = "file:///";
		#endif

		// Generate path to oracle cache

		// string cachePath = Application.dataPath;
		// #if UNITY_EDITOR
		// 	cachePath = Application.persistentDataPath;
		// #endif

		string cachePath = Application.persistentDataPath;

		pathToOracleNames = cachePath + "/OracleNames.txt";
		FileInfo nameCacheFile = new FileInfo( pathToOracleNames );
		if( nameCacheFile.Exists ) {
			oracleNames = JsonConvert.DeserializeObject<Dictionary<string,Card>>( File.ReadAllText(nameCacheFile.FullName) );
		}

		pathToOracleIDs = cachePath + "/OracleIDs.txt";
		FileInfo IdCacheFile = new FileInfo( pathToOracleIDs );
		if( IdCacheFile.Exists ) {
			oracleIDs = JsonConvert.DeserializeObject<Dictionary<string,Card>>( File.ReadAllText(IdCacheFile.FullName) );
		}

		cleanPathToOracleCache = cachePath + "/ImageCache/";
		DirectoryInfo imageCache = new DirectoryInfo( cleanPathToOracleCache );
		if( !imageCache.Exists ) imageCache.Create();
		pathToOracleCache = rootURL + cleanPathToOracleCache.SanitizePath();

		// Generate path to database

		pathToPrimaryDatabase = Application.streamingAssetsPath + "/Database/database.xml";

		// Generate path to additional databases
		// DirectoryInfo addDatabasesFolder = new DirectoryInfo(outsideApp.FullName + "/Databases/");
		// string[] pathsToAddDatabases = new string[1];
		// if( addDatabasesFolder.Exists ) {
		// 	FileInfo[] addDatabaseFiles = addDatabasesFolder.GetFiles("*" + ".xml");
		// 	pathsToAddDatabases = new string[addDatabaseFiles.Length + 1];
		// 	for( int index = 0; index < addDatabaseFiles.Length; index++ ) {
		// 		pathsToAddDatabases[index] = rootURL + addDatabaseFiles[index].FullName.SanitizePath();
		// 		print("Path to additional database generated: " + pathsToAddDatabases[index]);
		// 	}
		// }
		// pathsToAddDatabases[pathsToAddDatabases.Length - 1] = rootURL + pathToPrimaryDatabase.SanitizePath();
		string pathToDatabase = rootURL + pathToPrimaryDatabase.SanitizePath();

		// Generate path to images
		DirectoryInfo imagesFolder;
		DirectoryInfo cardsFolder;
		#if UNITY_STANDALONE  || UNITY_EDITOR
			imagesFolder = new DirectoryInfo( outsideApp.FullName + "/images/" );
			if( !imagesFolder.Exists ) imagesFolder.Create();

			cardsFolder = new DirectoryInfo( imagesFolder.FullName + "/cards/" );
			if( !cardsFolder.Exists ) cardsFolder.Create();

			pathToImages = rootURL + outsideApp.FullName.SanitizePath();
		#elif UNITY_ANDROID
			pathToImages = Application.persistentDataPath.SanitizePath();
		#elif UNITY_IOS
			pathToImages = Application.persistentDataPath.SanitizePath();
		#endif

		// Generate path to decks
		DirectoryInfo deckDir;
		#if UNITY_STANDALONE || UNITY_EDITOR
			deckDir = new DirectoryInfo(outsideApp.FullName + "/Decks/");

			if( !deckDir.Exists ) {
				deckDir.Create();
				CopyFilesFromTo( new DirectoryInfo(Application.streamingAssetsPath + "/Decks"), deckDir, ".txt" );
			}
		#elif UNITY_IOS || UNITY_ANDROID
			deckDir = new DirectoryInfo(Application.persistentDataPath + "/Decks/");
			if( !deckDir.Exists ) {
				deckDir.Create();
				CopyFilesFromTo( new DirectoryInfo(Application.streamingAssetsPath + "/Decks"), deckDir, ".txt" );
			}
		#endif
		pathToDecks = deckDir.FullName;

		// Generate path to backgrounds
		DirectoryInfo backgroundsDir;
		#if UNITY_STANDALONE || UNITY_EDITOR
			backgroundsDir = new DirectoryInfo(outsideApp.FullName + "/Backgrounds/");

			if( !backgroundsDir.Exists ) {
				backgroundsDir.Create();
				CopyFilesFromTo( new DirectoryInfo(Application.streamingAssetsPath + "/Backgrounds"), backgroundsDir, ".jpg" );
			}
		#elif UNITY_IOS || UNITY_ANDROID
			backgroundsDir = new DirectoryInfo(Application.persistentDataPath + "/Backgrounds/");
			if( !backgroundsDir.Exists ) {
				backgroundsDir.Create();
				CopyFilesFromTo( new DirectoryInfo(Application.streamingAssetsPath + "/Backgrounds"), backgroundsDir, ".jpg" );
			}
		#endif
		pathToBackgrounds = backgroundsDir.FullName;

		// Load databases
		cardsByName = new Dictionary<string, Card>();
		cardsByID = new Dictionary<string, Card>();
		Dictionary<string,Card> linkedCards = new Dictionary<string, Card>();

		// authorize playtesters and load external databases if so
		FileInfo[] ptKeys = new FileInfo[0];
		ptKeys = outsideApp.GetFiles("*.key");

		// Didn't detect a key here, so look for it in the next most likely place
		if( ptKeys.Length == 0 ) {
			playtestFolder = new DirectoryInfo( outsideApp.FullName + Path.DirectorySeparatorChar + "Playtest" );
			if( playtestFolder.Exists ) ptKeys = playtestFolder.GetFiles("*.key");
		}

		if( ptKeys.Length == 0 ) {
			DirectoryInfo keysFolder = new DirectoryInfo( outsideApp.FullName + Path.DirectorySeparatorChar + "PlaytestKeys/" );
			if( keysFolder.Exists ) ptKeys = keysFolder.GetFiles("*.key");
		}

		foreach( FileInfo ptKey in ptKeys ) {
			byte[] encryptedPTKey = File.ReadAllBytes( ptKey.FullName );
			Cipher.EncryptDecrypt( encryptedPTKey );
			byte[] decompressedPTKey = LZMAtools.DecompressLZMAByteArrayToByteArray( encryptedPTKey );

			PlaytesterKey externalKey = null;
			try {
				externalKey = JsonConvert.DeserializeObject<PlaytesterKey>( Encoding.ASCII.GetString( decompressedPTKey ) );
			}
			catch {
				continue;
			}

			// succeeded in loading an external key
			if( externalKey != null ) {

				WWW keyRequest = new WWW( pathToPlaytestManifest );
				yield return keyRequest;

				if( string.IsNullOrEmpty( keyRequest.error ) ) {

					byte[] encryptedPTManifest = keyRequest.bytes;
					Cipher.EncryptDecrypt( encryptedPTManifest );
					byte[] decompressedPTManifest = LZMAtools.DecompressLZMAByteArrayToByteArray( encryptedPTManifest );

					List<PlaytesterKey> authorizedKeys = JsonConvert.DeserializeObject<List<PlaytesterKey>>( Encoding.ASCII.GetString( decompressedPTManifest ) );

					foreach( PlaytesterKey authorizedKey in authorizedKeys ) {

						if( authorizedKey.name == externalKey.name ) {
							playtestGroup = externalKey.name;

							PhotonNetwork.PhotonServerSettings.AppID = externalKey.appID;

							// Authorized!  Load external databases...
							LoadExternalDatabases();

							break;
						}
					}
				}
				else {
					break;
				}
			}
		}

		if( playtestGroup == "" ) {
			// Load standard internal database

			WWW www = new WWW( pathToDatabase );
			yield return www;

			using( XmlReader reader = XmlTextReader.Create( new StringReader(www.text) ) ) {

				while( reader.ReadToFollowing("card") ) {
					Card newCard = new Card();

					reader.MoveToAttribute("id");
					newCard.id = reader.Value;

					reader.MoveToAttribute("type");
					newCard.type = reader.Value;

					while( reader.Read() ) {
						if( reader.NodeType == XmlNodeType.Element ) {
							switch( reader.Name ) {
								case "name":
									newCard.name = reader.ReadElementContentAsString();
								break;

								case "rarity":
									newCard.rarity = reader.ReadElementContentAsString();
								break;

								case "edition":
									string edition = reader.ReadElementContentAsString();
									newCard.edition = edition;
									newCard.editions.Add( edition );
									
									if( reader.Name == "image" ) {
										string imageString = reader.ReadElementContentAsString();
										newCard.images.Add( imageString );
									}

									if( !localDbSets.Contains(edition) ) localDbSets.Add( edition );
								break;

								case "legal":
									newCard.legal.Add( reader.ReadElementContentAsString() );
								break;

								case "text":
									newCard.text = reader.ReadElementContentAsString();
									newCard.text = newCard.text.Replace("<BR>", "<br>");
								break;

								case "artist":
									newCard.artist = reader.ReadElementContentAsString();
								break;

								case "flavor":
									newCard.flavor = reader.ReadElementContentAsString();
								break;

								case "cost":
									int.TryParse( reader.ReadElementContentAsString(), out newCard.cost );
								break;

								case "focus":
									int.TryParse( reader.ReadElementContentAsString(), out newCard.focus );
									if( newCard.focus > -1 ) newCard.deckType = Card.DeckType.Fate;
								break;

								case "clan":
									string clan = reader.ReadElementContentAsString();
									List<string> unalignedClans = new List<string>() { "ratling", "naga", "shadowlands", "toturi" };

									if( newCard.clans[0] == "unaligned" && !unalignedClans.Contains( clan ) ) newCard.clans = new List<string>();
									newCard.clans.Add( clan );
								break;

								case "chi":
									newCard.chi = reader.ReadElementContentAsString();
								break;

								case "force":
									newCard.force = reader.ReadElementContentAsString();
								break;

								case "personal_honor":
									string ph = reader.ReadElementContentAsString();
									if( !int.TryParse( ph, out newCard.personal_honor ) ) {
										newCard.personal_honor = -1;
									}
								break;

								case "honor_req":
									string honor_req = reader.ReadElementContentAsString();
									if( honor_req == "-" ) {
										newCard.honor_req = -1;
									}
									else {
										int.TryParse( honor_req, out newCard.honor_req );
									}
								break;

								case "province_strength":
									int.TryParse( reader.ReadElementContentAsString(), out newCard.province_strength );
								break;

								case "gold_production":
									int.TryParse( reader.ReadElementContentAsString(), out newCard.gold_production );
								break;

								case "starting_honor":
									int.TryParse( reader.ReadElementContentAsString(), out newCard.starting_honor );
								break;
							}
						}
						else if( reader.NodeType == XmlNodeType.EndElement ) {
							if( reader.Name == "card" ) {

								// Pair doubled strongholds together
								if( newCard.type == "stronghold" ) {

									Card sisterCard = null;
									if( newCard.name.EndsWith(" (1)") ) {
										newCard.name = newCard.name.Replace(" (1)", "");
										
										if( linkedCards.TryGetValue(newCard.id + "b", out sisterCard) ) {
											if( sisterCard.linkedCard == null ) {
												newCard.linkedCard = sisterCard;
												sisterCard.linkedCard = newCard;
											}
										}
										else linkedCards.Add(newCard.id + "b", newCard);
									}
									if( newCard.name.EndsWith(" (2)") ) {
										if( linkedCards.TryGetValue(newCard.id, out sisterCard) ) {
											if( sisterCard.linkedCard == null ) {
												newCard.linkedCard = sisterCard;
												sisterCard.linkedCard = newCard;
											}
										}
										else linkedCards.Add(newCard.id, newCard);
									}
								}

								// Add to list of all cards and move on to next XML card entry
								TryToAddCard( newCard );
								break;
							}
						}
					}
				}
			}
		}

		WWW cookieRequest = new WWW( pathToStreamingImages + "/oracleCookie.txt" );
		yield return cookieRequest;

		if( string.IsNullOrEmpty( cookieRequest.error ) ) {
			oracleCookie = cookieRequest.text;
		}

		loading = false;
	}

	void TryToAddCard( Card newCard ) {
		if( newCard == null ) print("Adding null card!");
		if( cardsByID == null ) print("Adding to null dictionary!");
		try {
			cardsByID.Add( newCard.id, newCard );
		}
		catch {
			print("Card with ID " + newCard.id + " and name " + newCard.name + " already in dictionary!");
		}

		// Deal with cards that have duplicate names
		try {
			cardsByName.Add( newCard.name.ToLower(), newCard );
		}
		catch {
			string origCardName = newCard.name.ToLower();
			Card oldCard = cardsByName[origCardName];
			string modifiedOldCardName = oldCard.name + " (" + oldCard.edition + ")";
			oldCard.name = modifiedOldCardName;
			cardsByName.Remove( origCardName ); // remove old entry and replace with new modified version
			cardsByName.Add( modifiedOldCardName.ToLower(), oldCard );
			// Debug.Log("Changing existing card with modified name to " + modifiedOldCardName.ToLower() + " to the dictionary");

			// Modified each card to include their set
			string modifiedNewCardName = newCard.name + " (" + newCard.edition + ")";
			if( cardsByName.ContainsKey(modifiedNewCardName.ToLower()) ) modifiedNewCardName += " 2";
			newCard.name = modifiedNewCardName;
			// print("Adding new copy of card " + modifiedNewCardName.ToLower());
			cardsByName.Add( modifiedNewCardName.ToLower(), newCard );
		}
	}

	void LoadExternalDatabases() {
		byte[] encryptedDBs = default(byte[]);

		// try to load local copy first
		string playtestPathExtension = "/Playtest/db.snm";
		FileInfo localCopy = new FileInfo( outsideApp.FullName + playtestPathExtension );
		if( localCopy.Exists ) {
			string fileSize = localCopy.Length.ToString();
			int hashLength = 5;
			ptDbHash = fileSize.Substring( fileSize.Length - Mathf.Min(fileSize.Length, hashLength) );
			print("External pt database hash is " + ptDbHash);
			encryptedDBs = File.ReadAllBytes( localCopy.FullName );
			print("Loaded local external database " + Time.time);
		}
		// else {
		// 	WWW databaseRequest = new WWW( pathToStreamingImages + playtestPathExtension );
		// 	yield return databaseRequest;

		// 	if( string.IsNullOrEmpty( databaseRequest.error ) ) {
		// 		encryptedDBs = databaseRequest.bytes;
		// 	}
		// }

		if( encryptedDBs != default(byte[]) ) {
			
			Cipher.EncryptDecrypt( encryptedDBs );
			byte[] decompressedDBs = LZMAtools.DecompressLZMAByteArrayToByteArray( encryptedDBs );
			List<Database> externalDatabases = JsonConvert.DeserializeObject<List<Database>>( Encoding.ASCII.GetString( decompressedDBs ) );

			Dictionary<Card, string> cardsToLink = new Dictionary<Card, string>();
			foreach( Database externalDatabase in externalDatabases ) {
				foreach( SerializedCard card in externalDatabase.cards ) {
					Card newCard = new Card();

					newCard.id = card.id;
					newCard.name = card.cardName;
					newCard.clans = card.clans;
					newCard.type = card.type;
					newCard.encrypted = true;

					if( newCard.type == "holding" ||
						newCard.type == "personality" ||
						newCard.type == "sensei" ||
						newCard.type == "stronghold" ||
						newCard.type == "celestial" ||
						newCard.type == "event" ||
						newCard.type == "region" ) {

						newCard.deckType = Card.DeckType.Dynasty;
					}
					else {
						newCard.deckType = Card.DeckType.Fate;
					}

					newCard.edition = card.edition;
					newCard.starting_honor = int.Parse(card.starting_honor);
					foreach( string legality in card.legalities ) {
						newCard.legal.Add( legality );
					}
					newCard.images = new List<string>();
					// newCard.cache = false; // don't cache externally loaded cards
					newCard.images.Add( card.imagePath );
					// int.TryParse( card.cost, out newCard.cost );
					// int.TryParse( card.focus, out newCard.focus );
					// int.TryParse( card.province_strength, out newCard.province_strength );
					// int.TryParse( card.gold_production, out newCard.gold_production );
					// int.TryParse( card.starting_honor, out newCard.starting_honor );
					// newCard.force = card.force;
					// newCard.chi = card.chi;
					// int.TryParse( card.personal_honor, out newCard.personal_honor );
					// int.TryParse( card.honor_req, out newCard.honor_req );
					// newCard.text = card.text;
					// newCard.flavor = card.flavor;

					// link card if necessary
					if( newCard.id.EndsWith("b") ) {
						 // remove "b" at end of id
						// print("Adding " + newCard.id + " to list of linked cards with pointer to " + newCard.id.Remove( newCard.id.Length - 1 ) );
						cardsToLink.Add( newCard, newCard.id.Remove( newCard.id.Length - 1 ) );
					}

					TryToAddCard( newCard );

					// print("Added card " + newCard.name + " to database from external source with id " + newCard.id);
				}
			}

			// link cards
			foreach( Card cardToLink in cardsToLink.Keys ) {
				Card sisterCard = cardsByID[ cardsToLink[cardToLink] ];

				print( "Trying to link " + cardToLink.name + " with id " + cardToLink.id + " to " + sisterCard.id );

				cardToLink.linkedCard = sisterCard;
				sisterCard.linkedCard = cardToLink;
			}
		}
	}

	public void FrameRateUpFor( float forTime ) {
		StartCoroutine( Turbo(forTime) );
	}

	public void FrameRateUp() {
		Application.targetFrameRate = highPowerFrameRate;
	}

	public void FrameRateDown() {
		if( PlayerPrefs.GetInt("optimizePower", 0) == 1 ) Application.targetFrameRate = lowPowerFrameRate;
	}

	IEnumerator Turbo( float forTime ) {
		if( performanceTimer == 0f ) {
			performanceTimer = forTime;
			FrameRateUp();
			// print("Frame rate set to 60 " + Time.time);

			while( performanceTimer > 0f ) {
				performanceTimer -= Time.deltaTime;
				yield return null;
			}

			FrameRateDown();
			performanceTimer = 0f;
			// print("Frame rate set to 15 " + Time.time);
		}
		else {
			performanceTimer = Mathf.Max( performanceTimer, forTime );
		}
	}

	// static database methods

		// Static methods for processing text

	public string TrimCardLine( string textToTrim ) {
		return TrimCardLine( textToTrim, false );
	}

	public string GetCardQuantity( string textToTrim ) {
		return TrimCardLine( textToTrim, true ).Trim(' ');
	}

	public int GetCardQuantityInt( string textToTrim ) {
		string quantityText = GetCardQuantity( textToTrim );

		quantityText = quantityText.Trim('x');

		int quantity = 1;

		if( quantityText != "" ) {
			int.TryParse( quantityText, out quantity );
		}

		return quantity;
	}

	public string TrimCardType( string cardText, bool returnType = false ) {
		int parenStart = cardText.IndexOf("(");

		if( parenStart == -1 && returnType ) cardText = "";

		while( parenStart != -1 ) {
			int parenEnd = cardText.IndexOf(")", parenStart);

			string potentialType = cardText.Substring( parenStart + 1, parenEnd - (parenStart + 1) ).ToLower();

			if( potentialType == "holding" ||
				potentialType == "personality" ||
				potentialType == "ring" ||
				potentialType == "spell" ||
				potentialType == "strategy" ||
				potentialType == "follower" ||
				potentialType == "item" ||
				potentialType == "sensei" ||
				potentialType == "stronghold" ||
				potentialType == "celestial" ||
				potentialType == "event" ||
				potentialType == "region" ) {

				return returnType ? potentialType : cardText.Remove(parenStart, parenEnd - (parenStart - 1)).Trim(' ');
			}

			parenStart = cardText.IndexOf("(", parenEnd);
		}

		return returnType ? "" : cardText;
	}

	public string TrimPrinting( string cardText, bool returnStub = false ) {
		int printingStartIndex = cardText.IndexOf( "[" ) + 1;
		int printingEndIndex = -1;

		if( printingStartIndex != 0 ) {
			printingEndIndex = cardText.IndexOf( "]", printingStartIndex );
		}

		if( printingStartIndex == 0 && returnStub ) cardText = "";

		if( printingStartIndex != 0 && returnStub ) {
			cardText = cardText.Substring( printingStartIndex, printingEndIndex - printingStartIndex);
		}

		if( printingStartIndex != 0 && !returnStub ) {
			cardText = cardText.Remove( printingStartIndex - 1 );
		}

		return cardText.Trim(' ');
	}

	// Must be called after all other trimmings because the contained edition could be anything
	public string TrimEdition( string cardText, bool returnEdition = false ) {
		int parenStart = cardText.IndexOf("(");

		if( parenStart > -1 ) {
			int parenEnd = cardText.IndexOf(")", parenStart);

			if( returnEdition ) return cardText.Substring( parenStart + 1, parenEnd - (parenStart + 1) ).Trim(' ');
			else return cardText.Remove(parenStart, parenEnd - (parenStart - 1)).Trim(' ');
		}
		else {
			if( returnEdition ) return "";
			else return cardText;
		}
	}

	public string TrimXPLevel( string cardText, bool returnXPLevel = false ) {

		int xpTagLength = 5; // end of the - exp tag
		int xpIndexStart = cardText.IndexOf("- inexp");
		if( xpIndexStart == -1 ) xpIndexStart = cardText.IndexOf("- exp");
		if( xpIndexStart > -1 ) {
			// print(cardText + " is experienced");
			string xpLvl = "experienced ";
			int xpLevel = 0;
			// if not just "experienced" but "experienced 2" etc.
			if( xpIndexStart + xpTagLength < cardText.Length ) {
				if( int.TryParse( cardText.Substring( xpIndexStart + xpTagLength, 1 ), out xpLevel ) ) {
					int xpLevelEndIndex = xpIndexStart + xpTagLength + 1;
					xpLvl += xpLevel + cardText.Substring( xpLevelEndIndex, cardText.Length - xpLevelEndIndex ).Trim(' ');
					print( cardText + " has xp level of " + xpLevel );
				}
				else {
					// print( "Could not parse " + cardText.Substring( xpIndexStart + xpTagLength, cardText.Length - (xpIndexStart + xpTagLength) ).Trim(' ') );
				}
			}

			if( returnXPLevel ) return xpLvl;
			else return cardText.Remove( xpIndexStart ).Trim(' ');
		}
		else {
			if( returnXPLevel ) return "";
			else return cardText;
		}
	}

	public string TrimCardLine( string textToTrim, bool returnStub ) { // stub is the number before the name
		string cleanedText = textToTrim.Trim(' ');
		if( cleanedText == "" ) return cleanedText;

		string firstWord = cleanedText;
		int firstSpace = cleanedText.IndexOf(' ');

		if( firstSpace > -1 ) {
			firstWord = cleanedText.Substring( 0, firstSpace );
			firstWord = firstWord.Replace("x",""); // remove x before numbers etc
		}
		firstSpace = cleanedText.IndexOf(' '); // because it may have changed above

		int quantity = 1;

		bool numbered = firstWord.Length > 0 && !firstWord.Contains(",") && firstSpace > -1 ? int.TryParse( firstWord[0] + "", out quantity ) : false; // in case the first word has been stripped of all usable data

		if( returnStub ) {
			return numbered ? cleanedText.Substring( 0, firstSpace ) : "";
		}
		else {
			return numbered ? cleanedText.Substring( firstSpace + 1 ) : cleanedText;
		}
	}

	public Card VerifyTextAgainstDatabase( string textToVerify ) {
		string trimmedText = TrimCardLine( textToVerify.ToLower().Trim(' ') );
		string edition = TrimEdition( trimmedText, true );
		string trimmedTextSansEdition = TrimEdition( trimmedText );

		if( trimmedTextSansEdition == "" ) return null;

		Card verifiedCard = null;
		for( int index = 0; index < 2; index++ ) {
			string textToSearchFor = trimmedTextSansEdition;

			if( index == 1 ) textToSearchFor = trimmedText; // for cards labeled with their edition in their name in the database

			if( Persistent.data.cardsByName.TryGetValue( textToSearchFor, out verifiedCard ) ) {
				if( !string.IsNullOrEmpty( edition ) ) {

					if( verifiedCard.ContainsEdition( edition ) ) {

						Card clone = verifiedCard.Clone();
						clone.edition = edition;
						return clone;
					}
					else {
						verifiedCard = null;
					}
				}
			}
		}

		return verifiedCard;

		// failed to verify
		// return null;
	}

	public Card VerifyIDAgainstDatabase( string idToVerify ) {

		Card verifiedCard = null;
		if( Persistent.data.cardsByID.TryGetValue( idToVerify, out verifiedCard ) ) {
			return verifiedCard;
		}

		// failed to verify
		return null;
	}

	public IEnumerator GetCardFromOracle( Card cardInDatabase, Action<Card> returnCard ) {
		// search for all card information possible here

		WWWForm queryPost = new WWWForm();
		queryPost.AddField( "search_13", TrimXPLevel( cardInDatabase.name ) ); // card name
		queryPost.AddField( "search_sel_14[]", cardInDatabase.type.Capitalize() );

		if( cardInDatabase.clans[0] != "toturi" ) {
			if( cardInDatabase.type == "personality" || cardInDatabase.type == "stronghold" ) {
				queryPost.AddField( "search_sel_12[]", cardInDatabase.clans[0].Capitalize() );
			}
		}

		if( cardInDatabase.edition != "" && cardInDatabase.edition.Length > 4 ) {
			string edition = cardInDatabase.edition.Capitalize();
			if( Persistent.data.baseSets.Contains( edition.Trim() ) ) edition += " Edition";
			
			queryPost.AddField( "search_sel_35[]", edition );
			print("Searching oracle for card with edition: " + edition );
		}

		if( cardInDatabase.rarity == "c" ) queryPost.AddField( "search_sel_38[]", "Common" );
		if( cardInDatabase.rarity == "u" ) queryPost.AddField( "search_sel_38[]", "Uncommon" );
		if( cardInDatabase.rarity == "r" ) queryPost.AddField( "search_sel_38[]", "Rare" );
		if( cardInDatabase.rarity == "f" ) queryPost.AddField( "search_sel_38[]", "Fixed" );
		if( cardInDatabase.rarity == "p" ) queryPost.AddField( "search_sel_38[]", "Promo" );

		int cardForce = cardInDatabase.IntForce();
		if( cardForce != -1 ) queryPost.AddField( "search_start_1", cardForce );
		if( cardForce != -1 ) queryPost.AddField( "search_end_1", cardForce );

		int cardChi = cardInDatabase.IntChi();
		if( cardChi != -1 ) queryPost.AddField( "search_start_2", cardChi );
		if( cardChi != -1 ) queryPost.AddField( "search_end_2", cardChi );

		if( cardInDatabase.honor_req != -99 && cardInDatabase.honor_req != -1 ) {
			queryPost.AddField( "search_start_3", cardInDatabase.honor_req );
			queryPost.AddField( "search_end_3", cardInDatabase.honor_req );
		}

		if( cardInDatabase.personal_honor != -99 ) {
			queryPost.AddField( "search_start_4", cardInDatabase.personal_honor );
			queryPost.AddField( "search_end_4", cardInDatabase.personal_honor );
		}

		if( cardInDatabase.cost != -99 ) {
			queryPost.AddField( "search_start_5", cardInDatabase.cost );
			queryPost.AddField( "search_end_5", cardInDatabase.cost );
		}

		if( cardInDatabase.focus != -99 ) {
			queryPost.AddField( "search_start_6", cardInDatabase.focus );
			queryPost.AddField( "search_end_6", cardInDatabase.focus );
		}

		if( cardInDatabase.province_strength != -99 ) {
			queryPost.AddField( "search_start_54", cardInDatabase.province_strength );
			queryPost.AddField( "search_end_54", cardInDatabase.province_strength );
		}

		if( cardInDatabase.gold_production != -99 ) {
			queryPost.AddField( "search_start_55", cardInDatabase.gold_production );
			queryPost.AddField( "search_end_55", cardInDatabase.gold_production );
		}

		if( cardInDatabase.starting_honor != -99 ) {
			queryPost.AddField( "search_start_56", cardInDatabase.starting_honor );
			queryPost.AddField( "search_end_56", cardInDatabase.starting_honor );
		}

		WWW postRequest = new WWW( Persistent.data.pathToOracle + "dosearch", queryPost );
		yield return postRequest;

		if( string.IsNullOrEmpty( postRequest.error ) ) {
			// print("GETTING " + postRequest.text);

			string cardId = "";
			int idStartIndex = postRequest.text.IndexOf("#cardid=") + 8;
			if( idStartIndex > 7 ) { // i.e. it wasn't -1 before adding the index above
				int idEndIndex = postRequest.text.IndexOf( ",", idStartIndex );
				cardId = postRequest.text.Substring(idStartIndex, idEndIndex - idStartIndex);

				yield return StartCoroutine( VerifyIDAgainstOracle( cardId, delegate(Card oracleCard, int q){ returnCard( oracleCard ); } ) );
			}
			else {
				returnCard( null );
			}
		}
		else {
			returnCard( null );
		}
	}

	public IEnumerator VerifyTextAgainstOracle( string nameToVerify, Action<Card, int> returnCardAndQuantity ) {

		if( playtestGroup != "" ) yield break;
		if( nameToVerify == "" ) yield break;
		
		string trimmedText = TrimCardLine( nameToVerify );
		// trimmedText = ReplaceCommonTerms( trimmedText );

		// finished accounting for xp level

		int quantity = GetCardQuantityInt( nameToVerify );

		// trim off and account for xp level
		string withoutType = TrimCardType( trimmedText );
		string cardType = TrimCardType( trimmedText, true );
		string withoutPrinting = TrimPrinting( withoutType );
		string searchText = TrimEdition( withoutPrinting );
		string edition = TrimEdition( withoutPrinting, true ); // just the tag
		string xpLvl = TrimXPLevel( searchText, true );
		searchText = TrimXPLevel( searchText );

		// check if card is cached in dynamic database
		Card cachedCard = null;
		// print("Searching oracle for card with text " + trimmedText);
		if( Persistent.data.oracleNames.TryGetValue( trimmedText, out cachedCard ) ) {
			// print("Found cached card for " + trimmedText);
			returnCardAndQuantity( cachedCard, quantity );
			yield break;
		}

		// doesn't appear to be, so search the Oracle

		// print("Searching oracle for card with name " + searchText + " and edition " + edition);

		// prepare javascript form submissions
		WWWForm nameSubmission = new WWWForm();

		nameSubmission.AddField( "search_13", searchText ); // card name
		if( xpLvl != "" ) nameSubmission.AddField( "search_7", xpLvl ); // card name
		if( edition != "" ) nameSubmission.AddField( "search_sel_35[]", edition);
		if( cardType != "" ) nameSubmission.AddField( "search_sel_14[]", cardType);

		// print("Creating search for card with name " + searchText + " and edition " + edition + " and type " + cardType);

		// print("POSTING " + Encoding.ASCII.GetString( nameSubmission.data ) );

		WWW postRequest = new WWW( Persistent.data.pathToOracle + "dosearch", nameSubmission );

		yield return postRequest;

		if( string.IsNullOrEmpty( postRequest.error ) ) {

			// print("GETTING " + postRequest.text);

			string cardId = "";
			int idStartIndex = postRequest.text.IndexOf("#cardid=") + 8;
			if( idStartIndex > 7 ) { // i.e. it wasn't -1 before adding the index above
				int idEndIndex = postRequest.text.IndexOf( ",", idStartIndex );
				cardId = postRequest.text.Substring(idStartIndex, idEndIndex - idStartIndex);

				// int hashIndexStart = postRequest.text.IndexOf( "#hashid=" ) + 8;
				// // print("Hash index starts at " + hashIndexStart + " of card text \n" + postRequest.text);

				// if( hashIndexStart > 7 ) {
				// 	int hashIndexEnd = postRequest.text.IndexOf( ",#", hashIndexStart );

				// 	string hashId = postRequest.text.Substring( hashIndexStart, hashIndexEnd - hashIndexStart );
					// print("Hash Id identified as " + hashId);

					yield return StartCoroutine( VerifyIDAgainstOracle( cardId, returnCardAndQuantity, quantity ) );
				// }
			}
			else {
				returnCardAndQuantity( null, 0 );
			}
		}
		else {
			// print("Failed to find card");
			returnCardAndQuantity( null, 0 );
		}
	}

	public static List<string> GetCardDetailsFromOracleQuery( string pageSlush, string nameStartSign, string nameEndSign, int startSignLength ) {
		List<string> cardNames = new List<string>();

		int nameStartIndex = pageSlush.IndexOf( nameStartSign ) + startSignLength;
		while( nameStartIndex > startSignLength - 1 && nameStartIndex < pageSlush.Length ) {
			int nameEndIndex = pageSlush.IndexOf( nameEndSign, nameStartIndex );

			if( nameEndIndex != -1 ) {
				string cardName = pageSlush.Substring( nameStartIndex, nameEndIndex - nameStartIndex );
				cardNames.Add( cardName.ReplaceCommonTerms() );

				nameStartIndex = pageSlush.IndexOf( nameStartSign, nameEndIndex ) + startSignLength;
			}
			else {
				// unclosed name field in source HTML.  should hopefully never happen
				nameStartIndex = -1;
			}
		}

		return cardNames;
	}

	public IEnumerator VerifyIDAgainstOracle( string cardId, Action<Card, int> returnCardAndQuantity ) {
		if( playtestGroup != "" ) yield break;

		yield return StartCoroutine( VerifyIDAgainstOracle( cardId, returnCardAndQuantity, 1 ) );
	}

	public IEnumerator VerifyIDAgainstOracle( string cardId, Action<Card, int> returnCardAndQuantity, int quantity ) {
		print("Initiating id query for id " + cardId );
		if( playtestGroup != "" ) yield break;

		Card card = null;

		if( Persistent.data.oracleIDs.TryGetValue( cardId, out card ) ) {
			print("Found existing cached oracle card " + card + " at path " + pathToOracleIDs );
			if( card.isProxy ) print("Deserializing card " + card.name + " that is proxy");

			returnCardAndQuantity( card, quantity );
			yield break;
		}

		print("Querying oracle for card with id " + cardId);

		WWWForm idSubmission = new WWWForm();
		idSubmission.AddField( "cardid", cardId );

		// WWW cardRequest = new WWW( Persistent.data.pathToOracle + "docard", idSubmission.data, GenerateRequestHeaders() );
		UnityWebRequest cardRequest = UnityWebRequest.Post( Persistent.data.pathToOracle + "docard", idSubmission );
		GenerateRequestHeaders( cardRequest );

		// yield return cardRequest;
		yield return cardRequest.Send();

		// if( string.IsNullOrEmpty( cardRequest.error ) ) {
		if( !cardRequest.isError ) {

			// found card data, so build card
			card = new Card();
			card.id = cardId;
			// card.cache = true;
			card.isFromOracle = true;

			print("Built card to fill with Oracle query: \n" + cardRequest.downloadHandler.text );

			string cardName = "";
			int nameStartIndex = cardRequest.downloadHandler.text.IndexOf( "greenhead" ) + 27;
			print("Name start index is " + nameStartIndex );
			if( nameStartIndex > 26 ) {
				int nameEndIndex = cardRequest.downloadHandler.text.IndexOf( "</td>", nameStartIndex );
				print("Name end index is " + nameEndIndex);

				cardName = cardRequest.downloadHandler.text.Substring(nameStartIndex, nameEndIndex - nameStartIndex);
				card.name = cardName.ReplaceCommonTerms();

				print("Card name harvested from Oracle = " + card.name);
			}

			string cardType = "";
			int typeStartIndex = cardRequest.downloadHandler.text.IndexOf( "Printed Card Type" ) + 92;
			if( typeStartIndex > 91 ) {
				int typeEndIndex = cardRequest.downloadHandler.text.IndexOf( "</", typeStartIndex );

				cardType = cardRequest.downloadHandler.text.Substring(typeStartIndex, typeEndIndex - typeStartIndex).ToLower();
				card.type = cardType;

				if( cardType == "strategy" || cardType == "follower" || cardType == "item" || cardType == "spell" || cardType == "ring" || cardType == "other" ) {
					card.deckType = Card.DeckType.Fate;
				}

				if( cardType == "proxy" ) {
					card.isProxy = true;
				}

				print("Card type determined to be " + card.type);
			}

			int setIndex = cardRequest.downloadHandler.text.IndexOf( "<span class=\"currentnest\">" ) + 26;
			if( setIndex > 25 ) {
				int setIndexEnd = cardRequest.downloadHandler.text.IndexOf( "</span>", setIndex );
				string setName = cardRequest.downloadHandler.text.Substring(setIndex, setIndexEnd - setIndex);
				card.setName = setName;
				card.edition = setName;
				card.editions.Add( setName );
				print("Queried for Oracle card with set name " + setName);
			}

			int clanIndex = cardRequest.downloadHandler.text.IndexOf( "Printed Clan" ) + 87;
			if( clanIndex > 86 ) {
				int clanIndexEnd = cardRequest.downloadHandler.text.IndexOf( "</", clanIndex );
				string clan = cardRequest.downloadHandler.text.Substring(clanIndex, clanIndexEnd - clanIndex);
				card.clans.Add( clan.ToLower() );
			}

			if( cardType == "sensei" ) {
				int stHonor = cardRequest.downloadHandler.text.IndexOf( "iconic hr" ) + 11;
				if( stHonor > 10 ) {
					int stHonorEnd = cardRequest.downloadHandler.text.IndexOf( "</", stHonor );
					card.starting_honor = 0;
					int.TryParse( cardRequest.downloadHandler.text.Substring(stHonor, stHonorEnd - stHonor).Trim('+'), out card.starting_honor );
				}
				else {
					card.starting_honor = 0;
				}
			}

			int keywordIndex = cardRequest.downloadHandler.text.IndexOf( "Printed Keywords" ) + 104;
			if( keywordIndex > 103 ) {
				int keywordIndexEnd = cardRequest.downloadHandler.text.IndexOf( "</", keywordIndex );
				string keywords = cardRequest.downloadHandler.text.Substring(keywordIndex, keywordIndexEnd - keywordIndex);
				card.text += keywords;
			}

			card.images = new List<string>();

			int imageStartIndex = cardRequest.downloadHandler.text.IndexOf( "showimage" );
			if( imageStartIndex > -1 ) {
				int imageEndIndex = cardRequest.downloadHandler.text.IndexOf( "\">", imageStartIndex );

				string imageURL = cardRequest.downloadHandler.text.Substring(imageStartIndex, imageEndIndex - imageStartIndex);
				card.images.Add( imageURL );

				if( card.type == "stronghold" ) {
					int secondImageStart = cardRequest.downloadHandler.text.IndexOf( "showimage", imageEndIndex );

					if( secondImageStart > -1 ) {
					int secondImageEnd = cardRequest.downloadHandler.text.IndexOf( "\">", secondImageStart );
						string secondImageURL = cardRequest.downloadHandler.text.Substring(secondImageStart, secondImageEnd - secondImageStart);

						card.secondSide = secondImageURL;
						print("Loaded second side of " + secondImageURL + " on " + card.name);
					}

					// I can't believe the Oracle doesn't have starting honor listed.  You have to do it here until it does... grumble.
					switch( card.clans[0] ) {
						case "scorpion":
							card.starting_honor = 1;
						break;

						case "spider":
							card.starting_honor = 0;
						break;

						case "unicorn":
							card.starting_honor = 4;
						break;

						case "lion":
							card.starting_honor = 7;
						break;

						case "dragon":
							card.starting_honor = 5;
						break;

						case "mantis":
							card.starting_honor = 2;
						break;

						case "naga":
							card.starting_honor = 2;
						break;

						case "crane":
							card.starting_honor = 6;
						break;

						case "phoenix":
							card.starting_honor = 6;
						break;

						case "crab":
							card.starting_honor = 3;
						break;

						case "unaligned":
							print("Parsing unaligned stronghold with id " + cardId);
							if( cardId == Draft.tradDraftStrongholdID ) {
								print("Draft stronghold starts at 8 honor");
								card.starting_honor = 8;
							}
							else {
								card.starting_honor = 0;
							}
						break;
					}

					int startingHonorStart = cardRequest.downloadHandler.text.IndexOf( "iconic hr" ) + 11;
					if( startingHonorStart > 10 ) {

						int startingHonorEnd = cardRequest.downloadHandler.text.IndexOf( "</td>", startingHonorStart );

						if( startingHonorEnd > -1 ) {
							int startingHonor = card.starting_honor;
							print("Trying to parse " + cardRequest.downloadHandler.text.Substring(startingHonorStart, startingHonorEnd - startingHonorStart));
							if( int.TryParse( cardRequest.downloadHandler.text.Substring(startingHonorStart, startingHonorEnd - startingHonorStart), out startingHonor ) ) {
								card.starting_honor = startingHonor;
							}
						}
					}
				}
			}
		}
		else {
			print( "Error requesting card by id from Oracle: " + cardRequest.error );
		}

		if( card != null ) {
			if( !Persistent.data.oracleNames.ContainsKey( card.name ) ) Persistent.data.oracleNames.Add( card.name, card );
			if( !Persistent.data.oracleIDs.ContainsKey( card.id ) ) Persistent.data.oracleIDs.Add( card.id, card );
		}

		returnCardAndQuantity(card, quantity);
	}

	public static Dictionary<string, string> GenerateRequestHeaders() {
		Dictionary<string, string> requestHeaders = new Dictionary<string, string>();
		requestHeaders.Add( "Accept", "*/*" );
		// requestHeaders.Add( "Accept-Encoding", "gzip, deflate" );
		requestHeaders.Add( "Accept-Language", "en-US,en;q=0.5" );
		// requestHeaders.Add( "Content-Length", "69" );
		requestHeaders.Add( "Content-Type", "application/x-www-form-urlencoded; charset=UTF-8" );
		requestHeaders.Add( "Cookie", data.oracleCookie );
		requestHeaders.Add( "Host", "oracleofthevoid.com" );
		requestHeaders.Add( "Referer", "http://oracleofthevoid.com/oracle/" );
		requestHeaders.Add( "User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:56.0) Gecko/20100101 Firefox/56.0" );
		requestHeaders.Add( "X-Requested-With", "XMLHttpRequest" );

		return requestHeaders;
	}

	public static UnityWebRequest GenerateRequestHeaders( UnityWebRequest webReq ) {
		webReq.SetRequestHeader( "Accept", "*/*" );
		webReq.SetRequestHeader( "Accept-Language", "en-US,en;q=0.5" );
		webReq.SetRequestHeader( "Content-Type", "application/x-www-form-urlencoded; charset=UTF-8" );
		webReq.SetRequestHeader( "Cookie", data.oracleCookie );
		// webReq.SetRequestHeader( "Host", "oracleofthevoid.com" );
		// webReq.SetRequestHeader( "Referer", "http://oracleofthevoid.com/oracle/" );
		// webReq.SetRequestHeader( "User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:56.0) Gecko/20100101 Firefox/56.0" );
		webReq.SetRequestHeader( "X-Requested-With", "XMLHttpRequest" );

		return webReq;
	}

	// public static bool PlaytestImageCached( Card playtestCard ) {
	// 	FileInfo cardImage = new FileInfo( Application.persistentDataPath + Path.DirectorySeparatorChar + playtestCard.images[0] );

	// 	if( cardImage.Exists ) {
	// 		return true;
	// 	}

	// 	return false;
	// }

	//

	void OnApplicationQuit() {
		if( oracleNames.Count == 0 || oracleIDs.Count == 0 ) return;

		if( Persistent.data == this ) {
			File.WriteAllText( pathToOracleNames, JsonConvert.SerializeObject(oracleNames) );
			File.WriteAllText( pathToOracleIDs, JsonConvert.SerializeObject(oracleIDs) );
		}

		// delete temp playtest caching folder
		DirectoryInfo tempPlaytestFolder = new DirectoryInfo( Application.persistentDataPath + Path.DirectorySeparatorChar + "Playtest" );
		if( tempPlaytestFolder.Exists ) tempPlaytestFolder.Delete( true );
	}
}

