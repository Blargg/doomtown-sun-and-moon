﻿using UnityEngine;
using System.Collections;

public class CreateSoloEmissary : MonoBehaviour {

	public LoadLevel levelToLoadAfter;

	public void CreateEmissary() {
		PhotonNetwork.offlineMode = true;
		
		levelToLoadAfter.LoadLevelByName();
	}
}
