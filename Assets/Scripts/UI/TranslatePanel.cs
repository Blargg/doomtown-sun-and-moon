﻿using UnityEngine;
using System.Collections;

public class TranslatePanel : MonoBehaviour {

	public RectTransform uiPanel;
	public UnfurlScroll scroll;

	[HideInInspector] public bool displaying = false;

	public float slideSpeed = 0.5f;

	// can be removed once Unity bug that reenables canvas group properties is fixed
	public CanvasGroup scrollHead;
	public CanvasGroup scrollFoot;

	public void Toggle() {
		if( !displaying ) gameObject.SetActive( true );

		LeanTween.value(
			gameObject, y => {uiPanel.anchoredPosition = new Vector2(uiPanel.anchoredPosition.x, y);},
			uiPanel.anchoredPosition.y,
			displaying ? 850f : 0f,
			slideSpeed
		)
		.setEase(LeanTweenType.easeInOutQuad)
		.setOnComplete( delegate(){ gameObject.SetActive( displaying ); } );

		Persistent.data.FrameRateUpFor( slideSpeed );

		if( displaying ) scroll.FurlNow();
		else scroll.UnfurlNow();

		displaying = !displaying;

		// Unity bug
		if( scrollHead ) {
			scrollHead.blocksRaycasts = false;
			scrollHead.interactable = false;
		}
		if( scrollFoot ) {
			scrollFoot.blocksRaycasts = false;
			scrollFoot.interactable = false;
		}
	}
}
