﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class IncreasePerformance : MonoBehaviour, IPointerDownHandler, IDragHandler {

	public void OnPointerDown( PointerEventData evt ) {
		Persistent.data.FrameRateUp();
	}

	public void OnDrag( PointerEventData evt ) {
		Persistent.data.FrameRateUpFor(1f);
	}
}
