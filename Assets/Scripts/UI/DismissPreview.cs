﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class DismissPreview : MonoBehaviour, IPointerDownHandler {

	public ListDeckContents deckEditor;

	public void OnPointerDown(PointerEventData evt) {
		print("Pointer up event received " + Time.time);
		if( deckEditor ) {
			deckEditor.ShowPreview( null, evt.position );
		}
	}
}
