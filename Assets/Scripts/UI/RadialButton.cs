﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;
using System;

public class RadialButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler {

	public enum ClickMode { Click, Drag, Hold }
	public ClickMode clickMode = ClickMode.Click;

	public InputInfo.Action action;

	public RectTransform buttonRect;
	public float hoverSize = 70f;
	float origSize = 0f;
	public float scaleSpeed = 0.2f;

	public Clickable clickableTarget;
	public Action onComplete;

	InputInfo held = null;

	void Start() {
		origSize = buttonRect.sizeDelta.x;
	}

	public void OnPointerEnter( PointerEventData evt ) {
		LeanTween.cancel( gameObject );
		LeanTween.value( gameObject, s => buttonRect.sizeDelta = new Vector2(s, s), buttonRect.sizeDelta.x, hoverSize, scaleSpeed ).setEase(LeanTweenType.easeInOutQuad);

		Persistent.data.FrameRateUpFor( scaleSpeed );
	}

	public void OnPointerExit( PointerEventData evt ) {
		LeanTween.cancel( gameObject );
		LeanTween.value( gameObject, s => buttonRect.sizeDelta = new Vector2(s, s), buttonRect.sizeDelta.x, origSize, scaleSpeed ).setEase(LeanTweenType.easeInOutQuad);

		Persistent.data.FrameRateUpFor( scaleSpeed );
	}

	public void OnPointerClick( PointerEventData evt ) {
		print("Clicking " + clickableTarget.name + " with action " + action + " " + Time.time);
		InputInfo simulatedInput = SimulateInput();

		clickableTarget.ClickActionDown( simulatedInput );
		clickableTarget.ClickActionUp( simulatedInput );

		onComplete();
	}

	public void OnPointerDown( PointerEventData evt ) {
		if( clickMode == ClickMode.Drag ) {
			clickableTarget.DragActionStart( SimulateInput() );
		}

		if( clickMode == ClickMode.Hold ) {
			held = SimulateInput();
		}
	}

	void Update() {
		if( held != null ) {
			clickableTarget.Hover( held );
		}
	}

	public void OnPointerUp( PointerEventData evt ) {

		if( clickMode == ClickMode.Hold ) {
			held = null;
			Table.data.ShowPreview( null );
			onComplete();
		}

		if( clickMode == ClickMode.Drag ) {
			onComplete();
			// let the Table's innate clickup security handle the release of cloned cards
			print("Ending drag on " + clickableTarget.name);
			clickableTarget.DragActionEnd( SimulateInput() );
		}
	}

	public InputInfo SimulateInput() {
		InputInfo simulatedInput = new InputInfo();

		simulatedInput.Clear();
		simulatedInput.modifier = simulatedInput.modifier | action;
		simulatedInput.input = simulatedInput.input | action;
		simulatedInput.isSimulated = true;

		return simulatedInput;
	}
}
