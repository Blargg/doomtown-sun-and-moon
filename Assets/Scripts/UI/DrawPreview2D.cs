﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using System.IO;

public class DrawPreview2D : MonoBehaviour {

	public RawImage cardPreview;
	public CardStandIn cardStandIn;
	public CanvasGroup previewGroup;

	public Text watermark;

	public PositionPreview2D positioner;

	Card currentCard = null;

	public void Clear() {
		cardStandIn.gameObject.SetActive(false);
		cardPreview.gameObject.SetActive(false);
		watermark.gameObject.SetActive(false);

		previewGroup.blocksRaycasts = false;

		currentCard = null;

		StopAllCoroutines();
	}

	public IEnumerator DrawCard( Card card, Vector2 inputPos = default(Vector2), float offsetPreview = 0f ) {

		if( card == currentCard ) yield break;

		Clear();

		currentCard = card;

		bool standInNeeded = true;
		WWW request = null;

		int imageToLoadIndex = card.GetImageIndex();
		if( imageToLoadIndex > -1 ) {
		// for( int imageIndex = card.images.Count - 1; imageIndex > -1; imageIndex-- ) {

			// check for local image, in case it's been loaded with another card
			if( card.isFromOracle ) {
				string localImageURL = Persistent.data.pathToOracleCache + card.id + ".jpg";
				request = new WWW( localImageURL );
				yield return request;

				if( string.IsNullOrEmpty( request.error ) ) {

					standInNeeded = false;
					// break;
				}
			}
			else {
				string localImageURL = Persistent.data.outsideApp.FullName + Path.DirectorySeparatorChar + card.images[imageToLoadIndex];

				FileInfo localFile = new FileInfo( localImageURL );

				if( localFile.Exists ) {

					request = new WWW( Persistent.data.rootURL + localImageURL.SanitizePath() );
					yield return request;

					if( string.IsNullOrEmpty( request.error ) ) {
						standInNeeded = false;
						// break;
					}
					else {
						print(request.error);
					}
				}
				// else print(localFile.FullName + " doesn't exist");
			}
		// }
		}

		
		if( standInNeeded ) cardStandIn.CreateStandInCardFrom( card );

		cardStandIn.gameObject.SetActive(true);
		cardPreview.gameObject.SetActive(true);

		if( positioner ) positioner.PositionPreview( inputPos, offsetPreview );

		if( standInNeeded ) {
			if( imageToLoadIndex > -1 ) {
			// for( int imageIndex = card.images.Count - 1; imageIndex > -1; imageIndex-- ) {	
				// otherwise, download image and cache
				if( card.isFromOracle ) {
					string streamingImageURL = Persistent.data.pathToOracle + card.images[0]; // could possibly be changed to accommodate set nesting 
					string localImageURL = Persistent.data.cleanPathToOracleCache + card.id + ".jpg";

					// #if UNITY_EDITOR
					// 	string localImageURL = Application.persistentDataPath + "/ImageCache/" + card.id + ".jpg";
					// #else
					// 	string localImageURL = Application.dataPath + "/ImageCache/" + card.id + ".jpg";
					// #endif

					request = new WWW( streamingImageURL );
					yield return request;

					if( string.IsNullOrEmpty( request.error ) ) {
						File.WriteAllBytes(localImageURL, request.bytes);

						standInNeeded = false;
						// break;
					}
					else {
					}
				}
				else {
					string streamingImageURL = Persistent.data.pathToStreamingImages + "/" + card.images[imageToLoadIndex];
					string localImageURL = Persistent.data.outsideApp.FullName + Path.DirectorySeparatorChar + card.images[imageToLoadIndex];

					request = new WWW( streamingImageURL );
					yield return request;

					if( string.IsNullOrEmpty( request.error ) ) {
						DirectoryInfo cardDir = new DirectoryInfo(localImageURL.Substring(0, localImageURL.LastIndexOf("/") ) );
						if( !cardDir.Exists ) {
							cardDir.Create();
						}
						File.WriteAllBytes(localImageURL, request.bytes);

						standInNeeded = false;
						// break;
					}
					else {
						standInNeeded = true;
					}
				}
			// }
			}
		}

		// if the preview was quickly moved to another card
		if( card != currentCard ) yield break;

		if( standInNeeded ) {
			print("Stand in still needed for card " + card.name);
			StartCoroutine( Persistent.data.GetCardFromOracle( card, DownloadOracleImageToFolderCallback ) );
		}
		else {
			if( card.encrypted ) {
				byte[] rawData = request.bytes;
				Cipher.EncryptDecrypt( rawData );
				Texture2D decryptedTexture = new Texture2D( 2, 2 );
				decryptedTexture.LoadImage( rawData );
				cardPreview.texture = decryptedTexture;
			}
			else {
				cardPreview.texture = request.textureNonReadable;
			}

			cardStandIn.gameObject.SetActive(false);
			cardStandIn.noImageWarning.gameObject.SetActive( false );

			// if the card is external and hasn't yet been watermarked / cached
			if( watermark && card.encrypted ) {
				watermark.text = Persistent.data.playtestGroup;
				watermark.gameObject.SetActive( true );
			}
			else {
				watermark.gameObject.SetActive( false );
			}
		}

		Persistent.data.FrameRateUpFor( 1f );
		if( request != null ) request.Dispose();
	}

	public void DownloadOracleImageToFolderCallback( Card oracleCard ) {
		if( oracleCard != null && oracleCard.images.Count > 0 ) StartCoroutine( DownloadOracleImageToFolder( oracleCard ) );
	}

	IEnumerator DownloadOracleImageToFolder( Card oracleCard ) {
		int imageIndex = currentCard.GetImageIndex();

		print("Getting first oracle image attached to card " + oracleCard.name);
		string streamingImageURL = Persistent.data.pathToOracle + oracleCard.images[0]; // oracle image

		string localImageURL = "";
		if( currentCard.isFromOracle ) {
			localImageURL = Persistent.data.pathToOracleCache + currentCard.id + ".jpg";
		}
		else {
			print("Saving oracle image to local folder with path of the first image of " + currentCard.name);
			localImageURL = Persistent.data.outsideApp.FullName + Path.DirectorySeparatorChar + currentCard.images[imageIndex]; // local path
		}

		// if( oracleCard != null ) print("Returning card found on oracle: " + oracleCard.name + " with internal url " + oracleCard.images[0] + " and image url to save " + currentCard.images[imageIndex]);

		print("Downloading oracle image to folder in DrawPreview2D with local path " + localImageURL.Substring(0, localImageURL.LastIndexOf("/") ) );
		DirectoryInfo cardDir = new DirectoryInfo( localImageURL.Substring(0, localImageURL.LastIndexOf("/") ) );
		if( !cardDir.Exists ) {
			cardDir.Create();
		}

		WWW request = new WWW( streamingImageURL );
		yield return request;

		if( string.IsNullOrEmpty( request.error ) ) {
			File.WriteAllBytes(localImageURL, request.bytes);

			// if we've switched cards since
			if( currentCard.name.ToLower() != oracleCard.name.ToLower() ) yield break;

			cardStandIn.gameObject.SetActive( false );
			cardPreview.texture = request.textureNonReadable;
		}

		if( request != null ) request.Dispose();
	}
}
