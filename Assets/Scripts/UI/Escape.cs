﻿using UnityEngine;
using System.Collections;

public class Escape : MonoBehaviour {

	public LoadLevel loadLevelScript;

	void Update() {
		if( Input.GetKeyDown(KeyCode.Escape) ) {
			loadLevelScript.LoadLevelByName();
		}
	}
}
