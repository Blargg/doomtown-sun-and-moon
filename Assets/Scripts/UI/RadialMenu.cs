﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class RadialMenu : MonoBehaviour {

	public RectTransform menuRoot;
	public RectTransform pivot;
	public CanvasScaler uiScale;
	public CanvasGroup fade;
	public float optionsRadius = 1f;
	public float scaleSpeed = 1f;
	public float rotSpeed = 1f;
	public float fadeSpeed = 1f;

	public List<RadialButton> currentButtons = new List<RadialButton>();

	public void Initialize( RadialButton[] withButtons, Clickable forTarget, Vector2 atScreenPoint ) {

		menuRoot.anchoredPosition = atScreenPoint * (uiScale.referenceResolution.y / Screen.height);

		foreach( RadialButton oldButton in currentButtons ) {
			Destroy( oldButton.gameObject );
		}

		currentButtons = new List<RadialButton>();

		float degreesSep = 360f / withButtons.Length;
		for( int index = 0; index < withButtons.Length; index++ ) {
			RadialButton instance = Instantiate(withButtons[index]) as RadialButton;
			instance.buttonRect.SetParent( pivot, false );

			instance.buttonRect.anchoredPosition = pivot.anchoredPosition;
			instance.buttonRect.anchoredPosition += new Vector2( optionsRadius, 0f );
			instance.buttonRect.RotateAround( pivot.position, pivot.transform.forward, degreesSep * index );
			instance.buttonRect.rotation = Quaternion.identity;

			instance.clickableTarget = forTarget;
			instance.onComplete = Dismiss;

			currentButtons.Add( instance );
		}

		Reveal();
	}
	
	public void Reveal() {
		Persistent.data.FrameRateUpFor( scaleSpeed );

		menuRoot.localScale = new Vector3( 0.1f, 0.1f, 0.1f );
		menuRoot.rotation = Quaternion.identity;
		fade.alpha = 0f;

		gameObject.SetActive( true );

		LeanTween.cancel( gameObject );
		LeanTween.value( gameObject, s => menuRoot.localScale = new Vector3(s, s, s), 0.1f, 1f, scaleSpeed ).setEase(LeanTweenType.easeInOutQuad);
		LeanTween.value( gameObject, s => { RotateButtons(s); }, 1000f, 0f, rotSpeed ).setEase(LeanTweenType.easeInOutQuad);
		LeanTween.value( gameObject, s => fade.alpha = s, 0f, 1f, fadeSpeed ).setEase(LeanTweenType.easeInOutQuad);
	}

	public void RotateButtons( float byDegrees ) {
		foreach( RadialButton btn in currentButtons ) {
			btn.buttonRect.RotateAround( pivot.position, pivot.transform.forward, byDegrees * Time.deltaTime );
			btn.buttonRect.rotation = Quaternion.identity;
		}

	}

	public void Dismiss() {
		Persistent.data.FrameRateUpFor( scaleSpeed );

		LeanTween.cancel( gameObject );
		LeanTween.value( gameObject, s => menuRoot.localScale = new Vector3(s, s, s), menuRoot.localScale.x, 0.1f, scaleSpeed ).setEase(LeanTweenType.easeInOutQuad);
		LeanTween.value( gameObject, s => { RotateButtons(s); }, 100f, 0f, rotSpeed ).setEase(LeanTweenType.easeInOutQuad);
		LeanTween.value( gameObject, s => fade.alpha = s, fade.alpha, 0f, scaleSpeed ).setEase(LeanTweenType.easeInOutQuad).setOnComplete( () => gameObject.SetActive(false) );
	}
}
