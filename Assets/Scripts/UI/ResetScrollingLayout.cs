﻿using UnityEngine;
using System.Collections;

public class ResetScrollingLayout : MonoBehaviour {

	public RectTransform scrollContent;

	IEnumerator Start () {
		yield return null;
		
		scrollContent.anchoredPosition = new Vector2( scrollContent.anchoredPosition.x, 0f );
	}
}
