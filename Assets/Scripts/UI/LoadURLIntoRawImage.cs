﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LoadURLIntoRawImage : MonoBehaviour {

	public float fadeSpeed = 0.5f;
	public LoadingSpinner loadingSpinner;
	public RawImage targetImage;
	public RectTransform imageRect;
	public bool resizeImage = false;
	public string loadFromPrefs = "";

	float origImageWidth = 1f;

	void Start() {
		origImageWidth = imageRect.sizeDelta.x;

		if( loadFromPrefs != "" ) Submit( PlayerPrefs.GetString( loadFromPrefs, "" ) );
	}

	public void Submit( string url ) {
		loadingSpinner.gameObject.SetActive( true );

		StartCoroutine( LoadURL( url ) );
	}

	IEnumerator LoadURL( string url ) {
		WWW request = new WWW( url );

		yield return request;

		if( string.IsNullOrEmpty( request.error ) ) {
			targetImage.texture = request.textureNonReadable;

			float aspectR = (float)targetImage.texture.width / (float)targetImage.texture.height;
			print("Target image width is " + request.texture.width + " and height is " + request.texture.height + " so aspect is " + aspectR);

			if( resizeImage ) {
				imageRect.sizeDelta = new Vector2( origImageWidth * aspectR, imageRect.sizeDelta.y );
			}

			if( targetImage.color.a != 1f ) LeanTween.value( targetImage.gameObject, Fade, targetImage.color.a, 1f, fadeSpeed ).setEase(LeanTweenType.easeInOutQuad);
		}
		else {
			if( targetImage.color.a != 0f ) LeanTween.value( targetImage.gameObject, Fade, targetImage.color.a, 0f, fadeSpeed ).setEase(LeanTweenType.easeInOutQuad);
		}

		loadingSpinner.gameObject.SetActive( false );
	}

	public void Fade( float a ) {
		targetImage.color = new Color( targetImage.color.r, targetImage.color.b, targetImage.color.g, a );
	}
}
