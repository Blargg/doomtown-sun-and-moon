using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SunAndMoon : MonoBehaviour {

	public RectTransform flare;
	public Graphic glowGraphic;
	public CanvasGroup glow;
	public CanvasGroup moon;

	public Color sunGlowColor;
	public Color moonGlowColor;

	public Light skyLight;
	public Color sunSkyColor;
	public Color moonSkyColor;

	[HideInInspector] public bool isMyAction = false;

	public AudioSource newTurnChime;
	public AudioSource attackChime;
	[HideInInspector] public bool firstChime = true;

	public Text turnLabel;
	public int turnCounter {
		get {
			return _turnCounter;
		}
		set {
			if( value > 0 ) { 
				_turnCounter = value;
				if( turnLabel ) turnLabel.text = value.ToString();
			}
		}
	}
	int _turnCounter = 1;

	public void StartAction() {
		if( isMyAction ) return;

		LeanTween.value( flare.gameObject, delegate(float s){ flare.localScale = new Vector3(s,s,s); }, flare.localScale.x, 1f, 0.5f ).setEase(LeanTweenType.linear);
		LeanTween.value( flare.gameObject, delegate(float r){ flare.eulerAngles = new Vector3(0f,0f,r); }, flare.eulerAngles.z, 180f, 0.5f ).setEase(LeanTweenType.linear);

		glowGraphic.CrossFadeColor( sunGlowColor, 0.5f, true, false );

		LeanTween.value( moon.gameObject, delegate(float a){ moon.alpha = a; }, moon.alpha, 0f, 0.5f ).setEase(LeanTweenType.linear);

		LeanTween.cancel( glow.gameObject );
		LeanTween.value( glow.gameObject, delegate(float g){ glow.alpha = g; }, 0.5f, 1f, 1f ).setEase(LeanTweenType.linear).setRepeat(-1).setLoopPingPong();

		if( skyLight ) {
			LeanTween.cancel( skyLight.gameObject );
			LeanTween.value( skyLight.gameObject, delegate(Color c){ skyLight.color = c; }, skyLight.color, sunSkyColor, 0.5f ).setEase(LeanTweenType.linear);
		}

		isMyAction = true;

		if( !firstChime && newTurnChime && PlayerPrefs.GetInt( "disableSounds", 0 ) == 0 ) newTurnChime.Play();

		Persistent.data.FrameRateUpFor( 1f );
	}

	public void EndAction() {
		if( !isMyAction ) return;

		print(name + " ending action ");

		LeanTween.value( flare.gameObject, delegate(float s){ flare.localScale = new Vector3(s,s,s); }, flare.localScale.x, 0f, 0.5f ).setEase(LeanTweenType.linear);
		LeanTween.value( flare.gameObject, delegate(float r){ flare.eulerAngles = new Vector3(0f,0f,r); }, flare.eulerAngles.z, 0f, 0.5f ).setEase(LeanTweenType.linear);

		glowGraphic.CrossFadeColor( moonGlowColor, 0.5f, true, false );

		LeanTween.value( moon.gameObject, delegate(float a){ moon.alpha = a; }, moon.alpha, 1f, 0.5f ).setEase(LeanTweenType.linear);

		LeanTween.cancel( glow.gameObject );
		LeanTween.value( glow.gameObject, delegate(float g){ glow.alpha = g; }, 1f, 0.5f, 0.5f ).setEase(LeanTweenType.linear);

		if( skyLight ) {
			LeanTween.cancel( skyLight.gameObject );
			LeanTween.value( skyLight.gameObject, delegate(Color c){ skyLight.color = c; }, skyLight.color, moonSkyColor, 0.5f ).setEase(LeanTweenType.linear);
		}

		isMyAction = false;

		Persistent.data.FrameRateUpFor( 1f );
	}
}
