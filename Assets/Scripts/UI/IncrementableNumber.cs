﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class IncrementableNumber : MonoBehaviour {

	public Text numberLabel;
	public int internalValue = 0;
	public int minValue = 0;
	public int maxValue = 0;

	void Start() {
		numberLabel.text = internalValue.ToString();
	}

	public void Increment( int incrementBy ) {
		internalValue += incrementBy;
		if( internalValue > maxValue ) internalValue = maxValue;
		if( internalValue < minValue ) internalValue = minValue;

		numberLabel.text = internalValue.ToString();
	}
}
