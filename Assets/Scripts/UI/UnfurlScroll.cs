﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UnfurlScroll : MonoBehaviour {

	public RectTransform scrollPaper;
	public CanvasGroup scrollAlphaGroup;

	public float startingWidthFrom = -1f;
	public float startingHeightFrom = -1f;
	public float startingWidthTo = -1f;
	public float startingHeightTo = -1f;
	public float furlSpeed = 0.5f;

	public float startingAlphaFrom = -1f;
	public float startingAlphaTo = -1f;
	public float fadeSpeed = 0.5f;

	public float startingDelay = 0f;
	public float endingDelay = 0f;

	// events
	public delegate void TransitionEvent();
	public delegate void FadedEvent( float a );

	public event TransitionEvent Furling;
	public event TransitionEvent Unfurling;
	public event FadedEvent Faded;

	[HideInInspector] public bool furling = false;

	void OnEnable() {
		LoadLevel.LoadingLevel += LoadingLevelEvent;
	}

	void Start() {
		if( startingWidthFrom != -1 ) scrollPaper.sizeDelta = new Vector2( startingWidthFrom, scrollPaper.sizeDelta.y );
		if( startingHeightFrom != -1 ) scrollPaper.sizeDelta = new Vector2( scrollPaper.sizeDelta.x, startingHeightFrom );
		if( startingAlphaFrom != -1 ) scrollAlphaGroup.alpha = startingAlphaFrom;

		if( startingDelay >= 0f ) StartCoroutine( Unfurl( startingDelay ) );

		Faded += a => gameObject.SetActive(a != 0f); 
	}

	public void ResizeRectWidthTo( float targetWidth ) {
		LeanTween.value( gameObject, ResizeWidth, scrollPaper.sizeDelta.x, targetWidth, furlSpeed ).setEase(LeanTweenType.easeInOutQuad);
		Persistent.data.FrameRateUpFor( furlSpeed );
	}

	public void ResizeRectWidthTo( float targetWidth, System.Action onComplete ) {
		LeanTween.value( gameObject, ResizeWidth, scrollPaper.sizeDelta.x, targetWidth, furlSpeed ).setEase(LeanTweenType.easeInOutQuad).setOnComplete( onComplete );
		Persistent.data.FrameRateUpFor( furlSpeed );
	}

	public void ResizeRectHeightTo( float targetHeight ) {
		LeanTween.value( gameObject, ResizeHeight, scrollPaper.sizeDelta.y, targetHeight, furlSpeed ).setEase(LeanTweenType.easeInOutQuad);
		Persistent.data.FrameRateUpFor( furlSpeed );
	}

	public void ResizeRectHeightTo( float targetHeight, System.Action onComplete ) {
		LeanTween.value( gameObject, ResizeHeight, scrollPaper.sizeDelta.y, targetHeight, furlSpeed ).setEase(LeanTweenType.easeInOutQuad).setOnComplete( onComplete );
		Persistent.data.FrameRateUpFor( furlSpeed );
	}

	public void FadeAlphaTo( float targetAlpha ) {
		LeanTween.value( gameObject, a => {scrollAlphaGroup.alpha = a;}, scrollAlphaGroup.alpha, targetAlpha, fadeSpeed ).setEase(LeanTweenType.easeInOutQuad).setOnComplete( delegate(){Faded(scrollAlphaGroup.alpha); furling = false;} );
		Persistent.data.FrameRateUpFor( furlSpeed );
	}

	public void ResizeHeight( float height ) {
		scrollPaper.sizeDelta = new Vector2( scrollPaper.sizeDelta.x, height );
	}

	public void ResizeWidth( float width ) {
		scrollPaper.sizeDelta = new Vector2( width, scrollPaper.sizeDelta.y );
	}

	public void UnfurlAfterDelay( float delay ) {
		gameObject.SetActive( true );

		StartCoroutine( Unfurl(delay) );
	}

	public void FurlAfterDelay( float delay ) {
		if( gameObject.activeInHierarchy) StartCoroutine( Furl(delay) );
	}

	public void UnfurlNow() {
		gameObject.SetActive( true );

		StartCoroutine( Unfurl(0f) );
	}

	public void FurlNow() {
		StartCoroutine( Furl(0f) );
	}

	IEnumerator Unfurl( float afterDelay ) {
		yield return new WaitForSeconds( afterDelay );

		furling = true;

		LeanTween.cancel( gameObject );
		if( Unfurling != null ) Unfurling();

		if( startingWidthTo != -1 ) ResizeRectWidthTo( startingWidthTo );
		if( startingHeightTo != -1 ) ResizeRectHeightTo( startingHeightTo );
		if( startingAlphaTo != -1 ) FadeAlphaTo( startingAlphaTo );
	}

	IEnumerator Furl( float afterDelay ) {
		yield return new WaitForSeconds( afterDelay );

		furling = true;

		LeanTween.cancel( gameObject );
		if( Furling != null ) Furling();

		if( startingWidthFrom != -1 ) ResizeRectWidthTo( startingWidthFrom );
		if( startingHeightFrom != -1 ) ResizeRectHeightTo( startingHeightFrom );
		if( startingAlphaFrom != -1 ) FadeAlphaTo( startingAlphaFrom );
	}

	// events

	public void LoadingLevelEvent() {

		StartCoroutine( Furl( endingDelay ) );
	}

	public void OnDisable() {
		LoadLevel.LoadingLevel -= LoadingLevelEvent;
	}
}
