﻿using UnityEngine;
using System.Collections;

public class PositionPreview2D : MonoBehaviour {

	public RectTransform previewRect;

	public void PositionPreview( Vector2 relPos, float offsetPreview ) {

		float cardWidth = previewRect.sizeDelta.x / 2f;

		if( relPos.x < Screen.width / 2f ) {
			cardWidth *= -1f;
			offsetPreview *= -1f;
		} 

		relPos -= new Vector2( cardWidth + offsetPreview, 0f );

		// Keep preview on screen
		if( relPos.y - previewRect.sizeDelta.y / 2f < 0f ) {
			relPos.y = previewRect.sizeDelta.y / 2f;
		}
		else if( relPos.y + previewRect.sizeDelta.y / 2f > Screen.height ) {
			relPos.y = Screen.height - previewRect.sizeDelta.y / 2f;
		}

		if( relPos.x - previewRect.sizeDelta.x / 2f < 0f ) {
			relPos.x = previewRect.sizeDelta.x / 2f;
		}
		else if( relPos.x + previewRect.sizeDelta.x / 2f > Screen.width ) {
			relPos.x = Screen.width - previewRect.sizeDelta.x / 2f;
		}

		previewRect.anchoredPosition = relPos;
	}
}
