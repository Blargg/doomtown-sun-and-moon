﻿using UnityEngine;
using System.Collections;

public class PulseScroll : MonoBehaviour {

	public UnfurlScroll scroll;
	public float pulseSpeed;
	public float afterDelay;

	IEnumerator Pulse () {
		scroll.Furling += delegate{LeanTween.cancel(gameObject);};

		yield return new WaitForSeconds(afterDelay);

		LeanTween.value( gameObject, (float a)=>scroll.scrollAlphaGroup.alpha=a, 1f, 0f, pulseSpeed ).setEase(LeanTweenType.easeInOutQuad).setLoopPingPong().setRepeat(-1);
	}

	public void OnEnable() {
		StartCoroutine( Pulse() );
	}

	public void OnDisable() {
		StopAllCoroutines();
	}
}
