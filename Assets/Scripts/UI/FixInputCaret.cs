﻿using UnityEngine;
using System.Collections;

public class FixInputCaret : MonoBehaviour {

	IEnumerator Start () {
		yield return null;

		RectTransform chatCaret = (RectTransform)transform.Find(gameObject.name + " Input Caret");
		if( chatCaret ) {
			chatCaret.pivot = new Vector2( 0f, chatCaret.pivot.y );
			chatCaret.SetSiblingIndex(1);
		}
	}
}
