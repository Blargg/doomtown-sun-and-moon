﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LoadingSpinner : MonoBehaviour {

	public RectTransform spinner;
	public CanvasGroup opacity;
	public Text error;

	float spinRate = -150f;
	const float fadeSpeed = 0.5f;

	void Start() {
		// spinner.rotation = Quaternion.Euler( Vector3.zero );

		opacity.alpha = 0f;
		Fade( 1f );

		Persistent.data.FrameRateUp();
	}

	void Update () {
		spinner.Rotate( 0f, 0f, spinRate * Time.deltaTime );
	}

	public void Fade( float to ) {
		gameObject.SetActive( true );
		LeanTween.value( gameObject, a => {opacity.alpha = a;}, opacity.alpha, to, fadeSpeed ).setEase(LeanTweenType.easeInOutQuad).setOnComplete( delegate(){ if(to == 0f) gameObject.SetActive(false); } );
	}

	void OnDisable() {
		Persistent.data.FrameRateDown();
	}
}
