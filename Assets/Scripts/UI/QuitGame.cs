﻿using UnityEngine;
using System.Collections;

public class QuitGame : MonoBehaviour {

	float delay = 0f;

void Update() {
	if( Input.GetKeyDown(KeyCode.Escape) ) {
		Clicked();
	}
}

public void Clicked() {
	StartCoroutine( WaitAndQuit() );
}

IEnumerator WaitAndQuit() {
	yield return new WaitForSeconds( delay );
	Application.Quit();
}
	
}
