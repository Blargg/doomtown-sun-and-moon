﻿using UnityEngine;
using System.Collections;

public class Pop : MonoBehaviour {

	public RectTransform popRect;
	public CanvasGroup popGroup;

	public float startingScaleFrom = -1f;
	public float startingScaleTo = -1f;
	public float popSpeed = 0.5f;

	public float startingAlphaFrom = -1f;
	public float startingAlphaTo = -1f;
	public float fadeSpeed = 0.5f;

	[HideInInspector] public bool popping = false;

	void Start() {
		if( startingScaleFrom != -1 ) popRect.localScale = new Vector3( startingScaleFrom, startingScaleFrom, startingScaleFrom );
		if( startingAlphaFrom != -1 ) popGroup.alpha = startingAlphaFrom;
	}

	void ScaleTo( float targetScale ) {
		LeanTween.value( gameObject, Rescale, popRect.localScale.x, targetScale, popSpeed ).setEase(LeanTweenType.easeOutElastic);
		Persistent.data.FrameRateUpFor( popSpeed );
	}

	void ScaleTo( float targetScale, System.Action onComplete ) {
		LeanTween.value( gameObject, Rescale, popRect.localScale.x, targetScale, popSpeed ).setEase(LeanTweenType.easeOutElastic).setOnComplete( onComplete );
		Persistent.data.FrameRateUpFor( popSpeed );
	}

	void FadeAlphaTo( float targetAlpha ) {
		LeanTween.value( gameObject, a => {popGroup.alpha = a;}, popGroup.alpha, targetAlpha, fadeSpeed ).setEase(LeanTweenType.easeOutElastic).setOnComplete( delegate(){popping = false; if(targetAlpha == 0f) gameObject.SetActive(false); } );
		Persistent.data.FrameRateUpFor( popSpeed );
	}

	void Rescale( float scale ) {
		popRect.localScale = new Vector3( scale, scale, scale );
	}

	public void PopIn() {
		gameObject.SetActive( true );

		popping = true;

		LeanTween.cancel( gameObject );

		if( startingScaleTo != -1 ) ScaleTo( startingScaleTo );
		if( startingAlphaTo != -1 ) FadeAlphaTo( startingAlphaTo );
	}

	public void PopOut() {
		popping = true;

		LeanTween.cancel( gameObject );

		if( startingScaleFrom != -1 ) ScaleTo( startingScaleFrom );
		if( startingAlphaFrom != -1 ) FadeAlphaTo( startingAlphaFrom );
	}
}
