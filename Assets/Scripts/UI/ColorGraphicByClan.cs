﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ColorGraphicByClan : MonoBehaviour {

	public int colorIndex = 1;
	public Graphic graphicToColor;

	void Start() {
		Table.data.TableResetEvent += ResetColors;

		ResetColors( Table.data.myClan );
	}

	public void ResetColors( string forClan ) {
		if( forClan == null ) return;
		
		Color[] fadedColors = Persistent.data.GetColorsForClan(forClan);
		if( graphicToColor ) graphicToColor.color = new Color( fadedColors[colorIndex].r, fadedColors[colorIndex].g, fadedColors[colorIndex].b, graphicToColor.color.a );
	}
}
