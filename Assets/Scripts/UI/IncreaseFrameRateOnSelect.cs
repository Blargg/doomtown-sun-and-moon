﻿using UnityEngine;
using System.Collections;

using UnityEngine.EventSystems;

public class IncreaseFrameRateOnSelect : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

	public void OnPointerDown( PointerEventData evt ) {
		Persistent.data.FrameRateUp();
	}

	public void OnPointerUp( PointerEventData evt ) {
		Persistent.data.FrameRateDown();
	}
}
