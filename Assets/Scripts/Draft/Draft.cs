﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

using Newtonsoft.Json;
using System.IO;

public class Draft : MonoBehaviour {

	// draft deck
	public string deckClan = "unaligned";
	public LegalityFilter legalityFilter;
	public Deck deckEmissaryPrefab;

	public Deck currentDeck;

	// card collections
	public List<Card> legalDynastyCards = new List<Card>();
	public List<Card> legalFateCards = new List<Card>();
	public List<Card> legalStrongholds = new List<Card>();

	public List<Card> commonDynastyCards = new List<Card>();
	public List<Card> uncommonDynastyCards = new List<Card>();
	public List<Card> rareDynastyCards = new List<Card>();
	public List<Card> promoDynastyCards = new List<Card>();
	public List<Card> fixedDynastyCards = new List<Card>();

	public List<Card> commonFateCards = new List<Card>();
	public List<Card> uncommonFateCards = new List<Card>();
	public List<Card> rareFateCards = new List<Card>();
	public List<Card> promoFateCards = new List<Card>();
	public List<Card> fixedFateCards = new List<Card>();

	public List<Card> commonPersonalities;
	public List<Card> uncommonPersonalities;
	public List<Card> rarePersonalities;
	public List<Card> fixedPersonalities;
	public List<Card> promoPersonalities;

	public List<Card> holdings;

	int maxDynasty = 30;
	int maxFate = 30;

	float chanceToRollHolding = 0.6f;
	float chanceToRerollPersonality = 0.75f;
	float chanceToRerollPromo = 0.9f;

	// cards quantities (must be even numbers to divide evenly between fate and dynasty)
	int numCommons = 8;
	int numUncommons = 4;
	int numRares = 1;
	int numFixed = 2;
	int numPromos = 1;
	int numHoldings = 1;
	int cullCardsAfterPick = 3;

	// card lists
	public DraftList dynasty;
	public DraftList fate;
	public TranslateBookmark legalityBookmark;
	public TranslateBookmark dynastyBookmark;
	public TranslateBookmark fateBookmark;

	// layout
	public HorizontalLayoutGroup strongholdLayout;
	public CanvasGroup strongholdSelectionTab;
	public CanvasGroup draftSelectionTab;
	public ScrollRect draftScrollRect;
	public GridLayoutGroup draftLayout;
	public RectTransform boosterListRect;
	public GameObject redraftButton;

	float tabFadeSpeed = 0.5f;

	// draft items
	public DraftItem draftItemPrefab;
	public List<DraftItem> strongholdSelection = new List<DraftItem>();
	public List<DraftItem> boosterSelection = new List<DraftItem>();

	public DraftItem tradDraftStronghold = null;
	public static string tradDraftStrongholdID = "11142";
	DraftItem selectedStronghold = null;

	// preview
	public DrawPreview2D drawPreview2D;
	public Card previewing = null;
	public bool pointerDown {
		get {
			return _pointerDown;
		}
		set {
			_pointerDown = value;
			if( _pointerDown == false ) ShowPreview( null );
		}
	}
	bool _pointerDown = false;

	// level loading
	public LoadLevel lobby;
	public LoadLevel restartDraft;

	// draft stats for decks in progress
	public DraftStats draftStats;

	int draftSeed = 0;

	public int loadingCount = 0;

	IEnumerator Start () {
		while( Persistent.data.loading ) yield return null;

		// is there an existing draft?
		int existingLegality = PlayerPrefs.GetInt( "draftLegality", -1 );
		if( existingLegality != -1 ) legalityFilter.legality = (Card.Legality)existingLegality;
		else legalityFilter.legality = Card.Legality.Legacy;

		string existingClan = PlayerPrefs.GetString( "draftClan", "" );
		if( existingClan != "" ) {
			deckClan = existingClan;

			StartCoroutine( LoadDraft() );
			yield break;
		}

		strongholdSelectionTab.gameObject.SetActive( true );
		StartCoroutine( legalityBookmark.DelayedTranslateToggle(true, 1f) );

		// or generate new one
		legalityFilter.onClicked = delegate{ LegalityChanged(); StartCoroutine( ResetStrongholds() ); };

		LegalityChanged();

		StartCoroutine( ResetStrongholds() );
	}

	public void LegalityChanged() {

		print("Legality changed.  Generating new legality list with legality " + legalityFilter.legality);

		legalDynastyCards = new List<Card>();
		legalFateCards = new List<Card>();

		commonDynastyCards = new List<Card>();
		uncommonDynastyCards = new List<Card>();
		rareDynastyCards = new List<Card>();
		promoDynastyCards = new List<Card>();
		fixedDynastyCards = new List<Card>();

		commonFateCards = new List<Card>();
		uncommonFateCards = new List<Card>();
		rareFateCards = new List<Card>();
		promoFateCards = new List<Card>();
		fixedFateCards = new List<Card>();

		foreach( Card card in Persistent.data.cardsByName.Values ) {
			if( LegalityMatches( card, legalityFilter.legality ) ) {
				if( card.deckType == Card.DeckType.Dynasty ) {
					legalDynastyCards.Add( card );

					if( card.type != "stronghold" ) {
						if( card.rarity == "c" ) commonDynastyCards.Add( card );
						if( card.rarity == "u" ) uncommonDynastyCards.Add( card );
						if( card.rarity == "r" ) rareDynastyCards.Add( card );
						if( card.rarity == "f" ) fixedDynastyCards.Add( card );
						if( card.rarity == "p" ) promoDynastyCards.Add( card );
					}
				}
				else {
					legalFateCards.Add( card );

					if( card.rarity == "c" ) commonFateCards.Add( card );
					if( card.rarity == "u" ) uncommonFateCards.Add( card );
					if( card.rarity == "r" ) rareFateCards.Add( card );
					if( card.rarity == "f" ) fixedFateCards.Add( card );
					if( card.rarity == "p" ) promoFateCards.Add( card );
				}

				
			}
		}

		BuildPersonalityList();
		BuildHoldingList();
	}

	void BuildPersonalityList() {
		commonPersonalities = new List<Card>();
		uncommonPersonalities = new List<Card>();
		rarePersonalities = new List<Card>();
		fixedPersonalities = new List<Card>();
		promoPersonalities = new List<Card>();

		foreach( Card card in legalDynastyCards ) {
			if( card.type == "personality" && card.clans.Contains( deckClan ) ) {
				if( card.rarity == "c" ) commonPersonalities.Add( card );
				else if( card.rarity == "u" ) uncommonPersonalities.Add( card );
				else if( card.rarity == "r" ) rarePersonalities.Add( card );
				else if( card.rarity == "f" ) fixedPersonalities.Add( card );
				else if( card.rarity == "p" ) promoPersonalities.Add( card );
			}
		}
	}

	void BuildHoldingList() {
		holdings = new List<Card>();

		foreach( Card card in legalDynastyCards ) {
			if( card.type == "holding" ) {
				holdings.Add( card );
			}
		}
	}

	public IEnumerator ResetStrongholds() {
		foreach( DraftItem stronghold in strongholdSelection ) {
			stronghold.FadeOut();
		}

		yield return new WaitForSeconds( 0.5f );

		foreach( Transform stronghold in strongholdLayout.transform ) {
			Destroy( stronghold.gameObject );
		}

		legalStrongholds = new List<Card>();
		foreach( Card legalCard in legalDynastyCards ) {
			if( legalCard.type == "stronghold" && !legalCard.name.Contains( "(2)" ) ) {
				legalStrongholds.Add( legalCard );
			}
		}

		LoadSeed();

		strongholdSelection = new List<DraftItem>();
		List<int> illegalIndexes = new List<int>();
		for( int index = 0; index < 3; index++ ) {
			int randomStrongholdIndex = Random.Range( 0, legalStrongholds.Count );

			if( illegalIndexes.Contains(randomStrongholdIndex) ) {
				print("Determined that " + legalStrongholds[randomStrongholdIndex].name + " is illegal");
				index--;
				continue;
			}
			
			DraftItem draftStronghold = Instantiate( draftItemPrefab ) as DraftItem;
			draftStronghold.group.alpha = 0f;
			draftStronghold.transform.SetParent( strongholdLayout.transform, false );
			StartCoroutine( draftStronghold.LoadCard( legalStrongholds[randomStrongholdIndex] ) );
			strongholdSelection.Add( draftStronghold );
			draftStronghold.onChosen = StrongholdPicked;
			draftStronghold.draft = this;

			print("Added " + draftStronghold.name);

			illegalIndexes.Add( randomStrongholdIndex );
		}

		tradDraftStronghold.draft = this;
		tradDraftStronghold.enabled = true;
		StartCoroutine( Persistent.data.VerifyIDAgainstOracle( Draft.tradDraftStrongholdID, 
			delegate( Card tradCard, int q ){ StartCoroutine( tradDraftStronghold.LoadCard( tradCard ) ); tradDraftStronghold.onChosen = StrongholdPicked; } ) 
		);
	}

	public void StrongholdPicked( DraftItem item ) {
		selectedStronghold = item;
		deckClan = item.card.clans[0];

		dynasty.AddCard( item );
		item.group.alpha = 0f;
		item.group.blocksRaycasts = false;

		BuildPersonalityList();

		FadeOutStrongholdSelection();
	}

	public void FadeOutStrongholdSelection() {
		Persistent.data.FrameRateUpFor( tabFadeSpeed );

		LeanTween.value( 
			strongholdSelectionTab.gameObject, 
			a => strongholdSelectionTab.alpha = a, 
			strongholdSelectionTab.alpha, 0f, tabFadeSpeed )
		.setEase(LeanTweenType.easeInOutQuad)
		.setOnComplete( delegate(){ strongholdSelectionTab.gameObject.SetActive( false ); FadeInDraftSelection(); } );

		legalityFilter.GetComponent<TranslateBookmark>().TranslateToggle( false );
		dynastyBookmark.TranslateToggle( true );
		fateBookmark.TranslateToggle( true );
	}

	public void FadeInDraftSelection() {
		Persistent.data.FrameRateUpFor( tabFadeSpeed );

		draftSelectionTab.alpha = 0f;
		draftSelectionTab.gameObject.SetActive( true );

		LeanTween.value( 
			draftSelectionTab.gameObject, 
			a => draftSelectionTab.alpha = a, 
			draftSelectionTab.alpha, 1f, tabFadeSpeed )
		.setEase(LeanTweenType.easeInOutQuad)
		.setOnComplete( GenerateBooster );

		redraftButton.SetActive( true );
	}

	public void FadeOutDraftSelection() {
		PlayerPrefs.SetInt( "draftFinished", 1 );
			LeanTween.value( 
				draftSelectionTab.gameObject, 
				a => draftSelectionTab.alpha = a, 
				draftSelectionTab.alpha, 0f, tabFadeSpeed )
			.setEase(LeanTweenType.easeInOutQuad)
			.setOnComplete( delegate(){ draftStats.FadeIn(); draftSelectionTab.gameObject.SetActive( false ); } );

			redraftButton.SetActive( false );
	}
	
	public void CardPicked( DraftItem item ) {
		if( item.card.deckType == Card.DeckType.Dynasty ) dynasty.AddCard( item );
		else fate.AddCard( item );

		item.group.alpha = 0f;
		item.enabled = false;
		item.group.blocksRaycasts = false;

		boosterSelection.Remove(item);

		for( int index = 0; index < cullCardsAfterPick; index++ ) {
			int randomCardInBooster = Random.Range( 0, boosterSelection.Count );
			DraftItem toRemove = boosterSelection[randomCardInBooster];
			toRemove.group.blocksRaycasts = false;
			toRemove.FadeOut();

			print("Removing from booster pack with count of " + boosterSelection.Count);
			boosterSelection.RemoveAt( randomCardInBooster );
		}

		SaveDraft();

		if( dynasty.cardCount >= maxDynasty && fate.cards.Count >= maxFate ) {
			GenerateDeckEmissary();
			draftStats.GenerateStats( currentDeck );
			FadeOutDraftSelection();

			return;
		}

		if( boosterSelection.Count == 0 ) {
			StartCoroutine( DelayedGenerateBooster( 0.5f ) );
		}

		ShowPreview( null );
	}

	IEnumerator DelayedGenerateBooster( float delay ) {
		yield return new WaitForSeconds( delay );

		GenerateBooster();
	}
	
	public void GenerateBooster() {

		draftLayout.gameObject.SetActive( true );

		foreach( Transform invisDraftItem in draftLayout.transform ) {
			Destroy( invisDraftItem.gameObject );
		}
		boosterSelection = new List<DraftItem>();

		// fill layout with options

		// generate holdings
		for( int index = 0; index < numHoldings; index++ ) {
			if( Random.value < chanceToRollHolding ) {
				Debug.Log("Generating holding");
				GenerateDraftForBooster( holdings, 1 );
			}
			else {
				Debug.Log("Not generating holding");
				GenerateDraftForBooster( commonDynastyCards, 1 );
			}
		}
		
		GenerateDraftForBooster( commonDynastyCards, numCommons / 2 - numHoldings ); // one less common dynasty card for the holding
		GenerateDraftForBooster( uncommonDynastyCards, numUncommons / 2 );
		GenerateDraftForBooster( fixedDynastyCards, numFixed / 2 );

		GenerateDraftForBooster( commonFateCards, numCommons / 2 );
		GenerateDraftForBooster( uncommonFateCards, numUncommons / 2 );
		GenerateDraftForBooster( fixedFateCards, numFixed / 2 );

		if( Random.value < rareDynastyCards.Count / (float)rareDynastyCards.Count / (float)(rareDynastyCards.Count + rareFateCards.Count) ) GenerateDraftForBooster( rareDynastyCards, numRares );
		else GenerateDraftForBooster( rareFateCards, numRares );

		if( Random.value > chanceToRerollPromo ) {
			if( Random.value < (float)promoDynastyCards.Count / (float)(promoDynastyCards.Count + promoFateCards.Count) ) GenerateDraftForBooster( promoDynastyCards, numPromos );
			else GenerateDraftForBooster( promoFateCards, numPromos );
		}
		else {
			if( Random.value < (float)rareDynastyCards.Count / (float)(rareDynastyCards.Count + rareFateCards.Count) ) GenerateDraftForBooster( rareDynastyCards, numRares );
			else GenerateDraftForBooster( rareFateCards, numRares );
		}

		StartCoroutine( DelayedSaveDraft( ) ); // to give the draft items in the transform list time to destroy and their replacements to appear
	}

	void GenerateDraftForBooster( List<Card> cardPool, int numCards ) {

		for( int index = 0; index < numCards; index++ ) {
			int randomCardIndex = Random.Range( 0, cardPool.Count - 1 );

			// ignore strongholds in random deck selection
			Card cardCandidate = cardPool[randomCardIndex];
			if( cardCandidate.type == "stronghold" ) {
				index--;
				continue;
			}

			// if we're not drafting traditionally...
			if( selectedStronghold.card.id != "11142" && cardCandidate.type == "personality" && (!cardCandidate.clans.Contains( deckClan ) && !cardCandidate.clans.Contains( "unaligned" ) ) ) {
				if( Random.value < chanceToRerollPersonality ) {
					if( cardCandidate.rarity == "c" ) GenerateDraftForBooster( commonPersonalities, 1 );
					else if( cardCandidate.rarity == "u" ) GenerateDraftForBooster( uncommonPersonalities, 1 );
					else if( cardCandidate.rarity == "r" ) GenerateDraftForBooster( rarePersonalities, 1 );
					else if( cardCandidate.rarity == "f" ) GenerateDraftForBooster( fixedPersonalities, 1 );
					else if( cardCandidate.rarity == "p" ) GenerateDraftForBooster( promoPersonalities, 1 );

					continue;
				}
			}

			if( selectedStronghold.card.id != "11142" && cardCandidate.type == "sensei" && !cardCandidate.text.ToLower().Contains( deckClan ) ) {
				index--;
				continue;
			}

			if( cardCandidate.type == "sensei" && fate.ContainsType(cardCandidate.type) ) {
				index--;
				continue;
			}

			if( legalityFilter.legality == Card.Legality.Modern && (dynasty.Contains(cardCandidate) || fate.Contains(cardCandidate)) ) {
				index--;
				continue;
			}

			DraftItem draftItem = Instantiate( draftItemPrefab ) as DraftItem;
			draftItem.itemRect.SetParent( draftLayout.transform, false );
			draftItem.group.alpha = 0f;
			draftItem.onChosen = CardPicked;
			draftItem.draft = this;

			StartCoroutine( draftItem.LoadCard( cardPool[randomCardIndex] ) );

			boosterSelection.Add( draftItem );
		} 
	}

	void GenerateDeckEmissary() {
		Deck emissary = Instantiate( deckEmissaryPrefab ) as Deck;

		foreach( DraftItem dynastyItem in dynasty.cards ) {
			emissary.cardList.Add( dynastyItem.card );
		}

		foreach( DraftItem fateItem in fate.cards ) {
			emissary.cardList.Add( fateItem.card );
		}

		emissary.clan = deckClan;
		emissary.legality = (int)legalityFilter.legality;

		currentDeck = emissary;

		DontDestroyOnLoad( emissary.gameObject );
	}

	public void ShowPreview( Card cardToPreview, Vector2 inputPos = default(Vector2), float offsetPreview = 0f ) {
		// called by pointer action on card, behaves like preview on card table

		if( cardToPreview == null ) {
			drawPreview2D.Clear();
			previewing = null;
		}
		else {
			StartCoroutine( drawPreview2D.DrawCard( cardToPreview, inputPos, offsetPreview ) );
		}
		previewing = cardToPreview;
	}

	bool LegalityMatches( Card card, Card.Legality legality ) {

		string[] legal = Persistent.data.LegalityEnumToStrings(legality);

		foreach( string legalPossibility in legal ) if( card.legal.Contains(legalPossibility) ) {
			return true;
		}

		return false;
	}

	void LoadSeed() {
		draftSeed = PlayerPrefs.GetInt("draftSeed", 0);
		if( draftSeed == 0 ) draftSeed = System.Environment.TickCount;
		PlayerPrefs.SetInt("draftSeed", draftSeed);
		Random.InitState( draftSeed );
	}

	// called by a button on the stats tab
	public void Redraft() {
		PlayerPrefs.SetInt( "draftSeed", System.Environment.TickCount );
		PlayerPrefs.SetInt( "draftFinished", 0 );
		PlayerPrefs.SetString( "draftClan", "" );
		PlayerPrefs.SetString( "draftDynasty", "" );
		PlayerPrefs.SetString( "draftFate", "" );
		PlayerPrefs.SetString( "draftBooster", "" );

		if( currentDeck ) {
			Destroy( currentDeck.gameObject );
		}

		restartDraft.LoadLevelByName();
	}

	public void LoadLobby() {
		if( currentDeck == null ) return; // if not all cards have been loaded in GenerateDeckEmissary

		if( selectedStronghold.card.id == tradDraftStrongholdID ) PlayerPrefs.SetInt( "draft", 2 );
		else PlayerPrefs.SetInt( "draft", 1 );
		lobby.LoadLevelByName();
	}

	// serialization

	IEnumerator DelayedSaveDraft() {
		yield return null;

		SaveDraft();
	}

	void SaveDraft() {
		PlayerPrefs.SetInt( "draftLegality", (int)legalityFilter.legality );
		PlayerPrefs.SetString( "draftClan", deckClan );

		List<SerializedDraftItem> savedDynasty = new List<SerializedDraftItem>();
		foreach( DraftItem dynastyItem in dynasty.cards ) {
			savedDynasty.Add( dynastyItem.Serialized() );
		}

		List<SerializedDraftItem> savedFate = new List<SerializedDraftItem>();
		foreach( DraftItem fateItem in fate.cards ) {
			savedFate.Add( fateItem.Serialized() );
		}

		List<SerializedDraftItem> savedBooster = new List<SerializedDraftItem>();
		int savedItems = 0;
		foreach( Transform boosterItem in boosterListRect ) {
			savedItems++;
			savedBooster.Add( boosterItem.GetComponent<DraftItem>().Serialized() );
		}
		print("Saving " + savedItems + " draft items " + Time.time);

		PlayerPrefs.SetString( "draftDynasty", JsonConvert.SerializeObject( savedDynasty ) );
		PlayerPrefs.SetString( "draftFate", JsonConvert.SerializeObject( savedFate ) );
		PlayerPrefs.SetString( "draftBooster", JsonConvert.SerializeObject( savedBooster ) );
	}

	IEnumerator LoadDraft() {
		// load all the UI pieces
		legalityFilter.gameObject.SetActive( false );
		StartCoroutine( dynastyBookmark.DelayedTranslateToggle( true, 1f ) );
		StartCoroutine( fateBookmark.DelayedTranslateToggle( true, 1f ) );

		// load saved draft decks
		string existingDynasty = PlayerPrefs.GetString( "draftDynasty", "" );
		if( existingDynasty != "" ) {
			// reload dynasty cards
			List<SerializedDraftItem> deserializedDynasty = JsonConvert.DeserializeObject<List<SerializedDraftItem>>(existingDynasty);
			RegenerateList( dynasty.cards, deserializedDynasty, dynasty.listRect );
			dynasty.ArrangeCardsInstantly();
		}

		string existingFate = PlayerPrefs.GetString( "draftFate", "" );
		if( existingFate != "" ) {
			// reload fate cards
			List<SerializedDraftItem> deserializedFate = JsonConvert.DeserializeObject<List<SerializedDraftItem>>(existingFate);
			RegenerateList( fate.cards, deserializedFate, fate.listRect );
			fate.ArrangeCardsInstantly();
		}

		if( PlayerPrefs.GetInt( "draftFinished", 0 ) == 0 ) {
			string existingBooster = PlayerPrefs.GetString( "draftBooster", "" );
			if( existingBooster != "" ) {
				// reload booster cards
				draftSelectionTab.gameObject.SetActive( true );

				List<SerializedDraftItem> deserializedBooster = JsonConvert.DeserializeObject<List<SerializedDraftItem>>(existingBooster);
				RegenerateList( boosterSelection, deserializedBooster, boosterListRect );

				int enabledCount = 0;
				foreach( DraftItem item in boosterSelection ) if( item.group.blocksRaycasts ) enabledCount++;
				// print("Loading " + boosterSelection.Count + " items into " + boosterListRect.childCount + " children with " + enabledCount + " enabled " + Time.time);
			}

			// reload all card banks
			LegalityChanged();

			BuildPersonalityList();

			redraftButton.SetActive( true );
		}

		while( loadingCount > 0 ) yield return null;

		dynasty.CountCards();
		fate.CountCards();

		if( PlayerPrefs.GetInt( "draftFinished", 0 ) == 1 ) {

			GenerateDeckEmissary();

			draftStats.GenerateStats( currentDeck );
			draftStats.FadeIn();
		}
	}

	void RegenerateList( List<DraftItem> listToRegenerate, List<SerializedDraftItem> regenerationSource, RectTransform listParent ) {
		if( listToRegenerate == boosterSelection ) print("Building list for boosters with source list length of " + regenerationSource.Count + " and existing c count of " + listParent.childCount);
		for( int index = 0; index < regenerationSource.Count; index++ ) {

			DraftItem draftItem = Instantiate( draftItemPrefab ) as DraftItem;

			draftItem.itemRect.anchorMin = new Vector2( 0f, 1f );
			draftItem.itemRect.anchorMax = new Vector2( 0f, 1f );
			draftItem.itemRect.SetParent( listParent, false );
			draftItem.group.alpha = 0f;
			draftItem.draft = this;
			listToRegenerate.Add( draftItem );

			loadingCount++; // counter to track when the deck's cards are all fully loaded

			Card cardToLoad = Persistent.data.VerifyIDAgainstDatabase( regenerationSource[index].cardID );

			if( cardToLoad != null ) {
				LoadCardIntoList( draftItem, cardToLoad, listToRegenerate, regenerationSource[index] );
			}
			else {
				SerializedDraftItem itemToRegenerate = regenerationSource[index];

				StartCoroutine( Persistent.data.VerifyIDAgainstOracle( itemToRegenerate.cardID, 
					delegate(Card loadedCard, int q){ LoadCardIntoList( draftItem, loadedCard, listToRegenerate, itemToRegenerate ); ArrangeListsInstantly(); } )
				);
			}
		}
	}

	void LoadCardIntoList( DraftItem draftItem, Card cardToLoad, List<DraftItem> listToRegenerate, SerializedDraftItem regenerationSource ) {
		draftItem.card = cardToLoad;

		if( !regenerationSource.visible ) {
			draftItem.group.blocksRaycasts = false;
			draftItem.group.alpha = 0f;
			if( listToRegenerate == boosterSelection ) listToRegenerate.Remove( draftItem );
		}
		else {
			draftItem.group.alpha = 0f;
			StartCoroutine( draftItem.LoadCard( cardToLoad ) );
			draftItem.onChosen = CardPicked;
		}

		if( cardToLoad.type == "stronghold" ) {
			listToRegenerate.Remove( draftItem );
			listToRegenerate.Insert( 0, draftItem );
			selectedStronghold = draftItem;
		}

		loadingCount--;
	}

	void ArrangeListsInstantly() {
		fate.ArrangeCardsInstantly();
		dynasty.ArrangeCardsInstantly();
	}
}	
