﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class DraftList : MonoBehaviour {

	public RectTransform listRect;
	float verticalSeparation = 30f;
	float tweenTime = 0.5f;

	public List<DraftItem> cards;
	public int cardCount {
		get {
			return _cardCount;
		}
		set {
			_cardCount = value;
			quantityLabel.text = "(" + _cardCount + ")";
		}
	}
	int _cardCount = 0;

	public Text quantityLabel;

	// arrangement
	float cardHeight = 270f;

	public void AddCard( DraftItem item ) {

		DraftItem newDraftItem = Instantiate( item, item.transform.position, item.transform.rotation ) as DraftItem;
		newDraftItem.itemRect.sizeDelta = item.itemRect.sizeDelta;
		newDraftItem.itemRect.anchorMin = new Vector2( 0f, 1f );
		newDraftItem.itemRect.anchorMax = new Vector2( 0f, 1f );
		newDraftItem.card = item.card;

		int lastIndexOfType = cards.Count - 1;
		for( int index = 0; index < cards.Count; index++ ) {
			if( cards[index].card.type == item.card.type ) {
				lastIndexOfType = index;
			}
		}

		cards.Insert( lastIndexOfType + 1, newDraftItem );
		listRect.sizeDelta = new Vector2( listRect.sizeDelta.x, cardHeight + verticalSeparation * cards.Count );

		newDraftItem.transform.SetParent( transform, true );
		newDraftItem.itemRect.localScale = new Vector3(1f,1f,1f);

		if( item.card.type != "stronghold" && item.card.type != "sensei" ) cardCount++;
		ArrangeCards();
	}

	public void ArrangeCards() {
		Persistent.data.FrameRateUpFor( tweenTime );

		for( int index = 0; index < cards.Count; index++ ) {
			cards[index].itemRect.SetSiblingIndex( index );
			LeanTween.move( cards[index].itemRect, new Vector3( 0f, -verticalSeparation * index, 0f ), tweenTime )
			.setEase(LeanTweenType.easeInOutQuad);
		}
	}

	public void ArrangeCardsInstantly() {
		for( int index = 0; index < cards.Count; index++ ) {
			cards[index].itemRect.anchoredPosition = new Vector2( 0f, -verticalSeparation * index );
		}
	}

	public void CountCards() {
		int countedCards = 0;
		foreach( DraftItem item in cards ) {
			if( item.card != null && item.card.type != "stronghold" && item.card.type != "sensei" ) countedCards++;
		}

		cardCount = countedCards;
	}

	public bool Contains( Card card ) {
		foreach( DraftItem item in cards ) {
			if( item.card == card ) return true;
		}

		return false;
	}

	public bool ContainsItem( DraftItem item ) {
		foreach( DraftItem addedItem in cards ) {
			if( addedItem == item ) return true;
		}

		return false;
	}

	public bool ContainsType( string cardType ) {
		foreach( DraftItem item in cards ) {
			if( item.card.type.ToLower() == cardType.ToLower() ) return true;
		}

		return false;
	}
}
