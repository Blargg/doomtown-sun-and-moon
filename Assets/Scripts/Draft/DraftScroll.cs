﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class DraftScroll : MonoBehaviour, IPointerUpHandler {

	public Draft draft;

	public void OnPointerUp( PointerEventData evt ) {
		draft.pointerDown = false;
	}
}
