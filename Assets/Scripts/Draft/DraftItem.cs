﻿using UnityEngine;
using System.Collections;

using UnityEngine.EventSystems;

public class DraftItem : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerUpHandler {

	public RectTransform itemRect;
	public Card card;
	public CanvasGroup group;

	public DrawPreview2D drawPreview2D;

	float fadeSpeed = 0.5f;

	public System.Action<DraftItem> onChosen = null;

	// preview
	[HideInInspector] public Draft draft;
	float previewOffset = 200f;
	float timeClicked = 0f;
	float clickInterval = 0.5f;

	float timeHeld = 0f;
	float timeBeforePreview = 0f;

	public IEnumerator LoadCard( Card card ) {
		this.card = card;
		name = card.name;

		yield return StartCoroutine( drawPreview2D.DrawCard(card) );

		FadeIn();
	}

	public void FadeIn() {
		Persistent.data.FrameRateUpFor( fadeSpeed );

		LeanTween.cancel( gameObject );
		LeanTween.value( gameObject, a => group.alpha = a, group.alpha, 1f, fadeSpeed ).setEase(LeanTweenType.easeInOutQuad).setOnComplete( () => group.blocksRaycasts = true );
	}

	public void FadeOut() {
		Persistent.data.FrameRateUpFor( fadeSpeed );

		LeanTween.cancel( gameObject );
		LeanTween.value( gameObject, a => group.alpha = a, group.alpha, 0f, fadeSpeed ).setEase(LeanTweenType.easeInOutQuad);
		group.blocksRaycasts = false;
		enabled = false;
	}

	public virtual void OnPointerDown(PointerEventData evt) {
		
		if( evt.pointerId == -2 ) {
			StartCoroutine( PointerHeld(evt.position) );
		}

		if( evt.pointerId == -1 ) {
			print("Clicked on card at " + Time.time);
			if( Time.time - timeClicked < clickInterval ) {
				print("Double clicked on card at " + Time.time);
				DoubleClick( evt.position );
			}
			else timeClicked = Time.time;
		}

		evt.Use();
	}

	IEnumerator PointerHeld(Vector3 pos) {
		draft.pointerDown = true;

		while( draft.pointerDown ) {

			if( draft.previewing == this.card ) yield break;

			if( timeHeld > timeBeforePreview ) {
				draft.ShowPreview( card, pos, previewOffset );
				timeHeld = 0f;
			}

			yield return null;
			timeHeld += Time.deltaTime;
		}
	}

	public virtual void OnPointerEnter(PointerEventData evt) {
		if( draft.previewing != null && draft.pointerDown ) draft.ShowPreview( card, evt.position, previewOffset );

		// right click
		if( draft.pointerDown ) draft.ShowPreview( card, evt.position, previewOffset );
	}

	public void OnPointerUp(PointerEventData evt) {
		if( evt.pointerId == -1 ) {
			draft.pointerDown = false;
			timeHeld = 0f;
		}
	}

	public virtual void DoubleClick( Vector3 clickPos ) {
		if( draft.dynasty.ContainsItem( this ) || draft.fate.ContainsItem( this ) ) {
			return;
		}

		if( draft.dynasty.Contains( card ) || draft.fate.Contains( card ) ) {
			if( card.text.ToLower().Contains( "unique" ) ) return;
		}
		
		if( onChosen != null ) onChosen(this);
		draft.pointerDown = false;
		timeHeld = 0f;
		timeClicked = 0f;
	}

	// serialization

	public SerializedDraftItem Serialized() {
		return new SerializedDraftItem {
			cardID = card.id,
			visible = enabled,
		};
	}
}

public class SerializedDraftItem {
	public string cardID = "";

	public bool visible = true;
}
