﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DraftStats : MonoBehaviour {

	public CanvasGroup statsPanel;
	public float fadeSpeed = 0.5f;

	public Text winsQuantity;
	public Text totalGamesQuantity;

	public Text personalitiesQuantity;
	public Text holdingsQuantity;
	public Text spellsQuantity;
	public Text strategiesQuantity;
	public Text eventsQuantity;
	public Text ringsQuantity;
	public Text itemsQuantity;
	public Text followersQuantity;
	public Text senseiQuantity;

	public Text averagePersGCQuantity;
	public Text totalTimePlayed;
	public Text longestGamePlayed;

	public void GenerateStats( Deck forDeck ) {
		int num_personalities = 0;
		int num_holdings = 0;
		int num_spells = 0;
		int num_strategies = 0;
		int num_events = 0;
		int num_rings = 0;
		int num_items = 0;
		int num_followers = 0;
		int num_sensei = 0;

		int totalGC = 0;

		foreach( Card card in forDeck.cardList ) {
			if( card.type == "personality" ) {
				num_personalities++;
				if( personalitiesQuantity ) personalitiesQuantity.text = num_personalities.ToString();
				if( card.cost > -1 ) totalGC += card.cost;
			}
			if( card.type == "holding" ) {
				num_holdings++;
				if( holdingsQuantity ) holdingsQuantity.text = num_holdings.ToString();
			}
			if( card.type == "spell" ) {
				num_spells++;
				if( spellsQuantity ) spellsQuantity.text = num_spells.ToString();
			}
			if( card.type == "strategy" ) {
				num_strategies++;
				if( strategiesQuantity ) strategiesQuantity.text = num_strategies.ToString();
			}
			if( card.type == "event" ) {
				num_events++;
				if( eventsQuantity ) eventsQuantity.text = num_events.ToString();
			}
			if( card.type == "ring" ) {
				num_rings++;
				if( ringsQuantity ) ringsQuantity.text = num_rings.ToString();
			}
			if( card.type == "item" ) {
				num_items++;
				if( itemsQuantity ) itemsQuantity.text = num_items.ToString();
			}
			if( card.type == "follower" ) {
				num_followers++;
				if( followersQuantity ) followersQuantity.text = num_followers.ToString();
			}
			if( card.type == "sensei" ) {
				num_sensei++;
				if( senseiQuantity ) senseiQuantity.text = num_sensei.ToString();
			}
		}

		if( averagePersGCQuantity && num_personalities > 0 ) averagePersGCQuantity.text = ((float)totalGC / (float)num_personalities).ToString();
		if( winsQuantity ) winsQuantity.text = PlayerPrefs.GetInt("draftWins", 0).ToString();
		if( totalGamesQuantity ) totalGamesQuantity.text = PlayerPrefs.GetInt("draftTotalGames", 0).ToString();
	}

	public void FadeIn() {
		gameObject.SetActive( true );
		Persistent.data.FrameRateUpFor( fadeSpeed );

		LeanTween.cancel( statsPanel.gameObject );
		LeanTween.value( gameObject, a => statsPanel.alpha = a, statsPanel.alpha, 1f, fadeSpeed ).setEase(LeanTweenType.easeInOutQuad);
	}

	public void FadeOut() {
		Persistent.data.FrameRateUpFor( fadeSpeed );

		LeanTween.cancel( gameObject );
		LeanTween.value( gameObject, a => statsPanel.alpha = a, statsPanel.alpha, 0f, fadeSpeed ).setEase(LeanTweenType.easeInOutQuad);
	}
}
