﻿using UnityEngine;
using System.Collections;

using UnityEngine.SceneManagement;

public class LoadLevel : MonoBehaviour {

	public string levelToLoad;
	public float afterDelay = 0f;
	public bool disconnectFromPhoton = false;

	public delegate void UIEvent();
	public static event UIEvent LoadingLevel;

	static bool loading = false;
	
	public void LoadLevelByName() {
		if( !loading && Time.timeSinceLevelLoad > afterDelay ) StartCoroutine( Load() );
	}

	public IEnumerator Load() {
		LoadLevel.LoadingLevel();

		loading = true;

		if( disconnectFromPhoton && PhotonNetwork.connectedAndReady ) {
			PhotonNetwork.Disconnect();
		}

		yield return new WaitForSeconds( afterDelay );

		loading = false;

		SceneManager.LoadScene( levelToLoad );
	}

	public void LoadNetworkLevelByName() {
		StartCoroutine( LoadOverNetwork() );
	}

	public IEnumerator LoadOverNetwork() {
		LoadLevel.LoadingLevel();

		yield return new WaitForSeconds( afterDelay );

		PhotonNetwork.LoadLevel( levelToLoad );
	}
}