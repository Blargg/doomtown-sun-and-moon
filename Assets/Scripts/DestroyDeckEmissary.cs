﻿using UnityEngine;
using System.Collections;

public class DestroyDeckEmissary : MonoBehaviour {

	public void DestroyEmissary() {
		GameObject emissary = GameObject.Find("DeckEditEmissary(Clone)" );

		if( emissary ) Destroy( emissary );
		else print("No emissary found to destroy");
	}
}
