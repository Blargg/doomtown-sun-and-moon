﻿using UnityEngine;
using System.Collections;

public class CleanUpEmissaries : MonoBehaviour {

	void Start () {

		// destroy deck emissaries
		Deck[] emissaries = GameObject.FindObjectsOfType( typeof(Deck) ) as Deck[];
		foreach( Deck emissary in emissaries ) {
			Destroy( emissary.gameObject );
		}

		// no longer loading draft
		PlayerPrefs.SetInt( "draft", 0 );
	}
}
