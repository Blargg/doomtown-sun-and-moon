﻿using UnityEngine;
using System.Collections;

public class DisableIfDrafting : MonoBehaviour {

	void Start () {
		if( PlayerPrefs.GetInt( "draft", 0 ) == 1 ) gameObject.SetActive( false );
	}
}
