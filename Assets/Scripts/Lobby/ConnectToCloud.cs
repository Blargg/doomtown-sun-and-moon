﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using System.IO;
using System.Text;
using SevenZip.Compression.LZMA;

using PhotonHash = ExitGames.Client.Photon.Hashtable;

public class ConnectToCloud : MonoBehaviour {

	public static string version = "1.5.93";

	public Text lobbyLabel;

	Deck deckToUse;

	public RectTransform lobbyList;
	public LobbyListItem lobbyListItemPrefab;

	bool connectionFailed = false;

	float refreshListFrequency = 0.5f;

	public InputField publicGameName;
	public InputField privateGameName;
	public InputField joinGameName;

	public LoadingSpinner publicSpinner;
	public LoadingSpinner privateSpinner;
	public LoadingSpinner joinSpinner;

	public Text publicPlayersCount;
	public Text privatePlayersCount;

	public Text playerCount;
	public Text gameCount;
	public Text inProgressCount;

	public LoadingSpinner spinner;

	public AudioSource ping;
	int runningGameTotal = 0;

	bool loading = false; // to prevent double-clicks on buttons that load a new level
	bool spectating = false;
	int drafting = 0;
	bool playtesting {
		get {
			return Persistent.data.playtestGroup != "";
		}
	}

	public UnfurlScroll confirmJoin;
	public Button confirmedJoin;

	public InputField reportStats;

	// Use this for initialization
	IEnumerator Start () {
		lobbyList.anchoredPosition = new Vector2( lobbyList.anchoredPosition.x, 0f ); // remove this once the beta bug is fixed

		Persistent.data.tournamentName = "";
		Persistent.data.tournamentMode = false;

		Persistent.data.roomPassword = "";

		GameObject deckObject = GameObject.Find("DeckEmissary(Clone)");
		if( deckObject ) deckToUse = deckObject.GetComponent<Deck>();

		PhotonNetwork.automaticallySyncScene = true;
		PhotonNetwork.autoJoinLobby = true;

		spinner.gameObject.SetActive( true );

		PhotonNetwork.playerName = PlayerPrefs.GetString("playfabUsername", "Guest") + Random.Range(1, 9999);

		drafting = PlayerPrefs.GetInt( "draft", 0 );
		if( PhotonNetwork.player.customProperties["spectating"] != null ) spectating = true;

		PhotonHash playerName = new PhotonHash();
		playerName.Add( "name", PlayerPrefs.GetString("onlineName", "Unnamed Ronin") );
		PhotonNetwork.player.SetCustomProperties( playerName );

		lobbyLabel.text = spectating ? "Spectate Games" : "Public Games";
		lobbyLabel.text = drafting == 1 ? "Arena Draft" : lobbyLabel.text.Trim();
		lobbyLabel.text = drafting == 2 ? "Traditional Draft" : lobbyLabel.text.Trim();

		if( PhotonNetwork.connectionStateDetailed == ClientState.PeerCreated ) {
			print("Connecting with appID " + PhotonNetwork.PhotonServerSettings.AppID);
			PhotonNetwork.ConnectUsingSettings(version);
		}

		while( !PhotonNetwork.connected ) {
			if (PhotonNetwork.connectionState == ConnectionState.Connecting) {
				spinner.error.text = "Connecting ...";
			}

			if( connectionFailed ) {
				// display error and allow repeat connection:
				PhotonNetwork.ConnectUsingSettings(version);
				spinner.error.text = "Connection failed.  Reconnecting ...";
			}

			Persistent.data.FrameRateUpFor(0.5f);
			yield return new WaitForSeconds(0.5f);
		}

		spinner.gameObject.SetActive( false );

		PhotonNetwork.autoCleanUpPlayerObjects = false;

		StartCoroutine( ListGames() );
	}

	public IEnumerator ListGames() {
		while( enabled ) {

			foreach( Transform existingGame in lobbyList ) {
				Destroy( existingGame.gameObject );
			}

			RoomInfo[] games = PhotonNetwork.GetRoomList();

			int visibleGames = 0;
			int gamesInProgress = 0;
			foreach (RoomInfo game in games)
			{
				gamesInProgress++;

				int maxParticipants = 2;
				int.TryParse((string)game.customProperties["maxParticipants"], out maxParticipants);
				int curParticipants = 0;
				int.TryParse((string)game.customProperties["curParticipants"], out curParticipants);

				int maxTargetParticipants = spectating ? 10 : maxParticipants;
				int minTargetParticipants = spectating ? 1 : 0;

				if( (string)game.customProperties["password"] == "" 
					&& curParticipants < maxTargetParticipants 
					&& curParticipants > minTargetParticipants 
					&& (string)game.customProperties["disconnected"] == null ) {

					print("Comparing my db hash " + Persistent.data.ptDbHash + " to theirs: " + (string)game.customProperties["ptHash"] );

					if( spectating && (string)game.customProperties["forbidSpectators"] != null ) continue;
					if( (string)game.customProperties["draft"] != null ) print( "Considering game with draft index of " + (string)game.customProperties["draft"] + " while my draft index is " + drafting.ToString() );
					if( drafting > 0 && (string)game.customProperties["draft"] != drafting.ToString() ) continue;
					if( playtesting && (string)game.customProperties["ptHash"] != Persistent.data.ptDbHash ) continue;

					LobbyListItem listItem = Instantiate( lobbyListItemPrefab ) as LobbyListItem;
					listItem.rectTransform.SetParent( lobbyList, false );
					listItem.uniqueName = game.name;

					listItem.SetClan( (Card.Clan)game.customProperties["clan"] );
					listItem.legality = (Card.Legality)( int.Parse( (string)game.customProperties["legality"] ) );

					listItem.callbackOnClicked = DeckInListClicked;

					string participantsTag = "";
					if( maxParticipants > 2 ) participantsTag = "[" + curParticipants + "/" + maxParticipants + " players]";
					listItem.gameTitle.text = "[" + ((Card.Legality)(int.Parse( (string)game.customProperties["legality"] ))).ToString().Replace("_", " ") + "] " + participantsTag + (string)game.customProperties["name"];

					visibleGames++;
				}
			}

			inProgressCount.text = gamesInProgress.ToString();
			gameCount.text = PhotonNetwork.countOfRooms.ToString();
			playerCount.text = PhotonNetwork.countOfPlayers.ToString();

			if( visibleGames > runningGameTotal ) {
				ping.Play();
				runningGameTotal = visibleGames;
			}

			if( games.Length > 0 ) refreshListFrequency = 3f;
			else refreshListFrequency = 0.5f;

			yield return new WaitForSeconds( refreshListFrequency );
		}
	}
	
	// hosts game using selected deck
	public void HostPublicGame() {
		if( loading ) return;
		loading = true;

		string onlineName = (string)PhotonNetwork.player.customProperties["name"];

		string[] propertiesAvailableInLobby = new string[10] { "name", "clan", "password", "legality", "maxParticipants", "curParticipants", "forbidSpectators", "disconnected", "draft", "ptHash"};
		PhotonHash roomProperties = new PhotonHash();
		roomProperties.Add( "name", "[" + onlineName + "] " + publicGameName.text.Trim() );
		roomProperties.Add( "clan", Persistent.data.ClanStringToEnum(deckToUse.clan) );
		roomProperties.Add( "password", "" );
		roomProperties.Add( "legality", deckToUse.legality.ToString() );
		roomProperties.Add( "maxParticipants", publicPlayersCount.text.Trim() );
		roomProperties.Add( "curParticipants", "0" );
		if( playtesting ) {
			print("Added pt hash " + Persistent.data.ptDbHash + " to hosted game properties");
			roomProperties.Add( "ptHash", Persistent.data.ptDbHash );
		}
		if( drafting > 0 ) roomProperties.Add( "draft", drafting.ToString() );

		publicSpinner.gameObject.SetActive( true );

		PhotonNetwork.CreateRoom(
			PhotonNetwork.playerName,
			new RoomOptions { 
				IsVisible = true,
				IsOpen = true,
				MaxPlayers = 99, 
				CleanupCacheOnLeave = PhotonNetwork.autoCleanUpPlayerObjects,
				CustomRoomProperties = roomProperties,
				CustomRoomPropertiesForLobby = propertiesAvailableInLobby
			},
			null
		);
	}

	// join game
	public void DeckInListClicked( LobbyListItem item ) {

		bool spectating = PhotonNetwork.player.customProperties["spectating"] != null;
		if( spectating ) {
			JoinPublicGame(item);
			return;
		}

		print("comparing " + item.legality + " to " + (Card.Legality)deckToUse.legality );
		if( item.legality != (Card.Legality)deckToUse.legality ) {
			confirmJoin.UnfurlNow();
			confirmedJoin.onClick.RemoveAllListeners();
			confirmedJoin.onClick.AddListener( delegate { confirmJoin.FurlNow(); JoinPublicGame(item); } );
		}
		else {
			JoinPublicGame(item);
		}
	}

	public void JoinPublicGame( LobbyListItem item ) {
		print("Joining game " + item.uniqueName);
		if( item != null ) item.spinner.gameObject.SetActive( true );
		PhotonNetwork.JoinRoom( item.uniqueName );
	}

	public void HostPrivateGame() {
		if( loading ) return;
		loading = true;

		// Tournament stat reporting
		if( Persistent.data.tournamentMode ) {
			Persistent.data.tournamentName = reportStats.text.Trim();
		}

		string[] propertiesAvailableInLobby = new string[1] { "password" };
		Persistent.data.roomPassword = privateGameName.text.Trim();
		PhotonHash roomProperties = new PhotonHash();
		roomProperties.Add( "password", privateGameName.text.Trim() );
		roomProperties.Add( "maxParticipants", privatePlayersCount.text.Trim() );
		roomProperties.Add( "curParticipants", "0" );
		if( drafting > 0 ) roomProperties.Add( "draft", drafting.ToString() );

		print("Hosted game with draft index of " + drafting);

		privateSpinner.gameObject.SetActive( true );
		PhotonNetwork.CreateRoom(
			PhotonNetwork.playerName,
			new RoomOptions { 
				IsVisible = true,
				IsOpen = true,
				MaxPlayers = 99, 
				CleanupCacheOnLeave = PhotonNetwork.autoCleanUpPlayerObjects,
				CustomRoomProperties = roomProperties,
				CustomRoomPropertiesForLobby = propertiesAvailableInLobby
			},
			null
		);
	}

	public void ToggleTournament( bool state ) {
		print("Tournament mode set to " + state + " in the lobby");
		Persistent.data.tournamentMode = state;

		// automatically fill in tourney name if the deck seems to have been created for one
		print("Toggling tournament for deck with path " + deckToUse.pathToDeckOnDisk);
		int tourneyIndex = deckToUse.pathToDeckOnDisk.IndexOf( " - in " );
		if( tourneyIndex > -1 ) {
			tourneyIndex += 6;
			string strippedOfName = deckToUse.pathToDeckOnDisk.Substring( tourneyIndex );
			int endTourneyNameIndex = strippedOfName.IndexOf( " Tourney.snm" );

			if( endTourneyNameIndex > -1 ) {
				string tourneyName = strippedOfName.Substring( 0, endTourneyNameIndex );
				reportStats.text = tourneyName;
			}
		}
	}

	public void JoinPrivateGame() {
		if( loading ) return;
		loading = true;

		foreach (RoomInfo game in PhotonNetwork.GetRoomList())
		{
			if( game.playerCount < game.maxPlayers && game.customProperties["password"].ToString() == joinGameName.text.Trim() ) {
				joinSpinner.gameObject.SetActive( true );

				PhotonNetwork.JoinRoom( game.name );
			}
		}
	}

	// Photon

    public void OnCreatedRoom()
    {
        PhotonNetwork.LoadLevel("Card Table");
    }


    public void OnDisconnectedFromPhoton()
    {
        Debug.Log("Disconnected from Photon.");
    }

	public void OnFailedToConnectToPhoton(object parameters)
    {
        connectionFailed = true;
        Debug.Log("OnFailedToConnectToPhoton. StatusCode: " + parameters);
    }
}
