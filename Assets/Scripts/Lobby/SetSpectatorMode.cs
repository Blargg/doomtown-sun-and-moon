﻿using UnityEngine;
using System.Collections;

using PhotonHash = ExitGames.Client.Photon.Hashtable;

public class SetSpectatorMode : MonoBehaviour {

	void Start() {
		SetSpectatorModeTo( false );
	}

	public void SetSpectatorModeTo( bool state ) {
		string hashState = state ? "1" : null;
		PhotonHash participantProps = new PhotonHash();
		participantProps.Add("spectating", hashState);
		PhotonNetwork.player.SetCustomProperties( participantProps );
	}
}
