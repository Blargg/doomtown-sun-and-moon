﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

// using PhotonHash = ExitGames.Client.Photon.Hashtable;

public class LobbyListItem : MonoBehaviour {

	public RectTransform rectTransform;
	public Button lobbyListItem;
	public Image clanMon;
	public Image spectatorButton;
	public Text gameTitle;
	public Deck associatedDeck;

	public LoadingSpinner spinner;

	public Action<LobbyListItem> callbackOnClicked;
	public Action<LobbyListItem> spectatorClicked;

	public string uniqueName = "";

	public Sprite dragonSprite;
	public Sprite unicornSprite;
	public Sprite crabSprite;
	public Sprite phoenixSprite;
	public Sprite mantisSprite;
	public Sprite scorpionSprite;
	public Sprite craneSprite;
	public Sprite spiderSprite;
	public Sprite lionSprite;

	public Card.Legality legality;
	
	public void SetClan( Card.Clan clan ) {

		switch( clan ) {

			case Card.Clan.dragon:
				clanMon.sprite = dragonSprite;
			break;

			case Card.Clan.unicorn:
				clanMon.sprite = unicornSprite;
			break;

			case Card.Clan.crab:
				clanMon.sprite = crabSprite;
			break;

			case Card.Clan.phoenix:
				clanMon.sprite = phoenixSprite;
			break;

			case Card.Clan.mantis:
				clanMon.sprite = mantisSprite;
			break;

			case Card.Clan.scorpion:
				clanMon.sprite = scorpionSprite;
			break;

			case Card.Clan.crane:
				clanMon.sprite = craneSprite;
			break;

			case Card.Clan.spider:
				clanMon.sprite = spiderSprite;
			break;

			case Card.Clan.lion:
				clanMon.sprite = lionSprite;
			break;
		}
	}

	public void StartSpinner() {
		spinner.gameObject.SetActive( true );
	}

	public void StopSpinner() {
		spinner.gameObject.SetActive( false );
	}

	public void Clicked() {
		if( spinner.gameObject.activeInHierarchy ) return;

		if( callbackOnClicked != null ) callbackOnClicked( this );
	}
}
