﻿using UnityEngine;
using System.Collections;

public class DisableIfSpectator : MonoBehaviour {

	void Start () {
		if( PhotonNetwork.player.customProperties["spectating"] != null ) gameObject.SetActive( false );
	}

}
