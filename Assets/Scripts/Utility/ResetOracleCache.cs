﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using System.IO;

public class ResetOracleCache : MonoBehaviour {

	public void Reset() {
		print("Resetting oracle cache " + Time.time);

		FileInfo oracleNames = new FileInfo( Persistent.data.pathToOracleNames );
		FileInfo oracleIDs = new FileInfo( Persistent.data.pathToOracleIDs );
		DirectoryInfo oracleImages = new DirectoryInfo( Persistent.data.cleanPathToOracleCache );
		
		if( oracleNames.Exists ) oracleNames.Delete();
		if( oracleIDs.Exists ) oracleIDs.Delete();
		if( oracleImages.Exists ) {
			oracleImages.Delete( true );
			oracleImages.Create();
		}

		Persistent.data.oracleNames = new Dictionary<string, Card>();
		Persistent.data.oracleIDs = new Dictionary<string, Card>();
	}
}
