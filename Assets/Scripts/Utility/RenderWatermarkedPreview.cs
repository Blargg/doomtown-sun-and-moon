﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using System.IO;

public class RenderWatermarkedPreview : MonoBehaviour {

	public int xHedge = 0;
	public int yHedge = 22;
	public int horizontalPixels = 428;
	public int verticalPixels = 600;

	Vector3 renderPos = Vector3.zero;
	string renderPath = "";

	public void CaptureScreen( Vector3 screenPos, string path ) {
		renderPos = screenPos;
		renderPath = path;
	}

	void OnPostRender() {
		if( renderPos != Vector3.zero ) {

			DirectoryInfo saveDir = new DirectoryInfo(renderPath.Substring(0, renderPath.LastIndexOf("/") ) );
			if( !saveDir.Exists ) {
				saveDir.Create();
			}

			Rect previewRect = new Rect( renderPos.x - horizontalPixels / 2 + xHedge, renderPos.y - verticalPixels / 2 + yHedge, horizontalPixels, verticalPixels );
			print( "Screen space preview rect is " + previewRect );

			Texture2D tex = new Texture2D(horizontalPixels, verticalPixels, TextureFormat.RGB24, false);

			tex.ReadPixels(previewRect, 0, 0, true);
			tex.Apply();

			File.WriteAllBytes( renderPath, tex.EncodeToJPG( 85 ) );

			DestroyImmediate(tex);
			renderPos = Vector3.zero;
		}
	}
}
