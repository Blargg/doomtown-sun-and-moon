﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using System.IO;

public class ResetCaches : MonoBehaviour {

	public bool clearCachesThisVersion = false;
	public Text versionLabel;

	void Start() {
		print("Last version was " + PlayerPrefs.GetString( "lastVersion", "" ) );
		if( clearCachesThisVersion && PlayerPrefs.GetString( "lastVersion", "" ) != versionLabel.text ) {

			FileInfo nameCacheFile = new FileInfo( Persistent.data.pathToOracleNames );
			if( nameCacheFile.Exists ) {
				print("Deleting name cache at " + Persistent.data.pathToOracleNames);
				nameCacheFile.Delete();
			}

			FileInfo idCacheFile = new FileInfo( Persistent.data.pathToOracleIDs );
			if( idCacheFile.Exists ) {
				print("Deleting ID cache at " + Persistent.data.pathToOracleIDs);
				idCacheFile.Delete();
			}

			print("Set last version to " + versionLabel.text );
			PlayerPrefs.SetString( "lastVersion", versionLabel.text );
		}
	}
}
