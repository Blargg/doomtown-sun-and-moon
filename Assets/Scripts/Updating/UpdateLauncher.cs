﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Threading;

using System.IO;
using System.Diagnostics;

public class UpdateLauncher : MonoBehaviour {

	public const string versionNumberURL = "http://www.hwstn.com/sunandmoon/version_updater.txt";

	public const string tempFolderName = "Updater";

	#if UNITY_STANDALONE_OSX || UNITY_EDITOR
		public const string updateURL = "http://www.hwstn.com/binaries/Sun%20and%20Moon%20-%20Mac.zip";
		public const string downloadedFolder = "Sun and Moon - Mac";
		public const string downloadedAppName = "Sun and Moon Updater.app";
		public const string updatedPath = "Sun and Moon Updater.app/Contents/MacOS/Sun and Moon Updater";
	#elif UNITY_STANDALONE_WIN
		public const string updateURL = "http://www.hwstn.com/binaries/Sun%20and%20Moon%20-%20Win.zip";
		public const string downloadedFolder = "Sun and Moon - Win";
		public const string downloadedAppName = "Sun and Moon Updater.exe";
		public const string updatedPath = "Sun and Moon Updater.exe";
	#elif UNITY_STANDALONE_LINUX
		public const string updateURL = "http://www.hwstn.com/binaries/Sun%20and%20Moon%20-%20Linux.zip";
		public const string downloadedFolder = "Sun and Moon - Linux";
		public const string downloadedAppName = "Sun and Moon Updater.x86_64";
		public const string updatedPath = "Sun and Moon Updater.x86_64";
	#endif

	public const string dataFolderName = "Sun and Moon Updater_Data";

	public RectTransform progressBar;
	public Text progressLabel;

	bool updateAvailable = false;

	DirectoryInfo tempFolder;

	bool downloadComplete = false;
	public int decompProgress = 0;
	bool decompComplete = false;
	string compressedFilePath = "";
	public bool updateComplete = false;

	public IEnumerator StartUpdate() {
		print("Starting update");
		FileInfo curVersFile = new FileInfo( Persistent.data.outsideApp.FullName + "/.updater_version.txt" );

		string currentVersion = "";
		if( curVersFile.Exists ) {
			currentVersion = File.ReadAllText(curVersFile.FullName);
		}

		WWW onlineVersion = new WWW(versionNumberURL);

		yield return onlineVersion;

		if( string.IsNullOrEmpty(onlineVersion.error) ) {

			if( onlineVersion.text != currentVersion ) {
				print("Update is available");
				updateAvailable = true;
			}
		}

		if( updateAvailable ) {
			tempFolder = new DirectoryInfo( Persistent.data.outsideApp.FullName + "/" + tempFolderName );
			if( !tempFolder.Exists ) tempFolder.Create();

			StartCoroutine( DownloadUpdate() );
		}
		else {
			print("Update not available");
			LaunchUpdatedApp();
		}
	}

	IEnumerator DownloadUpdate() {
		progressLabel.gameObject.SetActive( true );
		progressBar.gameObject.SetActive( true );

		print("Beginning to download update " + Time.time);
		WWW update = new WWW( updateURL );

		progressLabel.text = "Downloading updater...";

		StartCoroutine( UpdateDownloadProgress( update ) );
		yield return update;

		if( string.IsNullOrEmpty( update.error ) ) {
			downloadComplete = true;
		}
		else {
			print( "Launcher download failed because " + update.error );
		}

		if( downloadComplete ) {

			compressedFilePath = tempFolder.FullName + "/update.7z";
			File.WriteAllBytes( compressedFilePath, update.bytes );
			progressLabel.text = "Decompressing updater...";
			progressBar.localScale = Vector3.one;

			StartCoroutine( DecompressUpdate() );
		}
	}

	IEnumerator UpdateDownloadProgress( WWW download ) {
		while( !downloadComplete ) {
			progressBar.localScale = new Vector3( download.progress, 1f, 1f );

			yield return null;
		}
	}

	IEnumerator DecompressUpdate() {

		Thread th = new Thread(Decompress);
		th.Start();

		while( !decompComplete ) {
			progressBar.localScale = new Vector3( (float)decompProgress / 100f, 1f, 1f );

			yield return null;
		}
		progressBar.localScale = Vector3.one;

		string downloadedAppPath = Persistent.data.outsideApp.FullName + "/" + tempFolderName + "/" + downloadedFolder + "/" + downloadedAppName;
		string existingAppPath = Persistent.data.outsideApp.FullName + "/" + downloadedAppName;

		#if UNITY_STANDALONE_OSX || UNITY_EDITOR
			DirectoryInfo appToDelete = new DirectoryInfo( existingAppPath );
		#else
			FileInfo appToDelete = new FileInfo( existingAppPath );
		#endif
		DirectoryInfo dataToDelete = new DirectoryInfo( Persistent.data.outsideApp.FullName + "/" + dataFolderName );

		#if UNITY_STANDALONE_OSX || UNITY_EDITOR
			DirectoryInfo appToMove = new DirectoryInfo( downloadedAppPath );
		#else
			FileInfo appToMove = new FileInfo( downloadedAppPath );
		#endif
		DirectoryInfo dataToMove = new DirectoryInfo( Persistent.data.outsideApp.FullName + "/" + tempFolderName + "/" + downloadedFolder + "/" + dataFolderName );

		string moveToPath = Persistent.data.outsideApp.FullName;

		if( appToDelete.Exists ) {
			#if UNITY_STANDALONE_OSX || UNITY_EDITOR
				appToDelete.Delete( true );
			#else
				appToDelete.Delete();
			#endif
		}

		if( dataToDelete.Exists ) {
			dataToDelete.Delete( true );
		}

		if( appToMove.Exists ) {
			appToMove.MoveTo( moveToPath + "/" + downloadedAppName );
		}

		if( dataToMove.Exists ) {
			dataToMove.MoveTo( moveToPath + "/" + dataFolderName );
		}

		tempFolder.Delete( true );

		updateComplete = true;

		progressLabel.gameObject.SetActive( false );
		progressBar.gameObject.SetActive( false );

		LaunchUpdatedApp();
	}

	void Decompress() {
		lzma.doDecompress7zip( compressedFilePath, tempFolder.FullName );

		decompComplete = true;
	}

	void LaunchUpdatedApp() {

		FileInfo updatedFile = new FileInfo( Persistent.data.outsideApp.FullName + "/" + updatedPath );
		if( updatedFile.Exists ) {

			#if UNITY_STANDALONE_OSX || UNITY_EDITOR
				ProcessStartInfo chmod = new ProcessStartInfo();
				chmod.FileName = "/bin/chmod";
				chmod.WorkingDirectory = updatedFile.Directory.FullName;
				chmod.Arguments = "+x \"Sun and Moon Updater\"";
				chmod.UseShellExecute = false;
				Process c = Process.Start(chmod);
				c.WaitForExit();
				c.Close();
			#endif

			System.Diagnostics.Process.Start(updatedFile.FullName);
		}
		else {
			print( "No updated app found at " + updatedFile.FullName);
		}

			Application.Quit();
	}
}
