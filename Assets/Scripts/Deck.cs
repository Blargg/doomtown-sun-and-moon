using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;

using System.Text;
using SevenZip.Compression.LZMA;

public class Deck : MonoBehaviour {

	public string pathToDeckOnDisk;
	public string deckName = "Untitled";

	public FileInfo deckOnDisk = null;

	public string clan = "unaligned";

	public int wins = 0;
	public int gamesPlayed = 0;

	public List<Card> cardList = new List<Card>();
	int loadingCards = 0;

	public int legality = 0;

	[HideInInspector] public bool locked = false;

	public Dictionary<Card, string> proxies = new Dictionary<Card, string>();

	public static void CopyDeck( Deck from, Deck to ) {
		to.deckName = from.deckName;
		to.pathToDeckOnDisk = from.pathToDeckOnDisk;
		to.deckOnDisk = from.deckOnDisk;
		to.clan = from.clan;
		to.wins = from.wins;
		to.gamesPlayed = from.gamesPlayed;
		to.cardList = from.cardList;
		to.legality = from.legality;

		to.proxies = from.proxies;
	}

	public virtual IEnumerator GenerateCards() {
		if( deckOnDisk == null && pathToDeckOnDisk != "" ) deckOnDisk = new FileInfo( pathToDeckOnDisk );
		yield return StartCoroutine( GenerateCards( deckOnDisk ) );
	}

	public IEnumerator GenerateCards( FileInfo fromDeck, Action onComplete ) {
		yield return StartCoroutine( GenerateCards( fromDeck ) );

		if( onComplete != null ) onComplete();
	}

	public IEnumerator GenerateCards( FileInfo fromDeck ) {
		if( cardList != null && cardList.Count > 0 ) yield break;

		deckOnDisk = fromDeck;

		pathToDeckOnDisk = deckOnDisk.FullName;
		deckName = deckOnDisk.Name.Remove( deckOnDisk.Name.Length - 4 );
		cardList = new List<Card>();

		string deckSlush = GetSlush();
		string[] splitSlush = deckSlush.Split('\n');

		for( int index = 0; index < splitSlush.Length; index++ ) {
			if( splitSlush[index].Trim(' ') == "" ) continue;

			Card cardToAdd = null;

			int legalityIndex = splitSlush[index].IndexOf("Legality:");
			if( legalityIndex > -1 ) {
				legality = StringToLegality( (splitSlush[index].Substring(legalityIndex + 9)).Trim(' ') );
				continue;
			}

			string cardLine = splitSlush[index].ReplaceCommonTerms();

			// seems like a proxy
			string proxyType = "";
			int proxyTermIndex = cardLine.IndexOf("Proxy ");
			int proxyIndex = cardLine.IndexOf(": ");

			if( proxyIndex > -1 && proxyTermIndex > -1 ) {
				string typeLine = cardLine.Substring( proxyTermIndex, proxyIndex - proxyTermIndex ).ToLower();

				// ignore identified proxy types that might actually just be part of the card's name
				if( typeLine == "proxy holding" ) proxyType = typeLine;
				if( typeLine == "proxy personality" ) proxyType = typeLine;
				if( typeLine == "proxy ring" ) proxyType = typeLine;
				if( typeLine == "proxy spell" ) proxyType = typeLine;
				if( typeLine == "proxy strategy" ) proxyType = typeLine;
				if( typeLine == "proxy follower" ) proxyType = typeLine;
				if( typeLine == "proxy item" ) proxyType = typeLine;
				if( typeLine == "proxy sensei" ) proxyType = typeLine;
				if( typeLine == "proxy stronghold" ) proxyType = typeLine;
				if( typeLine == "proxy celestial" ) proxyType = typeLine;
				if( typeLine == "proxy event" ) proxyType = typeLine;
				if( typeLine == "proxy region" ) proxyType = typeLine;

				if( proxyType != "" ) {
					proxyType = proxyType.Replace("proxy ", "");
					cardLine = cardLine.Replace( "Proxy " + proxyType.Capitalize() + ": ", "" );
				}
			}

			// does not seem like a proxy
			int quantity = Persistent.data.GetCardQuantityInt(cardLine);

			cardToAdd = Persistent.data.VerifyTextAgainstDatabase( cardLine );

			bool playtestAuthorized = Persistent.data.playtestGroup != "";
			if( cardToAdd == null ) {
				if( !playtestAuthorized ) {
					loadingCards++;
					StartCoroutine( Persistent.data.VerifyTextAgainstOracle( 
						cardLine,
						AddCardToDeckWithType( string.Copy(proxyType) )
					) );
				}
			}
			else {
				AddCard( cardToAdd, quantity, proxyType );
			}
		}

		while( loadingCards > 0 ) yield return null;
	}

	// to get around delegate variable capture
	System.Action<Card, int> AddCardToDeckWithType( string forcedCardType ) {
		return delegate(Card returnedCard, int returnedQuantity){ AddCard(returnedCard, returnedQuantity, forcedCardType ); loadingCards--; };
	}

	public void AddCard( Card cardToAdd, int quantity, string proxyType = "" ) {
		if( cardToAdd != null && !cardToAdd.name.Contains("(2)") ) {

			if( proxyType != "" ) {
				print("Adding proxy card " + cardToAdd.name + " to deck " + deckName + " with printing " + cardToAdd.edition);

				proxies.Add( cardToAdd, proxyType );
				// cardToAdd.type = proxyType;
				// cardToAdd.isProxy = true;
			}

			for( int count = 0; count < quantity; count++ ) {
				cardList.Add( cardToAdd );
			}

			if( cardToAdd.type == "stronghold" ) {
				clan = cardToAdd.clans[0];
			}
		}
	}

	// Utility

	public int StringToLegality( string legality ) {
		Card.Legality[] enumlegality = (Card.Legality[])System.Enum.GetValues(typeof(Card.Legality));

		for( int index = 0; index < enumlegality.Length; index++ ) {
			if( enumlegality[index].ToString().ToLower() == legality.ToLower() ) {
				return (int)enumlegality[index];
			}
		}

		return 0;
	}

	public string GetSlush() {
		string deckSlush = "";

		if( deckOnDisk == null ) {

			deckSlush += "Legality: " + ((Card.Legality)legality).ToString();
			deckSlush += System.Environment.NewLine;

			for( int index = 0; index < cardList.Count; index++ ) {
				deckSlush += cardList[index].name;
				if( index < cardList.Count - 1 ) deckSlush += System.Environment.NewLine;
			}

			return deckSlush;
		}

		// print("Getting slush for deck on disk at " + deckOnDisk.FullName);

		if( deckOnDisk.Extension == ".txt" ) {
			deckSlush = File.ReadAllText(pathToDeckOnDisk);
		}
		// This deck is a locked tournament deck that can not be modified
		if( deckOnDisk.Extension == ".snm" ) {
			byte[] decompressedDeck = LZMAtools.DecompressLZMAByteArrayToByteArray( File.ReadAllBytes(pathToDeckOnDisk) );
			deckSlush = Encoding.ASCII.GetString( decompressedDeck );
			locked = true;
		}
		deckSlush = deckSlush.Replace( "\r\n", "\n" );

		return deckSlush;
	}
}