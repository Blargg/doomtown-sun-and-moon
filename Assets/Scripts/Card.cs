﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Card {

	public enum Legality { 
		Legacy = 0,
		Modern = 1, 
		// Twenty_Festivals_Strict = 2, 
		// Twenty_Festivals_Extended = 3, 
		Twenty_Festivals = 2,
		Ivory_Edition = 3,
		Emperor = 4,
		Celestial = 5,
		Samurai = 6,
		Lotus = 7,
		Diamond = 8,
		Gold = 9,
		Jade = 10,
		Imperial = 11,
		Onyx = 12,
	}

	public string id = "";
	public string name = "";
	public enum Clan { dragon, phoenix, crab, spider, crane, lion, mantis, naga, unicorn, scorpion, unaligned, neutral }
	public List<string> clans = new List<string>() { "unaligned" };
	public string type = ""; // personality, holding, region, event, celestial, follower, item, strategy, ring, stronghold
	public string rarity = "";
	public string edition = ""; // most current edition of the card
	public string setName = ""; // currently only used by the Oracle
	public List<string> legal = new List<string>();
	public List<string> images = new List<string>();
	public List<string> editions = new List<string>();
	public int cost = -99;
	public int focus = -99;
	public int province_strength = -99;
	public int gold_production = -99;
	public int starting_honor = -99;

	public string force = "";
	int int_force = -1;
	public string chi = "";
	int int_chi = -1;
	public int personal_honor = -99;
	public int honor_req = -99;
	
	public string text = "";
	public string flavor = "";
	public string artist = "";

	public enum DeckType { Fate, Dynasty }
	public DeckType deckType = DeckType.Dynasty;

	public bool isToken = false;
	public bool isProxy = false; // loaded as an external (not in deck) card... different from isToken
	public Card linkedCard = null;

	public bool isFromOracle = false; // temporary until all cards are from the Oracle
	public string secondSide = ""; // for Oracle strongholds

	public bool encrypted = false;

	public Card() {

	}

	public Card Clone() {
		return MemberwiseClone() as Card;
	}

	public int IntForce() {

		if( force.Length > 0 ) {
			bool forceNegative = force[0] == '-';

			string forceToParse = force.Trim('-');

			int.TryParse( forceToParse.ToString(), out int_force );
			if( forceNegative ) int_force *= -1;
		}

		return int_force;
	}

	public int IntChi() {

		if( chi.Length > 0 ) {
			bool chiNegative = chi[0] == '-';

			string chiToParse = chi.Trim('-');

			int.TryParse( chiToParse.ToString(), out int_chi );
			if( chiNegative ) int_chi *= -1;
		}

		return int_chi;
	}

	public int GetImageIndex() {
		int cardImageIndex = -1;

		for( int index = 0; index < editions.Count; index++ ) {
			string trimmedEdition = editions[index].ToLower().Trim();
			string trimmedImageIndex = edition.ToLower().Trim();

			if( trimmedEdition.Contains( trimmedImageIndex ) 
				|| trimmedImageIndex.Contains( trimmedEdition ) 
				|| trimmedEdition == trimmedImageIndex.Acronym()
				) {
				cardImageIndex = index;
			}
		}

		if( cardImageIndex == -1 && images.Count > 0 ) {
			cardImageIndex = images.Count - 1;
		}

		return cardImageIndex;
	}

	public bool ContainsEdition( string trimmedEdition ) {
		for( int index = 0; index < editions.Count; index++ ) {
			// Debug.Log("Comparing card edition " + editions[index] + " to trimmed card text edition " + trimmedEdition);
			if( editions[index].ToLower().Contains( trimmedEdition.ToLower() ) ) return true;
			if( trimmedEdition.ToLower().Contains( editions[index].ToLower() ) ) return true;
			if( editions[index].ToLower() == trimmedEdition.Acronym().ToLower() ) return true;
		}

		// Debug.Log("Card does not contain queried edition");
		return false;
	}

	public string DisplayText() {
		return text.Replace( "&#8226;", "•" ).Replace("<br>","\n");
	}
}