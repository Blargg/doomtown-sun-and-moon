﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ChangeTextIfPlaytester : MonoBehaviour {

	public GameObject objectToActivate;
	public Text label;

	public string textToChange;

	IEnumerator Start () {
		while( Persistent.data.loading ) yield return null;

		if( Persistent.data.playtestGroup != "" ) {
			objectToActivate.SetActive( true );
			label.text = textToChange.Replace("$", Persistent.data.playtestGroup);
			label.text = label.text.Replace("#", Persistent.data.ptDbHash);
		}
		else {
			objectToActivate.SetActive( false );
		}
	}
}
