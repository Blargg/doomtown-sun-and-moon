﻿using UnityEngine;
using System.Collections;

public class DisableIfPlaytester : MonoBehaviour {

	void Start () {
		if( Persistent.data.playtestGroup != "" ) {
			gameObject.SetActive( false );
		}
	}
}
