﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SerializedCard {
	public string[] legalities = new string[1] { "open" };
	public string id = "";
	public string cardName = "My New Card";
	public string type = "strategy"; // personality, holding, region, event, celestial, follower, item, strategy, ring, stronghold
	public List<string> clans = new List<string>();
	public string edition = "";
	public string imagePath = "";
	public string starting_honor = "0";

	public string overallLegality {
		get {
			return _overallLegality;
		}
		set {
			_overallLegality = value;

			switch( _overallLegality ) {
				case "Onyx Strict":
					legalities = new string[]{ "open", "onyx" };
				break;

				case "Onyx Arc":
					legalities = new string[]{ "open", "20F", "onyx" };
				break;

				case "Onyx Extended":
					legalities = new string[]{ "open", "ivory2", "20F", "onyx" };
				break;

				case "Onyx2 Strict":
					legalities = new string[]{ "open", "onyx2" };
				break;

				case "Onyx2 Arc":
					legalities = new string[]{ "open", "onyx", "onyx2" };
				break;

				case "Onyx2 Extended":
					legalities = new string[]{ "open", "20F", "onyx", "onyx2" };
				break;
			}
		}
	}
	string _overallLegality = "Onyx Strict";

	public string imageLastExported = "";
}

public class Database {
	public string name = "My New Database";
	public List<SerializedCard> cards = new List<SerializedCard>();
}
