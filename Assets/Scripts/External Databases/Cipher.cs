﻿using UnityEngine;
using System.Collections;

using System.Text;

public class Cipher : MonoBehaviour {

	const string cipherKey = "AmaterasuAndOnnotangu2015";
	static byte[] cipher;

	void Awake() {
		cipher = Encoding.ASCII.GetBytes( cipherKey );
	}

	public static void EncryptDecrypt(byte[] data)
    {
    	if( data == null ) {
    		print("No data!");
    		return;
    	}

        for (int i = 0; i < data.Length; i++)
            data[i] = (byte)(data[i] ^ cipher[i % cipher.Length]);
    }
}