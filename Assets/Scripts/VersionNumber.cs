﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

using System.IO;

public class VersionNumber : MonoBehaviour {

	public string versionNumberURL;
	public Text versionLabel;

	public UpdateLauncher updater;

	bool updateAvailable {
		get {
			return _updateAvailable;
		}
		set {
			if( value ) versionLabel.text += " (update available)";
			_updateAvailable = value;
		}
	}
	bool _updateAvailable = false;

	IEnumerator Start () {
		// save version number to disk in accessible place
		FileInfo versionFile = new FileInfo( Persistent.data.outsideApp.FullName + "/.player_version.txt" );
		if( versionFile.Exists ) {
			versionFile.Delete();
		}
		File.WriteAllText( versionFile.FullName, versionLabel.text );

		// hide file
		File.SetAttributes(versionFile.FullName, File.GetAttributes(versionFile.FullName) | FileAttributes.Hidden);

		WWW onlineVersion = new WWW(versionNumberURL);

		yield return onlineVersion;

		if( string.IsNullOrEmpty(onlineVersion.error) ) {
			if( onlineVersion.text != versionLabel.text ) updateAvailable = true;
		}
		else {
			versionLabel.text += " (not connected)";
		}
	}

	public void Clicked() {
		if( updateAvailable ) {
			StartCoroutine( updater.StartUpdate() );
		}
	}
}
