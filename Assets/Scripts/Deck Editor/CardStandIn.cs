﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CardStandIn : MonoBehaviour {

	public Text cardName;
	public Text force;
	public Text chi;
	public Text GC_stronghold;
	public Text PS;
	public Text SH;
	public Text GC_attachment;
	public Text GC_spell;
	public Text GC_strategy;
	public Text GC_holding;
	public Text GC_personality;
	public Text HR_attachment;
	public Text HR_personality;
	public Text PH_attachment;
	public Text PH_personality;
	public Text focus;
	public Text cardTextLong;
	public Text cardTextShort;
	public Text cardFlavor;
	public Text noImageWarning;
	public Text tokenWarning;
	public Text GP;

	public RectTransform standInCard;
	public RawImage cardImage;

	public Texture2D celestialImage;
	public Texture2D eventImage;
	public Texture2D holdingImage;
	public Texture2D itemImage;

	public Texture personalityImageCrab;
	public Texture personalityImageCrane;
	public Texture personalityImageDragon;
	public Texture personalityImageLion;
	public Texture personalityImageMantis;
	public Texture personalityImageNaga;
	public Texture personalityImagePhoenix;
	public Texture personalityImageScorpion;
	public Texture personalityImageSpider;
	public Texture personalityImageUnaligned;
	public Texture personalityImageUnicorn;

	public Texture2D regionImage;
	public Texture2D strategyImage;
	
	public Texture2D strongholdImageCrab;
	public Texture2D strongholdImageCrane;
	public Texture2D strongholdImageDragon;
	public Texture2D strongholdImageLion;
	public Texture2D strongholdImageMantis;
	public Texture2D strongholdImageNaga;
	public Texture2D strongholdImagePhoenix;
	public Texture2D strongholdImageScorpion;
	public Texture2D strongholdImageSpider;
	public Texture2D strongholdImageUnaligned;
	public Texture2D strongholdImageUnicorn;

	public Texture followerImage;
	public Texture ringImage;
	public Texture spellImage;
	public Texture senseiImage;

	public void CreateStandInCardFrom( Card card ) {
		DisableAllFields();
		PopulateFields( card );

		if( noImageWarning ) {
			noImageWarning.gameObject.SetActive(true);
			tokenWarning.gameObject.SetActive(false);
		}
		if( card.isToken && card.images.Count == 0 ) {
			noImageWarning.gameObject.SetActive(false);
			tokenWarning.gameObject.SetActive(true);
		}

		cardName.gameObject.SetActive( true );
		cardFlavor.gameObject.SetActive( true );

		switch( card.type ) {
			case "personality":
				if( !card.isToken ) {
					force.gameObject.SetActive( true );
					chi.gameObject.SetActive( true );
					GC_personality.gameObject.SetActive( true );
					HR_personality.gameObject.SetActive( true );
					PH_personality.gameObject.SetActive( true );
					cardTextShort.gameObject.SetActive( true );
				}

				switch( card.clans[0] ) {
					case "crab":
						cardImage.texture = personalityImageCrab;
					break;

					case "crane":
						cardImage.texture = personalityImageCrane;
					break;

					case "dragon":
						cardImage.texture = personalityImageDragon;
					break;

					case "lion":
						cardImage.texture = personalityImageLion;
					break;

					case "mantis":
						cardImage.texture = personalityImageMantis;
					break;

					case "naga":
						cardImage.texture = personalityImageNaga;
					break;

					case "phoenix":
						cardImage.texture = personalityImagePhoenix;
					break;

					case "scorpion":
						cardImage.texture = personalityImageScorpion;
					break;

					case "spider":
						cardImage.texture = personalityImageSpider;
					break;

					case "unaligned":
						cardImage.texture = personalityImageUnaligned;
					break;

					case "ratling":
						cardImage.texture = personalityImageUnaligned;
					break;

					case "unicorn":
						cardImage.texture = personalityImageUnicorn;
					break;
				}
			break;

			case "event":
				cardTextLong.gameObject.SetActive( true );
				cardImage.texture = eventImage;
			break;

			case "follower":
				force.gameObject.SetActive( true );
				chi.gameObject.SetActive( true );
				GC_attachment.gameObject.SetActive( true );
				focus.gameObject.SetActive( true );
				cardTextShort.gameObject.SetActive( true );

				cardImage.texture = followerImage;
			break;

			case "holding":
				GC_holding.gameObject.SetActive( true );
				cardTextLong.gameObject.SetActive( true );
				GP.gameObject.SetActive( true );

				cardImage.texture = holdingImage;
			break;

			case "item":
				force.gameObject.SetActive( true );
				chi.gameObject.SetActive( true );
				GC_holding.gameObject.SetActive( true );
				focus.gameObject.SetActive( true );
				cardTextLong.gameObject.SetActive( true );

				cardImage.texture = itemImage;
			break;

			case "region":
				cardTextShort.gameObject.SetActive( true );

				cardImage.texture = regionImage;
			break;

			case "ring":
				cardTextLong.gameObject.SetActive( true );
				focus.gameObject.SetActive( true );

				cardImage.texture = ringImage;
			break;

			case "sensei":
				GC_stronghold.gameObject.SetActive( true );
				PS.gameObject.SetActive( true );
				SH.gameObject.SetActive( true );
				cardTextLong.gameObject.SetActive( true );

				cardImage.texture = senseiImage;
			break;

			case "spell":
				GC_spell.gameObject.SetActive( true );
				focus.gameObject.SetActive( true );
				cardTextLong.gameObject.SetActive( true );

				cardImage.texture = spellImage;
			break;

			case "strategy":
				GC_strategy.gameObject.SetActive( true );
				focus.gameObject.SetActive( true );
				cardTextLong.gameObject.SetActive( true );

				cardImage.texture = strategyImage;
			break;

			case "stronghold":
				GC_stronghold.gameObject.SetActive( true );
				PS.gameObject.SetActive( true );
				SH.gameObject.SetActive( true );
				cardTextShort.gameObject.SetActive( true );

				switch( card.clans[0] ) {
					case "crab":
						cardImage.texture = strongholdImageCrab;
					break;

					case "crane":
						cardImage.texture = strongholdImageCrane;
					break;

					case "dragon":
						cardImage.texture = strongholdImageDragon;
					break;

					case "lion":
						cardImage.texture = strongholdImageLion;
					break;

					case "mantis":
						cardImage.texture = strongholdImageMantis;
					break;

					case "naga":
						cardImage.texture = strongholdImageNaga;
					break;

					case "phoenix":
						cardImage.texture = strongholdImagePhoenix;
					break;

					case "scorpion":
						cardImage.texture = strongholdImageScorpion;
					break;

					case "spider":
						cardImage.texture = strongholdImageSpider;
					break;

					case "unaligned":
						cardImage.texture = strongholdImageUnaligned;
					break;

					case "unicorn":
						cardImage.texture = strongholdImageUnicorn;
					break;
				}
			break;
		}

		standInCard.gameObject.SetActive(true);
		cardImage.gameObject.SetActive(true);
	}

	void DisableAllFields() {
		cardName.gameObject.SetActive( false );
		force.gameObject.SetActive( false );
		chi.gameObject.SetActive( false );
		GC_stronghold.gameObject.SetActive( false );
		PS.gameObject.SetActive( false );
		SH.gameObject.SetActive( false );
		GC_attachment.gameObject.SetActive( false );
		GC_spell.gameObject.SetActive( false );
		GC_strategy.gameObject.SetActive( false );
		GC_holding.gameObject.SetActive( false );
		GC_personality.gameObject.SetActive( false );
		HR_attachment.gameObject.SetActive( false );
		HR_personality.gameObject.SetActive( false );
		PH_attachment.gameObject.SetActive( false );
		PH_personality.gameObject.SetActive( false );
		GP.gameObject.SetActive( false );
		focus.gameObject.SetActive( false );
		cardTextLong.gameObject.SetActive( false );
		cardTextShort.gameObject.SetActive( false );
		cardFlavor.gameObject.SetActive( false );
		noImageWarning.gameObject.SetActive( false );
		tokenWarning.gameObject.SetActive( false );
	}

	void PopulateFields( Card card ) {
		cardName.text = card.name;
		GC_personality.text = card.cost == -99 ? "0" : card.cost.ToString();
		GC_holding.text = card.cost == -99 ? "0" : card.cost.ToString();
		GC_strategy.text = card.cost == -99 ? "0" : card.cost.ToString();
		GC_stronghold.text = card.cost == -99 ? "0" : card.cost.ToString();
		GC_attachment.text = card.cost == -99 ? "0" : card.cost.ToString();
		GC_spell.text = card.cost == -99 ? "0" : card.cost.ToString();
		focus.text = card.focus.ToString();
		PS.text = card.province_strength.ToString();
		SH.text = card.starting_honor.ToString();
		force.text = card.force;
		chi.text = card.chi;
		PH_personality.text = card.honor_req == -99 ? "" : card.personal_honor.ToString();
		PH_attachment.text = card.personal_honor == -99 ? "" : card.personal_honor.ToString();
		HR_personality.text = card.honor_req == -99 ? "" : card.honor_req.ToString();
		HR_attachment.text = card.honor_req == -99 ? "" : card.honor_req.ToString();
		GP.text = card.gold_production == -99 ? "" : card.gold_production.ToString();
		cardTextLong.text = card.DisplayText();
		cardTextShort.text = card.DisplayText();
		cardFlavor.text = card.flavor;

		// print("Card GP set to " + card.gold_production);
	}

	public void CreateStandInForFavor( Card.Clan status ) {
		DisableAllFields();

		cardTextLong.gameObject.SetActive( true );
		if( status == Card.Clan.neutral ) cardTextLong.text = "<b>Political Limited:</b> If you have higher Family Honor than each other player, bow your performing Personality with 1 Personal Honor or higher: Take the Imperial Favor.";
		else if( status == Card.Clan.unaligned ) cardTextLong.text = "<b>Political Limited:</b> Discard the Imperial Favor and a card: Draw a card.<br><b>Discard the Imperial Favor:</b> Move a target attacking enemy unit home.";
		noImageWarning.gameObject.SetActive(false);
		tokenWarning.gameObject.SetActive(false);
	}
}
