﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class DeckEntryInput : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerUpHandler, IDragHandler {

	public DeckEntryItem entry;
	public Text label;

	// to be removed once the PointerEventData.clickCount bug is fixed in Unity on mobile
	float timeClicked = 0f;
	float clickInterval = 0.5f;

	float timeHeld = 0f;
	float timeBeforePreview = 0.15f;

	public virtual void OnPointerDown(PointerEventData evt) {
		if( entry.deckList.previewing == null )
			StartCoroutine( PointerHeld(evt.position) );
		else
			entry.deckList.ShowPreview( null, evt.position );

		entry.deckList.SelectEntry( entry );
		
		if( Time.time - timeClicked < clickInterval ) DoubleClick( evt.position );
		timeClicked = Time.time;

		evt.Use();
	}

	IEnumerator PointerHeld(Vector3 pos) {
		entry.deckList.pointerDown = true;

		while( entry.deckList.pointerDown && entry.deckList.previewing == null ) {

			if( timeHeld > timeBeforePreview ) {
				if( entry.card != null ) entry.deckList.ShowPreview( entry.card.name, pos, 100f );
				timeHeld = 0f;
			}

			yield return null;
			timeHeld += Time.deltaTime;
		}
	}

	public virtual void OnPointerEnter(PointerEventData evt) {
		if( entry.deckList.previewing != null ) {
			entry.deckList.SelectEntry( entry );
			entry.deckList.ShowPreview( entry.card.name, evt.position, 100f );

			print("Showing " + entry.card.name);
		}
	}

	public void OnPointerUp(PointerEventData evt) {
		if( PlayerPrefs.GetInt( "modalPreview", 0 ) == 0 ) {
			entry.deckList.ShowPreview( null, evt.position );
		}
		entry.deckList.pointerDown = false;
		timeHeld = 0f;
	}

	public virtual void DoubleClick( Vector3 clickPos ) {
		// increment or decrement?
	}

	public void OnBeginDrag(PointerEventData evt) {
		evt.Use();
	}

	public void OnDrag(PointerEventData evt) {
		evt.Use();
	}

	public void OnEndDrag(PointerEventData evt) {
		evt.Use();
	}
}
