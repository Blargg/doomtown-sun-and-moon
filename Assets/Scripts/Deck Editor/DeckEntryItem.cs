﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class DeckEntryItem : MonoBehaviour {

	public Text label;
	[HideInInspector] public Card card;
	[HideInInspector] public int cardQuantity = 0;
	public Image background;
	public Button increment;
	public Button decrement;
	public Image smear;
	public RectTransform panel;

	public Color activeColor;
	public Color inactiveColor;

	[HideInInspector] public ListDeckContents deckList; // set by ListDeckContents

	[HideInInspector] public bool hidden = false; // used to flag entries when filtering the deck

	public void IncrementEntry() {
		if( deckList.deckToEdit.locked ) return;

		cardQuantity++;
		RegenerateLabel( cardQuantity - 1 );

		deckList.saveButton.text = "Save?";
	}

	public void DecrementEntry() {
		if( deckList.deckToEdit.locked ) return;

		cardQuantity--;
		
		if( cardQuantity < 1 ) deckList.DeleteEntry( this );
		else RegenerateLabel( cardQuantity + 1 );
		
		deckList.saveButton.text = "Save?";
	}

	public void Select() {
		smear.gameObject.SetActive( true );
		if( card != null ) increment.gameObject.SetActive( true );
		decrement.gameObject.SetActive( true );
		label.color = activeColor;

		if( card == null && !string.IsNullOrEmpty(label.text) ) {
			foreach( SuggestionFilter filter in deckList.filters ) {
				filter.isDefault = true;
			}

			deckList.nameFilter.filter.text = Persistent.data.TrimCardLine(label.text);
		}
	}

	public void Deselect() {
		smear.gameObject.SetActive( false );
		increment.gameObject.SetActive( false );
		decrement.gameObject.SetActive( false );
		label.color = inactiveColor;
	}

	public void RegenerateLabel( int oldQuantity ) {

		print("Regenerating label with old quantity " + oldQuantity + " and current quantity " + cardQuantity );

		string stub = Persistent.data.GetCardQuantity( label.text );
		if( stub != "" ) stub = stub.Replace( oldQuantity.ToString(), cardQuantity.ToString() );
		else stub = cardQuantity.ToString();

		string cardName = Persistent.data.TrimCardLine(label.text);
		if( cardName.Trim() == "" && card != null ) cardName = card.name;
		label.text = stub + " " + cardName;

		deckList.CountCards();
	}
}
