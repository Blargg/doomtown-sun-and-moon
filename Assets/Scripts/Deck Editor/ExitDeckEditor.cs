﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ExitDeckEditor : MonoBehaviour {

	public Text saveLabel;
	public UnfurlScroll confirmationScroll;
	public LoadLevel loadlLevel;

	public void Clicked() {
		if( saveLabel.text == "Save?" ) {
			confirmationScroll.UnfurlNow();
		}
		else {
			loadlLevel.LoadLevelByName();
		}
	}
}
