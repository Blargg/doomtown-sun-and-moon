﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class SuggestionListItem : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerUpHandler, IDragHandler {

	public Text label;

	[HideInInspector] public ListDeckContents deckList;

	// to be removed once the PointerEventData.clickCount bug is fixed in Unity on mobile
	float timeClicked = 0f;
	float clickInterval = 0.5f;

	float timeHeld = 0f;
	float timeBeforePreview = 0.15f;

	public virtual void OnPointerDown(PointerEventData evt) {
		if( deckList.previewing == null )
			StartCoroutine( PointerHeld(evt.position) );
		else
			deckList.ShowPreview( null, evt.position );

		if( Time.time - timeClicked < clickInterval ) {
			DoubleClick( evt.position );
			timeClicked = 0f;
		}
		else timeClicked = Time.time;

		evt.Use();
	}

	IEnumerator PointerHeld(Vector3 pos) {
		deckList.pointerDown = true;

		while( deckList.pointerDown && deckList.previewing == null ) {

			if( timeHeld > timeBeforePreview ) {
				deckList.ShowPreview( label.text, pos, 100f );
				timeHeld = 0f;
			}

			yield return null;
			timeHeld += Time.deltaTime;
		}
	}

	public virtual void OnPointerEnter(PointerEventData evt) {
		if( deckList.previewing != null ) {
			print("Previewing " + label.text);
			deckList.ShowPreview( label.text, evt.position, 100f );
		}
	}

	public void OnPointerUp(PointerEventData evt) {
		if( PlayerPrefs.GetInt( "modalPreview", 0 ) == 0 ) {
			deckList.ShowPreview( null, evt.position );
		}

		deckList.pointerDown = false;
		timeHeld = 0f;
	}

	public virtual void DoubleClick( Vector3 clickPos ) {
		deckList.SuggestionPicked( this );
		deckList.ShowPreview( null, clickPos );
	}

	public void OnBeginDrag(PointerEventData evt) {
		evt.Use();
	}

	public void OnDrag(PointerEventData evt) {
		evt.Use();
	}

	public void OnEndDrag(PointerEventData evt) {
		evt.Use();
	}
}
