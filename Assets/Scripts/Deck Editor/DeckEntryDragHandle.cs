﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class DeckEntryDragHandle : MonoBehaviour, IPointerDownHandler, IDragHandler {

	public DeckEntryItem entry;

	int origSiblingIndex;
	float origY = 0f;

	public void OnPointerDown( PointerEventData evt ) {
		print("Beginning to drag " + Time.time);
		origY = evt.position.y;
		origSiblingIndex = entry.transform.GetSiblingIndex();
		entry.deckList.SelectEntry( entry );

		evt.Use();
	}

	public void OnDrag( PointerEventData evt ) {
		print("Dragging " + Time.time);
		if( origY >= 0f ) {
			float overallScaleMod = entry.deckList.wholeScroll.localScale.y / (entry.deckList.uiScaler.referenceResolution.y / Screen.height);
			int newSiblingIndex = origSiblingIndex + (int)Mathf.Round( (origY - evt.position.y) / (entry.panel.rect.height * overallScaleMod) );
			
			if( newSiblingIndex != transform.GetSiblingIndex() && newSiblingIndex > -1 && newSiblingIndex < entry.deckList.deckLayout.childCount + 1 ) {
				print("Setting sibling index to " + newSiblingIndex);
				entry.deckList.SwitchEntries( entry, newSiblingIndex );
				entry.deckList.saveButton.text = "Save?";
			}
		}

		evt.Use();
	}

	public void OnEndDrag( PointerEventData evt ) {
		print("Finishing drag " + Time.time);
		origY = -1f;
		origSiblingIndex = -1;

		evt.Use();
	}
}
