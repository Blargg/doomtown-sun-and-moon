﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InputFilter : SuggestionFilter {

	const float emptyDropdownSize = 120f;
	const float listOptionHeight = 110f;

	public InputField filter;
	string defaultValue = "";
	public int minFilterLength = 0;

	// dropdowns
	public UnfurlScroll dropdown;
	public VerticalLayoutGroup dropdownEntries;
	public DropdownEntry dropdownEntryPrefab;
	public string[] dropdownOptions = new string[0];

	public override bool isDefault {
		get {
			return filter.text == defaultValue;
		}
		set {
			if( value ) {
				filter.text = defaultValue;
				base.isDefault = value;
			}
		}
	}

	public virtual void Start() {
		defaultValue = filter.text;

		filter.onEndEdit.AddListener( FilterChanged );
	}

	public void FilterChanged( string toValue ) {
		if( !enabled ) return;
		if( minFilterLength < toValue.Length ) {
			deckList.LookForSuggestions();
		}
	}

	public void FilterSelected() {
		if( !enabled ) return;

		foreach( Transform child in dropdownEntries.transform ) {
			Destroy( child.gameObject );
		}

		foreach( string dropdownOption in dropdownOptions ) {
			DropdownEntry newEntry = Instantiate( dropdownEntryPrefab ) as DropdownEntry;
			newEntry.transform.SetParent( dropdownEntries.transform, false );
			newEntry.label.text = dropdownOption;

			newEntry.button.onClick.AddListener( delegate{ DropdownEntryClicked(newEntry.label.text); } );
		}

		if( dropdown ) {
			// dropdown.startingHeightTo = emptyDropdownSize + dropdownOptions.Length * listOptionHeight;
			dropdown.UnfurlNow();
		}
	}

	public void DropdownEntryClicked( string entryText ) {
		if( !enabled ) return;

		FilterDeselected();
		filter.text = entryText;
		FilterChanged( entryText ); // don't need this after Unity allow text assignment in script to trigger valueChanged event
	}

	public void FilterDeselected() {
		if( !enabled ) return;

		dropdown.FurlNow();
	}

	void OnDisable() {
		filter.onEndEdit.RemoveListener( FilterChanged );
	}
}
