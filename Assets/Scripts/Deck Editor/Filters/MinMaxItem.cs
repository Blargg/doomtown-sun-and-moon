﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class MinMaxItem : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler {

	const int smallFontSize = 28;
	const int largeFontSize = 38;

	protected float YscreenDivision = 20f;
	protected float XscreenDivision = 20f;
	protected Vector2 originPoint = Vector2.zero;

	public MinMaxFilter filter;
	public Text itemText;

	protected int internalValue = 0;
	protected int delta = 0;

	Color origColor = Color.black;

	void Start() {
		if( !filter.enabled ) return;
		
		origColor = itemText.color;
	}

	public void OnPointerDown( PointerEventData evt ) {
		if( !filter.enabled ) return;

		originPoint = evt.position;

		internalValue = -2;
		itemText.text = "x";

		itemText.color = filter.deckList.selectedColor;
	}

	public virtual void OnDrag( PointerEventData evt ) {
		if( !filter.enabled ) return;

		print("Dragging to value " + (int)( (evt.position.y - originPoint.y) / YscreenDivision ) );
		int Ydelta = internalValue + (int)( (evt.position.y - originPoint.y) / YscreenDivision );
		int Xdelta = internalValue + (int)( (evt.position.x - originPoint.x) / XscreenDivision );
		delta = Mathf.Max( Ydelta, Xdelta );

		int itemValue = Mathf.Max( delta, -2 );
		// if( itemValue == -2 ) itemText.text = "x";
		// if( itemValue == -1 ) itemText.text = "-";
		itemText.text = itemValue > -2 ? itemValue.ToString() : "x";
		if( itemText.text.Length > 1 ) itemText.fontSize = smallFontSize;
		else itemText.fontSize = largeFontSize;
	}

	public void OnPointerUp( PointerEventData evt ) {
		if( !filter.enabled ) return;

		internalValue = delta;

		itemText.color = origColor;
		filter.deckList.LookForSuggestions();
	}
}
