﻿// serialized set names to exclude from set lists in the deck builder

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class ExcludedSets : MonoBehaviour {
	public List<string> excludeSets;
	public List<string> includeSets;
}
