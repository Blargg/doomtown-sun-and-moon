﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DeckListFilter : MonoBehaviour {

	public ListDeckContents deckList;
	public Text filterText;
	public Text quantityText;
	public int internalQuantity = 0;
	public TranslateBookmark bookmark;

	public DeckListFilter[] exclusiveBookmarks;
	public DeckListFilter[] childBookmarks;

	public virtual bool isActive {
		get {
			return _isActive;
		}
		set {

			if( _isActive && value == true ) {
				_isActive = false;
			}
			else {
				_isActive = value;
			}

			if( _isActive == false ) {
				foreach( DeckListFilter filter in childBookmarks ) {
					if( filter.bookmark.displaying ) {
						filter.isActive = false;
						filter.bookmark.TranslateToggle( false );
					}
				}
			}

			filterText.color = _isActive == true ? deckList.selectedColor : deckList.deselectedColor;
		}
	}
	protected bool _isActive = false;

	public Card.DeckType deckType = Card.DeckType.Dynasty;

	public virtual void FilterCards( DeckEntryItem[] cards ) {
		foreach( DeckEntryItem entry in cards ) {
			if( entry.card == null ) {
				entry.hidden = true;
				continue;
			}

			if( entry.card.deckType == this.deckType ) {
				entry.hidden = false;
			}
			else {
				entry.hidden = true;
			}
		}
	}

	public void Clicked() {
		isActive = true;

		foreach( DeckListFilter filter in childBookmarks ) {
			if( !filter.bookmark.displaying ) filter.bookmark.TranslateToggle( true );
		}

		foreach( DeckListFilter filter in exclusiveBookmarks ) {
			filter.isActive = false;
		}
	}

	public virtual void ApplyQuantity() {
		quantityText.text = "(" + internalQuantity.ToString() + ")";

		quantityText.color = internalQuantity < 40 ? deckList.selectedColor : deckList.deselectedColor;

		internalQuantity = 0;
	}
}
