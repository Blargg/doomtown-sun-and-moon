﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class HRItem : MinMaxItem {

	public override void OnDrag( PointerEventData evt ) {
		int Ydelta = internalValue + (int)( (evt.position.y - originPoint.y) / YscreenDivision );
		int Xdelta = internalValue + (int)( (evt.position.x - originPoint.x) / XscreenDivision );
		delta = Mathf.Max( Ydelta, Xdelta );

		int itemValue = Mathf.Max( delta, -2 );
		if( itemValue == -1 ) itemText.text = "-";
		else itemText.text = itemValue > -2 ? itemValue.ToString() : "x";

		if( itemText.text.Length > 1 ) itemText.fontSize = 28;
		else itemText.fontSize = 38;
	}
}
