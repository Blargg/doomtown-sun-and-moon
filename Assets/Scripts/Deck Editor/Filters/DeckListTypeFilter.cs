﻿using UnityEngine;
using System.Collections;

public class DeckListTypeFilter : DeckListFilter {

	public string cardType = "";

	public override void FilterCards( DeckEntryItem[] cards ) {
		foreach( DeckEntryItem entry in cards ) {
			if( entry.card == null ) {
				entry.hidden = true;
				continue;
			}
			if( cardType == "" ) continue;

			if( entry.card.type == cardType ) entry.hidden = false;
			else entry.hidden = true;
		}
	}

	public override void ApplyQuantity() {
		quantityText.text = "(" + internalQuantity.ToString() + ")";

		internalQuantity = 0;
	}
}
