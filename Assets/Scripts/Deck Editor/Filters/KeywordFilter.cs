﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

using System;

public class KeywordFilter : InputFilter {

	public override Card[] FilterCards( Card[] cards ) {

		if( isDefault ) return cards;

		string filterText = filter.text.ToLower().Trim('s');

		string[] andBooleans = filterText.Split( new string[] { " and " }, StringSplitOptions.RemoveEmptyEntries );
		string[] orBooleans = filterText.Split( new string[] { " or " }, StringSplitOptions.RemoveEmptyEntries );

		List<Card> filteredCards = new List<Card>();
		for( int index = 0; index < cards.Length; index++ ) {
			string lCaseName = cards[index].text.ToLower().Trim();

			if( orBooleans.Length > 1 ) {
				foreach( string orBoolean in orBooleans ) {
					if( lCaseName.Contains( orBoolean.Trim() ) ) {
						filteredCards.Add( cards[index] );
					}
				}
			}

			int matches = 0;
			foreach( string andBoolean in andBooleans ) {
				if( lCaseName.Contains( andBoolean.Trim() ) ) {
					matches++;
				}
			}
			if( matches == andBooleans.Length ) {
				filteredCards.Add( cards[index] );
			}
		}

		return filteredCards.ToArray();
	}
}
