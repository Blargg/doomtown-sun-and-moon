﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class PHMinMaxItem : MinMaxItem {

	public override void OnDrag( PointerEventData evt ) {
		base.OnDrag( evt );

		if( itemText.text == "-1" ) itemText.text = "-";
	}
}
