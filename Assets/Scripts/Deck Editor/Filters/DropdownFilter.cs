﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class DropdownFilter : OracleFilter {

	const float emptyDropdownSize = 75f;
	const float maxHeight = 2500f;

	const string listOptionStartSign = "OPTION VALUE=\"";
	const int listOptionStartLength = 14;
	const string listOptionEndSign = "\"";
	const string listEndSign = "</SELECT>";
	const float listOptionHeight = 133f;

	public UnfurlScroll dropdown;
	public VerticalLayoutGroup dropdownEntries;
	public DropdownEntry dropdownEntryPrefab;
	protected List<string> dropdownOptions = new List<string>();
	protected List<string> unsanitizedDropdownOptions = new List<string>();

	public List<string> extraOptions = new List<string>();
	public List<string> extraOptionsInternal = new List<string>();

	// select just a portion of text in a dropdown list
	public string excerptDropdownStart = "";
	public string excerptDropdownEnd = "";

	public bool markDeckAsDirty = false;

	bool blockInput = false;

	public override bool isDefault {
		get {
			return base.isDefault;
		}
		set {
			base.isDefault = value;

			dropdown.FurlNow();
		}
	}

	void Start() {}

	public override void Initialize( string oracleSearchPageSlush, ListDeckContents list ) {
		base.Initialize( oracleSearchPageSlush, list );

		for( int index = 0; index < extraOptions.Count; index++ ) {
			unsanitizedDropdownOptions.Add( extraOptionsInternal[index] );
			dropdownOptions.Add( extraOptions[index] );
		}

		int listStartIndex = oracleSearchPageSlush.IndexOf( filterQuery );
		if( listStartIndex == -1 ) return;

		int listEndIndex = oracleSearchPageSlush.IndexOf( listEndSign, listStartIndex );
		if( listEndIndex == -1 ) return;

		int optionStartIndex = -1;
		optionStartIndex = oracleSearchPageSlush.IndexOf( listOptionStartSign, listStartIndex ) + listOptionStartLength;
		while( optionStartIndex > listOptionStartLength - 1 && optionStartIndex < listEndIndex ) {
			int optionEndIndex = oracleSearchPageSlush.IndexOf( listOptionEndSign, optionStartIndex );

			if( optionEndIndex != -1 ) {
				string optionEntry = oracleSearchPageSlush.Substring( optionStartIndex, optionEndIndex - optionStartIndex );
				optionEntry = optionEntry.Replace( "&amp;", "&" );

				if( optionEntry != "" ) unsanitizedDropdownOptions.Add( optionEntry );

				if( excerptDropdownStart != "" ) {
					int excerptStartIndex = optionEntry.IndexOf( excerptDropdownStart ) + 1;
					if( excerptStartIndex > -1 ) {
						int excerptEndIndex = optionEntry.IndexOf( excerptDropdownEnd, excerptStartIndex );
						if( excerptEndIndex > -1 ) {
							optionEntry = optionEntry.Substring( excerptStartIndex, excerptEndIndex - excerptStartIndex );
						}
					}
				}

				optionEntry = optionEntry.Replace( "&nbsp;", " " );
				optionEntry = optionEntry.Replace( "&ndash;", " - " );
				if( optionEntry != "" ) dropdownOptions.Add( optionEntry );

				optionStartIndex = oracleSearchPageSlush.IndexOf( listOptionStartSign, optionEndIndex ) + listOptionStartLength;
			}
			else {
				print( "Unclosed option field in source HTML.  Should hopefully never happen!" );
				optionStartIndex = -1;
			}
		}

		FilterOptions(); // for other types of dropdown lists that might want to limit what items get put in the list

		if( enabled ) dropdown.startingHeightTo = Mathf.Min( emptyDropdownSize + dropdownOptions.Count * listOptionHeight, maxHeight );

		foreach( string dropdownOption in dropdownOptions ) {
			DropdownEntry newEntry = Instantiate( dropdownEntryPrefab ) as DropdownEntry;
			newEntry.transform.SetParent( dropdownEntries.transform, false );
			newEntry.label.text = dropdownOption;

			newEntry.button.onClick.AddListener( delegate{ DropdownEntryClicked(newEntry.label.text); } );
		}
	}

	public virtual void FilterOptions() {

	}

	public void FilterSelected() {
		print("Trying to select filter " + Time.time);
		
		if( !enabled ) return;

		print("Dropdown selected " + Time.time);

		// foreach( Transform child in dropdownEntries.transform ) {
		// 	Destroy( child.gameObject );
		// }

		dropdown.UnfurlNow();
	}

	public void DropdownEntryClicked( string entryText ) {
		if( !enabled ) return;
		if( blockInput ) return;
		blockInput = true;

		if( filterLabel ) filterLabel.text = entryText;
		if( filterInput ) filterInput.text = entryText;

		FilterDeselected();

		// de-sanitize dropdown index
		for( int index = 0; index < dropdownOptions.Count; index++ ) {
			if( dropdownOptions[index] == entryText ) {
				entryText = unsanitizedDropdownOptions[index];
			}
		}

		print(Time.time + " CLICKED DROPDOWN " + name);

		FilterChanged( entryText ); // FilterChanged is already called by InputField.onEndEdit, so no need to do so twice
		if( markDeckAsDirty ) deckList.saveButton.text = "Save?";

		StartCoroutine( BlockInput() );
	}

	IEnumerator BlockInput() {
		yield return new WaitForSeconds( 0.25f );
		blockInput = false;
	}

	public void FilterDeselected() {
		if( !enabled ) return;

		dropdown.FurlNow();
	}

	// void OnDisable() {
	// 	filterInput.onEndEdit.RemoveListener( FilterChanged );
	// }
}
