﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class EditionFilter : InputFilter {

	public override void Start() {
		dropdownOptions = Persistent.data.localDbSets.ToArray();

		base.Start();
	}

	public override Card[] FilterCards( Card[] cards ) {

		if( isDefault ) return cards;

		string filterText = filter.text.ToLower();

		List<Card> filteredCards = new List<Card>();
		for( int index = 0; index < cards.Length; index++ ) {
			for( int editionIndex = 0; editionIndex < cards[index].editions.Count; editionIndex++ ) {
				if( cards[index].editions[editionIndex].ToLower() == filterText.Trim(' ', '\n') ) {
					filteredCards.Add( cards[index] );
				}
			}
		}

		return filteredCards.ToArray();
	}
}
