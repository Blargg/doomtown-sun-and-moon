﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class OracleFilter : MonoBehaviour {

	public string filterQuery;
	public Text filterLabel;
	public InputField filterInput;

	public string defaultText = "";

	[HideInInspector] public ListDeckContents deckList;

	public virtual bool isDefault {
		get {
			if( filterLabel ) {
				return filterLabel.text == defaultText;
			}
			return filterInput.text == defaultText;
		}
		set {
			if( !enabled || !value ) return;

			if( filterInput ) filterInput.text = defaultText;
			if( filterLabel ) filterLabel.text = defaultText;

			deckList.SearchOracle();
		}
	}
	[HideInInspector] public string internalTerm = "";

	public virtual void Initialize( string oracleSearchPageSlush, ListDeckContents list ) {
		deckList = list;
	}

	public virtual string GetSearchTerm() {
		return internalTerm; // de-sanitized in the case of special characters
	}

	public virtual void FilterChanged( string toValue ) {
		if( !enabled ) return;

		internalTerm = toValue.Trim();

		if( Time.timeSinceLevelLoad > 1f ) deckList.SearchOracle();
	}
}
