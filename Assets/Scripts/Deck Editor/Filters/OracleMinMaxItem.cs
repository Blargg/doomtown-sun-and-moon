﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class OracleMinMaxItem : OracleFilter, IPointerDownHandler, IDragHandler, IPointerUpHandler {

	const int smallFontSize = 28;
	const int largeFontSize = 38;

	const float YscreenDivision = 20f;
	const float XscreenDivision = 20f;
	protected Vector2 originPoint = Vector2.zero;

	protected int internalValue = 0;
	protected int delta = 0;

	public Color selectedColor;
	Color origColor = Color.black;

	void Start() {
		if( !enabled ) return;

		origColor = filterLabel.color;
	}

	public void OnPointerDown( PointerEventData evt ) {
		if( !enabled ) return;

		originPoint = evt.position;

		internalValue = -2;
		filterLabel.text = "x";

		filterLabel.color = selectedColor;
	}

	public virtual void OnDrag( PointerEventData evt ) {
		if( !enabled ) return;

		int Ydelta = internalValue + (int)( (evt.position.y - originPoint.y) / YscreenDivision );
		int Xdelta = internalValue + (int)( (evt.position.x - originPoint.x) / XscreenDivision );
		delta = Mathf.Max( Ydelta, Xdelta );

		int itemValue = Mathf.Max( delta, -2 );
		filterLabel.text = itemValue > -2 ? itemValue.ToString() : "x";
		if( filterLabel.text == "-1" ) filterLabel.text = "-";
		if( filterLabel.text.Length > 1 ) filterLabel.fontSize = smallFontSize;
		else filterLabel.fontSize = largeFontSize;
	}

	public void OnPointerUp( PointerEventData evt ) {
		if( !enabled ) return;
		
		internalValue = delta;

		filterLabel.color = origColor;

		FilterChanged( filterLabel.text );
	}
}
