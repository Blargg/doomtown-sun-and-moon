﻿using UnityEngine;
using System.Collections;

public class DropdownSetFilter : DropdownFilter {

	public bool harvestExcludedSets = false;
	public ExcludedSets excludedSetsPrefab;

	// public override void Initialize( string oracleSearchPageSlush, ListDeckContents list ) {
	// 	base.Initialize( oracleSearchPageSlush, list );
	// }

	public override void FilterOptions() {

		if( harvestExcludedSets ) {
			excludedSetsPrefab.excludeSets.Clear();
			foreach( string harvestedOption in dropdownOptions ) {
				if( !excludedSetsPrefab.includeSets.Contains(harvestedOption) ) excludedSetsPrefab.excludeSets.Add( harvestedOption );
			}
		}

		for( int index = 0; index < excludedSetsPrefab.excludeSets.Count; index++ ) {
			string excludedSet = excludedSetsPrefab.excludeSets[index];
			int setToExcludeIndex = dropdownOptions.IndexOf( excludedSet );

			if( setToExcludeIndex > -1 ) {
				dropdownOptions.RemoveAt( setToExcludeIndex );
				unsanitizedDropdownOptions.RemoveAt( setToExcludeIndex );
			}
		}
	}
}
