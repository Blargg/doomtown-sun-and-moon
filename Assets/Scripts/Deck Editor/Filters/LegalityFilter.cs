﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

using Legality = Card.Legality;

public class LegalityFilter : SuggestionFilter {

	public Text filter;

	// dropdowns
	public UnfurlScroll dropdown;
	public VerticalLayoutGroup dropdownEntries;
	public DropdownEntry dropdownEntryPrefab;

	public System.Action onClicked;

	public Legality legality {
		get {
			return _legality;
		}
		set {
			_legality = value;
			legalityLabel.text = ParseLegality( _legality );
		}
	}
	Legality _legality = Legality.Legacy;

	public Text legalityLabel;

	public override bool isDefault {
		get {
			return legality == Legality.Legacy;
		}
		set {
			legality = Legality.Legacy;
			base.isDefault = value;
		}
	}

	public override Card[] FilterCards( Card[] cards ) {

		List<Card> filteredCards = new List<Card>();

		string[] legal = Persistent.data.LegalityEnumToStrings(legality);
		for( int index = 0; index < cards.Length; index++ ) {
			foreach( string legalPossibility in legal ) {
				if( legalPossibility.StartsWith("-") ) {
					string trimmedLegality = legalPossibility.Trim('-');
					if( cards[index].legal.Contains(trimmedLegality) ) {
						break;
					}
				}
				else if( cards[index].legal.Contains(legalPossibility) ) {
					filteredCards.Add( cards[index] );
					break;
				}
			}
		}

		return filteredCards.ToArray();
	}

	// Dropdown behavior

	public void FilterSelected() {
		if( !enabled ) return;

		foreach( Transform child in dropdownEntries.transform ) {
			Destroy( child.gameObject );
		}

		Legality[] enumlegalities = (Legality[])System.Enum.GetValues(typeof(Legality));

		for( int index = 0; index < enumlegalities.Length; index++ ) {
			DropdownEntry newEntry = Instantiate( dropdownEntryPrefab ) as DropdownEntry;
			newEntry.transform.SetParent( dropdownEntries.transform, false );
			newEntry.label.text = ParseLegality( enumlegalities[index] );

			Legality thisLegality = (Legality)index;

			newEntry.button.onClick.AddListener( delegate{ DropdownEntryClicked(thisLegality); } );
		}

		dropdown.UnfurlNow();
	}

	public void DropdownEntryClicked( Legality clickedLegality ) {
		if( !enabled ) return;

		FilterDeselected();

		filter.text = ParseLegality( clickedLegality );

		legality = clickedLegality;

		if( deckList ) deckList.saveButton.text = "Save?";
		if( deckList ) deckList.LookForSuggestions();

		if( onClicked != null ) onClicked();
	}

	public void FilterDeselected() {
		if( !enabled ) return;
		
		dropdown.FurlNow();
	}

	public static string ParseLegality(Legality l) {
		string s = l.ToString();

		s = s.Replace("_", " ");
		// s = s.Replace("Twenty Festivals", "20F");

		return s;
    }
}
