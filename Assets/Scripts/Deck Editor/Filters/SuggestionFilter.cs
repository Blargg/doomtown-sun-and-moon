﻿using UnityEngine;
using System.Collections;

public class SuggestionFilter : MonoBehaviour {

	public ListDeckContents deckList;

	public SuggestionFilter[] filtersToClear;

	public virtual bool isDefault {
		get {
			return true;
		}
		set {
			if( !enabled ) return;
			foreach( SuggestionFilter filter in filtersToClear ) filter.isDefault = value;
			deckList.LookForSuggestions();
		}
	}

	public virtual void Initialize() {}

	public virtual Card[] FilterCards( Card[] cards ) {
		return null;
	}

	public void TestForEmpty( string inValue ) {
		if( inValue == "" ) deckList.LookForSuggestions();
	}
}
