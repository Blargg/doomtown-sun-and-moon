﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class TypeFilter : InputFilter {

	public override Card[] FilterCards( Card[] cards ) {

		if( isDefault ) return cards;

		string filterText = filter.text.ToLower().Trim('s').Trim('e').Trim('i');

		List<Card> filteredCards = new List<Card>();
		for( int index = 0; index < cards.Length; index++ ) {
			string lCaseName = cards[index].type.ToLower();

			if( lCaseName.Contains( filterText ) || filterText.Contains( lCaseName ) ) {

				filteredCards.Add( cards[index] );
			}
		}

		return filteredCards.ToArray();
	}
}
