﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GCFilter : MinMaxFilter {

	public override Card[] FilterCards( Card[] cards ) {

		if( isDefault ) return cards;

		List<Card> filteredCards = new List<Card>();
		for( int index = 0; index < cards.Length; index++ ) {
			
			int minValue = int.TryParse(min.text, out minValue) ? minValue : 0;
			int maxValue = int.TryParse(max.text, out maxValue) ? maxValue : 99;

			if( (cards[index].cost >= minValue && cards[index].cost <= maxValue) || (cards[index].gold_production >= minValue && cards[index].gold_production <= maxValue) ) {
				filteredCards.Add( cards[index] );
			}
		}

		return filteredCards.ToArray();
	}
}
