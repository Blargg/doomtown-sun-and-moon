﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MinMaxFilter : SuggestionFilter {

	public Text min;
	public Text max;
	public string defaultValue = "x";

	public override bool isDefault {
		get {
			return min.text == defaultValue && max.text == defaultValue;
		}
		set {
			if( value ) {
				min.text = defaultValue;
				max.text = defaultValue;

				base.isDefault = value;
			}
		}
	}
}
