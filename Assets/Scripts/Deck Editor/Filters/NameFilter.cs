﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class NameFilter : SuggestionFilter {

	public InputField filter;
	public string defaultValue = "";
	public int minFilterLength = 0;

	public override bool isDefault {
		get {
			if( filter == null ) return true;
			return filter.text == defaultValue || string.IsNullOrEmpty( filter.text ) || filter.text.Length < minFilterLength;
		}
		set {
			if( value ) {
				filter.text = defaultValue;
				base.isDefault = value;
			}
		}
	}

	public override Card[] FilterCards( Card[] cards ) {

		if( isDefault ) {
			return cards;
		}

		print("Filtering by " + filter.text.ToLower() );

		string filterText = filter.text.ToLower().Sanitize();

		List<Card> filteredCards = new List<Card>();
		for( int index = 0; index < cards.Length; index++ ) {
			string lCaseName = cards[index].name.ToLower();

			if( lCaseName.Contains( filterText ) ) {

				filteredCards.Add( cards[index] );
			}
			else {
			}
		}

		return filteredCards.ToArray();
	}

	public void FilterChanged( string toValue ) {
		if( !enabled ) return;
		
		if( minFilterLength < toValue.Length ) {
			deckList.LookForSuggestions();
		}
	}
}
