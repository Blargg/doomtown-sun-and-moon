﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ClanFilter : InputFilter {

	public override Card[] FilterCards( Card[] cards ) {

		if( isDefault ) return cards;

		List<string> filterTexts = new List<string>();
		string filterText = filter.text.ToLower().Trim('s');
		filterTexts.Add( filterText );

		if( filterText.Contains("naga") ) filterTexts.Add( "akasha" );

		List<Card> filteredCards = new List<Card>();
		for( int index = 0; index < cards.Length; index++ ) {
			if( cards[index].clans.Count == 0 ) continue;

			foreach( string subFilter in filterTexts ) {
				foreach( string cardClan in cards[index].clans ) {
					if( cardClan.Contains( subFilter ) || subFilter.Contains( cardClan ) ) {
						filteredCards.Add( cards[index] );
					}
				}
			}
		}

		return filteredCards.ToArray();
	}
}
