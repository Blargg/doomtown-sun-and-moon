﻿using UnityEngine;
using System.Collections;

public class TranslateBookmark : MonoBehaviour {

	public RectTransform bookmark;
	public CanvasGroup opacity;

	public bool translateOnStart = true;

	float translateToX = 0f;
	public float translateFromX = 0f;
	public float translationDuration = 1f;

	public float randomDelayMin = 0.5f;
	public float randomDelayMax = 1f;

	public bool displaying = false;

	bool loading = false;

	void OnEnable() {
		LoadLevel.LoadingLevel += LoadingLevelEvent;

		translateToX = bookmark.anchoredPosition.x;

		bookmark.anchoredPosition = new Vector2( translateFromX, bookmark.anchoredPosition.y );
	}

	public IEnumerator Start() {
		opacity.alpha = 0f;

		if( !translateOnStart ) yield break;

		yield return new WaitForSeconds( Random.Range(randomDelayMin, randomDelayMax) );

		TranslateToggle( true );
	}

	public void TranslateToggle( bool state ) {
		if( loading ) return;
		loading = true;
		
		LeanTween.cancel( gameObject );

		opacity.alpha = 1f;
		
		LeanTween.value(
			gameObject, x => {bookmark.anchoredPosition = new Vector2(x, bookmark.anchoredPosition.y);},
			bookmark.anchoredPosition.x,
			state ? translateToX : translateFromX,
			translationDuration
		)
		.setEase(LeanTweenType.easeInOutQuad)
		.setOnComplete( delegate(){ displaying = state; opacity.alpha = displaying ? 1f : 0f; loading = false; } );

		Persistent.data.FrameRateUpFor( translationDuration );
	}

	public IEnumerator DelayedTranslateToggle( bool state, float delay ) {
		yield return new WaitForSeconds( delay );

		TranslateToggle( state );
	}

	public void LoadingLevelEvent() {
		TranslateToggle( false );
	}

	public void OnDisable() {
		LoadLevel.LoadingLevel -= LoadingLevelEvent;
	}
}
