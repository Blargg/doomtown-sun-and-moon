﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;

using System.Text;
using SevenZip.Compression.LZMA;

public class ListDeckContents : MonoBehaviour {

	public const string oracleFilterPageURL = "http://imperialassembly.com/oracle/";
	const string pageNumberSign = "{changepage($(this));}\">of ";
	const int pageNumberAfterSign = 27;
	const string endPageNumberSign = "&nbsp;";
	const string hashStartSign = "#hashid=";
	const string hashEndSign = ",#";
	[HideInInspector] public bool oracleOnline = false;

	[HideInInspector] public Deck deckToEdit;
	GameObject emissary;
	public InputField deckName;
	public RectTransform deckLayout;
	public RectTransform wholeScroll;
	public CanvasScaler uiScaler;
	public DrawPreview2D drawPreview2D;

	public Text dynastyCount;
	public Text fateCount;
	public Text persCount;
	public Text holdCount;

	public DeckEntryItem cardListItemPrefab;
	public SuggestionListItem suggestionPrefab;
	public RectTransform suggestionList;
	public SuggestionListItem[] suggestions;
	[HideInInspector] public Card previewing = null;
	[HideInInspector] public bool pointerDown = false;

	public NameFilter nameFilter;

	public Color deselectedColor;
	public Color selectedColor;
	public Color validColor;
	public Color invalidColor;

	List<DeckEntryItem> cardListEntries;

	string origDeckPath = ""; // so we can overwrite the file later
	public LegalityFilter legalityFilter;
	public DropdownFilter oracleLegalityFilter;
	public DeckListFilter[] deckTypeFilters;
	public DeckListTypeFilter[] cardTypeFilters;

	public LoadingSpinner suggestionsSpinner;
	public OracleFilter[] oracleFilters;
	public InputField editionFilter;
	int suggestionsAdded = 0;
	int pagesAvailable = 0;
	string currentPageHash = "";
	int currentPage = 0;
	bool loadingFromOracle = false;

	DeckEntryItem selectedEntry;

	public Text saveButton;
	public Text deleteButton;
	public LoadLevel previousLevel;

	public ScrollRect cardList;

	public List<SuggestionFilter> filters;

	int maxSuggestionsAllowed = 500;

	// Locking
	public CanvasGroup lockButton;
	public UnfurlScroll confirmLock;
	public UnfurlScroll confirmUnlock;
	
	IEnumerator Start () {

		int.TryParse( PlayerPrefs.GetString( "numSearchResults", "500" ), out maxSuggestionsAllowed );

		cardListEntries = new List<DeckEntryItem>();

		emissary = GameObject.Find("DeckEmissary(Clone)");
		if( emissary ) {
			deckToEdit = emissary.GetComponent<Deck>();
			origDeckPath = deckToEdit.pathToDeckOnDisk;

			yield return StartCoroutine( deckToEdit.GenerateCards() );
		}

		string deckSlush = deckToEdit.GetSlush();

		// Legality
		legalityFilter.legality = (Card.Legality)deckToEdit.legality;
		int legalityIndex = deckSlush.IndexOf("Legality:");
		if( legalityIndex > -1 ) {

			deckSlush = deckSlush.Remove( legalityIndex, (deckSlush.IndexOf("\n", legalityIndex) - legalityIndex) + 1 ); // remove line
		}

		// Separate cards
		string[] splitSlush = deckSlush.Split('\n');

		while( Persistent.data.cardsByName == null ) yield return null;

		for( int index = 0; index < splitSlush.Length; index++ ) {

			string cleaned = splitSlush[index].ReplaceCommonTerms();

			NewLine( index, cleaned );
		}
		CountCards();

		deckName.text = deckToEdit.deckName;
		saveButton.text = "Saved";

		// build suggestions

		suggestions = new SuggestionListItem[maxSuggestionsAllowed];
		for( int index = 0; index < maxSuggestionsAllowed; index++ ) {
			SuggestionListItem suggestion = Instantiate( suggestionPrefab ) as SuggestionListItem;
			suggestion.transform.SetParent( suggestionList, false );
			suggestion.deckList = this;

			suggestions[index] = suggestion;

			suggestion.gameObject.SetActive( false );
		}

		// locking
		if( !deckToEdit.locked ) lockButton.alpha = 0.5f;

		if( PlayerPrefs.GetInt( "searchOracle", 0 ) == 1 ) {
			// use oracle filters if you have access
			if( Persistent.data.playtestGroup == "" ) {
				WWW oracleFilterRequest = new WWW( oracleFilterPageURL );
				yield return oracleFilterRequest;

				if( string.IsNullOrEmpty( oracleFilterRequest.error ) ) {
					oracleOnline = true;

					for( int index = 0; index < oracleFilters.Length; index++ ) {
						oracleFilters[index].enabled = true;
						oracleFilters[index].Initialize( oracleFilterRequest.text, this );
					}
					oracleLegalityFilter.DropdownEntryClicked( legalityFilter.legalityLabel.text.Trim() );

					// clear local database filters; we won't be using them
					legalityFilter.enabled = false;
					nameFilter.enabled = false;
					for( int index = 0; index < filters.Count; index++ ) {
						filters[index].enabled = false;
					}
				}
				else {
					print("Failed to load oracle filter set.  Defaulting to internal filters.");
				}
			}
		}
		else {
			for( int index = 0; index < oracleFilters.Length; index++ ) {
				oracleFilters[index].enabled = false;
			}

			for( int index = 0; index < filters.Count; index++ ) {
				filters[index].enabled = true;
			}
		}
	}

	public void NewLine() {
		NewLine( cardListEntries.Count, "" );

		saveButton.text = "Save?";
	}

	public DeckEntryItem NewLine( int atIndex, string withText ) {
		DeckEntryItem newLine = Instantiate( cardListItemPrefab ) as DeckEntryItem;
		newLine.transform.SetParent( deckLayout, false );
		newLine.transform.SetSiblingIndex( atIndex );

		string sanitizedName = withText.ReplaceCommonTerms();
		newLine.label.text = sanitizedName;

		// check if the deck has already loaded a copy of this card, i.e. from the Oracle
		Card card = null;
		foreach( Card cardInDeck in deckToEdit.cardList ) {
			string trimmedText = Persistent.data.TrimCardLine( withText ).ToLower().ReplaceCommonTerms();
			trimmedText = Persistent.data.TrimCardType( trimmedText );
			// trimmedText = Persistent.data.TrimPrinting( trimmedText );
			trimmedText = Persistent.data.TrimEdition( trimmedText ); // cut off edition tag if there is one

			// print("Comparing card in deck " + cardInDeck.name.ToLower() + " to trimmed text in line " + trimmedText);
			if( cardInDeck.name.ToLower() == trimmedText ) {
				
				card = cardInDeck;
				break;
			}
		}

		cardListEntries.Insert( atIndex, newLine );

		if( card == null ) card = Persistent.data.VerifyTextAgainstDatabase( sanitizedName );
		if( card == null ) {
			StartCoroutine( Persistent.data.VerifyTextAgainstOracle( sanitizedName, delegate(Card c, int q){ CardLineConfirmedWithOracle(c, q, withText); } ) );
		}

		if( card != null ) {
			newLine.card = card;
			newLine.background.color = validColor;
		}
		else if( !string.IsNullOrEmpty( sanitizedName ) ) {
			if( newLine.card == null ) newLine.background.color = invalidColor; // might not be null if the oracle verification has already returned (i.e. it was cached)
		}
		newLine.cardQuantity = Persistent.data.GetCardQuantityInt( newLine.label.text );

		newLine.Deselect();

		newLine.deckList = this;

		return newLine;
	}

	public void CardLineConfirmedWithOracle( Card confirmedCard, int quantity, string lineText ) {
		if( confirmedCard == null ) return;

		foreach( DeckEntryItem entryItem in cardListEntries ) {
			if( entryItem.label.text.ToLower().Trim() == lineText.ToLower().Trim() ) {
				entryItem.card = confirmedCard;
				entryItem.background.color = validColor;
				entryItem.cardQuantity = quantity;

				print("Confirmed " + entryItem.label.text);
			}
		}

		CountCards();
	}

	public void DeleteEntry( DeckEntryItem lineToDelete ) {
		if( deckToEdit.locked ) return;

		cardListEntries.Remove( lineToDelete );

		Destroy( lineToDelete.gameObject );

		CountCards();
	}

	public void CountCards() {

		for( int i = 0; i < cardListEntries.Count; i++ ) {
			DeckEntryItem entry = cardListEntries[i];

			if( entry.card != null ) {
				if( entry.card.type == "stronghold" ) continue;
				if( entry.card.type == "sensei" ) continue;
				if( entry.card.type == "proxy" ) continue;

				foreach( DeckListFilter deckTypeFilter in deckTypeFilters ) {
					if( entry.card.deckType == deckTypeFilter.deckType ) deckTypeFilter.internalQuantity += entry.cardQuantity;
				}
				foreach( DeckListTypeFilter cardTypeFilter in cardTypeFilters ) {
					if( entry.card.type == cardTypeFilter.cardType ) cardTypeFilter.internalQuantity += entry.cardQuantity;
				}
			}
		}

		foreach( DeckListFilter deckTypeFilter in deckTypeFilters ) deckTypeFilter.ApplyQuantity();
		foreach( DeckListTypeFilter cardTypeFilter in cardTypeFilters ) cardTypeFilter.ApplyQuantity();
	}

	public void DeckNameSubmitted( string name ) {
		if( deckToEdit.locked ) return;

		deckToEdit.deckName = name;
		deckToEdit.pathToDeckOnDisk = Persistent.data.pathToDecks + name + ".txt";

		saveButton.text = "Save?";
	}

	public void SelectEntry( DeckEntryItem entryToSelect ) {
		foreach( DeckEntryItem existingEntry in cardListEntries ) {
			existingEntry.Deselect();
		}
		entryToSelect.Select();

		selectedEntry = entryToSelect;
	}

	public void FilterDeckList() {

		DeckEntryItem[] items = cardListEntries.ToArray();
		foreach( DeckEntryItem entry in items ) entry.hidden = false;

		foreach( DeckListFilter filter in deckTypeFilters ) if( filter.isActive ) filter.FilterCards( items );
		foreach( DeckListTypeFilter filter in cardTypeFilters ) if( filter.isActive ) filter.FilterCards( items );

		foreach( DeckEntryItem entry in cardListEntries ) {
			if( entry.hidden ) entry.gameObject.SetActive( false );
			else entry.gameObject.SetActive( true );
		}
	}

	// Oracle search

	public void SearchOracle() {
		// if( Persistent.data.playtestGroup != "" ) {
		// 	LookForSuggestions();
		// 	return;
		// }

		foreach( SuggestionListItem suggestion in suggestions ) suggestion.gameObject.SetActive( false );

		suggestionsAdded = 0;
		currentPage = 0;
		currentPageHash = "";
		for( int index = 1; index < 3; index++ ) {
			StartCoroutine( QueryOracle( index ) );
		}

		// disable remainder of suggestions, if any
		for( int index = suggestionsAdded; index < suggestions.Length; index++ ) {
			suggestions[index].gameObject.SetActive( false );
		}
	}

	public IEnumerator QueryOracle( int page ) {

		while( loadingFromOracle ) yield return null;
		loadingFromOracle = true;

		suggestionsSpinner.Fade( 1f );

		// string queriedURL = "";
		string queriedText = "";
		string queriedError = "Page " + page + " not found";

		// get the first page, or if you are continuing an existing search, search by the hash harvested on that first page
		if( page == 1 ) {
			WWWForm queryPost = new WWWForm();
			foreach( OracleFilter oracleFilter in oracleFilters ) {
				if( !oracleFilter.isDefault ) {
					print( "Searching oracle with filter " + oracleFilter.name + " ... adding filter term " + oracleFilter.GetSearchTerm() );
					queryPost.AddField( oracleFilter.filterQuery, oracleFilter.GetSearchTerm() );
				}
			}

			// ASCIIEncoding ascii = new ASCIIEncoding();
			// String decoded = ascii.GetString(queryPost.data);
			// print("Searching for cards on first page with WWWForm data " + decoded);

			WWW postRequest = new WWW( Persistent.data.pathToOracle + "dosearch", queryPost.data, Persistent.GenerateRequestHeaders() );
			yield return postRequest;

			if( string.IsNullOrEmpty( postRequest.error ) ) {
				currentPageHash = GetPageHash( postRequest.text );
				// queriedURL = postRequest.url;
				queriedText = postRequest.text;
				queriedError = postRequest.error;
			}
			else {
				print("Error loading first page of query: " + postRequest.error);
			}

			postRequest.Dispose();
		}
		else if( page <= pagesAvailable ) {

			while( currentPageHash == "" ) yield return null; // wait for the page hash to resolve, which it must as soon as there is a page request

			WWWForm queryPost = new WWWForm();
			queryPost.AddField( "page", page.ToString() );
			queryPost.AddField( "hashid", currentPageHash );
			// string pageURL = "http://imperialassembly.com/oracle/#hashid=" + currentPageHash + ",#page=" + page.ToString();

			// ASCIIEncoding ascii = new ASCIIEncoding();
			// String decoded = ascii.GetString(queryPost.data);

			WWW postRequest = new WWW( Persistent.data.pathToOracle + "dosearch", queryPost.data, Persistent.GenerateRequestHeaders() );
			// WWW postRequest = new WWW( pageURL );
			yield return postRequest;
			
			if( string.IsNullOrEmpty( postRequest.error ) ) {
				// queriedURL = postRequest.url;
				queriedText = postRequest.text;
				queriedError = postRequest.error;
			}
			else {
				print("Error loading page " + page + " of query: " + postRequest.error);
			}

			postRequest.Dispose();
		}

		if( string.IsNullOrEmpty( queriedError ) ) {
			// string resultsURL = queriedURL;

			List<string> cardNamesOnPage = new List<string>();

			string oneResultSign = "$.post";
			string idEndSign = "\",";
			int idStartLength = 27;
			List<string> cardIdOnPage = Persistent.GetCardDetailsFromOracleQuery( queriedText, oneResultSign, idEndSign, idStartLength );
			if( cardIdOnPage.Count == 1 ) {
				// found just one result, don't need a list
				yield return StartCoroutine( Persistent.data.VerifyIDAgainstOracle( cardIdOnPage[0], delegate(Card c, int q) { if( c != null ) cardNamesOnPage.Add( c.name ); } ) );
			}
			else {
				pagesAvailable = GetTotalPages( queriedText );

				string nameStartSign = "<span class=\"l5rfont\">";
				string nameEndSign = "</span>";
				int nameStartLength = 22;
				cardNamesOnPage = Persistent.GetCardDetailsFromOracleQuery( queriedText, nameStartSign, nameEndSign, nameStartLength );
			}

			// string setStartSign = "floatright setrarity\">";
			// string setEndSign = " &#149;";
			// int setStartLength = 22;
			// List<string> cardSetsOnPage = Persistent.GetCardDetailsFromOracleQuery( queriedText, setStartSign, setEndSign, setStartLength );

			// string cardNamesHarvestedInQuery = "";
			// foreach( string cardName in cardNamesOnPage ) {
			// 	cardNamesHarvestedInQuery += cardName + "/";
			// }
			// print(Time.time + " HARVESTED CARDS IN QUERY " + page + ": \n" + cardNamesHarvestedInQuery);

			for( int index = 0; index < cardNamesOnPage.Count; index++ ) {
				if( suggestionsAdded == maxSuggestionsAllowed ) break;

				string editionTag = "";
				if( editionFilter.text != "" ) {
					editionTag = " (" + editionFilter.text + ")";
				}

				suggestions[suggestionsAdded].label.text = cardNamesOnPage[index] + editionTag;
				suggestions[suggestionsAdded].gameObject.SetActive( true );

				suggestionsAdded++;
			}

			currentPage = page;
		}
		else {
			print("Error searching local database: " + queriedError);
			// search local database
			// LookForSuggestions();
		}

		loadingFromOracle = false;

		suggestionsSpinner.Fade( 0f );
	}

	int GetTotalPages( string pageSlush ) {
		int pageCounterIndex = pageSlush.IndexOf( pageNumberSign );

		int totalPages = 1;
		if( pageCounterIndex > -1 ) {
			int pageCountStartIndex = pageCounterIndex + pageNumberAfterSign;
			int pageCountEndIndex = pageSlush.IndexOf( endPageNumberSign, pageCounterIndex );

			string totalPagesString = pageSlush.Substring( pageCountStartIndex, pageCountEndIndex - pageCountStartIndex );

			int.TryParse( totalPagesString, out totalPages );
		}

		return totalPages;
	}

	string GetPageHash( string pageSlush ) {
		int hashStartIndex = pageSlush.IndexOf( hashStartSign ) + hashStartSign.Length;
		int hashEndIndex = pageSlush.IndexOf( hashEndSign, hashStartIndex );

		string pageHash = pageSlush.Substring( hashStartIndex, hashEndIndex - hashStartIndex );
		print("Page hash is " + pageHash);
		return pageHash;
	}

	public void SuggestionListScrolled( Single scrollValue ) {
		if( loadingFromOracle ) return;

		if( scrollValue < 0.01f ) {
			if( currentPage < pagesAvailable ) StartCoroutine( QueryOracle(currentPage + 1) );
		}
	}

	// Kamisasori database search

	public void LookForSuggestions() {

		bool anyFiltersActive = false;
		foreach( SuggestionFilter filter in filters ) {
			if( filter is LegalityFilter ) continue; // don't ever filter by just legality... the list is too long
			if( !filter.isDefault ) {
				anyFiltersActive = true;
				break;
			}
		}
		if( !anyFiltersActive ) {
			foreach( SuggestionListItem suggestion in suggestions ) suggestion.gameObject.SetActive( false );
			return;
		}

		Card[] allCards = new Card[Persistent.data.cardsByName.Count];
		Persistent.data.cardsByName.Values.CopyTo( allCards, 0 );

		for( int index = 0; index < filters.Count; index++ ) {

			SuggestionFilter currentFilter = filters[index];

			allCards = currentFilter.FilterCards( allCards );
		}

		if( allCards.Length == Persistent.data.cardsByName.Count ) return;

		// alphabetize
		List<string> cardNamesToList = new List<string>(allCards.Length);
		for( int index = 0; index < allCards.Length; index++ ) {
			cardNamesToList.Add( allCards[index].name );
		}
		cardNamesToList.Sort();

		int suggestionsAdded = 0;
		for( int index = 0; index < cardNamesToList.Count; index++ ) {
			string editionTag = "";
			if( editionFilter.text != "" ) {
				editionTag = " (" + editionFilter.text + ")";
			}

			suggestions[index].gameObject.SetActive( true );
			suggestions[index].label.text = cardNamesToList[index] + editionTag;

			// workaround Unity bug
			// suggestions[index].label.enabled = false;
			// suggestions[index].label.enabled = true;

			suggestionsAdded++;

			if( suggestionsAdded == maxSuggestionsAllowed ) break;
		}

		// disable remainder of suggestions, if any
		for( int index = suggestionsAdded; index < suggestions.Length; index++ ) {
			suggestions[index].gameObject.SetActive( false );
		}

		drawPreview2D.Clear();
	}

	// called by suggestion on click
	public void SuggestionPicked( SuggestionListItem suggestion ) {
		if( deckToEdit.locked ) return;

		string labelText = suggestion.label.text;
		Card suggestedCard = Persistent.data.VerifyTextAgainstDatabase( labelText );
		if( suggestedCard != null && suggestedCard.type == "stronghold" && labelText.Contains("(2)") ) labelText = labelText.Replace(" (2)", "");

		DeckEntryItem deckContainsCard = null;
		foreach( DeckEntryItem entry in cardListEntries ) {
			if( entry.card != null && entry.card.name == Persistent.data.TrimEdition( labelText ) ) {
				if( entry.card.type == "stronghold" ) return; // don't allow players to add more than one stronghold
				
				deckContainsCard = entry;
				break;
			}
		}

		if( selectedEntry && selectedEntry.card == null ) {
			print("Added card to selected entry " + Time.time);
			if( suggestedCard != null ) {
				selectedEntry.card = suggestedCard;
				selectedEntry.RegenerateLabel( selectedEntry.cardQuantity );
				selectedEntry.background.color = validColor;
				selectedEntry.Select();
			}
			else {
				selectedEntry.label.text = labelText;
				selectedEntry.background.color = validColor;
				StartCoroutine( Persistent.data.VerifyTextAgainstOracle( labelText, delegate(Card c, int q){ CardLineConfirmedWithOracle(c, q, labelText); } ) );
			}

			// nameFilter.filter.text = "";
			// LookForSuggestions();
		}
		else if( deckContainsCard ) {
			print("Deck contains this card, so incrementing existing... " + Time.time);
			deckContainsCard.IncrementEntry();
		}
		else {
			int lastActiveIndex = cardListEntries.Count - 1;
			for( int index = 0; index < cardListEntries.Count; index++ ) {
				if( cardListEntries[index].gameObject.activeInHierarchy ) lastActiveIndex = index;
			}

			int insertIndex = selectedEntry != null ? (selectedEntry.transform.GetSiblingIndex() + 1) : lastActiveIndex + 1;
			NewLine( insertIndex, labelText );
			CountCards();
		}

		saveButton.text = "Save?";

		drawPreview2D.Clear();
	}

	public void ShowPreview( string cardName, Vector2 inputPos = default(Vector2), float offsetPreview = 0f ) {

		if( cardName == null ) {
			drawPreview2D.Clear();
			previewing = null;
			loadingFromOracle = false;

			return;
		}

		Card cardToPreview = null;
		foreach( DeckEntryItem entryItem in cardListEntries ) {
			if( entryItem.card != null ) {
				if( entryItem.card.name.ToLower() == cardName.ToLower() ) {
					cardToPreview = entryItem.card;
					break;
				}
			}
		}
		if( cardToPreview == null ) {

			// determine if we need to preview the other side of e.g. a stronghold
			string trimmedCardName = cardName;
			int otherSideIndex = cardName.IndexOf( " (2)" );

			if( otherSideIndex != -1 ) {
				trimmedCardName = trimmedCardName.Remove( otherSideIndex ).Trim();
			}

			print("Previewing card name " + trimmedCardName + " " + Time.time);
			cardToPreview = Persistent.data.VerifyTextAgainstDatabase( trimmedCardName );

			if( otherSideIndex != -1 && cardToPreview != null && cardToPreview.linkedCard != null ) {
				cardToPreview = cardToPreview.linkedCard;
			}
		}

		if( cardToPreview == null ) {
			StartCoroutine( Persistent.data.VerifyTextAgainstOracle( 
				cardName,
				OraclePreviewFound( inputPos, offsetPreview )
			) );
			loadingFromOracle = true;
		}

		if( cardToPreview != null ) {
			StartCoroutine( drawPreview2D.DrawCard( cardToPreview, inputPos, offsetPreview ) );
			previewing = cardToPreview;
		}
	}

	// avoid delegate variable capture and provide extra parameters to the Oracle verification method
	System.Action<Card, int> OraclePreviewFound( Vector2 inputPos, float offsetPreview ) {
		return delegate(Card returnedCard, int returnedQuantity){ OraclePreviewFound(returnedCard, returnedQuantity, inputPos, offsetPreview ); };
	}

	public void OraclePreviewFound( Card foundCard, int cardQuantity, Vector2 inputPos, float offsetPreview ) {
		if( loadingFromOracle ) {
			StartCoroutine( drawPreview2D.DrawCard( foundCard, inputPos, offsetPreview ) );
			previewing = foundCard;
		}
		loadingFromOracle = false;
	}

	public void SwitchEntries( DeckEntryItem entryToSwitch, int newIndex ) {
		if( deckToEdit.locked ) return;

		int oldIndex = entryToSwitch.transform.GetSiblingIndex();

		DeckEntryItem tempEntry = cardListEntries[newIndex];
		cardListEntries[newIndex] = cardListEntries[oldIndex];
		cardListEntries[oldIndex] = tempEntry;

		entryToSwitch.transform.SetSiblingIndex( newIndex );
	}

	public void LockButtonClicked() {
		if( !deckToEdit.locked ) confirmLock.UnfurlNow();
		else confirmUnlock.UnfurlNow();
	}

	// Permanently lock the deck to read-only
	public void Lock() {
		deckToEdit.locked = true;
		WriteCardListToDisk();

		lockButton.alpha = 1f;
	}

	public void Unlock() {
		deckToEdit.locked = false;
		WriteCardListToDisk();

		lockButton.alpha = 0.5f;
	}

	public void SaveButtonClicked() {
		if( !deckToEdit.locked ) WriteCardListToDisk();
	}

	public void WriteCardListToDisk() {

		string textDeckList = "";
		string legality = "Legality: " + legalityFilter.legality.ToString();
		if( oracleLegalityFilter.enabled ) legality = "Legality: " + oracleLegalityFilter.filterLabel.text.Replace(" ", "_");

		textDeckList += legality;
		textDeckList += System.Environment.NewLine;
		deckToEdit.legality = (int)legalityFilter.legality;

		for( int index = 0; index < cardListEntries.Count; index++ ) {
			textDeckList += cardListEntries[index].label.text;
			if( index < cardListEntries.Count - 1 ) textDeckList += System.Environment.NewLine;
		}

		FileInfo oldDeck = new FileInfo( origDeckPath );

		if( deckToEdit.locked ) {
			deckToEdit.pathToDeckOnDisk = deckToEdit.pathToDeckOnDisk.Replace(".txt", ".snm");
			byte[] deckAsBytes = Encoding.ASCII.GetBytes(textDeckList);
			byte[] compressedDeck = LZMAtools.CompressByteArrayToLZMAByteArray( deckAsBytes );

			oldDeck.Delete();
			File.WriteAllBytes( deckToEdit.pathToDeckOnDisk, compressedDeck );
		}
		else {
			deckToEdit.pathToDeckOnDisk = deckToEdit.pathToDeckOnDisk.Replace(".snm", ".txt");

			oldDeck.Delete();
			File.WriteAllText( deckToEdit.pathToDeckOnDisk, textDeckList );
		}

		origDeckPath = deckToEdit.pathToDeckOnDisk;

		saveButton.text = "Saved";
	}

	public void DeleteDeck() {
		if( deleteButton.text == "Delete Deck" ) {
			deleteButton.text = "Delete Deck?";
			return;
		}

		FileInfo oldDeck = new FileInfo( origDeckPath );
		oldDeck.Delete();

		previousLevel.LoadLevelByName();
	}

	void OnDisable() {
		Destroy( emissary );
	}
}
