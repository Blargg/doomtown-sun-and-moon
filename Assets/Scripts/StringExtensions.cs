﻿using UnityEngine;
using System.Collections;

public static class StringExtensions {

	public static string Sanitize( this string text ) {
		string sanitized = text;

		string[] specialChars = new string[] { "\n" };
		string[] replacedChars = new string[] { "" };

		for (var i = 0; i < specialChars.Length; i++)
	    {
	        sanitized = sanitized.Replace(specialChars[i], replacedChars[i]);
	    }

	    return sanitized;
	}

	public static string SanitizePath( this string path ) {
	    string sanitized = path;

	    string[] specialChars = new string[] { "Ž", "ž", "Ÿ", "¡", "¢", "£", "¤", "¥", "¦", "§", "¨", "©", "ª", "À", "Á", "Â", "Ã", "Ä", "Å", "Æ", "Ç", "È", "É", "Ê", "Ë", "Ì", "Í", "Î", "Ï", "Ð", "Ñ", "Ò", "Ó", "Ô", "Õ", "Ö", "Ù", "Ú", "Û", "Ü", "Ý", "Þ", "ß", "à", "á", "â", "ã", "ä", "å", "æ", "ç", "è", "é", "ê", "ë", "ì", "í", "î", "ï", "ð", "ñ", "ò", "ó", "ô", "õ", "ö", "ù", "ú", "û", "ü", "ý", "þ", "ÿ" };
	    string[] escapedChars = new string[] { "%C5%BD", "%C5%BE", "%C5%B8", "%C2%A1", "%C2%A2", "%C2%A3", "%C2%A4", "%C2%A5", "%C2%A6", "%C2%A7", "%C2%A8", "%C2%A9", "%C2%AA", "%C3%80", "%C3%81", "%C3%82", "%C3%83", "%C3%84", "%C3%85", "%C3%86", "%C3%87", "%C3%88", "%C3%89", "%C3%8A", "%C3%8B", "%C3%8C", "%C3%8D", "%C3%8E", "%C3%8F", "%C3%90", "%C3%91", "%C3%92", "%C3%93", "%C3%94", "%C3%95", "%C3%96", "%C3%99", "%C3%9A", "%C3%9B", "%C3%9C", "%C3%9D", "%C3%9E", "%C3%9F", "%C3%A0", "%C3%A1", "%C3%A2", "%C3%A3", "%C3%A4", "%C3%A5", "%C3%A6", "%C3%A7", "%C3%A8", "%C3%A9", "%C3%AA", "%C3%AB", "%C3%AC", "%C3%AD", "%C3%AE", "%C3%AF", "%C3%B0", "%C3%B1", "%C3%B2", "%C3%B3", "%C3%B4", "%C3%B5", "%C3%B6", "%C3%B9", "%C3%BA", "%C3%BB", "%C3%BC", "%C3%BD", "%C3%BE", "%C3%BF" };

	    for (var i = 0; i < specialChars.Length; i++)
	    {
	        sanitized = sanitized.Replace(specialChars[i], escapedChars[i]);
	    }

	    sanitized = sanitized.Replace(" ", "%20");

	    return sanitized;
	}

	public static string ShortenString( this string longString, int toNumChars ) {
		string shortString = "";

		for( int index = 0; index < toNumChars; index++ ) {
			shortString += longString[index];
		}

		return shortString;
	}

	public static string ReplaceCommonTerms( this string textToProcess ) {
		string processedText = textToProcess;

		if( !processedText.Contains( " - exp" ) ) processedText = processedText.Replace( " exp", " - exp" );
		processedText = processedText.Replace( " - Experienced", " - exp" );
		processedText = processedText.Replace( " - experienced", " - exp" );
		processedText = processedText.Replace( " experienced ", " - exp" );
		processedText = processedText.Replace( " Experienced ", " - exp" );
		processedText = processedText.Replace( " experienced", " - exp" );
		processedText = processedText.Replace( " Experienced", " - exp" );
		processedText = processedText.Replace( " Inexperienced", " - inexp" );
		processedText = processedText.Replace( " inexperienced", " - inexp" );
		// processedText = processedText.Replace( " - inexp", "" );
		processedText = processedText.Replace( " XP", " - exp" );
		processedText = processedText.Replace( " xp", " - exp" );
		processedText = processedText.Replace( " -exp", " - exp" );
		processedText = processedText.Replace( "&#149;", "" );
		processedText = processedText.Replace( "  ", " " );
		processedText = processedText.Replace( "’", "'" );

		return processedText;
	}

	public static string Capitalize(this string s) {
		if (string.IsNullOrEmpty(s)) return string.Empty;
		return char.ToUpper(s[0]) + s.Substring(1);
    }

    public static string Acronym(this string s) {
		if (string.IsNullOrEmpty(s)) return string.Empty;
		string[] splitString = s.Split(' ');
		string acronym = "";

		foreach( string word in splitString ) {
			acronym += word.Trim()[0];
		}
		return acronym;
    }
}