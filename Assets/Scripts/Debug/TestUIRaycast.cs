﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;

public class TestUIRaycast : MonoBehaviour {

	void Update() {
		if( Input.GetMouseButtonDown(0) || Input.GetMouseButtonUp(0) ) {
			List<RaycastResult> mRaycastResults = new List<RaycastResult>();
			// List<GameObject> AllHitObjects = new List<GameObject>();

			if ( EventSystem.current.IsPointerOverGameObject() )
			{
				PointerEventData ped = new PointerEventData( EventSystem.current );
				ped.position = Input.mousePosition;
				mRaycastResults.Clear();
				EventSystem.current.RaycastAll( ped, mRaycastResults );

				for ( int i = 0 ; i < mRaycastResults.Count ; ++i )
				{
					if ( i == 0 ) print("Top UI hit is " + mRaycastResults[i].gameObject.name + " " + Time.time);
				}
			}
		}
	}
}
