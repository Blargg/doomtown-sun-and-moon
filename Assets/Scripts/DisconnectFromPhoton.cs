﻿using UnityEngine;
using System.Collections;

public class DisconnectFromPhoton : MonoBehaviour {

	public void DisconnectFromNetwork() {
		if( PhotonNetwork.connectionState == ConnectionState.Connected ) {
			PhotonNetwork.Disconnect();
		}
	}
}
